namespace :active_storage do

  desc 'Ensures all files are mirrored'
  task mirror_all: [:environment] do
    puts "active_storage:mirror_all started at #{Time.current}"

    ActiveStorage::Blob.where(service_name: 'local').find_each do |blob|
      local_file = blob.service.path_for(blob.key)
      next unless File.exists?(local_file)

      begin
        ActiveStorage::Blob.service.mirror(blob.key, checksum: blob.checksum)
        blob.update_column(:service_name, ActiveStorage::Blob.service.name)
      rescue => e
        puts "!!! ActiveStorage mirror error: #{blob.inspect}, #{e}"
      end
    end

    puts "active_storage:mirror_all ended at #{Time.current}"
  end
end
