namespace :duplicates do

  desc 'Ensures all files are mirrored'
  task update_all: [:environment] do
    puts "duplicates:update_all started at #{Time.current}"
    PressRelease.find_each(&:set_origins)
    puts "duplicates:update_all ended at #{Time.current}"
  end
end
