namespace :importing_tags do

  desc 'Importing tags'
  task import: [:environment] do
    puts "importing_tags:import started at #{Time.current}"

    require 'csv'
    current_national_project = nil
    csv = CSV.foreach('lib/tasks/tags.csv').with_index do |row, index|
      next if index < 3
      next if row.compact.empty?

      if row.first
        current_national_project = NationalProject.find_by(name: row.first.strip)
      end

      next unless current_national_project

      tag_names = row.last.split.map(&:strip)

      tag_names.each do |tag_name|
        Tag.create(name: tag_name, national_project_ids: [current_national_project.id])
      end
    end

    puts "importing_tags:import ended at #{Time.current}"
  end
end
