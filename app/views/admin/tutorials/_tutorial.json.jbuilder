json.extract! tutorial, :id, :name, :created_at, :updated_at
json.url tutorial_url(tutorial, format: :json)
