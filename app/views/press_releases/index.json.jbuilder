json.array! @press_releases do |press_release|
  json.value press_release.id
  json.text press_release.name
end