json.array! @organizations do |organization|
  json.value organization.id
  json.text organization.name
end
