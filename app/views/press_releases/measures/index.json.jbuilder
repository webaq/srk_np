json.array! @measures do |measure|
  json.value measure.id
  json.text measure.name
end