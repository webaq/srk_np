json.array! @urban_districts do |urban_district|
  json.value urban_district.id
  json.text urban_district.name
end