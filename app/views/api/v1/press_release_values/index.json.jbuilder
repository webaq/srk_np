json.array! @press_release_values do |press_release_value|
  json.id press_release_value.id
  json.name press_release_value.name
  json.category press_release_value.category.name
  json.subcategory press_release_value.subcategory.name
  json.subcategory_scope press_release_value.subcategory_scope&.name
end