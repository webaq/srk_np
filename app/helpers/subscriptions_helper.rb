module SubscriptionsHelper
  def subscription_federal_subjects_name(subscription)
    case subscription.federal_subjects_count
    when 0
      'Все регионы'
    else
      subscription.federal_subjects.sort_by(&:name).map(&:name).join(', ')
    end
  end

  def subscription_national_projects_name(subscription)
    case subscription.national_projects_count
    when 0
      'Все направления'
    else
      subscription.national_projects.sort_by(&:short_name).map(&:short_name).join(', ')
    end
  end

  def subscription_tags_name(subscription)
    case subscription.tags_count
    when 0
      'Все теги'
    else
      subscription.tags.sort_by(&:name).map { |tag| "##{tag.name}" }.join(', ')
    end
  end

  def subscription_organizations_name(subscription)
    case subscription.organizations_count
    when 0
      'Все организации'
    else
      subscription.organizations.sort_by(&:name).map(&:name).join(', ')
    end
  end
end