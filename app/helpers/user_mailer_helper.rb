module UserMailerHelper
  def number_to_percentage_with_sign(value, precision: 0)
    sign = "+" if value > 0
    sign.to_s + number_to_percentage(value, precision: precision)
  end

  def percentage_diff(value1, value2)
    return unless value1
    return unless value2

    return if value2.round == 0

    diff = value1 - value2
    return if diff == 0

    value = diff * 100.0 / value2
    class_name = if value > 0
      "text-success"
    elsif value < 0
      "text-danger"
    end
    tag.span("(#{number_to_percentage_with_sign(value)})", class: class_name)
  end
end