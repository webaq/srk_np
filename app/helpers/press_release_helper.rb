module PressReleaseHelper
  def press_release_specific_national_project(press_release)
    if press_release.national_projects.size == 1 && press_release.national_projects.first.avatar.attached?
      press_release.national_projects.first
    end
  end

  def national_project_icon_attachment_name(national_project)
    if national_project
      "#{national_project.short_name.parameterize(separator: '_')}.png"
    else
      'national_project.png'
    end
  end

  def national_project_icon_attachment_data(national_project)
    if national_project
      avatar = national_project.avatar

      if avatar.content_type == 'image/svg+xml'
        avatar = national_project.avatar.variant(format: :png).processed
      end

      avatar.download
    else
      File.read('app/assets/images/national_project_common_icon.png')
    end
  end

  def press_release_status(press_release)
    if current_user.organization&.media?
      press_release_media_user_status(press_release)
    else
      press_release_other_user_status(press_release)
    end
  end

  def press_release_media_user_status(press_release)
    if press_release.reposts.all.find { |r| r.user == current_user }
      'Опубликован'
    elsif press_release.bookmarks.all.find { |b| b.user == current_user }
      'В работе'
    end
  end

  def press_release_other_user_status(press_release)
    if press_release.approved? && press_release.published?
      'В работе у СМИ'
    elsif press_release.approved?
      <<~EOT.html_safe
      <div class="mb-1">Согласован</div>
      <div class="text-gray-700" style="font-size: 10px;">
        Доступен с #{press_release.published_at.strftime('%d.%m.%y')}
      </div>
      EOT
    elsif press_release.returned_at
      'Возврат на доработку'
    elsif press_release.need_pre_approvement? && press_release.pre_approved_at.nil?
      'На модерации у РОИВа'
    else
      'На модерации'
    end
  end
end