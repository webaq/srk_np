module Admin::PressReleasesHelper
  def press_release_value_name_with_subcategory_scope(press_release_value)
    return nil unless press_release_value
    name = press_release_value.name
    if press_release_value.subcategory_scope
      name += " (#{press_release_value.subcategory_scope.name})"
    end
    name
  end

  def press_release_value(press_release)
    if press_release.value_nullified?
      0.0
    else
      press_release.press_release_value&.value
    end
  end
end