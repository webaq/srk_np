module ApplicationHelper
  include Pagy::Frontend

  def bootstrap_class_for(flash_type)
    {
      success: "alert-success",
      error: "alert-danger",
      alert: "alert-danger",
      notice: "alert-danger"
    }.stringify_keys[flash_type.to_s] || flash_type.to_s
  end

  def press_release_brief(press_release)
    truncate(press_release.plain_text_content.remove(/\[.+?\]/), length: 198, separator: ' ')
  end

  def truncate_two_strings(str1, str2, max_length)
    if str1.size >= max_length
      str1 = truncate(str1, length: max_length, separator: ' ')
      str2 = ''
    elsif max_length - str1.size > 2
      str2 = truncate(str2, length: (max_length - str1.size), separator: ' ')
    else
      str2 = '...'
    end
    [str1, str2]
  end

  def link_to_add_fields(name, f, type)
    new_object = f.object.send "build_#{type}"
    id = "new_#{type}"
    fields = f.send("#{type}_fields", new_object, child_index: id) do |builder|
      render(type.to_s + '_fields', f: builder)
    end
    link_to(name, '', class: 'add_fields', data: { id: id, fields: fields.gsub("\n", "") })
  end

  def convert_to_filename(str)
    str.parameterize(separator: '_').truncate(40, separator: '_', omission: '')
  end

  def multiple_national_projects_image_url(national_projects)
    if national_projects.size == 1 && national_projects.first.avatar.attached?
      polymorphic_url(national_projects.first.avatar)
    else
      image_url('national_project_common_icon.png')
    end
  end

  def sort_link_title(text, key, sort_params)
    if sort_params.keys.first == key.to_s
      "#{text} #{sort_arrow_symbol(sort_params.values.first)}"
    else
      text
    end
  end

  def sort_link_sort_params(key, sort_params, default_dir = 'asc')
    dir = if sort_params&.keys&.first == key.to_s
      sort_params.values.first == 'asc' ? 'desc' : 'asc'
    else
      default_dir
    end
    {
      sort: { "#{key}" => dir }
    }
  end

  def sort_arrow_symbol(dir)
    dir == 'asc' ? "▲" : "▼"
  end

  def sortable_table_sort_link(name, options)
    classNames = ["sortable-table--link"]
    classNames << "active" if options[:active]
    link_to(name, "#", data: {
      sortable_table_target: "link",
      action: "sortable-table#sort",
      column_index: options.fetch(:column_index),
      direction: options.fetch(:direction, 'desc'),
      value_type: options.fetch(:value_type, nil),
    }, class: [classNames])
  end
end
