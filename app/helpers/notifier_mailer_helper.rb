module NotifierMailerHelper
  def subscription_period_text(subscription)
    return 'ежедневно' if subscription.daily?
    return 'еженедельно' if subscription.weekly?
  end

  def press_release_national_project_icon_url(press_release)
    if press_release.national_projects.size == 1 && press_release.national_projects.first.avatar.attached?
      url_for(press_release.national_projects.first.avatar)
    else
      asset_url("mail/national_project_common_icon.png", skip_pipeline: true)
    end
  end
end