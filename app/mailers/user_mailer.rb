class UserMailer < ApplicationMailer
  layout 'user_mailer'

  def dashboard_stats
    @subject = params[:subject]
    @dashboard = params[:dashboard]
    mail(to: params[:email], subject: @subject)
  end

  def god_nauki
    @subject = params[:subject]
    @starts_on = params[:starts_on]
    @ends_on = params[:ends_on]
    @created_press_releases_count = params[:created_press_releases_count]
    @taken_press_releases_count = params[:taken_press_releases_count]
    mail(to: params[:email], subject: @subject)
  end

  def tag_request
    @user = params[:user]
    @tag_request = params[:tag_request]
    mail(to: 'support.srk@nationalpriority.ru', subject: "СРК запрос тега: #{@tag_request.subject}", reply_to: @user.email, from: "#{@user.name} <noreply@nationalpriority.ru>")
  end

  def export_request
    @user = params[:user]
    @export_request = params[:export_request]
    mail(to: 'support.srk@nationalpriority.ru', subject: "СРК запрос на выгрузку", reply_to: @user.email, from: "#{@user.name} <noreply@nationalpriority.ru>")
  end

  def best_practice_created
    @best_practice = params[:best_practice]
    mail(to: 'support.srk@nationalpriority.ru', subject: 'Новая лучшая региональная практика')
  end
end
