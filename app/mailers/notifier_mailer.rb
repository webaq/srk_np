class NotifierMailer < ApplicationMailer
  include PressReleaseHelper
  helper :application
  helper :press_release

  SUBSCRIPTION_MAIL_SUBJECT = "Дайджест по национальным проектам".freeze

  default skip_premailer: true

  def comment_created(press_release, user)
    @press_release = press_release
    @user = user
    mail(to: @user.email, subject: 'Новый комментарий')
  end

  def complaint
    @complaint = Complaint.find(params[:complaint_id])
    mail(to: 'complaint@nationalpriority.ru', subject: "Жалоба", reply_to: @complaint.user.email, from: "#{@complaint.user.name} <noreply@nationalpriority.ru>")
  end

  def press_release_created(user, press_release)
    @press_release = press_release
    mail(to: user.email, subject: 'Новое событие')
  end

  def support_request(support_request, to)
    @support_request = support_request

    mail(to: to, subject: "СРК помощь #{@support_request.id}: #{@support_request.subject}", reply_to: @support_request.user.email, from: "#{@support_request.user.name} <noreply@nationalpriority.ru>")
  end

  def welcome_mail(user)
    @user = user
    attachments['srk_help.png'] = File.read(Rails.root.join('vendor/welcome_mail_mainpage.png'))
    mail(to: user.email, subject: 'Добро пожаловать!')
  end

  def unapproved_press_releases_mail(emails, press_releases)
    @press_releases = press_releases
    mail(to: emails, subject: 'Дайджест неопубликованных инфоповодов')
  end

  # def media_digest_mail(email, press_releases_count, press_releases)
  #   @press_releases_count = press_releases_count
  #   @press_releases = press_releases
  #
  #   attachments.inline['srk_logo.png'] = File.read('app/assets/images/top_logo.png')
  #   attachments.inline['national_priorities_logo.png'] = File.read('app/assets/images/footer_logo.png')
  #
  #   @press_releases.map { |press_release|
  #     press_release_national_project_mailer_image_attachment(press_release)
  #   }.uniq { |hash| hash[:name] }.each do |hash|
  #     attachments.inline[hash[:name]] = hash[:data]
  #   end
  #
  #   mail(to: email, subject: MEDIA_DIGEST_SUBJECT) do |format|
  #     format.html { render layout: nil }
  #   end
  # end

  def about_media_digest_mail(email)
    attachments.inline['srk_logo.png'] = File.read('app/assets/images/top_logo.png')
    attachments.inline['national_priorities_logo.png'] = File.read('app/assets/images/footer_logo.png')

    mail(to: email, subject: "Подписка на новости по национальным проектам") do |format|
      format.html { render layout: nil }
    end
  end

  def rejected(email)
    attachments.inline['srk_logo.png'] = File.read('app/assets/images/top_logo.png')
    attachments.inline['national_priorities_logo.png'] = File.read('app/assets/images/footer_logo.png')

    mail(to: email, subject: "Вам отказано в доступе к Системе сбора и распределения контента") do |format|
      format.html { render layout: nil }
    end
  end

  def registration_submitted(email)
    attachments.inline['srk_logo.png'] = File.read('app/assets/images/top_logo.png')
    attachments.inline['national_priorities_logo.png'] = File.read('app/assets/images/footer_logo.png')

    mail(to: email, subject: "Спасибо за регистрацию в Системе сбора и распределения контента") do |format|
      format.html { render layout: nil }
    end
  end

  def press_release_approved(author_email)
    attachments.inline['srk_logo.png'] = File.read('app/assets/images/top_logo.png')
    attachments.inline['national_priorities_logo.png'] = File.read('app/assets/images/footer_logo.png')

    mail(to: author_email, subject: "Вы успешно разместили новость") do |format|
      format.html { render layout: nil }
    end
  end

  def welcome_media(email)
    attachments.inline['srk_logo.png'] = File.read('app/assets/images/top_logo.png')
    attachments.inline['national_priorities_logo.png'] = File.read('app/assets/images/footer_logo.png')

    mail(to: email, subject: "Добро пожаловать в Систему сбора и распределения контента!") do |format|
      format.html { render layout: nil }
    end
  end

  def welcome_others(email)
    attachments.inline['srk_logo.png'] = File.read('app/assets/images/top_logo.png')
    attachments.inline['national_priorities_logo.png'] = File.read('app/assets/images/footer_logo.png')

    mail(to: email, subject: "Добро пожаловать в Систему сбора и распределения контента!") do |format|
      format.html { render layout: nil }
    end
  end

  def press_release_mail(email, press_release)
    @press_release = press_release
    mail(to: email, subject: 'Инфоповод по СРК')
  end

  def one_time_mail(email)
    attachments.inline['srk_logo.png'] = File.read('app/assets/images/top_logo.png')
    attachments.inline['national_priorities_logo.png'] = File.read('app/assets/images/footer_logo.png')

    mail(to: email, subject: 'Результаты работы СРК') do |format|
      format.html { render layout: nil }
    end
  end

  def main_topics
    @starts_on = params[:starts_on]
    @ends_on = params[:ends_on]
    @main_topics_press_releases_hash = params[:press_releases].group_by(&:main_topic)

    attachments.inline['srk_logo.png'] = File.read('app/assets/images/top_logo.png')
    attachments.inline['national_priorities_logo.png'] = File.read('app/assets/images/footer_logo.png')

    mail(to: params[:email], subject: 'Подборка актуальных инфоповодов по реализации национальных проектов') do |format|
      format.html { render layout: nil }
    end
  end

  def press_releases_export(user, press_releases)
    # attachments.inline['srk_logo.png'] = File.read('app/assets/images/top_logo.png')
    # attachments.inline['national_priorities_logo.png'] = File.read('app/assets/images/footer_logo.png')

    xlsx = render_to_string layout: false, handlers: [:axlsx], formats: [:xlsx], template: "press_releases/export", locals: { press_releases: press_releases }
    attachment = Base64.encode64(xlsx)
    attachments["press_releases.xlsx"] = {mime_type: Mime[:xlsx], content: attachment, encoding: 'base64'}

    @greeting = 'Здравствуйте!'

    mail(to: user.email, subject: "СРК выгрузка новостей", bcc: 'mail.jenja@gmail.com') do |format|
      format.html { render layout: 'mjml_layout' }
    end
  end

  def subscription_created
    @subscription = Subscription.find_by(id: params[:subscription_id])
    return unless @subscription

    # set_header_and_footer_attachments

    @greeting = 'Добрый день!'

    mail(to: @subscription.user.email, subject: "Отслеживать инфоповоды по нацпроектам стало еще удобнее!") do |format|
      format.html { render layout: 'mjml_layout' }
    end
  end

  def subscription_mail(email, press_releases, period_text)
    # set_header_and_footer_attachments

    @greeting = 'Добрый день!'
    @press_releases = press_releases.to_a
    @period_text = period_text

    @top_press_releases = @press_releases.first(5)

    # @top_press_releases.map { |press_release|
    #   press_release_specific_national_project(press_release)
    # }.uniq.each do |national_project|
    #   attachments.inline[national_project_icon_attachment_name(national_project)] = national_project_icon_attachment_data(national_project)
    # end

    mail(to: email, subject: SUBSCRIPTION_MAIL_SUBJECT) do |format|
      format.html { render layout: 'mjml_layout' }
    end
  end

  private
    def set_header_and_footer_attachments
      attachments.inline['srk_logo.png'] = File.read('app/assets/images/top_logo.png')
      attachments.inline['national_priorities_logo.png'] = File.read('app/assets/images/footer_logo.png')
    end
end
