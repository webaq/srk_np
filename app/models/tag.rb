class Tag < ApplicationRecord
  include PublicActivity::Common

  has_many :national_project_tags, dependent: :destroy
  has_many :national_projects, through: :national_project_tags
  has_many :press_release_tags, dependent: :destroy
  has_many :press_releases, through: :press_release_tags
  has_many :half_year_press_releases, -> { where(published_at: 6.month.ago...) }, through: :press_release_tags, source: :press_release
  has_many :show_activities, -> { where(key: 'tag.show') }, class_name: "::PublicActivity::Activity", as: :trackable
  has_many :half_year_show_activities, -> { where(key: 'tag.show', created_at: 6.month.ago...) }, class_name: "::PublicActivity::Activity", as: :trackable
  has_many :subscription_tags, dependent: :destroy

  validates :national_projects, presence: true
  validates :name, uniqueness: { case_sensitive: false }

  def name=(new_value)
    new_value = new_value.strip if new_value
    super
  end

  def value
    (press_releases_value + log_value) / 2.0
  end

  def log_value
    return 0 if Tag.log_max_count.zero?
    (Tag.log_counts.fetch(id, 0) - Tag.log_min_count) / Tag.log_max_count
  end

  def press_releases_value
    return 0 if Tag.press_releases_max_count.zero?
    (Tag.press_releases_counts.fetch(id, 0) - Tag.press_releases_min_count) / Tag.press_releases_max_count
  end

  def self.top(current_tag_id:)
    all_tags = Tag.all

    top_tags = []
    top_tags << all_tags.detect { |tag| tag.id == current_tag_id.to_i } if current_tag_id.present?

    top_tags += all_tags
      .reject { |tag| tag.id == current_tag_id.to_i }
      .sort_by(&:value)
      .reverse
      .first(10 - top_tags.size)
      .sort_by(&:name)

    top_tags
  end

  def self.log_counts
    @@log_counts ||= Tag.joins(:half_year_show_activities).group(:id).count
  end

  def self.log_min_count
    @@log_min_count ||= log_counts.size < Tag.count ? 0.0 : log_counts.values.min.to_f
  end

  def self.log_max_count
    @@log_max_count ||= log_counts.values.max.to_f
  end

  def self.press_releases_counts
    @@press_releases_counts ||= Tag.joins(:half_year_press_releases).group(:id).count
  end

  def self.press_releases_min_count
    @@press_releases_min_count ||= press_releases_counts.size < Tag.count ? 0.0 : press_releases_counts.values.min.to_f
  end

  def self.press_releases_max_count
    @@press_releases_max_count ||= press_releases_counts.values.max.to_f
  end
end
