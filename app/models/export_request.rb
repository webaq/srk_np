class ExportRequest
  include ActiveModel::Model
  include ActiveModel::Attributes

  attribute :subject, :string
  attribute :body, :string
  attribute :email, :string

  attr_accessor :federal_subject_ids
  attr_accessor :filters

  # validates :subject, presence: true
  validates :body, presence: true
  # validates :email, presence: true

  def federal_subject_name
    return 'Вся Россия' if federal_subjects.empty?
    federal_subjects.map(&:name).join(', ')
  end

  def federal_subjects
    @federal_subjects ||= FederalSubject.where(id: federal_subject_ids).order(:name)
  end
end