class VideoAlbum < ApplicationRecord
  has_one_attached :image
  has_many :video_clips, dependent: :restrict_with_error

  validates :name, presence: true
  validates :image, presence: true
end
