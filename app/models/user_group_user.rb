class UserGroupUser < ApplicationRecord
  belongs_to :user
  belongs_to :user_group, counter_cache: :users_count
end
