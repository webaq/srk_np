class PressReleasesFilter
  attr_reader :current_user
  attr_reader :dates
  attr_reader :federal_project_id
  attr_accessor :federal_subject_id
  attr_reader :main_topic_id
  attr_reader :national_project_ids
  attr_reader :organization_id
  attr_reader :period
  attr_reader :tag_id
  attr_reader :category_id

  def initialize(current_user, scope, params)
    @current_user = current_user
    @scope = scope

    @dates = params.fetch(:dates, nil)
    @federal_project_id = params.fetch(:federal_project_id, nil)
    @federal_subject_id = params.fetch(:federal_subject_id, nil)
    @main_topic_id = params.fetch(:main_topic_id, nil)
    @national_project_ids = params.fetch(:national_project_ids, []).map(&:to_i)
    @organization_id = params.fetch(:organization_id, nil)
    @period = params.fetch(:period, nil)
    @tag_id = params.fetch(:tag_id, nil)
    @category_id = params.fetch(:category_id, nil)
  end

  def applied_filters
    @applied_filters ||= {}
  end

  def press_releases
    # byebug
    return @press_releases if defined? @press_releases

    @press_releases = @scope

    if tag
      @press_releases = @press_releases.where(id: PressReleaseTag.where(tag_id: tag.id).select(:press_release_id))
      applied_filters['Тег'] = "##{tag.name}"
      tag.create_activity key: 'tag.show', owner: current_user
    end

    if dates.present?
      applied_filters['Период'] = "#{starts_at.strftime('%d.%m.%Y')} – #{ends_at.strftime('%d.%m.%Y')}"
      @press_releases = @press_releases.where(published_at: starts_at..ends_at)
    else
      @press_releases = case period
      when 'today'
        applied_filters['Период'] = 'Сегодня'
        @press_releases.where(published_at: Date.today.to_time..(Date.today + 1).to_time)
      when 'week'
        applied_filters['Период'] = 'Неделя'
        @press_releases.where(published_at: (Date.today - 1.week).to_time...)
      when 'month'
        applied_filters['Период'] = 'Месяц'
        @press_releases.where(published_at: (Date.today - 1.month).to_time...)
      else
        @press_releases
      end
    end

    if national_project_ids.any?
      @press_releases = @press_releases.where(id: PressRelease.joins(:national_projects).where(national_projects: { id: national_project_ids }))
      applied_filters['Направления'] = NationalProject.where(id: national_project_ids).pluck(:short_name).join(', ')
    end

    if federal_project_id.present?
      @press_releases = @press_releases.includes(:federal_projects).where(federal_projects: { id: federal_project_id })
      applied_filters['Федеральный проект'] = FederalProject.find(federal_project_id).name
    end

    if federal_subject_id.present?
      @press_releases = if federal_subject_id == '0'
        applied_filters['Регион'] = 'Межрегиональные'
        @press_releases.where(all_federal_subjects: true)
      else
        applied_filters['Регион'] = FederalSubject.find(federal_subject_id).name
        @press_releases.where(id: FederalSubject.find(federal_subject_id).press_releases)
      end
    end

    if main_topic_id.present?
      @press_releases = @press_releases.where(main_topic_id: main_topic_id)
      applied_filters['Главная тема'] = MainTopic.find(main_topic_id).name
    end

    if organization_id.present?
      @press_releases = @press_releases.includes(:user).where(users: { organization_id: organization_id })
      applied_filters['Организация'] = Organization.find(organization_id).name
    end

    if category_id.present?
      @press_releases = @press_releases.where(press_release_value_id: PressReleaseValue.joins(subcategory: :category).where(categories: { id: category_id }))
      applied_filters['Категория'] = Category.find(category_id).name
    end

    @press_releases
  end

  def available_national_projects
    NationalProjectPolicy::ViewableScope.new(current_user, NationalProject).resolve.order(:short_name)
  end

  def available_federal_projects
    if national_project_ids.one?
      FederalProject.where(national_project_id: national_project_ids.first).order(:name)
    else
      []
    end
  end

  def available_federal_subjects
    return @available_federal_subjects if defined? @available_federal_subjects

    federal_subjects = FederalSubject.order(:name)

    if !current_user.admin? && current_user.federal_subject_id && !current_user.has_ability?(:show_other_regions_press_releases)
      @available_federal_subjects = federal_subjects.where(id: current_user.federal_subject_id)
    else
      @available_federal_subjects = [OpenStruct.new(id: 0, name: 'Межрегиональные')] + federal_subjects
    end

    @available_federal_subjects
  end

  def selected_national_projects
    available_national_projects.where(id: national_project_ids).order(:short_name)
  end

  def starts_at
    dates.split(' — ').map { |str| Date.parse(str).to_time }.first
  end

  def ends_at
    dates.split(' — ').map { |str| (Date.parse(str) + 1).to_time }.last
  end

  def tag
    return @tag if defined? @tag
    @tag = if tag_id.present?
      Tag.find(tag_id)
    else
      nil
    end
    @tag
  end

  def count
    @count ||= press_releases.count
  end
end