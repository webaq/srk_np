class NationalProjectsReport
  Row = Struct.new(:organization_name, :national_project_name, :counters_by_day) do
    def total
      counters_by_day ? counters_by_day.values.sum : 0
    end
  end

  attr_reader :organizations_scope
  attr_reader :press_releases_scope

  def initialize(organizations_scope, press_releases_scope)
    @organizations_scope = organizations_scope
    @press_releases_scope = press_releases_scope
  end

  def rows
    @rows ||= begin
      r = []

      organizations.each do |organization|
        if grouped_data[organization.id]
          r.concat grouped_data[organization.id].map { |national_project_name, counters_by_day|
            Row.new(organization.name, national_project_name, counters_by_day)
          }
        else
          r << Row.new(organization.name)
        end
      end

      r
    end
  end

  def organizations
    organizations_scope.select(:id, :name)
  end

  def grouped_data
    @grouped_data ||= begin
      hash = {}

      grouped_press_releases.each do |(organization_id, national_project_name, date), value|
        hash[organization_id] ||= {}
        hash[organization_id][national_project_name.to_sym] ||= {}
        hash[organization_id][national_project_name.to_sym][date] = value
      end

      hash
    end
  end

  def grouped_press_releases
    press_releases_scope
      .joins(:user)
      .where(users: { organization_id: organizations_scope })
      .joins(:national_projects)
      .group('users.organization_id')
      .group('national_projects.short_name')
      .group_by_day(:created_at, series: false)
      .order('national_projects.short_name')
      .count
  end
end