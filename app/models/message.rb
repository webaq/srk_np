class Message < ApplicationRecord
  enum addressee: { press_office: 1, moderator: 2, media: 3 }

  belongs_to :sender, class_name: 'User'

  has_many :message_national_projects, dependent: :destroy
  has_many :national_projects, through: :message_national_projects
  has_many :federal_projects, through: :national_projects
  has_many :message_organizations, dependent: :destroy
  has_many :organizations, through: :message_organizations
  has_many :message_deliveries, dependent: :destroy

  before_validation :set_published_at, unless: :published_at?

  validates :subject, presence: true
  validates :content, presence: true
  validates :national_projects, presence: true, on: :manual_create

  scope :published, -> { where.not(published_at: Time.current...) }

  has_rich_text :content

  def use_late_published_at
    !!late_published_at
  end

  def set_published_at
    self.published_at = late_published_at.presence || Time.current
  end

  def relevant_users
    users = User.all

    if addressee
      permitted_users = UserFederalProjectPermission
        .where(federal_project_id: federal_project_ids)
        .select(:user_id)

      permitted_users = case addressee.to_sym
      when :press_office
        permitted_users.creatable
      when :moderator
        permitted_users.moderatable
      when :media
        permitted_users.repostable
      end

      users = users.where(id: permitted_users)
    end

    users = users.where(organization_id: organization_ids) if organizations.any?

    users
  end
end
