module PressReleaseOriginable
  extend ActiveSupport::Concern

  included do
    after_update do
      duplicate_press_releases.each(&:set_origins)
    end

    around_destroy :set_duplicates_origins_around_destroy

    PressReleaseOrigin::PERCENTAGES.each do |percentage|
      has_one :"origin#{percentage}", ->{ where(percentage: percentage) }, class_name: 'PressReleaseOrigin', dependent: :destroy
      has_one :"origin#{percentage}_press_release", class_name: 'PressRelease', through: :"origin#{percentage}", source: :original_press_release
      has_many :"duplicates#{percentage}", ->{ where(percentage: percentage) }, class_name: 'PressReleaseOrigin', foreign_key: :original_press_release_id
      has_many :"kept#{percentage}_duplicate_press_releases", ->{ kept }, through: :"duplicates#{percentage}", source: :press_release
    end

    has_many :duplicates, class_name: 'PressReleaseOrigin', foreign_key: :original_press_release_id, dependent: :destroy
    has_many :duplicate_press_releases, through: :duplicates, source: :press_release
  end

  def set_origins
    PressReleaseOrigin::PERCENTAGES.each do |percentage|
      self.send("origin#{percentage}_press_release=", find_origin(percentage))
    end
  end

  def find_origin(percentage)
    PressRelease
      .where.not(id: id)
      .where('created_at < ?', created_at || Time.current)
      .similiar_press_releases(lexemes, (percentage * 0.01).round(2))
      .order(:created_at)
      .select(:id)
      .first
  end

  def set_duplicates_origins_around_destroy
    old_duplicate_ids = duplicate_press_releases.pluck(:id)
    yield
    PressRelease.where(id: old_duplicate_ids).each(&:set_origins)
  end

  class_methods do
    def generate_lexemes(text)
      PressRelease.connection.select_value("SELECT array_to_string(tsvector_to_array(to_tsvector('russian', #{PressRelease.connection.quote(text)})), ',')").to_s.split(',')
    end
  end
end