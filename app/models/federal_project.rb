class FederalProject < ApplicationRecord
  belongs_to :national_project
  has_many :measures, dependent: :restrict_with_error
  has_many :user_federal_project_permissions, dependent: :destroy

  has_many :viewable_users, ->{ merge(UserFederalProjectPermission.viewable) }, through: :user_federal_project_permissions, source: :user, class_name: 'User'
  has_many :creatable_users, ->{ merge(UserFederalProjectPermission.creatable) }, through: :user_federal_project_permissions, source: :user, class_name: 'User'
  has_many :moderatable_users, ->{ merge(UserFederalProjectPermission.moderatable) }, through: :user_federal_project_permissions, source: :user, class_name: 'User'
  has_many :repostable_users, ->{ merge(UserFederalProjectPermission.repostable) }, through: :user_federal_project_permissions, source: :user, class_name: 'User'

  has_many :press_release_federal_projects, dependent: :restrict_with_error

  def import_permitted_users(permission, users)
    users.find_in_batches do |group|
      UserFederalProjectPermission.upsert_all(group.map { |user|
        { federal_project_id: id, permission: permission, user_id: user.id }
      }, unique_by: %i[ user_id federal_project_id permission ])
    end
  end

  def self.to_multiple_select
    self.includes(:national_project)
      .order('national_projects.short_name')
      .order('federal_projects.name')
      .group_by(&:national_project)
      .map { |national_project, federal_projects|
        [
          national_project.short_name,
          federal_projects.map { |federal_project|
            [
              (federal_project.name || '').truncate(40),
              federal_project.id,
              { data: { tokens: "#{national_project.short_name} #{federal_project.name}" }}
            ]
          }
        ]
      }
  end
end
