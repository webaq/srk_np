class SubscriptionNationalProject < ApplicationRecord
  belongs_to :subscription, counter_cache: :national_projects_count
  belongs_to :national_project
end
