class PressReleasesReport
  attr_reader :starts_on
  attr_reader :ends_on

  def initialize(starts_on, ends_on)
    @starts_on = starts_on
    @ends_on = ends_on
  end

  def chart_json
    [
      { name: 'Создано', data: created_press_releases_by_day },
      { name: 'Опубликовано', data: approved_press_releases_by_day },
      { name: 'Взято СМИ', data: created_bookmarks_by_day }
    ].to_json
  end

  def period
    (starts_on.to_time..(ends_on + 1).to_time)
  end

  def approved_press_releases_by_day
    approved_press_releases.group_by_day(:approved_at).count
  end

  def created_press_releases_by_day
    created_press_releases.group_by_day(:created_at).count
  end

  def created_bookmarks_by_day
    created_bookmarks.group_by_day(:created_at).count
  end

  def created_press_releases
    PressRelease.where(created_at: period)
  end

  def approved_press_releases
    PressRelease.where(approved_at: period)
  end

  def created_bookmarks
    Bookmark.where(created_at: period)
  end
end