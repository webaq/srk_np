class PressReleaseFederalProject < ApplicationRecord
  belongs_to :press_release
  belongs_to :federal_project
end
