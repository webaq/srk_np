class SupportEmail < ApplicationRecord
  validates :email, presence: true
end
