class OrganizationWebsite < ApplicationRecord
  belongs_to :organization

  validates :address, presence: true

  def address=(new_value)
    new_value.strip! if new_value
    super
  end
end
