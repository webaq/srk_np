class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def self.human_enum_name(attribute, enum_key)
     human_attribute_name("#{attribute}.#{enum_key}")
  end
end
