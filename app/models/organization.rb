class Organization < ApplicationRecord
  enum kind: { federal_authority: 1, regional_authority: 2, city_authority: 5, media: 3, other: 4 }

  has_many :users, dependent: :nullify
  has_many :bookmarks, through: :users

  has_many :organization_websites, dependent: :destroy
  accepts_nested_attributes_for :organization_websites, reject_if: :all_blank, allow_destroy: true

  belongs_to :federal_subject, required: false
  has_many :message_organizations, dependent: :destroy
  has_many :mailing_lists, dependent: :destroy
  belongs_to :rating_addon_organization, required: false, class_name: 'Organization'
  has_many :subscription_organizations, dependent: :destroy

  validates :name, presence: true, uniqueness: true

  scope :with_users, -> { where.not(users_count: 0) }

  def admin?
    name == 'АНО Национальные приоритеты'
  end

  def name=(new_value)
    new_value.strip! if new_value
    super
  end

  def self.new_from_registration(registration)
    new({
      kind: registration.kind,
      federal_subject_id: registration.federal_subject_id,
      name: registration.organization_name,
      media_subscribers: registration.media_subscribers,
      organization_websites: [OrganizationWebsite.new(address: registration.website)],
    })
  end

  # def self.ransackable_attributes(auth_object = nil)
  #   %w[ id name kind created_at ]
  # end
end
