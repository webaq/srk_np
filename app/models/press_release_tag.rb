class PressReleaseTag < ApplicationRecord
  belongs_to :press_release
  belongs_to :tag
end
