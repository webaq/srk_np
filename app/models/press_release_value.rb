class PressReleaseValue < ApplicationRecord
  acts_as_list scope: :subcategory

  belongs_to :subcategory
  belongs_to :subcategory_scope, required: false
  has_many :press_releases_with_discarded, ->{ with_discarded }, dependent: :restrict_with_error, class_name: "PressRelease"

  validates :name, presence: true
  validates :value, presence: true
  validates :subcategory_scope, uniqueness: { scope: [:subcategory, :name] }

  delegate :category, to: :subcategory
end
