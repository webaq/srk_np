class NationalProjectTag < ApplicationRecord
  belongs_to :national_project
  belongs_to :tag
end
