class Dashboard
  MediaOrganizationsTable = Struct.new(
    :shown_total,
    :shown_period1,
    :shown_period2,
    :shown_period3,
    :shown_period4,

    :created_total,
    :created_upto_period1,
    :created_upto_period2,
    :created_upto_period3,
    :created_upto_period4,

    :reposted_total,
    :reposted_period1,
    :reposted_period2,
    :reposted_period3,
    :reposted_period4,

    keyword_init: true
  ) do
    def unshown_total
      created_total - shown_total
    end

    def unshown_period1
      created_upto_period1 - shown_period1
    end

    def unshown_period2
      created_upto_period2 - shown_period2
    end

    def unshown_period3
      created_upto_period3 - shown_period3
    end

    def unshown_period4
      created_upto_period4 - shown_period4
    end
  end

  include ActionView::Helpers::NumberHelper

  attr_reader :starts_on
  attr_reader :ends_on
  attr_reader :starts2_on
  attr_reader :ends2_on
  attr_reader :federal_subject

  attr_accessor :chart1_period
  attr_accessor :chart2_period
  attr_accessor :chart3_organizations_period
  attr_accessor :chart3_users_period
  attr_accessor :chart4_period

  def initialize(options = {})
    @starts_on = options.fetch(:starts_on)
    @ends_on = options.fetch(:ends_on)
    @starts2_on = options.fetch(:starts2_on)
    @ends2_on = options.fetch(:ends2_on)
    @federal_subject = options.fetch(:federal_subject, nil)

    @chart1_period = options.fetch(:chart1_period, 'week')
    @chart2_period = options.fetch(:chart2_period, 'week')
    @chart3_organizations_period = options.fetch(:chart3_organizations_period, 'week')
    @chart3_users_period = options.fetch(:chart3_users_period, 'week')
    @chart4_period = options.fetch(:chart4_period, 'week')
  end

  def starts_at
    starts_on.to_time
  end

  def ends_at
    (ends_on + 1).to_time
  end

  def starts2_at
    starts2_on.to_time
  end

  def ends2_at
    (ends2_on + 1).to_time
  end

  def starts3_at
    starts_at - 1.year
  end

  def ends3_at
    ends_at - 1.year
  end

  def both_periods_starts_at
    [starts_at, starts2_at].compact.min
  end

  def both_periods_ends_at
    [ends_at, ends2_at].compact.max
  end

  def media_organizations_table
    @media_organizations_table ||= begin
      organizations = Organization.media
      organizations = organizations.where(federal_subject: federal_subject) if federal_subject

      t = MediaOrganizationsTable.new
      t.shown_total = organizations.joins(users: :show_activities).distinct.count(:id)
      t.shown_period1 = organizations.joins(users: :show_activities).where(activities: { created_at: starts_at..ends_at }).distinct.count(:id)
      t.shown_period2 = organizations.joins(users: :show_activities).where(activities: { created_at: starts2_at..ends2_at }).distinct.count(:id)
      t.shown_period3 = organizations.joins(users: :show_activities).where(activities: { created_at: starts3_at..ends3_at }).distinct.count(:id)
      t.shown_period4 = organizations.joins(users: :show_activities).where(activities: { created_at: Time.current.beginning_of_year... }).distinct.count(:id)

      t.created_total = organizations.count(:id)
      t.created_upto_period1 = organizations.where('created_at < ?', ends_at).count(:id)
      t.created_upto_period2 = organizations.where('created_at < ?', ends2_at).count(:id)
      t.created_upto_period3 = organizations.where('created_at < ?', ends3_at).count(:id)
      t.created_upto_period4 = organizations.count(:id)

      t.reposted_total = organizations.joins(users: :reposts).distinct.count(:id)
      t.reposted_period1 = organizations.joins(users: :reposts).where(reposts: { created_at: starts_at..ends_at }).distinct.count(:id)
      t.reposted_period2 = organizations.joins(users: :reposts).where(reposts: { created_at: starts2_at..ends2_at }).distinct.count(:id)
      t.reposted_period3 = organizations.joins(users: :reposts).where(reposts: { created_at: starts3_at..ends3_at }).distinct.count(:id)
      t.reposted_period4 = organizations.joins(users: :reposts).where(reposts: { created_at: Time.current.beginning_of_year... }).distinct.count(:id)

      t
    end
  end

  def media_organizations_count
    # byebug
    @media_organizations_count ||= organization_counts.find { |oc| oc.first == 'media' }.try(:[], 1) || 0
  end

  def media_users_count
    User
      .joins(:organization)
      .merge(Organization.media)
      .where.not(created_at: (period1.end + 1).to_time...)
      .count
  end

  def media_activated_users_count
    User
      .joins(:organization)
      .merge(Organization.media)
      .where.not(created_at: (period1.end + 1).to_time...)
      .where.not(last_sign_in_at: nil)
      .count
  end

  def media_activate_users_count
    User
      .joins(:organization)
      .merge(Organization.media)
      .where.not(created_at: (period1.end + 1).to_time...)
      .includes(:visits)
      .where(visits: { created_at: starts_on.to_time..(ends_on + 1).to_time })
      .count
  end

  def national_projects_list
    @national_projects_list ||= Dashboard::NationalProjectsList.new({
      starts_at: starts_at,
      ends_at: ends_at,
      starts2_at: starts2_at,
      ends2_at: ends2_at,
      federal_subject_id: federal_subject&.id,
    })
  end

  def organization_counts
    @organization_counts ||= organizations
      .with_users
      .group(:kind)
      .order(:kind)
      .pluck(Arel.sql("
        organizations.kind,
        COUNT(organizations.id) FILTER (WHERE organizations.created_at <= '#{(period1.end + 1).to_time}'),
        COUNT(organizations.id) FILTER (WHERE organizations.created_at BETWEEN '#{period1.begin.to_time}' AND '#{(period1.end + 1).to_time}')
      "))
  end

  def organization_counts_total
    @organization_counts_total ||= organization_counts.sum { |oc| oc[1] }
  end

  def organization_counts_total_diff
    @organization_counts_total_diff ||= organization_counts.sum { |oc| oc[2] }
  end

  def press_releases_count
    @press_releases_count ||= press_releases.where(published_at: starts_at..ends_at).count
  end

  def press_releases_total
    @press_releases_total ||= press_releases.where('published_at <= ?', ends_at).count
  end

  def reposted_media_organizations_count
    media_organizations
      .where('organizations.created_at <= ?', (ends_on + 1).to_time)
      .joins(users: :reposts)
      .where(reposts: { created_at: starts_on.to_time..(ends_on + 1).to_time })
      .group(:id)
      .count
      .count
  end

  def reposted_organization_counts
    press_releases
      .where(published_at: starts_on.to_time..(ends_on + 1).to_time)
      .joins(reposts: { user: :organization })
      .group('organizations.kind')
      .order('organizations.kind')
      .pluck(Arel.sql("
        organizations.kind,
        COUNT(reposts.id)
      "))
  end

  def shows_period_count
    press_releases
      .joins(:media_show_activities)
      .where(activities: { created_at: starts_on.to_time..(ends_on + 1).to_time })
      .count
  end

  def shows_period_press_releaes_period_count
    press_releases
      .where(published_at: starts_on.to_time..(ends_on + 1).to_time)
      .joins(:media_show_activities)
      .where(activities: { created_at: starts_on.to_time..(ends_on + 1).to_time })
      .count
  end

  def shown_press_releases_count
    press_releases
      .where(published_at: starts_on.to_time..(ends_on + 1).to_time)
      .joins(:media_show_activities)
      .group(:id)
      .count # returns hash { id => shows_count }
      .count # count keys (distinct press_release ids)
  end

  def unshown_press_releases_count
    press_releases
      .where(published_at: starts_on.to_time..(ends_on + 1).to_time)
      .left_joins(:media_show_activities)
      .group(:id)
      .having('COUNT(activities.id) = 0')
      .count # returns hash { id => count }
      .count # count keys (distinct press_release ids)
  end

  def views_press_releases_media_organizations_count
    media_organizations
      .where('organizations.created_at <= ?', (ends_on + 1).to_time)
      .joins(users: :show_activities)
      .where(activities: { created_at: starts_on.to_time..(ends_on + 1).to_time })
      .group(:id)
      .count
      .count
  end

  def press_releases_chart
    [
      {
        name: 'Инфоповодов в СРК, ед',
        data: press_releases
          .where(published_at: starts_at..ends_at)
          .group_by_day(:published_at)
          .count
      }
    ]
  end

  def chart1
    chart = []

    chart << {
      name: 'Размещённые инфоповоды',
      data: press_releases
        .where(published_at: both_periods_starts_at..both_periods_ends_at)
        .group_by_period(chart1_period, :published_at)
        .count
        .map { |key, value|
          ["#{key.strftime('%d.%m')}…", value]
        }
    }

    chart << {
      name: 'Просмотренных инфоповодов',
      data: press_releases
        .where(published_at: both_periods_starts_at..both_periods_ends_at)
        .joins(:media_show_activities)
        .group_by_period(chart1_period, :published_at)
        .count('DISTINCT press_releases.id')
        .map { |key, value|
          ["#{key.strftime('%d.%m')}…", value]
        }
    }

    chart << {
      name: 'Инфоповодов с ссылками',
      data: press_releases
        .where(published_at: both_periods_starts_at..both_periods_ends_at)
        .joins(:reposts)
        .group_by_period(chart1_period, :published_at)
        .count('DISTINCT press_releases.id')
        .map { |key, value|
          ["#{key.strftime('%d.%m')}…", value]
        }
    }

    chart << {
      name: 'Опубликованных материалов в СМИ',
      data: press_releases
        .where(published_at: both_periods_starts_at..both_periods_ends_at)
        .joins(:reposts)
        .where(reposts: { created_at: both_periods_starts_at..both_periods_ends_at })
        .group_by_period(chart1_period, 'reposts.created_at')
        .count
        .map { |key, value|
          ["#{key.strftime('%d.%m')}…", value]
        }
    }

    chart
  end

  def chart2
    chart = []

    chart << {
      name: 'СМИ (федеральные) просмотревшие инфоповоды',
      data: Organization
        .media
        .where(federal_subject: nil)
        .joins(users: :show_activities)
        .where(activities: { created_at: starts_at..ends_at })
        .group_by_period(chart2_period, 'activities.created_at')
        .count('DISTINCT organizations.id')
        .map { |key, value|
          ["#{key.strftime('%d.%m')}…", value]
        }
    }

    chart << {
      name: 'СМИ (региональные) просмотревшие инфоповоды',
      data: Organization
        .media
        .where.not(federal_subject: nil)
        .joins(users: :show_activities)
        .where(activities: { created_at: starts_at..ends_at })
        .group_by_period(chart2_period, 'activities.created_at')
        .count('DISTINCT organizations.id')
        .map { |key, value|
          ["#{key.strftime('%d.%m')}…", value]
        }
    }

    chart
  end

  def chart3_users
    chart = []

    before_non_media_users_count = User
      .joins(:organization)
      .where.not(organizations: { kind: Organization.kinds[:media] })
      .where('users.created_at < ?', starts_at)
      .count

    created_non_media_users_by_date = User
      .joins(:organization)
      .where.not(organizations: { kind: Organization.kinds[:media] })
      .where(created_at: starts_at..ends_at)
      .group_by_period(chart3_users_period, :created_at)
      .count

    not_media_user_visit_counts = User
      .joins(:organization)
      .where.not(organizations: { kind: Organization.kinds[:media] })
      .joins(:visits)
      .where(visits: { created_at: starts_at..ends_at })
      .group_by_period(chart3_users_period, 'visits.created_at')
      .count('DISTINCT users.id')

    chart << {
      name: 'Кол-во пользователей органов власти',
      data: created_non_media_users_by_date.map { |key, value|
        users_on_date = before_non_media_users_count + created_non_media_users_by_date.select { |date, count| date <= key }.values.sum
        ["#{key.strftime('%d.%m')}…", users_on_date]
      }
    }

    chart << {
      name: 'Кол-во залогинов органов власти',
      data: not_media_user_visit_counts
        .map { |key, value|
          ["#{key.strftime('%d.%m')}…", value]
        }
    }

    # chart << {
    #   name: '% залогинов от источников',
    #   data: not_media_user_visit_counts.map { |key, value|
    #       users_on_date = before_non_media_users_count + created_non_media_users_by_date.select { |date, count| date <= key }.values.sum
    #       value2 = value.to_f / users_on_date
    #       [key, number_to_percentage(value2 * 100)]
    #     }
    # }

    before_media_users_count = User
      .joins(:organization)
      .where(organizations: { kind: Organization.kinds[:media] })
      .where('users.created_at < ?', starts_at)
      .count

    created_media_users_by_date = User
      .joins(:organization)
      .where(organizations: { kind: Organization.kinds[:media] })
      .where(created_at: starts_at..ends_at)
      .group_by_period(chart3_users_period, :created_at)
      .count

    media_user_visit_counts = User
      .joins(:organization)
      .where(organizations: { kind: Organization.kinds[:media] })
      .joins(:visits)
      .where(visits: { created_at: starts_at..ends_at })
      .group_by_period(chart3_users_period, 'visits.created_at')
      .count('DISTINCT users.id')

    chart << {
      name: 'Кол-во пользователей СМИ',
      data: created_media_users_by_date.map { |key, value|
        users_on_date = before_media_users_count + created_media_users_by_date.select { |date, count| date <= key }.values.sum
        ["#{key.strftime('%d.%m')}…", users_on_date]
      }
    }

    chart << {
      name: 'Кол-во залогинов СМИ',
      data: media_user_visit_counts
        .map { |key, value|
          ["#{key.strftime('%d.%m')}…", value]
        }
    }

    # chart << {
    #   name: '% залогинов от СМИ',
    #   data: media_user_visit_counts.map { |key, value|
    #       users_on_date = before_media_users_count + created_media_users_by_date.select { |date, count| date <= key }.values.sum
    #       value2 = value.to_f / users_on_date
    #       [key, number_to_percentage(value2 * 100)]
    #     }
    # }

    chart
  end

  def chart3_organizations
    chart = []

    before_non_media_organizations_count = Organization
      .where.not(kind: Organization.kinds[:media])
      .where('created_at < ?', starts_at)
      .count

    created_non_media_organizations_by_date = Organization
      .where.not(kind: Organization.kinds[:media])
      .where(created_at: starts_at..ends_at)
      .group_by_period(chart3_organizations_period, :created_at)
      .count

    not_media_organization_visit_counts = Organization
      .where.not(organizations: { kind: Organization.kinds[:media] })
      .joins(users: :visits)
      .where(visits: { created_at: starts_at..ends_at })
      .group_by_period(chart3_organizations_period, 'visits.created_at')
      .count('DISTINCT organizations.id')

    chart << {
      name: 'Кол-во организаций органов власти',
      data: created_non_media_organizations_by_date.map { |key, value|
        organizations_on_date = before_non_media_organizations_count + created_non_media_organizations_by_date.select { |date, count| date <= key }.values.sum
        ["#{key.strftime('%d.%m')}…", organizations_on_date]
      }
    }

    chart << {
      name: 'Кол-во залогинов органов власти',
      data: not_media_organization_visit_counts
        .map { |key, value|
          ["#{key.strftime('%d.%m')}…", value]
        }
    }

    before_media_organizations_count = Organization
      .media
      .where('created_at < ?', starts_at)
      .count

    created_media_organizations_by_date = Organization
      .media
      .where(created_at: starts_at..ends_at)
      .group_by_period(chart3_organizations_period, :created_at)
      .count

    media_organization_visit_counts = Organization
      .media
      .joins(users: :visits)
      .where(visits: { created_at: starts_at..ends_at })
      .group_by_period(chart3_organizations_period, 'visits.created_at')
      .count('DISTINCT organizations.id')

    chart << {
      name: 'Кол-во организаций СМИ',
      data: created_media_organizations_by_date.map { |key, value|
        organizations_on_date = before_media_organizations_count + created_media_organizations_by_date.select { |date, count| date <= key }.values.sum
        ["#{key.strftime('%d.%m')}…", organizations_on_date]
      }
    }

    chart << {
      name: 'Кол-во залогинов СМИ',
      data: media_organization_visit_counts
        .map { |key, value|
          ["#{key.strftime('%d.%m')}…", value]
        }
    }

    chart
  end

  def chart4
      Visit
        .where(created_at: starts_at..ends_at)
        .joins(user: :organization)
        .left_joins(user: { organization: :federal_subject })
        .group("
          CASE
            WHEN organizations.kind = #{Organization.kinds[:media]} AND federal_subjects.id IS NULL THEN 98
            WHEN organizations.kind = #{Organization.kinds[:media]} AND federal_subjects.id IS NOT NULL THEN 99
            ELSE organizations.kind
          END
        ")
        .group_by_period(chart4_period, 'visits.created_at') #, format: Proc.new { |d, d2| "#{I18n.l(d, format: '%-d')}-#{I18n.l(d.to_date + 6, format: '%-d %B')}" }
        .count('DISTINCT users.id')
        .map { |key, value|
          key[0] = case key[0]
          when 98; "#{Organization.human_attribute_name('kinds.media')} (федеральные)"
          when 99; "#{Organization.human_attribute_name('kinds.media')} (региональные)"
          else Organization.human_attribute_name("kinds.#{Organization.kinds.key(key[0])}")
          end
          key[1] = "#{key[1].strftime('%d.%m')}…"
          [key, value]
        }.to_h
  end

  def media_digest_deliveries_chart
    media_digest_deliveries = MediaDigestDelivery
      .where(delivered_at: starts_at..ends_at)
      .group_by_day(:delivered_at)

    chart = []

    chart << {
      name: 'Доставлено',
      data: media_digest_deliveries
        .count
        .map { |key, value|
          ["#{key.strftime('%d.%m')}", value]
        }
    }

    chart << {
      name: 'Прочитано',
      data: media_digest_deliveries
        .where.not(opened_at: nil)
        .count
        .map { |key, value|
          ["#{key.strftime('%d.%m')}", value]
        }
    }

    chart << {
      name: 'Переход по ссылке',
      data: media_digest_deliveries
        .where.not(clicked_at: nil)
        .count
        .map { |key, value|
          ["#{key.strftime('%d.%m')}", value]
        }
    }

    chart
  end

  def press_releases
    return @press_releases if defined? @press_releases
    @press_releases = PressRelease.all
    @press_releases = @press_releases.where(id: PressRelease.joins(:federal_subjects).where(federal_subjects: { id: federal_subject.id })) if federal_subject
    @press_releases
  end

  def press_releases_total_count
    @press_releases_total_count ||= press_releases.count
  end

  def press_releases_period1_count
    @press_releases_period1_count ||= press_releases.where(created_at: starts_at..ends_at).count
  end

  def press_releases_period2_count
    @press_releases_period2_count ||= press_releases.where(created_at: starts2_at..ends2_at).count
  end

  def press_releases_period3_count
    @press_releases_period3_count ||= press_releases.where(created_at: starts3_at..ends3_at).count
  end

  def press_releases_period4_count
    @press_releases_period4_count ||= press_releases
      .where(created_at: Time.current.beginning_of_year...)
      .count
  end

  def press_releases_average_count
    @press_releases_average_count ||= press_releases_period4_count.to_f / weeks_since_beginning_of_year
  end

  def press_releases_shown_total_count
    @press_releases_shown_total_count ||= press_releases.joins(:media_show_activities).distinct.count
  end

  def press_releases_shown_period1_count
    @press_releases_shown_peiod1_count ||= press_releases
      .where(created_at: starts_at..ends_at)
      .joins(:media_show_activities)
      .distinct
      .count
  end

  def press_releases_shown_period2_count
    @press_releases_shown_peiod2_count ||= press_releases
      .where(created_at: starts2_at..ends2_at)
      .joins(:media_show_activities)
      .distinct
      .count
  end

  def press_releases_shown_period3_count
    @press_releases_shown_peiod3_count ||= press_releases
      .where(created_at: starts3_at..ends3_at)
      .joins(:media_show_activities)
      .distinct
      .count
  end

  def press_releases_shown_period4_count
    @press_releases_shown_period4_count ||= press_releases
      .where(created_at: Time.current.beginning_of_year...)
      .joins(:media_show_activities)
      .distinct
      .count
  end

  def press_releases_shown_average_count
    @press_releases_shown_average_count ||= press_releases_shown_period4_count.to_f / weeks_since_beginning_of_year
  end

  def weeks_since_beginning_of_year
    Date.current.cweek
  end

  def press_releases_unshown_total_count
    press_releases_total_count - press_releases_shown_total_count
  end

  def press_releases_unshown_period1_count
    press_releases_period1_count - press_releases_shown_period1_count
  end

  def press_releases_unshown_period2_count
    press_releases_period2_count - press_releases_shown_period2_count
  end

  def press_releases_unshown_period3_count
    press_releases_period3_count - press_releases_shown_period3_count
  end

  def press_releases_unshown_average_count
    (press_releases_period4_count - press_releases_shown_period4_count).to_f / weeks_since_beginning_of_year
  end

  def press_releases_reposted_total_count
    @press_releases_reposted_total_count ||= press_releases
      .where(id: Repost.distinct.select(:press_release_id))
      .count
  end

  def press_releases_reposted_period1_count
    @press_releases_reposted_period1_count ||= press_releases
      .where(created_at: starts_at..ends_at, id: Repost.distinct.select(:press_release_id))
      .count
  end

  def press_releases_reposted_period2_count
    @press_releases_reposted_period2_count ||= press_releases
      .where(created_at: starts2_at..ends2_at, id: Repost.distinct.select(:press_release_id))
      .count
  end

  def press_releases_reposted_period3_count
    @press_releases_reposted_period3_count ||= press_releases
      .where(created_at: starts3_at..ends3_at, id: Repost.distinct.select(:press_release_id))
      .count
  end

  def press_releases_reposted_average_count
    @press_releases_reposted_average_count ||= press_releases
      .where(created_at: Time.current.beginning_of_year..., id: Repost.distinct.select(:press_release_id))
      .count
      .to_f / weeks_since_beginning_of_year
  end

  def reposts_total_count
    @reposts_total_count ||= press_releases
      .joins(:reposts)
      .count
  end

  def reposts_period1_count
    @reposts_period1_count ||= press_releases
      .where(created_at: starts_at..ends_at)
      .joins(:reposts)
      .count
  end

  def reposts_period2_count
    @reposts_period2_count ||= press_releases
      .where(created_at: starts2_at..ends2_at)
      .joins(:reposts)
      .count
  end

  def reposts_period3_count
    @reposts_period3_count ||= press_releases
      .where(created_at: starts3_at..ends3_at)
      .joins(:reposts)
      .count
  end

  def reposts_average_count
    @reposts_average_count ||= press_releases
      .where(created_at: Time.current.beginning_of_year...)
      .joins(:reposts)
      .count
      .to_f / weeks_since_beginning_of_year
  end

  def organizations_list
    @organizations_list ||= Dashboard::OrganizationsList.new({
      federal_subject_id: federal_subject&.id,
      starts_at: starts_at,
      ends_at: ends_at,
      starts2_at: starts2_at,
      ends2_at: ends2_at,
    })
  end

  def users_list
    @users_list ||= Dashboard::UsersList.new({
      federal_subject_id: federal_subject&.id,
      starts_at: starts_at,
      ends_at: ends_at,
      starts2_at: starts2_at,
      ends2_at: ends2_at,
    })
  end

  private
    def media_organizations
      organizations.where(kind: [
        Organization.kinds[:media],
      ])
    end

    def organizations
      @organizations ||= begin
        q = Organization.all
        q = q.where(federal_subject_id: federal_subject.id) if federal_subject
        q
      end
    end
end