class RegionalAuthoritiesMonthRating
  FederalSubjectRow = Struct.new(:name, :current_count, :prev_count, :rank, :partition, keyword_init: true)
  MediaOrganizationRow = Struct.new(:name, :current_count, :prev_count, :rank, :partition, keyword_init: true)

  attr_reader :rating_month
  attr_reader :scoring

  delegate :month, :prev_month, :starts_at, :ends_at, :prev_starts_at, :prev_ends_at, :show_prev_month?, to: :rating_month

  def initialize(rating_month, scoring = nil)
    @rating_month = rating_month
    @scoring = (scoring || "quantity").to_sym
  end

  def starts_at_db
    starts_at.utc.to_s(:db)
  end

  def ends_at_db
    ends_at.utc.to_s(:db)
  end

  def prev_starts_at_db
    prev_starts_at.utc.to_s(:db)
  end

  def prev_ends_at_db
    prev_ends_at.utc.to_s(:db)
  end

  def all_federal_subjects
    @all_federal_subjects ||= FederalSubject.all
  end

  def federal_authorities_stats
    return @federal_authorities_stats if defined? @federal_authorities_stats

    @federal_authorities_stats = case scoring
    when :quantity
      federal_authorities_stats_quantity
    when :value
      Rating::FederalAuthoritiesRating.new(starts_at..ends_at, prev_starts_at..prev_ends_at).call
    else
      raise
    end

    @federal_authorities_stats
  end

  def federal_authorities_stats_quantity
    @federal_authorities_stats_quantity ||= begin
      addon_organizations_hash = Organization.where.not(rating_addon_organization_id: nil)
        .left_joins(users: :approved_press_releases)
        .group(:id)
        .select('organizations.rating_addon_organization_id')
        .select('organizations.name')
        .select("COUNT(press_releases.id) FILTER (WHERE press_releases.published_at BETWEEN '#{starts_at_db}' AND '#{ends_at_db}') AS current_count")
        .select("COUNT(press_releases.id) FILTER (WHERE press_releases.published_at BETWEEN '#{prev_starts_at_db}' AND '#{prev_ends_at_db}') AS prev_count")
        .order(:name)
        .group_by(&:rating_addon_organization_id)

      rows = Organization.federal_authority.where(rated: true)
        .left_joins(users: :approved_press_releases)
        .group(:id)
        .select('organizations.id')
        .select('organizations.name')
        .select("COUNT(press_releases.id) FILTER (WHERE press_releases.published_at BETWEEN '#{starts_at_db}' AND '#{ends_at_db}') AS current_count")
        .select("COUNT(press_releases.id) FILTER (WHERE press_releases.published_at BETWEEN '#{prev_starts_at_db}' AND '#{prev_ends_at_db}') AS prev_count")
        .map do |organization|
          name = organization.name
          current_count = organization.current_count
          prev_count = organization.prev_count

          addon_organizations = addon_organizations_hash.fetch(organization.id, [])

          if organization.id == 3
            name << ',<br>в том числе: вузы и научные организации'
            current_count += addon_organizations.sum(&:current_count)
            prev_count += addon_organizations.sum(&:prev_count)

          elsif addon_organizations.present?
            name << ',<br>в том числе:<br>'
            name << addon_organizations.map do |addon_organization|
              "#{addon_organization.name} – #{addon_organization.current_count} #{I18n.t('press_release', count: addon_organization.current_count)}"
            end.join('<br>')

            current_count += addon_organizations.sum(&:current_count)
            prev_count += addon_organizations.sum(&:prev_count)
          end

          FederalSubjectRow.new(
            name: name,
            current_count: current_count,
            prev_count: prev_count
          )
        end.sort_by { |row| -row.current_count }

      uniq_values = rows.map(&:current_count).uniq

      other_values_splitted = uniq_values.size > 10 ? uniq_values[10..-2].in_groups(3, fill_with: false) : []

      rows.each do |row|
        rank = uniq_values.index(row.current_count)
        row.rank = rank

        row.partition = if row.current_count > 0 && rank < 10
          0
        elsif rank == uniq_values.length - 1
          4
        else
          1 + other_values_splitted.index { |group| row.current_count.in?(group) }
        end
      end

      rows
    end
  end

  def media_organizations_stats
    @media_organizations_stats ||= begin
      rows = Organization.media
        .joins(users: :reposts)
        .group(:id)
        .select('organizations.name')
        .select("COUNT(reposts.id) FILTER (WHERE reposts.created_at BETWEEN '#{starts_at_db}' AND '#{ends_at_db}') AS current_count")
        .select("COUNT(reposts.id) FILTER (WHERE reposts.created_at BETWEEN '#{prev_starts_at_db}' AND '#{prev_ends_at_db}') AS prev_count")
        .order(current_count: 'DESC')
        .limit(30)
        .map do |organization|
          MediaOrganizationRow.new(
            name: organization.name,
            current_count: organization.current_count,
            prev_count: organization.prev_count
          )
        end
      # byebug
      uniq_values = rows.map(&:current_count).uniq
      if rows.size > 10
        other_values_splitted = uniq_values[10..-2].in_groups(3, fill_with: false)
      end

      rows.each do |row|
        rank = uniq_values.index(row.current_count)
        row.partition = if row.current_count > 0 && rank < 10
          0
        elsif rank == uniq_values.length - 1
          4
        else
          1 + other_values_splitted.index { |group| row.current_count.in?(group) }
        end
      end

      rows
    end
  end

  def rated_media_organizations_stats
    @media_organizations_stats ||= begin
      rows = Organization.media.where(rated: true)
        .left_joins(users: :reposts)
        .group(:id)
        .select('organizations.name')
        .select("COUNT(reposts.id) FILTER (WHERE reposts.created_at BETWEEN '#{starts_at_db}' AND '#{ends_at_db}') AS current_count")
        .select("COUNT(reposts.id) FILTER (WHERE reposts.created_at BETWEEN '#{prev_starts_at_db}' AND '#{prev_ends_at_db}') AS prev_count")
        .order(current_count: 'DESC')
        .order(:name)
        .limit(30)
        .map do |organization|
          MediaOrganizationRow.new(
            name: organization.name,
            current_count: organization.current_count,
            prev_count: organization.prev_count
          )
        end
      # byebug
      uniq_values = rows.map(&:current_count).uniq
      if rows.size > 10
        other_values_splitted = uniq_values[10..-2].in_groups(3, fill_with: false)
      end

      rows.each do |row|
        rank = uniq_values.index(row.current_count)
        row.partition = if row.current_count > 0 && rank < 10
          0
        elsif rank == uniq_values.length - 1
          4
        else
          1 + other_values_splitted.index { |group| row.current_count.in?(group) }
        end
      end

      rows
    end
  end

  def federal_subjects_press_releases_stats
    return @federal_subjects_press_releases_stats if defined? @federal_subjects_press_releases_stats

    @federal_subjects_press_releases_stats = case scoring
    when :quantity
      federal_subjects_press_releases_quantity_stats
    when :value
      Rating::FederalSubjectsRating.new(starts_at..ends_at, prev_starts_at..prev_ends_at).call
    else
      raise
    end

    @federal_subjects_press_releases_stats
  end

  def federal_subjects_press_releases_quantity_stats
    rows = all_federal_subjects.map { |federal_subject|
      FederalSubjectRow.new(
        name: federal_subject.name,
        current_count: federal_subject_press_releases_stats_hash.fetch(federal_subject.id, [0, 0]).first,
        prev_count: federal_subject_press_releases_stats_hash.fetch(federal_subject.id, [0, 0]).last
      )
    }.sort_by { |federal_subject_row|
      [-federal_subject_row.current_count, federal_subject_row.name]
    }

    uniq_counts = rows.map(&:current_count).uniq

    rank0_lowest_count = uniq_counts.first(10).last
    rank4_count = uniq_counts.last

    other_counts_splitted = uniq_counts.size > 10 ? uniq_counts[10..-2].in_groups(3, fill_with: false) : []

    rows.each do |row|
      row.rank = uniq_counts.index(row.current_count)

      row.partition = if row.current_count >= rank0_lowest_count
        0
      elsif row.current_count == rank4_count
        4
      else
        1 + other_counts_splitted.index { |group| row.current_count.in?(group) }
      end
    end

    rows
  end

  def federal_subject_press_releases_stats_hash
    @federal_subject_press_releases_stats_hash ||= begin
      hash = {}
      Organization
        .where(kind: [
          Organization.kinds[:regional_authority],
          Organization.kinds[:city_authority],
          Organization.kinds[:other]
        ])
        .joins(users: :approved_press_releases)
        .group(:federal_subject_id)
        .pluck(Arel.sql("
          organizations.federal_subject_id,
          COUNT(press_releases.id) FILTER (WHERE press_releases.published_at BETWEEN '#{starts_at_db}' AND '#{ends_at_db}'),
          COUNT(press_releases.id) FILTER (WHERE press_releases.published_at BETWEEN '#{prev_starts_at_db}' AND '#{prev_ends_at_db}'),
          ARRAY_AGG(press_releases.id) FILTER (WHERE press_releases.published_at BETWEEN '#{starts_at_db}' AND '#{ends_at_db}')
        "))
        .each do |federal_subject_id, current_count, prev_count, press_release_ids|
          Rails.logger.debug "!federal_subject_press_releases_stats_hash: #{federal_subject_id}: #{press_release_ids}"
          hash[federal_subject_id] = [current_count, prev_count]
      end
      hash
    end
  end

  def federal_subjects_national_projects_stats
    NationalProject
      .where(used_in_rating: true)
      .joins(press_releases: { user: { organization: :federal_subject }})
      .where(organizations: {
        kind: [
          Organization.kinds[:regional_authority],
          Organization.kinds[:city_authority],
          Organization.kinds[:other]
        ]
      })
      .merge(PressRelease.approved)
      .where(press_releases: {
        published_at: starts_at..ends_at
      })
      .where.not(id: 14)
      .group(:id, 'federal_subjects.id')
      .select(:id)
      .select(:short_name)
      .select('federal_subjects.name AS federal_subject_name')
      .select('COUNT(press_releases.id) AS press_releases_count')
      .order(press_releases_count: 'DESC')
      .order(:federal_subject_name)
      .group_by(&:id)
      .sort_by { |id, national_projects| -national_projects.sum(&:press_releases_count) }
  end

  def federal_subjects_media_organizations_stats
    @federal_subjects_media_organizations_stats ||= begin
      rows = all_federal_subjects.map { |federal_subject|
        FederalSubjectRow.new(
          name: federal_subject.name,
          current_count: federal_subject_media_organizations_stats_hash.fetch(federal_subject.id, 0)
        )
      }.sort_by {
        |row| [-row.current_count, row.name]
      }

      uniq_values = rows.map(&:current_count).uniq
      other_values_splitted = uniq_values.size > 10 ? uniq_values[10..-2].in_groups(3, fill_with: false) : []

      rows.each do |row|
        rank = uniq_values.index(row.current_count)
        row.rank = rank

        row.partition = if rank < 10
          0
        elsif rank == uniq_values.length - 1
          4
        else
          1 + other_values_splitted.index { |group| row.current_count.in?(group) }
        end
      end

      rows
    end
  end

  def federal_subject_media_organizations_stats_hash
    @federal_subject_media_organizations_stats_hash ||= Organization.media
      .joins(:federal_subject)
      .where.not(created_at: ends_at...)
      .group(:federal_subject_id)
      .count('organizations.id')
  end
end