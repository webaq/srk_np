class SubcategoryScope < ApplicationRecord
  belongs_to :subcategory

  acts_as_list scope: :subcategory

  validates :name, presence: true
end
