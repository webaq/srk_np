class NotificationPopup < ApplicationRecord
  include PublicActivity::Common

  has_rich_text :content

  validates :name, presence: true
  validates :starts_on, presence: true
  validates :ends_on, presence: true
  validates :timeout, presence: true

  def self.for_user(user)
    s = where("starts_on <= ?", Date.current)
      .where("ends_on >= ?", Date.current)
      .where("? = ANY (organization_kinds)", user.organization&.kind)
      .where.not(id: NotificationPopup.joins(:activities).where(activities: { key: "show", owner_id: user.id }))

    s = s.where(only_regional_organizations: false) unless user.organization&.federal_subject

    s.first
  end
end
