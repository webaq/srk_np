class MessageOrganization < ApplicationRecord
  belongs_to :message
  belongs_to :organization
end
