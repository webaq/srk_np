class Repost < ApplicationRecord
  belongs_to :user
  belongs_to :press_release, counter_cache: true

  counter_culture [:user, :organization]

  validates :link, presence: true, url: true
  # validates :link, organization_website_presence: true, organization_website_match: true,
  #   unless: Proc.new { |r| r.user&.organization&.skip_repost_domain_validation }

  def link=(new_value)
    new_value.strip! if new_value
    super
  end
end
