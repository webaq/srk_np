class User < ApplicationRecord
  include Discard::Model

  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable, :registerable

  acts_as_token_authenticatable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable, :trackable, :confirmable,
         :omniauthable, omniauth_providers: %i[vkontakte facebook odnoklassniki google_oauth2]

  attribute :asked_to_send_confirmation_notification, :boolean, default: false
  # enum role: {
  #   media: 3,
  #   press_office: 1,
  #   moderator: 2,
  #   admin: 0,
  # }

  enum sex: [ :male, :female ], _scopes: false

  validates :first_name, presence: true, on: :account_setup
  validates :last_name, presence: true, on: :account_setup
  validates :contact_position, presence: true, on: :account_setup
  validates :contact_phone, presence: true, on: :account_setup
  validates :user_groups, presence: true, on: :registration_confirmation

  # validates :contact_name, presence: true, if: :organization?
  # validates :website, presence: true, if: :media?
  # validates :media_registration_number, presence: true, if: :media?
  # validates :media_subscribers, presence: true, numericality: { greater_than: 0 }, if: :media?
  # validates :role, presence: true

  has_many :press_releases, dependent: :destroy
  has_many :approved_press_releases, -> { approved }, class_name: 'PressRelease'
  has_many :bookmarks, dependent: :restrict_with_error
  has_many :reposts, dependent: :restrict_with_error
  has_many :comments, dependent: :restrict_with_error
  belongs_to :federal_subject, required: false
  belongs_to :urban_district, required: false
  has_one :subscription, dependent: :destroy
  belongs_to :organization, required: false, counter_cache: true
  accepts_nested_attributes_for :organization
  has_many :sended_messages, class_name: 'Message', foreign_key: :sender_id
  has_many :message_deliveries, dependent: :destroy

  has_many :user_federal_project_permissions, dependent: :destroy

  has_many :viewable_federal_project_permissions, -> { viewable }, class_name: 'UserFederalProjectPermission'
  has_many :viewable_federal_projects, ->{ merge(UserFederalProjectPermission.viewable) }, class_name: 'FederalProject', through: :user_federal_project_permissions, source: :federal_project
  has_many :viewable_national_projects, -> { distinct }, class_name: 'NationalProject', through: :viewable_federal_projects, source: :national_project

  has_many :creatable_federal_project_permissions, -> { creatable }, class_name: 'UserFederalProjectPermission'
  has_many :creatable_federal_projects, ->{ merge(UserFederalProjectPermission.creatable) }, class_name: 'FederalProject', through: :user_federal_project_permissions, source: :federal_project
  has_many :creatable_national_projects, -> { distinct }, class_name: 'NationalProject', through: :creatable_federal_projects, source: :national_project

  has_many :moderatable_federal_project_permissions, -> { moderatable }, class_name: 'UserFederalProjectPermission'
  has_many :moderatable_federal_projects, ->{ merge(UserFederalProjectPermission.moderatable) }, class_name: 'FederalProject', through: :user_federal_project_permissions, source: :federal_project
  has_many :moderatable_national_projects, -> { distinct }, class_name: 'NationalProject', through: :moderatable_federal_projects, source: :national_project

  has_many :repostable_federal_project_permissions, -> { repostable }, class_name: 'UserFederalProjectPermission'
  has_many :repostable_federal_projects, ->{ merge(UserFederalProjectPermission.repostable) }, class_name: 'FederalProject', through: :user_federal_project_permissions, source: :federal_project
  has_many :repostable_national_projects, -> { distinct }, class_name: 'NationalProject', through: :repostable_federal_projects, source: :national_project

  has_many :activities, class_name: '::PublicActivity::Activity', foreign_key: :owner_id
  has_many :show_activities, -> { where(key: 'press_release.show') }, class_name: "::PublicActivity::Activity", foreign_key: :owner_id

  has_one :registration, dependent: :destroy

  has_many :press_release_show_activities, -> { where(key: 'press_release.show') }, class_name: "::PublicActivity::Activity", foreign_key: :owner_id

  has_many :visits, dependent: :destroy
  has_many :placements, dependent: :restrict_with_error
  has_many :support_requests, dependent: :destroy

  has_one_attached :avatar

  has_many :omniauth_accounts, dependent: :destroy

  has_many :favourites, dependent: :destroy
  has_many :favourite_press_releases, through: :favourites, source: :press_release

  has_many :best_practices, dependent: :restrict_with_error
  has_many :impressions, dependent: :destroy
  has_many :press_release_likes, dependent: :destroy

  has_many :complaints, dependent: :destroy
  has_many :subscription_messages, dependent: :nullify

  has_many :user_group_users, dependent: :destroy
  has_many :user_groups, through: :user_group_users
  has_many :user_group_abilities, through: :user_groups

  default_scope -> { kept }
  scope :by_current_local_hour, -> (hour) {
    where(id:
      left_joins(organization: :federal_subject)
      .where("
        EXTRACT(
          HOUR FROM (
            :current_time AT TIME ZONE
              COALESCE(NULLIF(federal_subjects.time_zone, ''), :default_timezone)
          )
        ) = :hour", {
        current_time: Time.current,
        default_timezone: Time.zone.tzinfo.name,
        hour: hour
      })
    )
  }

  scope :confirmed, ->{ where.not(confirmed_at: nil) }

  counter_culture :organization,
    column_name: proc {|user| user.confirmed? ? 'confirmed_users_count' : nil },
    column_names: {
      User.confirmed => :confirmed_users_count
    }

  before_create do
    @skip_confirmation_notification = !asked_to_send_confirmation_notification
  end

  def all_federal_subjects?
    federal_subject_id.nil?
  end

  def bookmarked?(press_release)
    Bookmark.where(user: self, press_release: press_release).any?
  end

  def can_approve?
    # TODO: deprecated
    admin || can_moderate_federal_project_ids.any?
  end

  def can_create_federal_project_ids
    # TODO: deprecated
    creatable_federal_project_ids
  end

  def can_moderate_federal_project_ids
    # TODO: deprecated
    moderatable_federal_project_ids
  end

  def can_view_federal_project_ids
    # TODO: deprecated
    viewable_federal_project_ids
  end

  def can_repost_federal_project_ids
    # TODO: deprecated
    repostable_federal_project_ids
  end

  def press_release_impression(press_release_id)
    impressions.find_or_initialize_by(press_release_id: press_release_id)
  end

  def press_office?
    creatable_national_project_ids.any?
  end

  def media?
    repostable_national_project_ids.any?
  end

  def moderator?
    moderatable_national_project_ids.any?
  end

  def name
    [first_name, last_name].select(&:present?).join(' ')
  end

  def relevant_onboarding
    if press_office?
      Tutorial.where(creatable_permitted: true, onboarding: true).last
    elsif media?
      Tutorial.where(repostable_permitted: true, onboarding: true).last
    end
  end

  def mailing_lists
    organization&.mailing_lists || MailingList.none
  end

  def liked?(press_release)
    press_release_likes.exists?(press_release_id: press_release.id)
  end

  def has_ability?(ability)
    raise "Unknown ability: #{ability}" unless UserGroupAbility.abilities.keys.include?(ability.to_s)
    user_group_abilities.any?(&:"#{ability}?")
  end

  # def self.ransackable_attributes(auth_object = nil)
  #   %w[email first_name last_name created_on confirmed_on created_at confirmed_at]
  # end

  def self.new_from_registration(registration)
    new({
      federal_subject_id: registration.federal_subject_id,
      email: registration.email,
      first_name: registration.first_name,
      last_name: registration.last_name,
      contact_position: registration.contact_position,
      contact_phone: registration.contact_phone,
      organization: Organization.find_by(name: registration.organization_name) || Organization.new_from_registration(registration)
    })
  end

  def self.from_omniauth(auth)
    where(id: OmniauthAccount.where(provider: auth.provider, uid: auth.uid).select(:user_id)).first
  end

  ransacker :created_on, type: :date do
    Arel.sql("date(users.created_at at time zone 'UTC' at time zone '#{Time.zone.name}')")
  end

  ransacker :confirmed_on, type: :date do
    Arel.sql("date(users.confirmed_at at time zone 'UTC' at time zone '#{Time.zone.name}')")
  end
end
