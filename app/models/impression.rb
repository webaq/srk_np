class Impression < ApplicationRecord
  belongs_to :press_release
  belongs_to :user

  def score_rate
    score / 10.0
  end
end
