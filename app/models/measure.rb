class Measure < ApplicationRecord
  belongs_to :federal_project
  has_many :press_releases, dependent: :restrict_with_error
end
