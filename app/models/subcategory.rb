class Subcategory < ApplicationRecord
  acts_as_list scope: :category

  belongs_to :category
  has_many :subcategory_scopes, ->{ order(:position) }, dependent: :restrict_with_error
  has_many :press_release_values, ->{ order(:position) }, dependent: :restrict_with_error

  validates :name, presence: true
end
