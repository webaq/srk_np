class SubscriptionDelivery
  attr_reader :frequency
  attr_reader :subscriptions

  def initialize(frequency, subscriptions = nil)
    @frequency = frequency.to_sym
    raise unless %i[ daily weekly ].include?(@frequency)

    @subscriptions = if subscriptions
      Subscription.where(id: subscriptions.map(&:id))
    else
      Subscription.public_send(frequency)
    end
  end

  def call
    subscriptions.find_each do |subscription|
      press_releases = SubscriptionPressReleasesFilter.new(subscription, frequency).call
      next unless press_releases.any?

      period_text = case @frequency
      when :daily
        'прошедший день'
      when :weekly
        'прошедшую неделю'
      end

      message = NotifierMailer.subscription_mail(subscription.user.email, press_releases, period_text)#.deliver_now
      message_id = SendPulseApi.send_message(message)
      subscription.user.subscription_messages.create!({
        message_id: message_id,
        recipient: subscription.user.email,
        frequency: @frequency,
      })
    end
  end
end