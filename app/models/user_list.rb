class UserList
  include ActiveModel::Model

  attr_accessor :organization_id
  attr_accessor :federal_subject_id
  attr_accessor :viewable_federal_project_ids
  attr_accessor :creatable_federal_project_ids
  attr_accessor :moderatable_federal_project_ids
  attr_accessor :repostable_federal_project_ids
  attr_accessor :users

  validate :all_users_valid

  def initialize(params = {})
    super
    @users ||= []
    @viewable_federal_project_ids ||= []
    @creatable_federal_project_ids ||= []
    @moderatable_federal_project_ids ||= []
    @repostable_federal_project_ids ||= []
  end

  def users_attributes=(users_attributes)
    self.users = users_attributes.map do |key, value|
      User.new(value.merge({
        password: SecureRandom.base58,
        organization_id: organization_id,
        federal_subject_id: federal_subject_id
      }))
    end
  end

  def save!
    users.each do |user|
      User.transaction do
        user.save
        user.update!({
          viewable_federal_project_ids: viewable_federal_project_ids,
          creatable_federal_project_ids: creatable_federal_project_ids,
          moderatable_federal_project_ids: moderatable_federal_project_ids,
          repostable_federal_project_ids: repostable_federal_project_ids
        })
      end
    end
  end

  private
    def all_users_valid
      users.each do |user|
        unless user.valid?
          user.errors.each do |msg|
            errors.add(:users, msg)
          end
        end
      end
    end
end