class Rating::FederalAuthoritiesPeriodPerformances
  attr_reader :period

  def initialize(period)
    @period = period
  end

  def starts_at
    period.begin
  end

  def ends_at
    period.end
  end

  def call
    owned_values_hash.merge(addon_values_hash) do |key, old_value, new_value|
      Rating::FederalAuthorityPerformance.new({
        owned_value: old_value.owned_value + new_value.owned_value,
        addons: old_value.addons + new_value.addons,
      })
    end
  end

  def owned_values_hash
    PressRelease
      .approved
      .joins({ user: :organization }, :press_release_value)
      .where({
        published_at: starts_at..ends_at,
        organizations: {
          kind: Organization.kinds[:federal_authority],
          rated: true,
        }
      })
      .group("organizations.id")
      .sum("press_release_values.value")
      .map { |organization_id, owned_value|
        [organization_id, Rating::FederalAuthorityPerformance.new(owned_value: owned_value, addons: [])]
      }
      .to_h
  end

  def addon_values_hash
    PressRelease
      .approved
      .joins({ user: :organization }, :press_release_value)
      .where(published_at: starts_at..ends_at)
      .where.not(organizations: { rating_addon_organization_id: nil })
      .group("organizations.rating_addon_organization_id")
      .group("organizations.id")
      .sum("press_release_values.value")
      .map { |(rating_addon_organization_id, organization_id), value|
        [rating_addon_organization_id, Rating::FederalAuthorityPerformance.new(owned_value: 0.0, addons:  [[organization_id, value]])]
      }
      .to_h
  end
end