class Rating::FederalAuthoritiesRating
  attr_reader :current_period
  attr_reader :prev_period

  def initialize(current_period, prev_period)
    @current_period = current_period
    @prev_period = prev_period
  end

  def call
    rows = Organization
      .federal_authority
      .where(rated: true)
      .map { |organization|
      RegionalAuthoritiesMonthRating::FederalSubjectRow.new(
        name: generate_name(organization, current_values_hash[organization.id]&.addons || []),
        current_count: current_values_hash[organization.id]&.value || 0.0,
        prev_count: prev_values_hash[organization.id]&.value || 0.0,
      )
    }.sort_by(&:current_count).reverse

    uniq_values = rows.map(&:current_count).uniq

    other_values_splitted = uniq_values.size > 10 ? uniq_values[10..-2].in_groups(3, fill_with: false) : []

    rows.each do |row|
      rank = uniq_values.index(row.current_count)
      row.rank = rank

      row.partition = if row.current_count > 0 && rank < 10
        0
      elsif rank == uniq_values.length - 1
        4
      else
        1 + other_values_splitted.index { |group| row.current_count.in?(group) }
      end
    end
    rows
  end

  def current_values_hash
    @current_values_hash ||= Rating::FederalAuthoritiesPeriodPerformances.new(current_period).call
  end

  def prev_values_hash
    @prev_values_hash ||= Rating::FederalAuthoritiesPeriodPerformances.new(prev_period).call
  end

  def addon_organizations_hash
    @organizations_hash ||= Organization
      .where(id: current_values_hash.values.map(&:addon_organization_ids).flatten.to_set)
      .map { |organization| [organization.id, organization] }
      .to_h
  end

  def generate_name(organization, addons)
    name = organization.name

    if organization.id == 3
      name << ',<br>в том числе: вузы и научные организации'
    elsif addons.any?
      name << ',<br>в том числе:<br>'
      name << addons.map do |(addon_organization_id, value)|
        "#{addon_organizations_hash.fetch(addon_organization_id).name} – #{value}"
      end.join('<br>')
    end
    name
  end
end
