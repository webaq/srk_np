class Rating::FederalSubjectValuesCalculator
  REGIONAL_MEDIA_REPOST_VALUE = 0.5
  FEDERAL_MEDIA_REPOST_VALUE = 1
  AWARD_VALUE = 0.4
  IMPRESSION_VALUE_RANGE = 0.01..01
  ACTIVITY_VALUE_RANGE = 0.01..0.85

  attr_reader :federal_subject_id
  attr_reader :quality
  attr_reader :starts_at
  attr_reader :ends_at

  def initialize(params)
    @federal_subject_id = params.fetch(:federal_subject_id).to_i if params.fetch(:federal_subject_id)
    @quality = params.fetch(:quality)
    @starts_at = params.fetch(:starts_at)
    @ends_at = params.fetch(:ends_at)
  end

  def press_releases_count
    press_releases.size
  end

  def press_releases
    @press_releases ||= PressRelease
    .approved
    .joins(user: { organization: :federal_subject })
    .includes(:press_release_value, :return_to_author_activities, :impressions, reposts: { user: { organization: :federal_subject }})
    .where({
      federal_subjects: {
        id: federal_subject_id,
      },
      published_at: starts_at..ends_at,
      organizations: {
        kind: [
          Organization.kinds[:regional_authority],
          Organization.kinds[:city_authority],
          Organization.kinds[:other]
        ],
      }
    })
    .to_a
  end

  def press_releases_value
    press_releases.map do |press_release|
      press_release.press_release_value&.value || 0.0
    end.sum.round(3)
  end

  def press_releases_value_with_nullified
    press_releases.map do |press_release|
      if press_release.value_nullified?
        0.0
      else
        press_release.press_release_value&.value || 0.0
      end
    end.sum.round(3)
  end

  def value_with_returns
    press_releases.map do |press_release|
      v = if press_release.value_nullified?
        0.0
      else
        press_release.press_release_value&.value || 0.0
      end

      v -= press_release.return_to_author_activities.size * 0.001

      [0.0, v].max
    end.sum.round(3)
  end

  def regional_media_links_count
    press_releases.map do |press_release|
      press_release.reposts.filter { |repost| repost.user.organization&.media? && repost.user.organization.federal_subject }.size
    end.sum
  end

  def federal_media_links_count
    press_releases.map do |press_release|
      press_release.reposts.filter { |repost| repost.user.organization.media? && repost.user.organization.federal_subject.nil? }.size
    end.sum
  end

  def awarded_count
    press_releases.count(&:awarded?)
  end

  def likeness
    press_releases.map do |press_release|
      press_release.press_release_likes_count * 0.05 +
      press_release.impressions.sum { |impression| IMPRESSION_VALUE_RANGE.begin + (IMPRESSION_VALUE_RANGE.end - IMPRESSION_VALUE_RANGE.begin) * impression.score_rate }
    end.sum.round(3)
  end

  def activity_rate
    return 0.0 if activities_hash.values.sum.to_f == 0
    (activities_hash.fetch(federal_subject_id, 0) / activities_hash.values.sum.to_f).round(3)
  end

  def activities_hash
    return @activities_hash if defined? @activities_hash

    impressions_hash = Impression
      .joins(user: :organization)
      .where({
        created_at: starts_at..ends_at,
        organizations: {
          kind: [
            Organization.kinds[:regional_authority],
            Organization.kinds[:city_authority],
            Organization.kinds[:other]
          ],
        }
      })
      .where.not(organizations: { federal_subject_id: nil })
      .group("organizations.federal_subject_id")
      .count

    likes_hash = PressReleaseLike
      .joins(user: :organization)
      .where({
        created_at: starts_at..ends_at,
        organizations: {
          kind: [
            Organization.kinds[:regional_authority],
            Organization.kinds[:city_authority],
            Organization.kinds[:other]
          ],
        }
      })
      .where.not(organizations: { federal_subject_id: nil })
      .group("organizations.federal_subject_id")
      .count

    @activities_hash = impressions_hash.merge(likes_hash) do |key, oldval, newval|
      oldval + newval
    end
  end

  def total_value
    return nil unless quality.present?
    (
      value_with_returns * quality.to_f * 0.01 +
      regional_media_links_count * REGIONAL_MEDIA_REPOST_VALUE +
      federal_media_links_count * FEDERAL_MEDIA_REPOST_VALUE +
      awarded_count * AWARD_VALUE +
      likeness +
      ACTIVITY_VALUE_RANGE.begin + (ACTIVITY_VALUE_RANGE.end - ACTIVITY_VALUE_RANGE.begin) * activity_rate
    ).round(3)
  end
end