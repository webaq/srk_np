class Rating::FederalSubjectsRating
  attr_reader :current_period
  attr_reader :prev_period

  def initialize(current_period, prev_period)
    @current_period = current_period
    @prev_period = prev_period
  end

  def call
    rows = FederalSubject.all.map do |federal_subject|
      RegionalAuthoritiesMonthRating::FederalSubjectRow.new(
        name: federal_subject.name,
        current_count: current_values_hash[federal_subject.id] || 0.0,
        prev_count: prev_values_hash[federal_subject.id] || 0.0,
      )
    end.sort_by { |federal_subject_row|
      [-federal_subject_row.current_count, federal_subject_row.name]
    }

    uniq_values = rows.map(&:current_count).uniq

    other_values_splitted = uniq_values.size > 10 ? uniq_values[10..-2].in_groups(3, fill_with: false) : []

    rows.each do |row|
      rank = uniq_values.index(row.current_count)
      row.rank = rank

      row.partition = if row.current_count > 0 && rank < 10
        0
      elsif rank == uniq_values.length - 1
        4
      else
        1 + other_values_splitted.index { |group| row.current_count.in?(group) }
      end
    end
    rows
  end

  def current_values_hash
    @current_values_hash ||= Rating::FederalSubjectsPeriodPerformances.new(current_period).call
  end

  def prev_values_hash
    @prev_values_hash ||= Rating::FederalSubjectsPeriodPerformances.new(prev_period).call
  end
end