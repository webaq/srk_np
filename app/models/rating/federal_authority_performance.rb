Rating::FederalAuthorityPerformance = Struct.new(:owned_value, :addons, keyword_init: true) do
  def addon_organization_ids
    addons.map(&:first)
  end

  def value
    owned_value + addons.map(&:last).sum
  end
end