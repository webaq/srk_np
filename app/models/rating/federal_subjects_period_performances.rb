class Rating::FederalSubjectsPeriodPerformances
  attr_reader :period

  def initialize(period)
    @period = period
  end

  def starts_at
    period.begin
  end

  def ends_at
    period.end
  end

  def call
    PressRelease
      .approved
      .joins({ user: { organization: :federal_subject }}, :press_release_value)
      .where({
        published_at: starts_at..ends_at,
        organizations: {
          kind: [
            Organization.kinds[:regional_authority],
            Organization.kinds[:city_authority],
            Organization.kinds[:other]
          ],
        }
      })
      .group("federal_subjects.id")
      .sum("press_release_values.value")
      .map { |federal_subject_id, value|
        [federal_subject_id, value]
      }
      .to_h
  end
end