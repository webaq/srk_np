class NationalProject < ApplicationRecord
  validates :name, presence: true

  has_many :federal_projects, dependent: :restrict_with_error
  has_many :branding_assets, dependent: :restrict_with_error
  has_many :message_national_projects, dependent: :restrict_with_error
  has_many :press_release_national_projects, dependent: :restrict_with_error
  has_many :press_releases, through: :press_release_national_projects
  has_many :placement_national_projects, dependent: :destroy
  has_many :placements, through: :placement_national_projects
  has_many :main_topic_national_projects, dependent: :destroy
  has_many :main_topics, through: :main_topic_national_projects
  has_many :national_project_tags, dependent: :destroy
  has_many :subscription_national_projects, dependent: :destroy

  has_one_attached :avatar
  has_rich_text :content
  has_rich_text :branding

  def color
    case short_name
    when 'Здравоохранение'; '#e56b91'
    when 'Дороги'; '#deae3c'
    when 'Демография'; '#00accd'
    when 'Экология'; '#92c36c'
    when 'Образование'; '#00b0f0'
    when 'Жильё и ГС'; '#47b070'
    when 'Культура'; '#978bc0'
    when 'Предпринимательство '; '#b7b7b7'
    when 'Производительность труда'; '#ed7c02'
    when 'Цифровая экономика'; '#95b4db'
    when 'Экспорт'; '#8497b0'
    when 'Инфраструктура'; '#b8787a'
    when 'Наука'; '#3d9dc8'
    when 'Год науки и технологий'; '#286CB8'
    end
  end

  def self.ransackable_attributes(auth_object = nil)
    %w[ id short_name ]
  end
end
