class Permission < ApplicationRecord
  belongs_to :user
  belongs_to :federal_project
end
