class MediaDigest
  attr_reader :user

  def initialize(user)
    @user = user
  end

  def press_releases
    @press_releases ||= PressReleasePolicy::Scope.new(user, user.subscribed_press_releases).resolve
      .where(press_releases: { published_at: 24.hours.ago..Time.current })
      .includes(national_projects: { avatar_attachment: :blob })
      .with_rich_text_content_and_embeds
      .order(published_at: 'DESC')
  end

  def press_releases_count
    @press_releases_count ||= press_releases.count
  end

  def limited_press_releases
    press_releases.limit(5)
  end
end