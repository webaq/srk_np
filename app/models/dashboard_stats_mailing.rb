class DashboardStatsMailing
  attr_reader :emails

  def initialize(emails = SystemSubscriber.dashboard_stats.pluck(:email))
    @emails = emails
  end

  def send_daily
    starts_on = Date.current - 2.days
    ends_on = starts_on
    starts2_on = Date.current.prev_day
    ends2_on = starts2_on

    dashboard ||= Dashboard.new({
      starts_on: starts_on,
      ends_on: ends_on,
      starts2_on: starts2_on,
      ends2_on: ends2_on,
      federal_subject: nil,
    })

    subject = 'Ежедневная статистика'

    emails.each do |email|
      UserMailer.with({
        email: email,
        subject: subject,
        dashboard: dashboard,
      }).dashboard_stats.deliver
    end
  end

  def send_weekly
    starts_on = Date.current.beginning_of_week - 14.days
    ends_on = starts_on.end_of_week
    starts2_on = Date.current.beginning_of_week - 7.days
    ends2_on = starts2_on.end_of_week

    dashboard ||= Dashboard.new({
      starts_on: starts_on,
      ends_on: ends_on,
      starts2_on: starts2_on,
      ends2_on: ends2_on,
      federal_subject: nil,
    })

    subject = 'Еженедельная статистика'

    emails.each do |email|
      UserMailer.with({
        email: email,
        subject: subject,
        dashboard: dashboard,
      }).dashboard_stats.deliver
    end
  end
end