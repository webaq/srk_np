class Category < ApplicationRecord
  acts_as_list

  has_many :subcategories, ->{ order(:position) }, dependent: :restrict_with_error
end
