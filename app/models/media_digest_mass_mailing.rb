class MediaDigestMassMailing
  MAILING_LOCAL_HOUR = 9

  def call
    users.each do |user|
      media_digest = MediaDigest.new(user)

      if media_digest.press_releases_count.positive?
        NotifierMailer.media_digest_mail(
          user.email,
          media_digest.press_releases_count,
          media_digest.limited_press_releases
        ).deliver
      end
    end
  end

  def users
    User
      .where(id:
        User.where(id: Organization.media.joins(:users).select('users.id'))
        .or(User.where(subscribed_to_media_digest: true))
      )
      .by_current_local_hour(MAILING_LOCAL_HOUR)
  end
end