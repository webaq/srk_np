class BestPractice < ApplicationRecord
  include PublicActivity::Common

  belongs_to :user

  has_rich_text :content
  has_one_attached :image
  has_many_attached :images

  before_validation :set_published_at, unless: :published_at?

  validates :name, presence: true
  validates :content, presence: true
  validates :image, presence: true
  validates :filename, presence: true
  validate :acceptable_image

  scope :approved, -> { where.not(approved_at: nil) }
  scope :published, -> { where('published_at < ?', Time.current) }

  def acceptable_image
    return unless image.attached?

    acceptable_types = ["application/pdf"]
    unless acceptable_types.include?(image.content_type)
      errors.add(:image, "должен быть в формате PDF")
    end
  end

  def draft?
    approved_at.nil?
  end

  def set_published_at
    self.published_at = Time.current
  end
end
