class Bookmark < ApplicationRecord
  belongs_to :user
  belongs_to :press_release, counter_cache: true

  counter_culture [:user, :organization]
end
