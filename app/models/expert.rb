class Expert < ApplicationRecord
  belongs_to :press_release, required: false
  acts_as_list scope: :press_release
end
