class MailingList < ApplicationRecord
  belongs_to :organization

  has_many :mailing_list_items, dependent: :destroy
  accepts_nested_attributes_for :mailing_list_items, reject_if: :all_blank, allow_destroy: true
end
