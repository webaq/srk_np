class UserGroupAbility < ApplicationRecord
  enum ability: {
    edit_user_groups: 1,
    show_video_clips: 2,
    create_placements: 3,
    show_rating: 4,
    show_help_telegram: 5,
    show_mainpage_banners: 6,
    show_mainpage_aside: 7,
    show_returned_press_releases: 8,
    show_my_press_releases: 9,
    show_media_works_press_releases: 10,
    show_main_topics_slider: 11,
    edit_national_projects: 12,
    edit_main_topics: 13,
    show_archived_main_topics: 14,
    edit_best_practices: 15,
    edit_complaints: 16,
    edit_mainpage_banners: 17,
    show_other_regions_press_releases: 18,
    export_press_releases: 19,
    edit_notification_popups: 20,
  }

  belongs_to :user_group, counter_cache: true

  validates :ability, presence: true
end
