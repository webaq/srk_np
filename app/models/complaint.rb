class Complaint < ApplicationRecord
  belongs_to :user, ->{ with_discarded }
  belongs_to :press_release, ->{ with_discarded }

  validates :body, presence: true
end
