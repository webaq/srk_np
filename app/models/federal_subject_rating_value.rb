class FederalSubjectRatingValue < ApplicationRecord
  belongs_to :federal_subject
  belongs_to :rating_month

  validates :federal_subject, uniqueness: { scope: :rating_month }
  validates :press_releases_count, presence: true
  validates :press_releases_value, presence: true
  validates :press_releases_value_with_nullified, presence: true
  validates :value_with_returns, presence: true
  validates :quality, presence: true

  validates :regional_media_links_count, presence: true
  validates :federal_media_links_count, presence: true
  validates :awarded_count, presence: true
  validates :likeness, presence: true
  validates :activity_rate, presence: true

  validates :total_value, presence: true
end
