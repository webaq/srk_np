class SubscriptionMessage < ApplicationRecord
  belongs_to :user, required: false

  enum frequency: {
    daily: 1,
    weekly: 2,
  }

  scope :clicked, ->{ where.not(clicked_at: nil) }
  scope :delivered, ->{ where.not(delivered_at: nil) }
  scope :opened, ->{ where.not(opened_at: nil) }
  scope :spamed, ->{ where.not(spamed_at: nil) }
  scope :undelivered, ->{ where.not(undelivered_at: nil) }
  scope :unsubscribed, ->{ where.not(unsubscribed_at: nil) }
end
