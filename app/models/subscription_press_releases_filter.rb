class SubscriptionPressReleasesFilter
  attr_reader :starts_at
  attr_reader :ends_at

  delegate :user, :federal_subject_ids, :organization_ids, :national_project_ids, :tag_ids, to: :@subscription

  def initialize(subscription, frequency)
    @subscription = subscription
    set_period(frequency)
  end

  def call
    p = Pundit.policy_scope!(user, PressRelease)
    p = p.where(published_at: (starts_at..ends_at))
    p = p.where(id: PressRelease.joins(:federal_subjects).where(federal_subjects: { id: federal_subject_ids })) if federal_subject_ids.any?
    p = p.joins(:user).where(users: { organization_id: organization_ids }) if organization_ids.any?
    p = p.where(id: PressRelease.joins(:national_projects).where(national_projects: { id: national_project_ids })) if national_project_ids.any?
    p = p.where(id: PressRelease.joins(:tags).where(tags: { id: tag_ids })) if tag_ids.any?
    p.to_a
  end

  def set_period(frequency)
    case frequency.to_sym
    when :daily
      @starts_at = 1.day.ago.beginning_of_day
      @ends_at = Time.current.beginning_of_day
    when :weekly
      @starts_at = 1.week.ago.beginning_of_week.beginning_of_day
      @ends_at = Time.current.beginning_of_week.beginning_of_day
    end
  end
end