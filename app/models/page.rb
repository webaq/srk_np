class Page < ApplicationRecord
  has_rich_text :content

  validates :slug, presence: true, uniqueness: true
  validates :name, presence: true

  def to_param; slug; end

  def embedded_pdf_attachment
    content.embeds_attachments.find { |attachment| attachment.content_type == 'application/pdf' }
  end
end
