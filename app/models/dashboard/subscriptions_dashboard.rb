class Dashboard::SubscriptionsDashboard
  include ActiveModel::Model
  include ActiveModel::Attributes

  attribute :starts_at, :datetime
  attribute :ends_at, :datetime
  attribute :starts2_at, :datetime
  attribute :ends2_at, :datetime

  def period1_subscriptions
    Subscription.where(created_at: starts_at..ends_at)
  end

  def period2_subscriptions
    Subscription.where(created_at: starts2_at..ends2_at)
  end

  def period1_messages
    SubscriptionMessage.where(created_at: starts_at..ends_at)
  end

  def period2_messages
    SubscriptionMessage.where(created_at: starts2_at..ends2_at)
  end

  def total_messages
    SubscriptionMessage.all
  end


  def top_subscribing_federal_subjects
    FederalSubject
      .joins(organizations: { users: :subscription })
      .group(:id)
      .select(:name)
      .select("COUNT(subscriptions.id) AS _subscriptions_count")
      .order("_subscriptions_count DESC")
      .order("name ASC")
      .limit(10)
  end

  def top_subscribed_federal_subjects
    FederalSubject
      .joins(:subscription_federal_subjects)
      .group(:id)
      .select(:name)
      .select("COUNT(subscription_federal_subjects.id) AS _subscriptions_count")
      .order("_subscriptions_count DESC")
      .order("name ASC")
      .limit(10)
  end

  def national_projects
    NationalProject
      .joins(:subscription_national_projects)
      .group(:id)
      .select(:name)
      .select("COUNT(subscription_national_projects.id) AS _subscriptions_count")
      .order("_subscriptions_count DESC")
      .order("name ASC")
  end
end