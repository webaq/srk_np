class Dashboard::NationalProjectsList
  include ActiveModel::Model
  include ActiveModel::Attributes

  NationalProjectRow = Struct.new(
    :name,
    :total,
    :period1,
    :period2,
    :period3,
    :period4,
    keyword_init: true
  )

  attribute :starts_at, :datetime
  attribute :ends_at, :datetime
  attribute :starts2_at, :datetime
  attribute :ends2_at, :datetime
  attribute :federal_subject_id, :integer

  attr_accessor :sort

  def initialize(params = {})
    super
    @sort = params.fetch(:sort, { 'total' => 'desc' })
  end

  def starts3_at
    starts_at - 1.year
  end

  def ends3_at
    ends_at - 1.year
  end

  def rows
    rows = []

    national_projects_total_counts = NationalProject
      .joins(:press_releases)
      .where(press_releases: { id: press_releases })
      .group(:id)
      .count

    national_projects_period1_counts = NationalProject
      .joins(:press_releases)
      .where(press_releases: { id: press_releases, created_at: starts_at..ends_at })
      .group(:id)
      .count

    national_projects_period2_counts = NationalProject
      .joins(:press_releases)
      .where(press_releases: { id: press_releases, created_at: starts2_at..ends2_at })
      .group(:id)
      .count

    national_projects_period3_counts = NationalProject
      .joins(:press_releases)
      .where(press_releases: { id: press_releases, created_at: starts3_at..ends3_at })
      .group(:id)
      .count

    national_projects_period4_counts = NationalProject
      .joins(:press_releases)
      .where(press_releases: { id: press_releases, created_at: Time.current.beginning_of_year... })
      .group(:id)
      .count

    NationalProject.all.map do |national_project|
      rows << NationalProjectRow.new(name: national_project.short_name, total: national_projects_total_counts.fetch(national_project.id, 0), period1: national_projects_period1_counts.fetch(national_project.id, 0), period2: national_projects_period2_counts.fetch(national_project.id, 0), period3: national_projects_period3_counts.fetch(national_project.id, 0), period4: national_projects_period4_counts.fetch(national_project.id, 0))
    end

    # byebug
    rows.sort_by! do |row|
      [row.public_send(sort.keys.first), row.name]
    end

    rows.reverse! if sort.values.first == 'desc'

    row = NationalProjectRow.new(name: 'Всего инфоповодов')
    row.total = press_releases.count
    row.period1 = press_releases.where(created_at: starts_at..ends_at).count
    row.period2 = press_releases.where(created_at: starts2_at..ends2_at).count
    row.period3 = press_releases.where(created_at: starts3_at..ends3_at).count
    row.period4 = press_releases.where(created_at: Time.current...).count
    rows.prepend(row)

    rows
  end

  def press_releases
    return @press_releases if defined? @press_releases
    @press_releases = PressRelease.all
    @press_releases = @press_releases.where(id: PressRelease.joins(:federal_subjects).where(federal_subjects: { id: federal_subject_id })) if federal_subject_id
    @press_releases
  end

  def params
    {
      federal_subject_id: federal_subject_id,
      starts_at: starts_at,
      ends_at: ends_at,
      starts2_at: starts2_at,
      ends2_at: ends2_at,
    }
  end
end