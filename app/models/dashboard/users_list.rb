class Dashboard::UsersList
  attr_reader :starts_at
  attr_reader :ends_at
  attr_reader :starts2_at
  attr_reader :ends2_at
  attr_reader :federal_subject_id

  def initialize(params = {})
    @starts_at = params.fetch(:starts_at)
    @ends_at = params.fetch(:ends_at)
    @starts2_at = params.fetch(:starts2_at)
    @ends2_at = params.fetch(:ends2_at)
    @federal_subject_id = params.fetch(:federal_subject_id, nil)
  end

  def starts3_at
    starts_at - 1.year
  end

  def ends3_at
    ends_at - 1.year
  end

  def total_counts_hash
    @total_counts_hash ||= users
      .group(group_param)
      .count
  end

  def period1_counts_hash
    @period1_counts_hash ||= users
      .where(created_at: starts_at..ends_at)
      .group(group_param)
      .count
  end

  def period2_counts_hash
    @period2_counts_hash ||= users
      .where(created_at: starts2_at..ends2_at)
      .group(group_param)
      .count
  end

  def period3_counts_hash
    @period3_counts_hash ||= users
      .where(created_at: starts3_at..ends3_at)
      .group(group_param)
      .count
  end

  def period4_counts_hash
    @period4_counts_hash ||= users
      .where(created_at: Time.current.beginning_of_year...)
      .group(group_param)
      .count
  end

  def upto_period1_counts_hash
    @upto_period1_counts_hash ||= users
      .where('users.created_at < ?', ends_at)
      .group(group_param)
      .count
  end

  def upto_period2_counts_hash
    @upto_period2_counts_hash ||= users
      .where('users.created_at < ?', ends2_at)
      .group(group_param)
      .count
  end

  def upto_period3_counts_hash
    @upto_period3_counts_hash ||= users
      .where('users.created_at < ?', ends3_at)
      .group(group_param)
      .count
  end

  def confirmed_total_counts_hash
    @confirmed_total_counts_hash ||= users
      .where.not(confirmed_at: nil)
      .group(group_param)
      .count
  end

  def confirmed_period1_counts_hash
    @confirmed_period1_counts_hash ||= users
      .where(confirmed_at: starts_at..ends_at)
      .group(group_param)
      .count
  end

  def confirmed_period2_counts_hash
    @confirmed_period2_counts_hash ||= users
      .where(confirmed_at: starts2_at..ends2_at)
      .group(group_param)
      .count
  end

  def confirmed_period3_counts_hash
    @confirmed_period3_counts_hash ||= users
      .where(confirmed_at: starts3_at..ends3_at)
      .group(group_param)
      .count
  end

  def confirmed_period4_counts_hash
    @confirmed_period4_counts_hash ||= users
      .where(confirmed_at: Time.current.beginning_of_year...)
      .group(group_param)
      .count
  end

  def visited_period1_counts_hash
    @visited_period1_counts_hash ||= users
      .joins(:visits)
      .where(visits: { created_at: starts_at..ends_at })
      .distinct
      .group(group_param)
      .count
  end

  def visited_period2_counts_hash
    @visited_period2_counts_hash ||= users
      .joins(:visits)
      .where(visits: { created_at: starts2_at..ends2_at })
      .distinct
      .group(group_param)
      .count
  end

  def visited_period3_counts_hash
    @visited_period3_counts_hash ||= users
      .joins(:visits)
      .where(visits: { created_at: starts3_at..ends3_at })
      .distinct
      .group(group_param)
      .count
  end

  def visited_period4_counts_hash
    @visited_period4_counts_hash ||= users
      .joins(:visits)
      .where(visits: { created_at: Time.current.beginning_of_year... })
      .distinct
      .group(group_param)
      .count
  end

  def users
    return @users if defined? @users
    @users = User.joins(:organization)
    @users = @users.where(organizations: { federal_subject_id: federal_subject_id }) if federal_subject_id
    @users
  end

  def group_param
    #return :kind
    "
    CASE
      WHEN organizations.kind = #{ Organization.kinds[:media] }
        AND organizations.federal_subject_id IS NULL THEN 98
      WHEN organizations.kind = #{ Organization.kinds[:media] }
        AND organizations.federal_subject_id IS NOT NULL THEN 99
      ELSE organizations.kind
    END
    "
  end

  def custom_kind(kind)
    return 98 if kind.to_s == 'media_federal'
    return 99 if kind.to_s == 'media_regional'
    Organization.kinds[kind]
  end
end