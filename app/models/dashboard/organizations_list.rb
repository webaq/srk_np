class Dashboard::OrganizationsList
  attr_reader :starts_at
  attr_reader :ends_at
  attr_reader :starts2_at
  attr_reader :ends2_at
  attr_reader :federal_subject_id

  def initialize(params = {})
    @starts_at = params.fetch(:starts_at)
    @ends_at = params.fetch(:ends_at)
    @starts2_at = params.fetch(:starts2_at)
    @ends2_at = params.fetch(:ends2_at)
    @federal_subject_id = params.fetch(:federal_subject_id, nil)
  end

  def starts3_at
    starts_at - 1.year
  end

  def ends3_at
    ends_at - 1.year
  end

  def total_counts_hash
    @total_counts_hash ||= organizations
      .group(group_param)
      .count
  end

  def period1_counts_hash
    @period1_counts_hash ||= organizations
      .where(created_at: starts_at..ends_at)
      .group(group_param)
      .count
  end

  def period2_counts_hash
    @period2_counts_hash ||= organizations
      .where(created_at: starts2_at..ends2_at)
      .group(group_param)
      .count
  end

  def period3_counts_hash
    @period3_counts_hash ||= organizations
      .where(created_at: starts3_at..ends3_at)
      .group(group_param)
      .count
  end

  def period4_counts_hash
    @period4_counts_hash ||= organizations
      .where(created_at: Time.current.beginning_of_year...)
      .group(group_param)
      .count
  end

  def upto_period1_counts_hash
    @upto_period1_counts_hash ||= organizations
      .where('created_at < ?', ends_at)
      .group(group_param)
      .count
  end

  def upto_period2_counts_hash
    @upto_period2_counts_hash ||= organizations
      .where('created_at < ?', ends2_at)
      .group(group_param)
      .count
  end

  def upto_period3_counts_hash
    @upto_period3_counts_hash ||= organizations
      .where('created_at < ?', ends3_at)
      .group(group_param)
      .count
  end

  def upto_period4_counts_hash
    @upto_period4_counts_hash ||= organizations
      .group(group_param)
      .count
  end

  def confirmed_total_counts_hash
    @confirmed_total_counts_hash ||= organizations
      .joins(:users)
      .where.not(users: { confirmed_at: nil })
      .distinct
      .group(group_param)
      .count
  end

  def confirmed_period1_counts_hash
    @confirmed_period1_counts_hash ||= organizations
      .joins(:users)
      .where(users: { confirmed_at: starts_at..ends_at })
      .distinct
      .group(group_param)
      .count
  end

  def confirmed_period2_counts_hash
    @confirmed_period2_counts_hash ||= organizations
      .joins(:users)
      .where(users: { confirmed_at: starts2_at..ends2_at })
      .distinct
      .group(group_param)
      .count
  end

  def confirmed_period3_counts_hash
    @confirmed_period3_counts_hash ||= organizations
      .joins(:users)
      .where(users: { confirmed_at: starts3_at..ends3_at })
      .distinct
      .group(group_param)
      .count
  end

  def confirmed_period4_counts_hash
    @confirmed_period4_counts_hash ||= organizations
      .joins(:users)
      .where(users: { confirmed_at: Time.current.beginning_of_year... })
      .distinct
      .group(group_param)
      .count
  end

  def visited_period1_counts_hash
    @visited_period1_counts_hash ||= organizations
      .joins(users: :visits)
      .where(visits: { created_at: starts_at..ends_at })
      .distinct
      .group(group_param)
      .count
  end

  def visited_period2_counts_hash
    @visited_period2_counts_hash ||= organizations
      .joins(users: :visits)
      .where(visits: { created_at: starts2_at..ends2_at })
      .distinct
      .group(group_param)
      .count
  end

  def visited_period3_counts_hash
    @visited_period3_counts_hash ||= organizations
      .joins(users: :visits)
      .where(visits: { created_at: starts3_at..ends3_at })
      .distinct
      .group(group_param)
      .count
  end

  def visited_period4_counts_hash
    @visited_period4_counts_hash ||= organizations
      .joins(users: :visits)
      .where(visits: { created_at: Time.current.beginning_of_year... })
      .distinct
      .group(group_param)
      .count
  end

  def published_total_counts_hash
    @published_total_counts_hash ||= organizations
      .where(id:
        PressRelease
          .joins(:user)
          .distinct
          .select('users.organization_id')
      )
      .group(group_param)
      .count
  end

  def published_period1_counts_hash
    @published_period1_counts_hash ||= organizations
      .where(id:
        PressRelease
          .where(created_at: starts_at..ends_at)
          .joins(:user)
          .distinct
          .select('users.organization_id')
      )
      .group(group_param)
      .count
  end

  def published_period2_counts_hash
    @published_period2_counts_hash ||= organizations
      .where(id:
        PressRelease
          .where(created_at: starts2_at..ends2_at)
          .joins(:user)
          .distinct
          .select('users.organization_id')
      )
      .group(group_param)
      .count
  end

  def published_period3_counts_hash
    @published_period3_counts_hash ||= organizations
      .where(id:
        PressRelease
          .where(created_at: starts3_at..ends3_at)
          .joins(:user)
          .distinct
          .select('users.organization_id')
      )
      .group(group_param)
      .count
  end

  def published_period4_counts_hash
    @published_period3_counts_hash ||= organizations
      .where(id:
        PressRelease
          .where(created_at: Time.current.at_beginning_of_year...)
          .joins(:user)
          .distinct
          .select('users.organization_id')
      )
      .group(group_param)
      .count
  end

  def shown_total_counts_hash
    @shown_total_counts_hash ||= organizations
      .where(id:
        User
          .joins(:show_activities)
          .distinct
          .select('users.organization_id')
      )
      .group(group_param)
      .count
  end

  def shown_period1_counts_hash
    @shown_period1_counts_hash ||= organizations
      .where(id:
        User
          .joins(:show_activities)
          .where(activities: { created_at: starts_at..ends_at })
          .distinct
          .select('users.organization_id')
      )
      .group(group_param)
      .count
  end

  def shown_period2_counts_hash
    @shown_period2_counts_hash ||= organizations
      .where(id:
        User
          .joins(:show_activities)
          .where(activities: { created_at: starts2_at..ends2_at })
          .distinct
          .select('users.organization_id')
      )
      .group(group_param)
      .count
  end

  def shown_period3_counts_hash
    @shown_period3_counts_hash ||= organizations
      .where(id:
        User
          .joins(:show_activities)
          .where(activities: { created_at: starts3_at..ends3_at })
          .distinct
          .select('users.organization_id')
      )
      .group(group_param)
      .count
  end

  def shown_period4_counts_hash
    @shown_period4_counts_hash ||= organizations
      .where(id:
        User
          .joins(:show_activities)
          .where(activities: { created_at: Time.current.beginning_of_year... })
          .distinct
          .select('users.organization_id')
      )
      .group(group_param)
      .count
  end

  def reposted_total_counts_hash
    @reposted_total_counts_hash ||= organizations
      .where(id:
        User
          .joins(:reposts)
          .distinct
          .select('users.organization_id')
      )
      .group(group_param)
      .count
  end

  def reposted_period1_counts_hash
    @reposted_period1_counts_hash ||= organizations
      .where(id:
        User
          .joins(:reposts)
          .where(reposts: { created_at: starts_at..ends_at })
          .distinct
          .select('users.organization_id')
      )
      .group(group_param)
      .count
  end

  def reposted_period2_counts_hash
    @reposted_period2_counts_hash ||= organizations
      .where(id:
        User
          .joins(:reposts)
          .where(reposts: { created_at: starts2_at..ends2_at })
          .distinct
          .select('users.organization_id')
      )
      .group(group_param)
      .count
  end

  def reposted_period3_counts_hash
    @reposted_period3_counts_hash ||= organizations
      .where(id:
        User
          .joins(:reposts)
          .where(reposts: { created_at: starts3_at..ends3_at })
          .distinct
          .select('users.organization_id')
      )
      .group(group_param)
      .count
  end

  def reposted_period4_counts_hash
    @reposted_period4_counts_hash ||= organizations
      .where(id:
        User
          .joins(:reposts)
          .where(reposts: { created_at: Time.current.beginning_of_year... })
          .distinct
          .select('users.organization_id')
      )
      .group(group_param)
      .count
  end

  def unshown_total_count(custom_kind)
    total_counts_hash.fetch(custom_kind, 0) - shown_total_counts_hash.fetch(custom_kind, 0)
  end

  def unshown_period1_count(custom_kind)
    upto_period1_counts_hash.fetch(custom_kind, 0) - shown_period1_counts_hash.fetch(custom_kind, 0)
  end

  def unshown_period2_count(custom_kind)
    upto_period2_counts_hash.fetch(custom_kind, 0) - shown_period2_counts_hash.fetch(custom_kind, 0)
  end

  def unshown_period3_count(custom_kind)
    upto_period3_counts_hash.fetch(custom_kind, 0) - shown_period3_counts_hash.fetch(custom_kind, 0)
  end

  def unshown_period4_count(custom_kind)
    upto_period4_counts_hash.fetch(custom_kind, 0) - shown_period4_counts_hash.fetch(custom_kind, 0)
  end

  def organizations
    return @organizations if defined? @organizations
    @organizations = Organization.all
    @organizations = @organizations.where(federal_subject_id: federal_subject_id) if federal_subject_id
    @organizations
  end

  def group_param
    #return :kind
    "
    CASE
      WHEN organizations.kind = #{ Organization.kinds[:media] }
        AND organizations.federal_subject_id IS NULL THEN 98
      WHEN organizations.kind = #{ Organization.kinds[:media] }
        AND organizations.federal_subject_id IS NOT NULL THEN 99
      ELSE organizations.kind
    END
    "
  end

  def custom_kind(kind)
    return 98 if kind.to_s == 'media_federal'
    return 99 if kind.to_s == 'media_regional'
    Organization.kinds[kind]
  end

  def media?(key)
    key.in? [98, 99]
  end

  # def media_organizations_total_count
  #   @media_organizations_total ||= total_counts_hash.select { |key, value| media?(key) }.values.sum
  # end
  #
  # def media_organizations_upto_period1_count
  #   @media_organizations_upto_period1_count ||= upto_period1_counts_hash.select { |key, value| media?(key) }.values.sum
  # end
  #
  # def media_organizations_upto_period2_count
  #   @media_organizations_upto_period2_count ||= upto_period2_counts_hash.select { |key, value| media?(key) }.values.sum
  # end
end