class SubscriptionTag < ApplicationRecord
  belongs_to :subscription, counter_cache: :tags_count
  belongs_to :tag
end
