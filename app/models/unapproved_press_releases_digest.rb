class UnapprovedPressReleasesDigest
  def call
    NotifierMailer.unapproved_press_releases_mail(emails, press_releases).deliver
  end

  def emails
    UnapprovedPressReleasesDigestSubscriber.pluck(:email)
  end

  def press_releases
    PressRelease.unapproved.includes(user: :organization).order(:created_at)
  end
end