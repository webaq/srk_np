class FaqItem < ApplicationRecord
  acts_as_list

  has_rich_text :answer
end
