class CommentCreatedNotification
  include Rails.application.routes.url_helpers
  include ActionView::Helpers

  attr_reader :comment
  delegate :press_release, to: :comment

  def initialize(comment)
    @comment = comment
  end

  def call
    create_messages
    send_emails
  end

  def create_messages
    message = Message.create!({
      sender: comment.user,
      subject: "Новый комментарий: #{press_release.title}",
      content: "<div>#{comment.text}</div><br><div>#{link_to('Перейти к инфоповоду', press_release_comments_path(press_release))}</div>"
    })

    all_involved_users.reject { |user|
      user == comment.user
    }.each { |user|
      message.message_deliveries.create!({
        user: user
      })
    }
  end

  def send_emails
    all_involved_users.select { |user|
      user != comment.user && user.receive_notifications_by_email
    }.each { |user|
      NotifierMailer.comment_created(press_release, user).deliver_later
    }
  end

  def all_involved_users
    (
      [press_release.user] +
      press_release.comments.map(&:user) +
      optional_regional_moderators
    ).uniq
  end

  def optional_regional_moderators
    if press_release.user.federal_subject
      User.includes(:moderatable_national_projects).where({
        regional_moderator: true,
        federal_subject_id: press_release.user.federal_subject.id,
      }).where(national_projects: { id: press_release.national_project_ids })
    else
      []
    end
  end
end