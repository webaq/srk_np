class UserFederalProjectPermission < ApplicationRecord
  belongs_to :user
  belongs_to :federal_project

  enum permission: { viewable: 1, creatable: 2, moderatable: 3, repostable: 4 }
end
