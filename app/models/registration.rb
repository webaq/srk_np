class Registration < ApplicationRecord
  include Discard::Model

  enum kind: { federal_authority: 1, regional_authority: 2, city_authority: 5, media: 3, other: 4 }

  attr_reader :password
  attr_accessor :password_confirmation

  belongs_to :federal_subject, optional: true
  belongs_to :user, optional: true
  belongs_to :confirmed_user, -> { with_discarded }, optional: true, class_name: 'User'

  validates :kind, presence: true
  validates :federal_subject, presence: true

  validates :organization_name, presence: true
  # validates :organization_name, length: { minimum: 2 }, unless: Proc.new { |r| r.errors[:organization_name].any? }

  validate :email, :email_devisable
  validates_uniqueness_of :email, case_sensitive: false
  validate :password, :password_devisable
  validates :password, confirmation: true

  validates :first_name, presence: true
  validates :first_name, length: { minimum: 2 }, unless: Proc.new { |r| r.errors[:first_name].any? }

  validates :last_name, presence: true
  validates :last_name, length: { minimum: 2 }, unless: Proc.new { |r| r.errors[:last_name].any? }

  validates :contact_position, presence: true
  # validates :contact_position, length: { minimum: 2 }, unless: Proc.new { |r| r.errors[:contact_position].any? }

  validates :contact_phone, presence: true
  validates :contact_phone, format: { with: /([0-9\(\)\/\+ \-]{17})\z/, message: "Введите номер телефона из 11 цифр" }, on: :create, unless: Proc.new { |r| r.errors[:contact_phone].any? }

  validates :website, presence: { message: 'Поле не может быть пустым' }, if: :media_and_new_organization?
  validates :media_subscribers, presence: { message: 'Поле не может быть пустым' }, if: :media_and_new_organization?


  def email=(new_value)
    new_value.strip! if new_value
    super
  end

  def password=(new_password)
    @password = new_password
    self.encrypted_password = password_digest(@password) if @password.present?
  end

  private
    def email_devisable
      probe_user = User.new(email: email)
      probe_user.valid?
      probe_user.errors[:email].each do |error|
        errors.add(:email, error)
      end
    end

    def password_devisable
      probe_user = User.new(password: email)
      probe_user.valid?
      probe_user.errors[:password].each do |error|
        errors.add(:password, error)
      end
    end

    def media_and_new_organization?
      kind == 'media' && !Organization.find_by(name: organization_name)
    end

    def password_digest(password)
      Devise::Encryptor.digest(User, password)
    end
end
