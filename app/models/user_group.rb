class UserGroup < ApplicationRecord
  has_many :user_group_abilities, dependent: :destroy
end
