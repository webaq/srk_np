class RatingMonth < ApplicationRecord
  validates :month, uniqueness: true

  has_many :federal_subject_rating_values, dependent: :restrict_with_error

  def name
    "#{I18n.t("months.#{month.strftime('%B')}")} #{month.year}"
  end

  def last_month?
    RatingMonth.order(:month).last == self
  end

  def starts_at
    return Time.new(2020, 12, 29) if month == Date.new(2021, 1, 1)
    month.to_time
  end

  def ends_at
    return Time.new(2020, 12, 29) if month == Date.new(2020, 12, 1)
    month.next_month.to_time
  end

  def prev_month
    prev_rating&.month
  end

  def prev_starts_at
    prev_rating.starts_at
  end

  def prev_ends_at
    prev_rating.ends_at
  end

  def show_prev_month?
    return false unless prev_rating
    return false if month == Date.new(2020, 11, 1)
    true
  end

  def prev_rating
    return @prev_rating if defined? @prev_rating
    @prev_rating = RatingMonth.where('month < ?', month).order(:month).last
    @prev_rating
  end
end
