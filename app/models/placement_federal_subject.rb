class PlacementFederalSubject < ApplicationRecord
  belongs_to :placement
  belongs_to :federal_subject
end
