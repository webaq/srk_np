class SendPulseApi
  include HTTParty
  base_uri 'https://api.sendpulse.com'
  debug_output

  def self.get_token
    options = {
      body: {
         "grant_type" => "client_credentials",
         "client_id" => Rails.application.credentials.send_pulse_api[:id],
         "client_secret" => Rails.application.credentials.send_pulse_api[:secret],
      }
    }
    res = post("/oauth/access_token", options)
    @@token_type = res["token_type"]
    @@access_token = res["access_token"]
    @@token_expires_at = Time.current + res["expires_in"]
  end

  def self.send_message(message)
    return unless access_token

    options = {
      headers: {
        "Authorization" => "#{@@token_type} #{access_token}",
      },
      body: {
        email: {
          html: Base64.encode64(message.body.to_s),
          text: message.subject,
          subject: message.subject,
          from: {
            email: message.from.first,
          },
          to: [
            {
              email: message.to.first,
            }
          ],
        }.to_json
      }
    }
    res = post("/smtp/emails", options)
    res["id"]
  end

  def self.access_token
    unless defined?(@@access_token) && @@token_expires_at.future?
      get_token
    end

    @@access_token
  end
end