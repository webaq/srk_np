class PressRelease < ApplicationRecord
  include PublicActivity::Common
  include PressReleaseOriginable
  include PgSearch::Model
  include Discard::Model

  before_validation :set_published_at, on: :create, unless: :published_at?

  validates :category, presence: true, on: :press_release_submit
  validates :published_at, presence: true
  validates :title, presence: true
  validates :national_projects, presence: true
  validates :selected_federal_subject_ids, presence: true
  validate :content_uniqueness, on: :press_release_submit
  validates :tags, presence: true, on: :press_release_submit
  validates :approved_at, absence: true, if: :returned?

  belongs_to :user, ->{ with_discarded }
  belongs_to :measure, required: false
  # has_one :national_project, through: :federal_project, autosave: false, required: false
  # belongs_to :federal_subject, required: false
  belongs_to :urban_district, required: false
  has_many :bookmarks, dependent: :destroy
  has_many :reposts, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :experts, -> { order(position: :asc) }, dependent: :destroy
  has_many :press_release_national_projects, dependent: :destroy
  has_many :national_projects, through: :press_release_national_projects, dependent: :destroy
  has_many :press_release_federal_subjects, dependent: :destroy
  has_many :federal_subjects, through: :press_release_federal_subjects
  has_many :press_release_federal_projects, dependent: :destroy
  has_many :federal_projects, through: :press_release_federal_projects
  belongs_to :main_topic, required: false
  has_many :show_activities, -> { where(key: 'press_release.show') }, class_name: "::PublicActivity::Activity", as: :trackable
  has_many :approve_activities, -> { where(key: 'press_release.approve') }, class_name: '::PublicActivity::Activity', as: :trackable
  has_many :media_show_activities, -> { where(key: 'press_release.show').where(owner_id: User.joins(:organization).merge(Organization.media)) }, class_name: "::PublicActivity::Activity", as: :trackable
  has_many :return_to_author_activities, -> { where(key: 'press_release.return_to_author') }, class_name: "::PublicActivity::Activity", as: :trackable
  has_one :top_list_item, dependent: :destroy
  has_many :favourites, dependent: :destroy
  has_many :press_release_tags, dependent: :destroy
  has_many :tags, through: :press_release_tags
  has_many :press_release_relations, -> { order(:position) }, dependent: :destroy, inverse_of: :press_release
  has_many :related_press_releases, through: :press_release_relations, class_name: 'PressRelease', source: :related_press_release#, validate: false
  has_many :press_release_related_relations, class_name: 'PressReleaseRelation', foreign_key: :related_press_release_id, dependent: :destroy
  has_many :impressions, dependent: :destroy
  has_many :press_release_likes, dependent: :destroy
  has_many :complaints, dependent: :destroy
  belongs_to :press_release_value, required: false
  has_many :seen_users, through: :show_activities, source: :user
  has_many :seen_organizations, through: :seen_users, source: :organization

  accepts_nested_attributes_for :experts, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :press_release_relations, reject_if: :all_blank, allow_destroy: true

  has_rich_text :content
  has_many_attached :images

  default_scope -> { kept }
  scope :approved, -> { where.not(approved_at: nil) }
  scope :unapproved, -> { where(approved_at: nil) }
  scope :bookmarked, -> { where(bookmarks_count: 1...)}
  scope :published, -> { where.not(published_at: Time.now...) }

  scope :filter_out_unpre_approved, ->(flag) {
    where.not(id: PressRelease
      .includes(user: :organization)
      .references(user: :organization)
      .where.not(organizations: { federal_subject_id: nil })
      .where(organizations: { kind: [Organization.kinds[:regional_authority], Organization.kinds[:city_authority], Organization.kinds[:other]] })
      .where(pre_approved_at: nil)
    ) if flag }

  scope :unapproved_for_user, -> (user) {
    if user.organization&.federal_subject
      where(approved_at: nil, pre_approved_at: nil)
    else
      where(approved_at: nil)
    end
  }

  scope :moderatable_by_user, -> (user) {
    if user.admin?
      all
    else
      where(id: joins(:national_projects).where(national_projects: { id: user.moderatable_national_project_ids }))
    end
  }

  pg_search_scope :search_full_text,
    against: [
      [:title, 'A'],
      [:plain_text_content, 'B'],
    ],
    using: {
      tsearch: { dictionary: "russian" }
    },
    ranked_by: '1'#,
    #order_within_rank: "press_releases.published_at DESC"

  delegate :category, :subcategory, to: :press_release_value, allow_nil: true

  alias_attribute :name, :title

  def content_uniqueness
    if similiar(1).any?
      errors.add :content, :uniqueness
    end
  end

  def images=(attachables)
    super(attachables.filter(&:present?))
  end

  def image_urls=(arr)
    arr.each do |image_url|
      url = URI.parse(image_url)
      filename = File.basename(url.path)
      images.attach(io: URI.open(image_url), filename: filename)
    end
  end

  def status
    approved_at ? :published : :draft
  end

  def published?
    published_at < Time.current
  end

  def returned?
    !!returned_at?
  end

  def approve
    touch(:approved_at)
  end

  def approved?
    !!approved_at
  end

  def draft?
    !approved?
  end

  def use_main_topic
    main_topic_id.present?
  end

  def use_late_published_at
    !!late_published_at
  end

  def use_starts_at
    !!starts_at
  end

  def late_published_at=(new_value)
    super

    if self.late_published_at.present?
      self.published_at = self.late_published_at
    elsif self.published_at.nil?
      self.published_at = Time.current
    end
  end

  def content=(body)
    self.content.body = body

    self.plain_text_content = content.to_plain_text
    self.lexemes = PressRelease.generate_lexemes(plain_text_content)
    set_origins
  end

  def need_pre_approvement?
    user.organization&.federal_subject&.present? && (user.organization&.regional_authority? || user.organization&.city_authority? || user.organization&.other?)
  end

  def set_published_at
    self.published_at = self.late_published_at.presence || Time.current
  end

  def to_ics(url)
    cal = Icalendar::Calendar.new

    cal.event do |e|
      e.dtstart = starts_at
      e.summary = title
      e.url = url
    end

    cal.to_ical
  end

  def selected_federal_subject_ids
    all_federal_subjects ? FederalSubject.pluck(:id) : federal_subject_ids
  end

  def selected_federal_subject_ids=(new_value)
    if new_value.reject(&:blank?).count == FederalSubject.count
      self.all_federal_subjects = true
      new_value.clear
    end
    self.federal_subject_ids = new_value
  end

  def update_on_context(attributes, context = nil)
    with_transaction_returning_status do
      assign_attributes(attributes)
      save(context: context)
    end
  end

  def similiar(threshold = 0.86)
    return PressRelease.none if lexemes.empty?

    PressRelease.similiar_press_releases(lexemes, threshold)
      .where.not(id: id)
      .select('*')
      .select(PressRelease.sanitize_sql_array(["smlar(lexemes, ARRAY[?]::text[]) AS _similarity", lexemes]))
      .order(_similarity: 'desc')
      .limit(10)
  end

  def press_releases_with_same_tags
    PressRelease
      .approved
      .published
      .includes(:tags)
      .where.not(id: id)
      .where(tags: { id: tag_ids })
      .order(published_at: 'DESC')
  end

  def max_approved_at
    published_at.next_month.change(day: 10).to_date.to_time
  end

  def self.similiar_press_releases(lexemes, threshold)
    lexemes = generate_lexemes(lexemes) unless lexemes.is_a?(Array)
    return PressRelease.none if lexemes.empty?

    PressRelease.connection.execute("SET smlar.threshold = #{threshold};")
    PressRelease.where("lexemes % ARRAY[?]::text[]", lexemes)
  end

  def self.extract_id_from_url(term)
    $1.to_i if term =~ /^https?:\/\/.*\/press_releases\/(\d+)$/
  end
end
