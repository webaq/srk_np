class Favourite < ApplicationRecord
  belongs_to :user
  belongs_to :press_release
end
