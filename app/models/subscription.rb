class Subscription < ApplicationRecord
  include PublicActivity::Common

  enum frequency: {
    daily: 1,
    weekly: 2,
  }

  belongs_to :user

  has_many :subscription_federal_subjects, dependent: :destroy
  has_many :federal_subjects, through: :subscription_federal_subjects, dependent: :destroy

  has_many :subscription_organizations, dependent: :destroy
  has_many :organizations, through: :subscription_organizations, dependent: :destroy

  has_many :subscription_national_projects, dependent: :destroy
  has_many :national_projects, through: :subscription_national_projects, dependent: :destroy

  has_many :subscription_tags, dependent: :destroy
  has_many :tags, through: :subscription_tags, dependent: :destroy

  validates :frequency, presence: true
end
