class PressReleaseLike < ApplicationRecord
  belongs_to :press_release, counter_cache: true
  belongs_to :user
end
