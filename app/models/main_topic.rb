class MainTopic < ApplicationRecord
  has_many :press_releases, dependent: :nullify
  has_many :main_topic_national_projects, dependent: :destroy
  has_many :national_projects, through: :main_topic_national_projects

  has_rich_text :content

  validates :name, presence: true
  validates :national_projects, presence: true

  scope :published, -> {
    where('(starts_at IS NULL OR starts_at < NOW()) AND (ends_at IS NULL OR ends_at > NOW())')
  }

  def published?
    return false if starts_at && starts_at > Time.current
    return false if ends_at && ends_at < Time.current
    true
  end
end
