class Placement < ApplicationRecord
  belongs_to :user

  has_many :placement_national_projects, dependent: :destroy
  has_many :national_projects, through: :placement_national_projects
  has_many :placement_federal_subjects, dependent: :destroy
  has_many :federal_subjects, through: :placement_federal_subjects

  has_rich_text :content
  has_many_attached :images

  validates :national_projects, presence: true
  validates :federal_subjects, presence: true
  validates :title, presence: true
  validates :images, presence: true, on: :create

  def all_federal_subjects?
    federal_subjects.size == FederalSubject.count
  end
end
