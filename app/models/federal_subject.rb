class FederalSubject < ApplicationRecord
  validates :name, presence: true

  has_many :urban_districts, dependent: :restrict_with_error
  has_many :press_release_federal_subjects, dependent: :restrict_with_error
  has_many :press_releases, through: :press_release_federal_subjects
  has_many :users, dependent: :restrict_with_error
  has_many :repostable_users, ->{ where(id: UserFederalProjectPermission.repostable.select(:user_id) ) }, class_name: 'User'
  has_many :organizations, dependent: :restrict_with_error
  has_many :placement_federal_subjects, dependent: :destroy
  has_many :placements, through: :placement_federal_subjects
  has_many :subscription_federal_subjects, dependent: :destroy
end
