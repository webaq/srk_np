class UrbanDistrict < ApplicationRecord
  validates :name, presence: true

  belongs_to :federal_subject
  has_many :press_releases, dependent: :restrict_with_error
  has_many :users, dependent: :restrict_with_error
end
