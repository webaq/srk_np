class PressReleaseNationalProject < ApplicationRecord
  belongs_to :press_release, counter_cache: :national_projects_count
  belongs_to :national_project
end
