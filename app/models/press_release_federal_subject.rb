class PressReleaseFederalSubject < ApplicationRecord
  belongs_to :press_release, counter_cache: :federal_subjects_count
  belongs_to :federal_subject
end
