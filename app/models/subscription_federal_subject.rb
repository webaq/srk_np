class SubscriptionFederalSubject < ApplicationRecord
  belongs_to :subscription, counter_cache: :federal_subjects_count
  belongs_to :federal_subject
end
