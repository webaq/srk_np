class PressReleaseOrigin < ApplicationRecord
  PERCENTAGES = [75, 80, 85, 90, 95].freeze

  belongs_to :press_release
  belongs_to :original_press_release, class_name: 'PressRelease'
end
