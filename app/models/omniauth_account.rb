class OmniauthAccount < ApplicationRecord
  belongs_to :user

  validates :provider, presence: true
  validates :uid, presence: true, uniqueness: { scope: :provider, message: 'уже привязан к другой учётной записи' }
end
