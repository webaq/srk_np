class PlacementPressReleaseBuilder
  attr_reader :placement

  def initialize(placement)
    @placement = placement
  end

  def press_release
    PressRelease.new({
      user: placement.user,
      national_projects: placement.national_projects,
      selected_federal_subject_ids: placement.federal_subject_ids,
      title: placement.title,
      content: placement.content.body,
      images: placement.images_blobs,
      created_at: placement.created_at,
      updated_at: placement.updated_at,
      published_at: placement.created_at,
      authors_contacts_published: true,
    })
  end
end