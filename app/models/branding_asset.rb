class BrandingAsset < ApplicationRecord
  belongs_to :national_project

  has_one_attached :avatar
  has_many_attached :images

  validates :avatar, presence: true
end
