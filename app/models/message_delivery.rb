class MessageDelivery < ApplicationRecord
  belongs_to :message
  belongs_to :user

  scope :published, -> { joins(:message).merge(Message.published) }
  scope :unread, -> { where(read_at: nil) }
end
