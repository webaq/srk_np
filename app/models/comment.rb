class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :press_release

  validates :text, presence: true
end
