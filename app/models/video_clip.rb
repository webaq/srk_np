class VideoClip < ApplicationRecord
  belongs_to :video_album
  has_one_attached :image
  has_one_attached :video

  validates :name, presence: true
  validates :image, presence: true
  validate :video_mp4_content_type

  def youtube_id
    youtube_url =~ /(youtu\.be\/|youtube\.com\/(watch\?(.*&)?v=|(embed|v)\/))([^\?&"'>]+)/
    $5
  end

  def video_mp4_content_type
    if video.attached? && !video.content_type.in?(%w[ video/mp4 ])
      errors.add :video, 'должно быть в формате mp4'
    end
  end
end
