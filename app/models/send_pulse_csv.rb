require 'csv'

class SendPulseCsv
  #["Email ID", "Дата", "Отправитель", "Получатель", "Тема письма", "Общий размер (КБ)", "Статус", "Причина", "Техническая информация", "Прочитано", "Данные клиента", "Переход по ссылке", "Данные клиента"]

  attr_reader :filepath

  def initialize(filepath)
    @filepath = filepath
  end

  def enumerator
    CSV.foreach(filepath, {
      col_sep: ';',
      quote_char: ';',
      headers: true,
    })
  end
end