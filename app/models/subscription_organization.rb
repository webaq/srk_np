class SubscriptionOrganization < ApplicationRecord
  belongs_to :subscription, counter_cache: :organizations_count
  belongs_to :organization
end
