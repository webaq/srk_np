class PressReleaseRelation < ApplicationRecord
  belongs_to :press_release
  belongs_to :related_press_release, class_name: 'PressRelease'

  acts_as_list scope: :press_release_id
end
