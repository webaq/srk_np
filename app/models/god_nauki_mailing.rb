class GodNaukiMailing
  def initialize(emails = SystemSubscriber.god_nauki.pluck(:email))
    @emails = emails
  end

  def send_weekly
    @starts_at = 1.week.ago.change(hour: 10)
    @ends_at = Time.current.change(hour: 10)
    @subject = 'Еженедельная рассылка по Году науки'
    send_emails
  end

  def send_monthly
    @starts_at = 1.month.ago.beginning_of_month
    @ends_at = Time.current.beginning_of_month
    @subject = 'Ежемесячная рассылка по Году науки'
    send_emails
  end

  private
    def send_emails
      press_releases = PressRelease.where(created_at: @starts_at..@ends_at).includes(:national_projects).where(national_projects: { id: 15 })
      created_press_releases_count = press_releases.count
      taken_press_releases_count = press_releases.joins(:bookmarks).distinct.count

      @emails.each do |email|
        UserMailer.with({
          email: email,
          subject: @subject,
          starts_on: @starts_at.to_date,
          ends_on: @ends_at.to_date,
          created_press_releases_count: created_press_releases_count,
          taken_press_releases_count: taken_press_releases_count,
        }).god_nauki.deliver
      end
    end
end