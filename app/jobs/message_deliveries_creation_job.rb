class MessageDeliveriesCreationJob < ApplicationJob
  queue_as :default

  def perform(message)
    now = Time.current

    message.relevant_users.find_in_batches do |group|
      MessageDelivery.insert_all!(group.map { |user|
        {
          message_id: message.id,
          user_id: user.id,
          updated_at: now,
          created_at: now,
        }
      })
    end
  end
end
