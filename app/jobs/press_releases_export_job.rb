class PressReleasesExportJob < ApplicationJob
  queue_as :default

  def perform(user_id, safe_params)
    current_user = User.find(user_id)
    @safe_params = safe_params

    press_releases = Pundit.policy_scope!(current_user, PressRelease)
    press_releases = press_releases.search_full_text(@safe_params[:q]) if @safe_params[:q].present?
    press_releases_for_totals = press_releases

    press_releases = case @safe_params[:kind]
    when 'all_unapproved'
      PressReleasePolicy::RecommendedApprovableScope.new(current_user, press_releases).resolve.unapproved_for_user(current_user).filter_out_unpre_approved(current_user.admin? && @safe_params[:approvement] == 'pre_approved').where(returned_at: nil)
    when 'media_works'
      press_releases.where('approved_at IS NOT NULL AND published_at < ?', Time.current)
    when 'returned'
      press_releases.where.not(returned_at: nil)
    when 'my'
      press_releases.where(user_id: current_user.id)
    when 'my_unapproved'
      press_releases.where(user_id: current_user.id).unapproved
    when 'my_approved'
      press_releases.where(user_id: current_user.id).approved
    when 'my_bookmarked'
      press_releases.where(user_id: current_user.id).bookmarked
    when 'main_topics'
      press_releases.where.not(main_topic_id: nil)
    when 'i_bookmarked'
      press_releases.where(id: current_user.bookmarks.select(:press_release_id))
    when 'i_reposted'
      press_releases.where(id: current_user.reposts.select(:press_release_id))
    when 'event'
      press_releases.where.not(starts_at: nil)
    else
      press_releases
    end

    @press_releases_filter = PressReleasesFilter.new(current_user, press_releases, @safe_params)

    press_releases = @press_releases_filter.press_releases
      .order(published_at: 'DESC')
      .includes(national_projects: { avatar_attachment: :blob })
      .includes(:federal_subjects)
      .includes(:urban_district)
      .includes(:user)

    NotifierMailer.press_releases_export(current_user, press_releases).deliver_now
  end
end
