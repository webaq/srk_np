import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "body", "downloadButton", "sendButton" ]

  connect() {
    this.updateButtons()
  }

  bodyChanged() {
    this.updateButtons()
  }

  updateButtons() {
    if (this.bodyTarget.value) {
      this.sendButtonTarget.disabled = false
      if (this.hasDownloadButtonTarget) {
        this.downloadButtonTarget.classList.toggle("disabled", true)
      }
    } else {
      this.sendButtonTarget.disabled = true
      if (this.hasDownloadButtonTarget) {
        this.downloadButtonTarget.classList.toggle("disabled", false)
      }
    }
  }
}
