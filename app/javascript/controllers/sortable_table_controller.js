import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "link" ]

  static values = {
    columnIndex: String // default - ""
  }

  sort(e) {
    e.preventDefault()

    const clickedLink = e.currentTarget
    const columnIndex = Number(clickedLink.getAttribute("data-column-index"))

    if (this.columnIndexValue == columnIndex) {
      const oppositeDirection = clickedLink.getAttribute("data-direction") == "asc" ? "desc" : "asc"
      clickedLink.setAttribute("data-direction", oppositeDirection)
    }
    this.columnIndexValue = columnIndex

    this.linkTargets.forEach(link => {
      link.classList.toggle("active", link.getAttribute("data-column-index") == columnIndex)
    })

    // console.log("columnIndex", columnIndex)
    let rowsWithValues = Array.from(this.element.querySelectorAll("tbody > tr")).map((tr, index) => {
      const td = tr.querySelector("td:nth-child(" + (columnIndex + 1) + ")")
      return {
        tr: tr,
        value: td.getAttribute("data-sortable-table-value"),
      }
    })

    switch (clickedLink.getAttribute("data-value-type")) {
      case "string":
        rowsWithValues.sort((a, b) => a.value.localeCompare(b.value))
        break
      default:
        rowsWithValues.sort((a, b) => Number(a.value) - Number(b.value))
    }

    // console.log("direction", clickedLink.getAttribute("data-direction"))
    if (clickedLink.getAttribute("data-direction") == "desc") {
      rowsWithValues.reverse()
    }

    // console.log("sorted rows", rowsWithValues)

    rowsWithValues.forEach(function(rowWithValue) {
      this.element.querySelector("tbody").appendChild(rowWithValue.tr)
    }.bind(this))
  }
}
