import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "hint" ]

  valueChanged(e) {
    this.hintTarget.classList.toggle('d-none', e.target.value)
  }
}
