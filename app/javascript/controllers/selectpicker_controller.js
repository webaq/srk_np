import { Controller } from "stimulus"

export default class extends Controller {
  connect() {
    $(this.element).selectpicker()

    if (this.element.hasAttribute('data-abs-ajax-url')) {
      $(this.element).ajaxSelectPicker({
        ajax: {
          type: 'GET',
          dataType: 'json',
        },
      })
    }
  }
}
