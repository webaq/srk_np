import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "select" ]

  connect() {
    console.log('organization with kinds multiselect connected')
    $(this.selectTarget).on('show.bs.select', this.onShow.bind(this))
  }

  onShow(event) {
    console.log('onShow')
    this.actionsBox.innerHTML = this.modifiedActionsBoxContent
  }

  selectByKind(event) {
    const kind = event.target.getAttribute('data-kind')

    var selectedValues = $(this.selectTarget).selectpicker('val')

    for(var i = 0; i < this.selectTarget.options.length; i++) {
      if (this.selectTarget.options[i].getAttribute('data-kind') == kind) {
        selectedValues.push(this.selectTarget.options[i].value)
      }
    }

    $(this.selectTarget).selectpicker('val', selectedValues);
  }

  disconnect() {
    $(this.selectTarget).off('show.bs.select')
    console.log('organization with kinds multiselect disconnected')
  }

  get actionsBox() {
    return this.element.querySelector('.bs-actionsbox')
  }

  get modifiedActionsBoxContent() {
    const kinds = JSON.parse(this.data.get('kinds'))

    var html = '<div class="btn-group btn-group-sm btn-block">'
    kinds.forEach((kind) => {
      html += '  <button type="button" class="btn btn-link font-decoration-underline w-auto" data-action="organization-with-kinds-multiselect#selectByKind" data-kind="' + kind[1] + '">' + kind[0] + '</button>'
    })
    html += '  <button type="button" class="actions-btn bs-deselect-all btn btn-link w-auto">Отменить все</button>'
    html += '</div>'
    return html
  }
}
