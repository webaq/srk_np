import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "kindSelect", 'websiteWrapper', 'mediaSubscribersWrapper' ]

  updateMediaFields() {
    if (this.kindSelectTarget.value == 'media') {
      this.websiteWrapperTarget.classList.remove('d-none')
      this.mediaSubscribersWrapperTarget.classList.remove('d-none')
    } else {
      this.websiteWrapperTarget.classList.add('d-none')
      this.mediaSubscribersWrapperTarget.classList.add('d-none')
    }
  }
}
