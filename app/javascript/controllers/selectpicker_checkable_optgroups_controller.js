// Visit The Stimulus Handbook for more details
// https://stimulusjs.org/handbook/introduction
//
// This example controller works with specially annotated HTML like:
//
// <div data-controller="hello">
//   <h1 data-target="hello.output"></h1>
// </div>

import { Controller } from "stimulus"

export default class extends Controller {
  connect() {
    var self = this;
    this.listening = false
    $(this.element).on('shown.bs.select', function (e) {
      // console.log('shown', e)
      if (!self.listening) {
        self.listening = true
        $(e.target).siblings('.dropdown-menu').find('.dropdown-header').click(function(e) {
          var dropdownHeader = $(e.target).closest('.dropdown-header')[0]
          var classList = dropdownHeader.className.split(/\s+/)
          var optGroupClass = classList[classList.length - 1]
          // console.log(optGroupClass)
          // console.log($(dropdownHeader).siblings('.' + optGroupClass))
          $(dropdownHeader).siblings('.' + optGroupClass).find('a').click()
        })
      }
    });
  }
}
