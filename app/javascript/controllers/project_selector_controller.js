import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ 'nationalProjectsSelect', 'federalProjectsSelect', 'tagsSelect' ]

  connect() {
    $(this.nationalProjectsSelectTarget).on('hide.bs.select', function(e) {
      this.updateFederalProjectSelect()
      this.updateTagsSelect()
    }.bind(this))
  }

  updateFederalProjectSelect() {
    const selectedNationalProjectIds = $(this.nationalProjectsSelectTarget).selectpicker('val')
    const singleNationalProjectId = selectedNationalProjectIds.length == 1 ? selectedNationalProjectIds[0] : null

    for (var i = 0; i < this.federalProjectsSelectTarget.length; i++) {
      if (singleNationalProjectId && singleNationalProjectId == this.federalProjectsSelectTarget.options[i].getAttribute('data-national-project-id')) {
        this.federalProjectsSelectTarget.options[i].removeAttribute('disabled')
      } else {
        this.federalProjectsSelectTarget.options[i].setAttribute('disabled', '')
      }
    }

    if (singleNationalProjectId && singleNationalProjectId != 14) {
      $(this.federalProjectsSelectTarget).closest('.form-group').removeClass('d-none')
      $(this.federalProjectsSelectTarget).selectpicker('refresh')
    } else {
      $(this.federalProjectsSelectTarget).closest('.form-group').addClass('d-none')
    }
  }

  updateTagsSelect() {
    const selectedNationalProjectIds = $(this.nationalProjectsSelectTarget).selectpicker('val')

    for (var i = 0; i < this.tagsSelectTarget.length; i++) {
      const optionNationalProjectIdsStr = this.tagsSelectTarget.options[i].getAttribute('data-national-project-ids')
      const optionNationalProjectIds = JSON.parse(optionNationalProjectIdsStr)
      const intersection = selectedNationalProjectIds.filter(value => optionNationalProjectIds.includes(parseInt(value)))
      // console.log(selectedNationalProjectIds, optionNationalProjectIds, intersection)

      if (intersection.length > 0) {
        this.tagsSelectTarget.options[i].removeAttribute('disabled')
      } else {
        this.tagsSelectTarget.options[i].setAttribute('disabled', '')
      }
    }

    $(this.tagsSelectTarget).selectpicker('refresh')
  }
}
