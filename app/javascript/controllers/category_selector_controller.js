import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "notice", "categorySelector", "subcategoryRadioButton", "category", "subcategory", "pressReleaseValue" ]

  categoryChanged() {
    const categoryId = this.categorySelectorTarget.value
    console.log("categoryChanged", categoryId)

    this.noticeTargets.forEach(el => {
      el.classList.toggle("d-none", el.getAttribute("data-category-id") != categoryId)
    })

    this.categoryTargets.forEach(el => {
      el.classList.toggle("d-none", !(el.getAttribute("data-category-id") == categoryId && el.getAttribute("data-subcategories-size") > 1))
    })

    this.subcategoryRadioButtonTargets.forEach(el => {
      const categoryDiv = $(el).closest("[data-category-id]")[0]
      const checked = categoryDiv.getAttribute("data-category-id") == categoryId && categoryDiv.getAttribute("data-subcategories-size") == 1
      el.checked = checked
    })

    this.subcategoryChanged()
  }

  subcategoryChanged() {
    const subcategoryId = this.subcategoryRadioButtonTargets.find(el => el.checked)?.value
    console.log("subCategoryChanged", subcategoryId)

    this.subcategoryTargets.forEach(el => {
      const shown = el.getAttribute("data-subcategory-id") == subcategoryId && el.getAttribute("data-press-release-values-size") > 1
      el.classList.toggle("d-none", !shown)
    })

    this.pressReleaseValueTargets.forEach(el => {
      const subcategoryDiv = $(el).closest("[data-subcategory-id]")[0]
      console.log(subcategoryDiv)
      const checked = subcategoryDiv.getAttribute("data-subcategory-id") == subcategoryId && subcategoryDiv.getAttribute("data-press-release-values-size") == 1
      el.checked = checked
    })
  }
}
