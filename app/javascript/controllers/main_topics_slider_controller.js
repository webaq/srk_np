import { Controller } from "stimulus"
import Swiper from 'swiper';

export default class extends Controller {
  // static targets = [ "output" ]

  connect() {
    // this.outputTarget.textContent = 'Hello, Stimulus!'
    var mySwiper = new Swiper(this.element, {
      slidesPerView: 3,
      spaceBetween: 30,
      loop: false,
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
      },
      // navigation: {
      //   nextEl: '.swiper-button-next',
      //   prevEl: '.swiper-button-prev',
      // },
    });
  }
}
