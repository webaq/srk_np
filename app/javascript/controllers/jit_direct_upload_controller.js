import { Controller } from "stimulus"
import { DirectUpload } from "@rails/activestorage"

export default class extends Controller {
  static values = {
    callbackUrl: String,
  }


  fileInputChanged(e) {
    console.log("fileInputChanged", e)
    const input = e.target

    Array.from(input.files).forEach(function(file) {
      const fsize = file.size;
      const kb = Math.round((fsize / 1024));
      // The size of the file.
      // console.log('size', file)
      if (kb >= 100000) {
        alert( "Вы выбрали слишком большой файл.");
        e.preventDefault()
        input.value = null
        return false
      }
    })

    const url = input.dataset.directUploadUrl
    const callbackUrl = this.callbackUrlValue

    Array.from(input.files).forEach(function(file) {
      const upload = new DirectUpload(file, url)

      upload.create(function(error, blob) {
        console.log("uploaded blob", blob)
        if (error) {
          // Handle the error
        } else {
          // Add an appropriately-named hidden input to the form with a
          //  value of blob.signed_id so that the blob ids will be
          //  transmitted in the normal upload flow
          $.ajax({
            url: callbackUrl,
            dataType: 'script',
            data: {
              blob_id: blob.id,
            },
          })
        }
      })
    })

    // you might clear the selected files from the input
    input.value = null
  }
}
