import { Controller } from "stimulus"

export default class extends Controller {
  connect() {
    console.log('trix w/o attachments')

    this.element.addEventListener("trix-attachment-add", function(event) {
      event.stopPropagation()
      event.preventDefault()
      event.attachment.remove()
      console.log('no attachments')
      return false
    })

    $(this.element).siblings('trix-toolbar').find('[data-trix-button-group="file-tools"]').hide()
  }
}
