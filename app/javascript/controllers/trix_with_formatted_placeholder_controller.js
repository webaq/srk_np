import { Controller } from "stimulus"

export default class extends Controller {
  connect() {
    console.log('trix w/ formatted placeholder')
    this.setPlaceholderIfEmpty()
    this.element.addEventListener("trix-focus", this.onFocus.bind(this))
    this.element.addEventListener("trix-blur", this.onBlur.bind(this))
    $(this.element).closest('form').on('submit', this.beforeSubmit.bind(this))
  }

  onFocus() {
    console.log('onFocus')
    this.setEmptyIfPlaceholder()
    this.element.classList.remove('trix-with-formatted-placeholder')
  }

  onBlur() {
    console.log('onBlur')
    this.setPlaceholderIfEmpty()
  }

  beforeSubmit() {
    // alert('before submit')
    this.setEmptyIfPlaceholder()
  }

  setEmptyIfPlaceholder() {
    if (this.element.innerHTML == this.placeholderHTML) {
      console.log('make empty')
      this.element.innerHTML = ''
    }
  }

  setPlaceholderIfEmpty() {
    console.log('setPlaceholderIfEmpty')
    if (this.element.innerHTML == '') {
      console.log('set placeholder')
      this.element.editor.insertHTML(this.placeholderContent)
      this.placeholderHTML = this.element.innerHTML
      this.element.classList.add('trix-with-formatted-placeholder')
      this.element.blur()
    }
  }

  get placeholderContent() {
    return this.element.getAttribute('data-trix-with-formatted-placeholder-content')
  }
}
