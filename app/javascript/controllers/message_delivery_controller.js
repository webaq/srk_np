import { Controller } from "stimulus"

export default class extends Controller {
  read() {
    this.element.classList.remove('message_delivery--unread')
  }

  destroyed() {
    this.element.remove()
  }
}
