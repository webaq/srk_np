import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "newOrganizationFields" ]

  organizationChanged(event) {
    console.log("organizationChanged", event)
    if (event.target.value) {
      this.newOrganizationFieldsTarget.classList.add('d-none')
    } else {
      this.newOrganizationFieldsTarget.classList.remove('d-none')
    }
  }

  destroyed() {
    this.element.remove()
  }
}
