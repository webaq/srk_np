import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "selectable" ]

  select(event) {
    event.preventDefault()
    this.selectableTargets.forEach((el) => {
      el.checked = true
    })
  }

  deselect(event) {
    event.preventDefault()
    this.selectableTargets.forEach((el) => {
      el.checked = false
    })
  }

}
