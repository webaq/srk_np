import { Controller } from "stimulus"

export default class extends Controller {
  static values = {
    backdrop: String
  }

  connect() {
    // this.outputTarget.textContent = 'Hello, Stimulus!'
    // console.log('modal connected')
    let options = {}

    if (this.hasBackdropValue) {
      options.backdrop = this.backdropValue
    }

    $(this.element).modal(options)
    $(this.element).on('hidden.bs.modal', function(e) {
      // console.log('modal hidden')
      this.element.remove()
    }.bind(this))
  }

  disconnect() {
    // console.log('modal disconnected', this.element)
    $(this.element).modal('dispose')
  }
}
