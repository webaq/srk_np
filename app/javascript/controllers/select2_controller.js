import { Controller } from "stimulus"

export default class extends Controller {
  static values = {
    url: String,
  }

  connect() {
    let options = {}

    if (this.urlValue) {
      // console.log("select2 ajax url", this.urlValue)
      options.ajax = {
        url: this.urlValue,
        data: function (params) {
          return { search: params.term }
        },
        dataType: "JSON",
        processResults: function(data) {
          return {
            results: data.map(function(item) {
              return { id: item.id, text: item.name }
            })
          }
        },
      }
    }

    // console.log("select2 options", options)
    $(this.element).select2(options)

    $(this.element).on("change", function() {
      this.element.dispatchEvent(new CustomEvent("select2:change"))
    }.bind(this))
  }
}
