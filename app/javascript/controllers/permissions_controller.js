import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "nationalProject", "federalProject" ]

  permissionClicked(event) {
    const permission = event.target.getAttribute('data-permission')
    var checked = event.target.getAttribute('data-checked') == 'true'
    checked = !checked

    this.nationalProjectTargets.forEach((el) => {
      if (el.getAttribute('data-permission') == permission) {
        el.checked = checked
      }
    })

    this.federalProjectTargets.forEach((el) => {
      if (el.getAttribute('data-permission') == permission) {
        el.checked = checked
      }
    })

    event.target.setAttribute('data-checked', checked)
  }

  nationalProjectClicked(event) {
    const nationalProjectId = event.target.getAttribute('data-national-project-id')
    const permission = event.target.getAttribute('data-permission')
    const checked = event.target.checked
    this.federalProjects(nationalProjectId, permission).forEach((el) => {
      el.checked = checked
    })
  }

  federalProjectClicked(event) {
    const nationalProjectId = event.target.getAttribute('data-national-project-id')
    const permission = event.target.getAttribute('data-permission')
    this.updateNationalProject(nationalProjectId, permission)
  }

  federalProjects(nationalProjectId, permission) {
    return this.federalProjectTargets.filter((el) => {
      return el.getAttribute('data-national-project-id') == nationalProjectId && el.getAttribute('data-permission') == permission
    })
  }

  updateNationalProject(nationalProjectId, permission) {
    console.log('updateNationalProject', nationalProjectId, permission)
    this.nationalProjectTargets.find((el) => {
      return el.getAttribute('data-national-project-id') == nationalProjectId && el.getAttribute('data-permission') == permission
    }).checked = this.federalProjects(nationalProjectId, permission).every((el) => {
      return el.checked
    })
  }
}
