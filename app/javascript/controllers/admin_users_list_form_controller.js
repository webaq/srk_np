import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "checkbox" ]

  connect() {
    console.log('admin users list form')
  }

  setAction(event) {
    const action = event.target.getAttribute('data-url')
    const method = event.target.getAttribute('data-method')

    this.element.setAttribute('action', action)
    this.element.setAttribute('method', method)

    $.rails.fire(this.element, 'submit')
  }

  ajaxSuccess() {
    this.checkboxTargets.forEach((el) => {
      el.checked = false
    })
  }
}
