import { Controller } from "stimulus"
import Swiper from 'swiper'

export default class extends Controller {
  static targets = [ "container", "next", "prev" ]
  static values = {
    slidesOffsetBefore: Number,
    slidesOffsetAfter: Number,
    slidesPerView: Number,
  }

  connect() {
    // console.log('swiper controller', this.slidesOffsetBeforeValue, this.slidesOffsetAfterValue)

    const options = {
      slidesPerView: this.slidesPerViewValue,
      spaceBetween: 15,
      loop: false,
      // slidesOffsetBefore: this.slidesOffsetBeforeValue,
      // slidesOffsetAfter: 30,
    }

    // console.log('swiper options', options)

    if (this.hasNextTarget && this.hasPrevTarget) {
      options.navigation = {
        nextEl: this.nextTarget,
        prevEl: this.prevTarget,
      }
    }

    new Swiper(this.containerTarget, options);
  }
}
