import { Controller } from "stimulus"

export default class extends Controller {
  static values = {
    timer: Number,
    closeButtonWhenUserClicksLink: Boolean,
  }

  static targets = ["timer", "close"]

  connect() {
    if (this.closeButtonWhenUserClicksLinkValue) {
      this.timerTarget.classList.add("d-none")

      const self = this
      $(this.element).find(".trix-content a").click(function(e) {
        $(this).attr("target","_blank")
        self.closeTarget.classList.remove("d-none")
      });
    } else {
      $(this.element).on('shown.bs.modal', function(e) {
        this.startTimer()
      }.bind(this))
    }
  }

  startTimer() {
    this.interval = setInterval(this.timerStep.bind(this), 1000)
  }

  timerStep() {
    if (this.timerValue > 1) {
      this.timerValue -= 1
      this.timerTarget.innerHTML = this.timerValue
      return
    }
    else {
      console.log("Stop timer")
      clearInterval(this.interval)
      this.timerTarget.classList.add("d-none")
      this.closeTarget.classList.remove("d-none")
    }
  }
}
