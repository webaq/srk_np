import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ 'confirmation', "submitButton" ]

  confirmClicked() {
    this.submitButtonTarget.disabled = this.confirmationTargets.some(el => !el.checked)
  }
}
