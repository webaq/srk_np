import { Controller } from "stimulus"
import YouTubePlayer from 'youtube-player'

export default class extends Controller {
  static values = {
    name: String,
    youtubeId: String,
    wasStarted: Boolean,
  }

  connect() {
    if (this.youtubeIdValue) {
      // console.log("video clip player", this.youtubeIdValue)
      let player
      player = YouTubePlayer(this.element)
      player.loadVideoById(this.youtubeIdValue)
      player.on('stateChange', function(event) {
        switch(event.data) {
        case 1:
          this.playerStarted()
        }
      }.bind(this))
    }
  }

  playerStarted() {
    if (this.wasStartedValue) {
      return
    }
    console.log("playerStarted")
    this.wasStartedValue = true

    window.dataLayer.push({
      event: 'videoPleerStart',
      eventCategory: 'videoPleer',
      eventAction: 'Start',
      eventLabel: this.nameValue,
    });
  }
}
