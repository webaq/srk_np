import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "input" ]

  changed(event) {
    var self = this;
    var disabled = !event.target.checked

    this.inputTargets.forEach((el, i) => {
      self.updateInput(el, disabled)

      if (el.classList.contains('flatpickr-input')) {
        self.updateInput(el.nextSibling, disabled)
      }

      if (el.classList.contains('select')) {
        const selectpicker = el.nextSibling
        if (disabled) {
          selectpicker.classList.add('disabled')
        } else {
          selectpicker.classList.remove('disabled')
        }
      }
    })
  }

  updateInput(el, disabled) {
    if (disabled) {
      el.setAttribute('disabled', true)
    } else {
      el.removeAttribute('disabled')
    }
  }
}