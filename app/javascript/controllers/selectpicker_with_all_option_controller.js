import { Controller } from "stimulus"

export default class extends Controller {
  connect() {
    if ($(this.element).selectpicker('val').length == 0) {
      $(this.element).selectpicker('val', ['0'])
    }
    $(this.element).on('changed.bs.select', this.valueChanged.bind(this));
  }

  valueChanged(e, clickedIndex, isSelected, previousValue) {
    // console.log('valueChanged', clickedIndex)
    if (clickedIndex == null) {
      // console.log('no clickedIndex')
      return
    }

    if (clickedIndex == 0) {
      // console.log('All option clicked', $(this.element).selectpicker('val'))
      $(this.element).selectpicker('val', ['0'])
      // console.log('became', $(this.element).selectpicker('val'))
    } else {
      // console.log('Other option clicked', $(this.element).selectpicker('val'))
      const currentSelectedOptions = $(this.element).selectpicker('val')
      $(this.element).selectpicker('val', currentSelectedOptions.filter(id => {
        return id != 0
      }))
    }
  }

  disconnect() {
    $(this.element).off('changed.bs.select');
  }
}
