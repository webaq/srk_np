import { Controller } from "stimulus"
import Swiper from 'swiper'

export default class extends Controller {
  static targets = [ "next", "prev" ]

  connect() {
    const galleryThumbs = new Swiper('.gallery-thumbs', {
      spaceBetween: 10,
      slidesPerView: 4,
      freeMode: true,
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
    });

    const galleryTop = new Swiper('.gallery-top', {
      spaceBetween: 10,
      navigation: {
        nextEl: this.nextTarget,
        prevEl: this.prevTarget,
      },
      thumbs: {
        swiper: galleryThumbs
      }
    });
  }
}
