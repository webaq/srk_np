// Visit The Stimulus Handbook for more details
// https://stimulusjs.org/handbook/introduction
//
// This example controller works with specially annotated HTML like:
//
// <div data-controller="hello">
//   <h1 data-target="hello.output"></h1>
// </div>

import { Controller } from "stimulus"

export default class extends Controller {
  static targets = ['urbanDistrictSelect']

  federalSubjectChanged(e) {
    $(this.urbanDistrictSelectTarget).empty()
    var option = document.createElement("option");
    option.text = 'Все';
    option.value = '';
    this.urbanDistrictSelectTarget.add(option);
    $(this.urbanDistrictSelectTarget).selectpicker('refresh')

    $.ajax({
      url: this.data.get('urbanDistrictsPath'),
      data: { federal_subject_id: e.target.value },
      success: function(data) {
        $.each(data, function(i, el) {
          var option = document.createElement("option");
          option.text = el.text;
          option.value = el.value
          this.urbanDistrictSelectTarget.add(option);
        }.bind(this))

        $(this.urbanDistrictSelectTarget).selectpicker('refresh')
      }.bind(this)
    })
  }
}
