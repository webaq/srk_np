import { Controller } from "stimulus"
import Swiper from 'swiper';

export default class extends Controller {
  // static targets = [ "output" ]

  connect() {
    // this.outputTarget.textContent = 'Hello, Stimulus!'
    var mySwiper = new Swiper(this.element.querySelector('.swiper-container'), {
      slidesPerView: 3,
      spaceBetween: 15,
      loop: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  }
}
