import { Controller } from "stimulus"

export default class extends Controller {
  connect() {
    //$(this.element).mask('+7 (000) 000-0000');

    var customOptions = {
      onKeyPress: function(val, e, field, options) {

        if (val.replace(/\D/g, '').length===2)
        {
            val = val.replace('8','');    
            field.val(val);
         }
         field.mask("+7 (999) 999-99-99", options);
        },
        placeholder: "+7 (___) ___-__-__"
    };

    $(this.element).mask("+7 (999) 999-99-99", customOptions);
  }
}
