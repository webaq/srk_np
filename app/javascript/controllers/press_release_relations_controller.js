import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "search", "addAssociationLink", "relatedPressReleaseId", "relatedPressReleaseName" ]

  add(event) {
    const pressReleaseId = event.detail.value
    const pressReleaseName = event.detail.textValue
    this.searchTarget.value = ''

    $(this.addAssociationLinkTarget)[0].click()
    const index = this.relatedPressReleaseIdTargets.length - 1
    this.relatedPressReleaseIdTargets[index].value = pressReleaseId
    this.relatedPressReleaseNameTargets[index].innerHTML = pressReleaseName
  }
}
