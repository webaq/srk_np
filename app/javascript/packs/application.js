// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

import Rails from "@rails/ujs"
import * as ActiveStorage from "@rails/activestorage"

Rails.start()
ActiveStorage.start()

require("trix")
require("@rails/actiontext")

require("chartkick")
require("chart.js")
import ChartDataLabels from 'chartjs-plugin-datalabels';
// Chart.plugins.unregister(ChartDataLabels)
Chart.helpers.merge(Chart.defaults.global.plugins.datalabels, {
  // color: '#FE777B'
  align: 'top'
})

window.$ = $
window.Rails = Rails
require('jquery-mask-plugin')

import "bootstrap"
import "bootstrap-select"
import "bootstrap-select/js/i18n/defaults-ru_RU"
import "ajax-bootstrap-select"
import "ajax-bootstrap-select/src/js/ajaxSelectPicker.locale/ru-RU.js"
$.fn.ajaxSelectPicker.defaults.langCode = 'ru'

require("@nathanvda/cocoon")

import "controllers"

import 'jquery-pjax'
$.pjax.defaults.timeout = 3000
$.pjax.defaults.scrollTo = false

// $(document).pjax('a[data-pjax]', '[data-pjax-container]')
$(document).on('click', 'a[data-pjax]', function(event) {
  var options = {}

  const elementContainer = $(event.target).attr('data-pjax-container')
  if (elementContainer) {
    options['container'] = elementContainer
    options['fragment'] = elementContainer
  } else {
    options['container'] = '[data-pjax-container]'
  }

  $.pjax.click(event, options)
})

$(document).on('submit', 'form[data-pjax]', function(event) {
  var options = {}

  const elementContainer = $(event.target).attr('data-pjax-container')
  if (elementContainer) {
    options['container'] = elementContainer
    options['fragment'] = elementContainer
  } else {
    options['container'] = '[data-pjax-container]'
  }

  $.pjax.submit(event, options)
})

$(document).on('pjax:popstate', function() {
  $('.modal').remove()
  $('.modal-backdrop').remove()
})

$(document).on('ajax:success', '[data-pjax-reload]', function() {
  $.pjax.reload('[data-pjax-container]')
})


import smoothscroll from 'smoothscroll-polyfill';
smoothscroll.polyfill();
$(document).on('click', '[data-smooth-scrollto]', function(e) {
  const elementId = $(e.target).attr('data-smooth-scrollto')
  $(elementId)[0].scrollIntoView({ behavior: 'smooth'})
})


// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)

// $.fn.selectPicker.locale = 'ru'
// $.fn.ajaxSelectPicker.locale = 'ru'

require("select2");
require("select2/dist/js/i18n/ru");
$.fn.select2.defaults.set("theme", "bootstrap4");
$.fn.select2.defaults.set("language", "ru");
