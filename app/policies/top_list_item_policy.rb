class TopListItemPolicy < ApplicationPolicy
  def create?
    return true if user.admin?
    false
  end

  def destroy?
    return true if user.admin?
    false
  end
end
