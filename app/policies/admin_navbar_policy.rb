class AdminNavbarPolicy < Struct.new(:user, :admin_navbar)
  def show?
    user && (user.admin? || user.editor? || user.organization&.admin?)
  end
end