class RatingWidgetPolicy < ApplicationPolicy
  def show?
    # !user.organization&.media?
    user.has_ability?(:show_rating)
  end
end
