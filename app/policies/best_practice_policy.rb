class BestPracticePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.approved.published.or(scope.where(user_id: user.id))
    end
  end

  def create?
    not user.organization&.media?
  end

  def show?
    true
  end

  def update?
    user.admin? || user == record.user
  end

  def approve?
    user.admin?
  end
end
