class PlacementPolicy < ApplicationPolicy
  def index?
    user.admin?
  end

  def show?
    user.admin? || user == record.user
  end

  def create?
    # user.admin? || (user.organization && !user.organization.media?)
    user.has_ability?(:create_placements)
  end

  def update?
    user.admin?
  end

  def destroy?
    user.admin?
  end

  def convert_to_press_release?
    user.admin?
  end
end
