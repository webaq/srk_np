class Admin::PressReleaseCreationsReportPolicy < ApplicationPolicy
  def show?
    user.admin?
  end
end
