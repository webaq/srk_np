class Admin::PressReleasePolicy < ApplicationPolicy
  def index?
    user.admin?
  end
end
