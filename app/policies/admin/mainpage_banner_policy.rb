class Admin::MainpageBannerPolicy < ApplicationPolicy
  def index?
    user.has_ability?(:edit_mainpage_banners)
  end

  def create?
    index?
  end

  def update?
    index?
  end

  def destroy?
    index?
  end
end
