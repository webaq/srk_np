class Admin::RepostPolicy < ApplicationPolicy
  def index?
    user.admin? || user.reposts_list_permitted?
  end
end
