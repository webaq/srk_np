class Admin::RegionalAuthoritiesActivitiesReportPolicy < ApplicationPolicy
  def show?
    user.admin?
  end
end
