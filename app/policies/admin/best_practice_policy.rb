class Admin::BestPracticePolicy < ApplicationPolicy
  def index?
    user.has_ability?(:edit_best_practices)
  end
end
