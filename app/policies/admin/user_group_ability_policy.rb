class Admin::UserGroupAbilityPolicy < ApplicationPolicy
  def create?
    user.has_ability?(:edit_user_groups)
  end

  def update?
    user.has_ability?(:edit_user_groups)
  end

  def destroy?
    user.has_ability?(:edit_user_groups)
  end
end
