class Admin::NationalProjectsReportPolicy < ApplicationPolicy
  def show?
    user.admin?
  end
end
