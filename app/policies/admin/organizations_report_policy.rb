class Admin::OrganizationsReportPolicy < ApplicationPolicy
  def show?
    user.admin?
  end
end
