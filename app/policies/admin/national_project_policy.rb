class Admin::NationalProjectPolicy < ApplicationPolicy
  def index?
    user.has_ability?(:edit_national_projects)
  end

  def show?
    index?
  end

  def create?
    index?
  end

  def update?
    index?
  end

  def destroy?
    index?
  end
end
