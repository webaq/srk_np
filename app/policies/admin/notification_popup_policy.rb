class Admin::NotificationPopupPolicy < ApplicationPolicy
  def index?
    user.has_ability?(:edit_notification_popups)
  end

  def show?
    index?
  end

  def create?
    index?
  end

  def update?
    index?
  end

  def destroy?
    index?
  end
end
