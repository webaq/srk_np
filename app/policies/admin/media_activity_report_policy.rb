class Admin::MediaActivityReportPolicy < ApplicationPolicy
  def show?
    user.admin?
  end
end
