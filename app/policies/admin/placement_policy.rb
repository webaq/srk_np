class Admin::PlacementPolicy < ApplicationPolicy
  def index?
    user.admin?
  end
end
