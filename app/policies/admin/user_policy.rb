class Admin::UserPolicy < ApplicationPolicy
  def index?
    user.admin?
  end

  def create?
    user.admin?
  end

  def show?
    user.admin?
  end

  def update?
    if record.has_ability?(:edit_user_groups)
      return user.has_ability?(:edit_user_groups)
    end
    user.admin?
  end

  def destroy?
    if record.has_ability?(:edit_user_groups)
      return user.has_ability?(:edit_user_groups)
    end
    user.admin?
  end

  def edit_multiple?
    user.admin?
  end

  def update_multiple?
    user.admin?
  end

  def edit_multiple_permissions?
    user.admin?
  end

  def update_multiple_permissions?
    user.admin?
  end

end
