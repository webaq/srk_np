class Admin::UserGroupPolicy < ApplicationPolicy
  class UserEditingScope < Scope
    def resolve
      return scope.all if user.has_ability?(:edit_user_groups)
      scope.where.not(id: UserGroupAbility.edit_user_groups.select(:user_group_id))
    end
  end

  def index?
    can_edit_user_groups?
  end

  def show?
    can_edit_user_groups?
  end

  def create?
    can_edit_user_groups?
  end

  def update?
    can_edit_user_groups?
  end

  def destroy?
    can_edit_user_groups?
  end

  private
    def can_edit_user_groups?
      user.has_ability?(:edit_user_groups)
    end
end
