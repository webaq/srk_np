class Admin::ComplaintPolicy < ApplicationPolicy
  def index?
    user.has_ability?(:edit_complaints)
  end
end
