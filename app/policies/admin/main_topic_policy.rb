class Admin::MainTopicPolicy < ApplicationPolicy
  def index?
    user.has_ability?(:edit_main_topics)
  end

  def show?
    index?
  end

  def create?
    index?
  end

  def update?
    index?
  end

  def destroy?
    index?
  end
end
