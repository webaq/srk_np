class Admin::PressReleasesReportPolicy < ApplicationPolicy
  def show?
    user.admin?
  end
end
