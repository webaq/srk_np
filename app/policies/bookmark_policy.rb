class BookmarkPolicy < ApplicationPolicy
  def create?
    return true if user.admin?
    return true if user_repostable?
    false
  end

  def destroy?
    return true if user.admin?
    return true if user == record.user
    false
  end

  private
    def user_repostable?
      (record.press_release.national_project_ids & user.repostable_national_project_ids).any?
    end
end
