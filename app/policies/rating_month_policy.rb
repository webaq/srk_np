class RatingMonthPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.admin?
        scope.all
      else
        scope.where(published: true)
      end
    end
  end

  def index?
    true
  end

  def create?
    user.admin?
  end

  def show?
    return true if user.admin?
    return true if record.published? && user.has_ability?(:show_rating)
    false
  end

  def update?
    user.admin?
  end

  def destroy?
    user.admin?
  end
end
