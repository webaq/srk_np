class NationalProjectPolicy < ApplicationPolicy
  class ViewableScope < Scope
    def resolve
      return scope.all if user.admin?
      return scope.where(id: user.viewable_national_project_ids)
    end
  end

  def index?
    user.admin?
  end

  def show?
    user.admin?
  end

  def create?
    user.admin?
  end

  def update?
    user.admin?
  end

  def destroy?
    user.admin?
  end
end
