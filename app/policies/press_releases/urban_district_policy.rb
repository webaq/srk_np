class PressReleases::UrbanDistrictPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      return scope.where(id: user.urban_district_id) if user.urban_district_id
      scope.all
    end
  end
end
