class PressReleases::MeasurePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.admin?
        scope.all
      else
        scope.where(federal_project: user.can_create_federal_project_ids)
      end
    end
  end
end
