class TutorialPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      return scope.all if user.admin?
      return scope.where(creatable_permitted: true) if user.press_office?
      return scope.where(repostable_permitted: true) if user.media?
      scope.none
    end
  end

  def index?
    user.admin? || user.press_office? || user.media?
  end

  def show?
    return true if user.admin?
    return true if user.press_office? && record.creatable_permitted?
    return true if user.media? && record.repostable_permitted?
    false
  end
end
