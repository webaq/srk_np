class ImpressionPolicy < ApplicationPolicy
  def create?
    return false if same_region
    true
  end

  private
    def same_region
      user.organization&.federal_subject && record.press_release.federal_subjects.include?(user.organization&.federal_subject)
    end
end
