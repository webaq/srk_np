class PressReleasePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      return scope.all if user.admin?

      my_scope = scope.all

      if user.federal_subject_id && !user.has_ability?(:show_other_regions_press_releases)
        my_scope = my_scope.where(id: PressReleaseFederalSubject.where(federal_subject: user.federal_subject_id).select(:press_release_id))
      end

      if user.show_unpublished_press_releases? || user.moderatable_national_project_ids.any?
        return my_scope
      end

      my_scope = my_scope.where(id: PressRelease.where(user_id: user.id).or(PressRelease.approved.published))

      my_scope
    end
  end

  class RecommendedApprovableScope < Scope
    def resolve
      return scope.all if user.admin || user&.organization&.list_all_unapproved_press_releases?

      return scope.where.not(federal_subjects_count: 1) if user.organization&.federal_authority?

      return scope.where(id: scope
        .joins(:federal_subjects)
        .where(pre_approved_at: nil)
        .group(:id)
        .having('array_agg(federal_subjects.id) = ARRAY[?]::bigint[]', user.federal_subject_id)
      )
    end
  end

  def index?
    true
  end

  def new?
    return true if user.admin?
    return true if user.creatable_national_project_ids.any?
    false
  end

  def show?
    return true if user.admin?
    return true if author?

    if user.federal_subject_id && !user.has_ability?(:show_other_regions_press_releases)
      return false unless record.federal_subjects.include?(user.federal_subject)
    end

    return true if user.moderatable_national_project_ids.any? # user_moderatable?
    return true if record.approved? && (record.published? || user.show_unpublished_press_releases?)

    false
  end

  def create?
    return true if user.admin?
    return true if user_creatable?
    false
  end

  def update?
    return true if user.admin?
    return true if record.draft? && user_moderatable?
    return true if record.draft? && author?
    false
  end

  def destroy?
    return true if user.admin?
    return true if user_moderatable?
    return true if record.draft? && author?
    false
  end

  def pre_approve?
    return false unless record.max_approved_at.future?
    return false unless record.user.organization&.federal_subject == user.federal_subject
    return false unless user_moderatable?
    return false unless user.regional_moderator?
    true
  end

  def approve?
    return true if user.admin?
    return false unless record.max_approved_at.future?

    return true if user.organization&.federal_authority? && user_moderatable?

    if record.user.federal_subject
      return true if record.user.federal_subject.regional_moderated? && user.federal_subject == record.user.federal_subject && user.regional_moderator? && user_moderatable?
    end

    # if user_moderatable?
    # return true if user.all_federal_subjects?
    # return true if record.federal_subject_ids.difference([user.federal_subject_id]).empty?
    # end
    false
  end

  def award?
    user.admin?
  end

  def view_award?
    !user.organization&.media?
  end

  def bookmark?
    return true if user.admin?
    return true if user_repostable?
    false
  end

  def comments?
    return true if user.admin?
    return true if user_moderatable?
    return true if author?
    false
  end

  def show_reposts?
    return true if user.admin?
    return true if user_moderatable?
    return true if author?
    return true if user_repostable?
    false
  end

  def show_bookmarked_users?
    return true if user.admin?
    return true if author?
    false
  end

  def show_reposted_users?
    return true if user.admin?
    return true if author?
    false
  end

  def show_logs?
    user.admin?
  end

  def show_moderators?
    user.admin?
  end

  def update_main_topic?
    user.admin? || user.editor?
  end

  def return_to_author?
    return false if record.returned_at
    return false if author?
    return false unless (approve? || pre_approve?)
    true
  end

  def export?
    user.has_ability?(:export_press_releases)
  end

  private
    def author?
      record.user_id == user.id
    end

    def user_creatable?
      (record.national_project_ids & user.creatable_national_project_ids).any?
    end

    def user_moderatable?
      (record.national_project_ids & user.moderatable_national_project_ids).any?
    end

    def user_repostable?
      (record.national_project_ids & user.repostable_national_project_ids).any?
    end

    def user_viewable?
      (record.national_project_ids & user.viewable_national_project_ids).any?
    end
  end
