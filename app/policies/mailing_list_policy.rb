class MailingListPolicy < ApplicationPolicy
  def index?
    user.organization && !user.organization.media? && user.organization.id == 1
  end

  def create?
    index?
  end

  def update?
    user.organization == record.organization
  end

  def destroy?
    update?
  end
end