class FaqItemPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      return scope.all if user.admin?

      if user.organization&.kind == 'media'
        scope.where(media_allowed: true)
      else
        scope.where(non_media_allowed: true)
      end
    end
  end
end
