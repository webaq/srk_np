class PressReleaseEditing::NationalProjectPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      return scope.all if user.admin?
      return scope.where(id: (user.creatable_national_project_ids + user.moderatable_national_project_ids))
    end
  end
end
