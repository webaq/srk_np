class PressReleaseEditing::FederalProjectPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      return scope.where(available: true) if user.admin?
      return scope.where(available: true, id: (user.can_create_federal_project_ids | user.can_moderate_federal_project_ids))
    end
  end
end
