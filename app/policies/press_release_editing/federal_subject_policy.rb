class PressReleaseEditing::FederalSubjectPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      # return scope.where(id: user.federal_subject_id) if user.federal_subject
      return scope.all if user.admin?
      return scope.all if user.organization&.federal_authority?
      return scope.all if user.organization&.other? && user.organization.federal_subject.nil?
      return scope.where(id: user.federal_subject_id)
    end
  end
end
