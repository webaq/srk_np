class CommentPolicy < ApplicationPolicy
  def create?
    return true if user.admin?
    return true if record.press_release.user == user
    return true if (record.press_release.national_project_ids & user.moderatable_national_project_ids).any?
    false
  end

  def destroy?
    user.admin? || user == record.user
  end
end
