class PressReleaseLikePolicy < ApplicationPolicy
  def create?
    return true unless same_region
    false
  end

  private
    def same_region
      user.organization&.federal_subject && record.press_release.federal_subjects.include?(user.organization&.federal_subject)
    end
end
