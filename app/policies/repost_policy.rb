class RepostPolicy < ApplicationPolicy
  class Scope < Scope
    attr_reader :user, :scope, :press_release

    def initialize(user, scope, press_release)
      @user = user
      @scope = scope
      @press_release = press_release
    end

    def resolve
      if user.admin? || user.id == press_release.user_id || (press_release.national_project_ids & user.moderatable_national_project_ids).any?
        scope.all
      else
        scope.where(user_id: user.id)
      end
    end
  end

  def create?
    return true if user.admin?
    return true if user_repostable?
    false
  end

  def destroy?
    user.admin? || user.id == record.user_id
  end

  private
    def user_repostable?
      (record.press_release.national_project_ids & user.repostable_national_project_ids).any?
    end
end
