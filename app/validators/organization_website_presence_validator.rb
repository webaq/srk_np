class OrganizationWebsitePresenceValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    record.errors[attribute] << (options[:message] || "не может быть добавлена, если у организации не заполнен адрес сайта") unless record.user&.organization&.organization_websites.present?
  end
end