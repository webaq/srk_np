class OrganizationWebsiteMatchValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    (record&.user&.organization&.organization_websites || []).each do |organization_website|
      return if url_valid?(organization_website.address, value)
    end

    record.errors[attribute] << (options[:message] || "не соответствует адресу организации")
  end

  def url_valid?(organization_url, url)
    url = Addressable::URI.parse(url) rescue false
    organization_url = Addressable::URI.parse(organization_url) rescue false
    url.host == organization_url.host
  end
end