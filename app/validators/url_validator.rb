class UrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless url_valid?(value)
      record.errors.add(attribute, :invalid, message: (options[:message] || "должна быть вида http://... или https://..."))
    end
  end

  def url_valid?(url)
    url = Addressable::URI.parse(url)
    url.scheme.in? %w[http https]
  rescue
    false
  end
end