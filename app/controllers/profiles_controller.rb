class ProfilesController < ApplicationController
  include MainTopicsListable
  before_action :set_compact_header

  def show
    @message_deliveries = current_user.message_deliveries.published.includes(:message).order(published_at: 'DESC')
  end

  def settings
  end

  def update
    if current_user.update(user_params)
      respond_to do |format|
        format.html do
          # sign_out(current_user)
          bypass_sign_in current_user, scope: :user
          flash.notice = 'Изменения сохранены.'
          redirect_back fallback_location: settings_profile_url
        end
        format.js { head :ok }
      end
    else
      respond_to do |format|
        format.html do
          flash.alert = 'Ошибка.'
          redirect_back fallback_location: settings_profile_url
        end
      end
    end
  end

  private
    def set_compact_header
      @compact_header = true
    end

    def user_params
      if params[:user][:password].blank?
        params[:user].delete(:password)
        params[:user].delete(:password_confirmation)
      end

      params.require(:user).permit([
        :first_name,
        :last_name,
        :contact_phone,
        :avatar,
        :contact_position,
        :password,
        :password_confirmation,
        :email,
        :receive_notifications_by_email,
      ])
    end
end