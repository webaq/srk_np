class ApplicationController < ActionController::Base
  include Pundit

  before_action :http_authenticate, if: ->{ ENV['HTTP_AUTHENTICATE_USERNAME'].present? }
  before_action :authenticate_user!
  before_action :create_visit, if: -> { current_user }

  before_action do
    if current_user && current_user.admin?
      Rack::MiniProfiler.authorize_request
    end
  end

  layout 'user'

  private
    def create_visit
      current_user.visits.find_or_create_by!(created_on: Date.today)
    end

    def http_authenticate
      authenticate_or_request_with_http_basic('Restricted Access!') do |username, password|
        username == ENV['HTTP_AUTHENTICATE_USERNAME'] && password == ENV['HTTP_AUTHENTICATE_PASSWORD']
      end
    end
end
