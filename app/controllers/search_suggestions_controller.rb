class SearchSuggestionsController < ApplicationController
  def index
    term = params[:q]

    if term.present?
      @q = Tag.ransack(name_cont: term.gsub(' ', ''))
      @tags = @q.result.order(:name).limit(5)
    else
      @tags = []
    end

    render partial: 'autocomplete', collection: @tags, as: :tag
  end
end