class NationalProjectsController < ApplicationController
  include MainTopicsListable

  def index
    @national_projects = NationalProject.with_attached_avatar.order(:name)
    @national_project_branding_link = params[:national_project_branding_link]
  end

  def show
    @national_project = NationalProject.find(params[:id])

    respond_to do |format|
      format.html

      format.docx do
        render docx: 'show', filename: "#{@national_project.short_name}.docx"
      end
    end
  end

  def branding
    @national_project = NationalProject.find(params[:national_project_id])
  end
end