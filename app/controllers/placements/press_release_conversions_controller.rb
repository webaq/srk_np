class Placements::PressReleaseConversionsController < ApplicationController
  def create
    placement = Placement.find(params[:placement_id])
    authorize placement, :convert_to_press_release?

    press_release = PlacementPressReleaseBuilder.new(placement).press_release

    Placement.transaction do
      press_release.save!
      placement.destroy!
    end

    redirect_to admin_placements_url
  end
end