class PolicyAgreementsController < ApplicationController
  def create
    PublicActivity::Activity.create!(trackable: current_user, key: "policy_agreement.shown")
  end
end