class PlacementsController < ApplicationController
  before_action :set_compact_header
  before_action :set_placement, only: [:show, :edit, :update, :destroy]

  def new
    @placement = current_user.placements.new
    authorize @placement
    render :edit
  end

  def create
    @placement = current_user.placements.new(placement_params)
    if @placement.save
      redirect_to placement_url(@placement)
    else
      render :edit
    end
  end

  def show
  end

  def update
    if @placement.update(placement_params)
      redirect_to placement_url(@placement)
    else
      render :edit
    end
  end

  def destroy
    if @placement.destroy
      redirect_to press_releases_url
    else
      render :edit
    end
  end

  private
    def set_placement
      @placement = Placement.find(params[:id])
      authorize @placement
    end

    def set_compact_header
      @compact_header = true
    end

    def placement_params
      params.require(:placement).permit([
        { national_project_ids: [] },
        { federal_subject_ids: [] },
        :title,
        :content,
        { images: [] },
      ])
    end
end