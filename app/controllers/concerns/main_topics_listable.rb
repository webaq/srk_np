module MainTopicsListable
  extend ActiveSupport::Concern

  included do
    helper_method :main_topics_list
    helper_method :main_topics_press_releases
  end

  def main_topics_list
    @main_topics_list ||= MainTopic.published.order(:starts_at).limit(3)
  end

  # def main_topics_press_releases
  #   @main_topics_press_releases ||= policy_scope(PressRelease)
  #     .approved
  #     .published
  #     .joins(:show_activities)
  #     .where(published_at: Date.today.to_time...)
  #     .group(:id)
  #     .order('COUNT(activities.id) DESC')
  #     .order(published_at: 'DESC')
  #     .limit(3)
  # end
  def main_topics_press_releases
    @main_topics_press_releases ||= PressRelease.where(id: TopListItem.select(:press_release_id)).order(published_at: 'DESC')
  end
end