class TutorialsController < ApplicationController
  before_action :set_tutorials

  def index
    tutorial = @tutorials.first

    if tutorial
      redirect_to tutorial_url(tutorial)
    else
      redirect_to press_releases_url
    end
  end

  def show
    @tutorial = @tutorials.find(params[:id])
    authorize @tutorial
  end

  private
    def set_tutorials
      @tutorials = policy_scope(Tutorial).order(:name)
    end
end