class RegistrationsController < ApplicationController
  skip_before_action :authenticate_user!

  def new
    @registration = Registration.new
  end

  def create
    @registration = Registration.new(registration_params)
    if @registration.save
      NotifierMailer.registration_submitted(@registration.email).deliver
    else
      render :new
    end
  end

  def organizations
    organizations = Organization.where('name ilike ?', "%#{params[:q]}%").order(:name).limit(5)
    render partial: 'organization', collection: organizations
  end

  private
    def registration_params
      params.require(:registration).permit [
        :kind,
        :federal_subject_id,
        :organization_name,
        :email,
        :first_name,
        :last_name,
        :contact_position,
        :contact_phone,
        :website,
        :media_subscribers,
        :password,
        :password_confirmation
      ]
    end
end