class WelcomeController < ApplicationController
  include MainTopicsListable

  def index
    redirect_to press_releases_url
  end

  def help; end

  def faq
    @faq_items = policy_scope(FaqItem).order(:position)
  end
end