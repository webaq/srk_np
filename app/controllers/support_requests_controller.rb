class SupportRequestsController < ApplicationController
  skip_before_action :authenticate_user!

  def new
    current_user
    @support_request = SupportRequest.new
  end

  def create
    @support_request = current_user.support_requests.new(support_request_params)
    if @support_request.save
      NotifierMailer.support_request(@support_request, SupportEmail.pluck(:email)).deliver_later
    else
      render :reload_form
    end
  end

  private
    def support_request_params
      params.require(:support_request).permit(:subject, :body)
    end

    def current_user
      current_user = User.find_by!(email: params[:email])
    end
end