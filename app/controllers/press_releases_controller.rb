class PressReleasesController < ApplicationController
  include Pagy::Backend
  include MainTopicsListable

  def index
    authorize PressRelease

    @safe_params = params.permit([
      :q,
      :kind,
      :period,
      { national_project_ids: [] },
      :federal_project_id,
      :dates,
      :federal_subject_id,
      :main_topic_id,
      :organization_id,
      :approvement,
      :tag_id,
      :category_id,
    ])

    if @safe_params[:kind].blank? && @safe_params[:q].blank? # no filters in search mode
      @safe_params[:kind] = 'all'
    end

    @safe_params[:approvement] = params.fetch(:approvement, 'pre_approved')

    press_releases = policy_scope(PressRelease)
    press_releases = press_releases.search_full_text(@safe_params[:q]) if @safe_params[:q].present?
    press_releases_for_totals = press_releases

    press_releases = case @safe_params[:kind]
    when 'all_unapproved'
      PressReleasePolicy::RecommendedApprovableScope.new(current_user, press_releases).resolve.moderatable_by_user(current_user).unapproved_for_user(current_user).filter_out_unpre_approved(current_user.admin? && @safe_params[:approvement] == 'pre_approved').where(returned_at: nil)
    when 'media_works'
      press_releases.where('approved_at IS NOT NULL AND published_at < ?', Time.current)
    when 'returned'
      press_releases.where.not(returned_at: nil)
    when 'my'
      press_releases.where(user_id: current_user.id)
    when 'my_unapproved'
      press_releases.where(user_id: current_user.id).unapproved
    when 'my_approved'
      press_releases.where(user_id: current_user.id).approved
    when 'my_bookmarked'
      press_releases.where(user_id: current_user.id).bookmarked
    when 'main_topics'
      press_releases.where.not(main_topic_id: nil)
    when 'i_bookmarked'
      press_releases.where(id: current_user.bookmarks.select(:press_release_id))
    when 'i_reposted'
      press_releases.where(id: current_user.reposts.select(:press_release_id))
    when 'event'
      press_releases.where.not(starts_at: nil)
    else
      press_releases
    end

    @press_releases_filter = PressReleasesFilter.new(current_user, press_releases, press_releases_filter_params)

    if !params.has_key?(:federal_subject_id) && current_user.organization&.media? && current_user.organization.federal_subject
      @press_releases_filter.federal_subject_id = current_user.organization.federal_subject_id
      @safe_params[:federal_subject_id] = @press_releases_filter.federal_subject_id
    end

    respond_to do |format|
      format.html do
        @press_releases = @press_releases_filter.press_releases
          .order(published_at: 'DESC')
          .includes(national_projects: { avatar_attachment: :blob })
          .includes(:federal_subjects)
          .includes(:urban_district)
          .includes(:user)

        @pagy, @press_releases = pagy(@press_releases, items: 10, link_extra: 'data-pjax data-smooth-scrollto="#list"')
        @press_releases = @press_releases.to_a

        @totals = Hash.new do |hash, key|
          case key
          when :all_unapproved
            hash[key] = PressReleasePolicy::RecommendedApprovableScope.new(current_user, press_releases_for_totals).resolve.moderatable_by_user(current_user).unapproved_for_user(current_user).filter_out_unpre_approved(current_user.admin? && @safe_params[:approvement] == 'pre_approved').where(returned_at: nil).count
          # when :media_works
          #   hash[key] = press_releases.where('approved_at IS NOT NULL AND published_at < ?', Time.current).count
          when :returned
            hash[key] = press_releases_for_totals.where.not(returned_at: nil).count
          when :my_unapproved
            hash[key] = press_releases_for_totals.where(user_id: current_user.id).unapproved.count
          when :my_approved
            hash[key] = press_releases_for_totals.where(user_id: current_user.id).approved.count
          when :my_bookmarked
            hash[key] = press_releases_for_totals.where(user_id: current_user.id).bookmarked.count
          end
        end

        @show_policy_agreement_popup = PublicActivity::Activity.where(trackable: current_user, key: "policy_agreement.shown").none?

        unless @show_policy_agreement_popup
          unless current_user.onboarded_at
            current_user.touch(:onboarded_at)
            @onboarding_tutorial = current_user.relevant_onboarding
          end
        end

        if !@show_policy_agreement_popup && @onboarding_tutorial.nil?
          @notification_popup = NotificationPopup.for_user(current_user)
          @notification_popup.create_activity(key: 'show', owner: current_user) if @notification_popup
        end
      end

      format.js do
        if params[:ok]
          PressReleasesExportJob.perform_later(current_user.id, @safe_params)
          render :close_modal
        end
      end

      format.json do
        @press_releases = @press_releases_filter.press_releases
          .order(published_at: 'DESC')
          .limit(10)
      end
    end
  end

  def new
    @press_release = current_user.press_releases.new(params.permit(:main_topic_id))
    authorize @press_release

    available_federal_subjects = policy_scope([:press_release_editing, FederalSubject])
    if available_federal_subjects.size == 1
      @press_release.federal_subjects = available_federal_subjects
    end

    unless PublicActivity::Activity.where(trackable: current_user, key: "categories_tutorial.shown").any?
      PublicActivity::Activity.create!(trackable: current_user, key: "categories_tutorial.shown")
      @show_categories_tutorial = true
    end

    render :edit
  end

  def create
    @press_release = current_user.press_releases.new(press_release_params)
    authorize @press_release if @press_release.valid?

    if @press_release.save(context: :press_release_submit)
      @press_release.create_activity key: 'press_release.create', owner: current_user

      if policy(@press_release).approve?
        @press_release.approve
        @press_release.create_activity key: 'press_release.approve', owner: current_user
      end
      # @press_release.federal_project.subscribed_users.select { |user| user.can_moderate_federal_project_ids.include?(@press_release.federal_project_id) }.each do |user|
      #   NotifierMailer.press_release_created(user, @press_release).deliver_later
      # end
      redirect_to @press_release, notice: 'Инфоповод добавлен'
    else
      render :edit
    end
  end

  def show
    @press_release = PressRelease.with_rich_text_content_and_embeds.includes(:federal_subjects).find(params[:id])
    authorize @press_release

    if current_user != @press_release.user && @press_release.show_activities.where({
        owner_id: current_user.id
      }).none?
      @press_release.create_activity key: 'press_release.show', owner: current_user
    end

    respond_to do |format|
      format.html do
        @reposts = RepostPolicy::Scope.new(current_user, Repost, @press_release).resolve
          .where(press_release_id: @press_release.id)
          .includes(:user)
          .order(created_at: 'DESC')
      end

      format.ics do
        render body: @press_release.to_ics(press_release_url(@press_release)), mime_type: Mime::Type.lookup("text/calendar")
      end

      format.docx do
        render docx: 'show', filename: "#{helpers.convert_to_filename(@press_release.title)}.docx"
      end
    end
  end

  def edit
    @press_release = PressRelease.find(params[:id])
    authorize @press_release
  end

  def update
    @press_release = PressRelease.find(params[:id])
    authorize @press_release if @press_release.valid?

    if @press_release.update_on_context(press_release_params.merge(returned_at: nil), :press_release_submit)
      @press_release.create_activity key: 'press_release.update', owner: current_user
      redirect_to @press_release#, notice: 'Пресс-релиз сохранён.'
    else
      render :edit
    end
  end

  def destroy
    @press_release = PressRelease.find(params[:id])
    authorize @press_release

    @press_release.create_activity key: 'press_release.destroy', owner: current_user
    @press_release.discard
    redirect_to press_releases_url#, notice: 'Пресс-релиз удалён.'
  end

  private
    def press_release_params
      fields = [
        :press_release_value_id,
        { national_project_ids: [] },
        { federal_project_ids: [] },
        { selected_federal_subject_ids: [] },
        :title,
        :content,
        { images: [] },
        {
          experts_attributes: [
            :id,
            :name,
            :_destroy
          ]
        },
        :main_topic_id,
        { tag_ids: [] },
      ]

      fields << :late_published_at if params.dig(:press_release, :use_late_published_at) == '1'
      fields << :starts_at if params.dig(:press_release, :use_starts_at) == '1'
      fields << { press_release_relations_attributes: [:id, :_destroy, :related_press_release_id] } if current_user.admin?
      fields << :value_nullified if current_user.admin?

      params[:press_release][:main_topic_id] = nil if params[:press_release][:use_main_topic] == '0'

      params.require(:press_release).permit(fields).merge(authors_contacts_published: true)
    end

    def press_releases_filter_params
      params.permit([
        :dates,
        :federal_subject_id,
        :federal_project_id,
        :main_topic_id,
        { national_project_ids: [] },
        :organization_id,
        :period,
        :tag_id,
        :category_id,
      ])
    end
end