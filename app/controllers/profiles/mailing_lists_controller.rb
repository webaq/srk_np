class Profiles::MailingListsController < ApplicationController
  include MainTopicsListable
  before_action :set_compact_header
  before_action :set_mailing_list, only: [:edit, :update, :destroy]

  def index
    authorize MailingList
    @mailing_lists = current_user.organization.mailing_lists.order(:name)
  end

  def new
    @mailing_list = current_user.organization.mailing_lists.new
    authorize @mailing_list
    render :edit
  end

  def create
    @mailing_list = current_user.organization.mailing_lists.new(mailing_list_params)
    authorize @mailing_list

    respond_to do |format|
      if @mailing_list.save
        format.html { redirect_to profile_mailing_lists_url, notice: 'Список рассылки создан.' }
        # format.json { render :show, status: :created, location: @support_email }
      else
        format.html { render :edit }
        # format.json { render json: @support_email.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @mailing_list.update(mailing_list_params)
        format.html { redirect_to profile_mailing_lists_url, notice: 'Список рассылки обновлён.' }
        # format.json { render :show, status: :ok, location: @support_email }
      else
        format.html { render :edit }
        # format.json { render json: @support_email.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @mailing_list.destroy
    respond_to do |format|
      format.html { redirect_to profile_mailing_lists_url, notice: 'Список рассылки удалён.' }
      # format.json { head :no_content }
    end
  end

  private
    def set_compact_header
      @compact_header = true
    end

    def set_mailing_list
      @mailing_list = current_user.organization.mailing_lists.find(params[:id])
      authorize @mailing_list
    end

    def mailing_list_params
      params.require(:mailing_list).permit([
        :name,
        mailing_list_items_attributes: [
          :id, :email, :_destroy
        ]
      ])
    end
end