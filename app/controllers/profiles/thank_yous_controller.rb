class Profiles::ThankYousController < ApplicationController
  def show
    @disposition = 'inline'

    authorize current_user, :thank_you?
    render pdf: 'blagodarstvennoe_pismo', layout: 'thank_you_note', margin: { top: 0, bottom: 0, left: 0, right: 0 }, disposition: @disposition
  end

  def download
    @disposition = 'attachment'

    authorize current_user, :thank_you?
    render pdf: 'blagodarstvennoe_pismo', layout: 'thank_you_note', margin: { top: 0, bottom: 0, left: 0, right: 0 }, disposition: @disposition
  end
end