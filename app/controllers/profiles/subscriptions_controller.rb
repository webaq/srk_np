class Profiles::SubscriptionsController < ApplicationController
  include MainTopicsListable
  before_action :set_compact_header

  def new
    current_user.build_subscription
    render :edit
  end

  def create
    current_user.build_subscription(subscription_params)
    if current_user.subscription.save
      current_user.subscription.create_activity key: 'create', owner: current_user
      NotifierMailer.with(subscription_id: current_user.subscription.id).subscription_created.deliver_later
      redirect_to profile_subscription_url
    else
      render :edit
    end
  end

  def show; end

  def update
    if current_user.subscription.update(subscription_params)
      respond_to do |format|
        format.html do
          redirect_to profile_subscription_url, notice: 'Изменения сохранены.'
        end
        format.js { head :ok }
      end
    else
      respond_to do |format|
        format.html do
          render :edit
        end
      end
    end
  end

  def destroy
    current_user.subscription.create_activity key: 'destroy', owner: current_user
    current_user.subscription.destroy!
    redirect_to profile_subscription_url
  end

  private
    def set_compact_header
      @compact_header = true
    end

    def subscription_params
      p = params.require(:subscription).permit([
        { federal_subject_ids: [] },
        { organization_ids: [] },
        { national_project_ids: [] },
        { tag_ids: [] },
        :frequency,
        :activated,
      ])

      p[:federal_subject_ids] = [] if p[:federal_subject_ids]&.include?('0')
      p[:organization_ids] = [] if p[:organization_ids]&.include?('0')
      p[:national_project_ids] = [] if p[:national_project_ids]&.include?('0')
      p[:tag_ids] = [] if p[:tag_ids]&.include?('0')

      p
    end
end