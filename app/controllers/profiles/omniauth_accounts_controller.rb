class Profiles::OmniauthAccountsController < ApplicationController
  before_action :set_omniauth_account

  def destroy
    @provider = @omniauth_account.provider
    current_user.omniauth_accounts.destroy(@omniauth_account)
  end

  private
    def set_omniauth_account
      @omniauth_account = current_user.omniauth_accounts.find(params[:id])
    end
end