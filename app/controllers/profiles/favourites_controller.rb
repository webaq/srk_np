class Profiles::FavouritesController < ApplicationController
  include MainTopicsListable

  before_action :set_compact_header
  before_action :set_press_release, only: [:destroy]

  def index
    @press_releases = current_user.favourite_press_releases.order(published_at: 'DESC')
  end

  def destroy
    current_user.favourite_press_releases.destroy(@press_release)
  end

  private
    def set_compact_header
      @compact_header = true
    end

    def set_press_release
      @press_release = PressRelease.find(params[:id])
    end
end