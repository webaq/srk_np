class MainTopicsController < ApplicationController
  include MainTopicsListable

  def index
    @main_topics = MainTopic.published.order(:starts_at)
  end

  def show
    @main_topic = policy_scope(MainTopic).find(params[:id])
    authorize @main_topic

    @press_releases = policy_scope(PressRelease)
      .where(main_topic: @main_topic)
      .order(published_at: 'DESC')
  end
end