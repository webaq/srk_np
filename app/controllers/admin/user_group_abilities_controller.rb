class Admin::UserGroupAbilitiesController < AdminController
  before_action :set_user_group, only: [:new, :create]
  before_action :set_user_group_ability, only: [:edit, :update, :destroy]

  def new
    @user_group_ability = @user_group.user_group_abilities.new
    authorize [:admin, @user_group_ability]
    render :edit
  end

  def create
    @user_group_ability = @user_group.user_group_abilities.new(user_group_ability_params)
    authorize [:admin, @user_group_ability]

    if @user_group_ability.save
      redirect_to [:admin, @user_group_ability.user_group]
    else
      render :edit
    end
  end

  def edit
  end

  def update
    if @user_group_ability.update(user_group_ability_params)
      redirect_to [:admin, @user_group_ability.user_group]
    else
      render :edit
    end
  end

  def destroy
    if @user_group_ability.destroy
      redirect_to [:admin, @user_group_ability.user_group]
    else
      render :edit
    end
  end

  private
    def set_user_group
      @user_group = UserGroup.find(params[:user_group_id])
    end

    def set_user_group_ability
      @user_group_ability = UserGroupAbility.find(params[:id])
      authorize [:admin, @user_group_ability]
    end

    def user_group_ability_params
      params.require(:user_group_ability).permit(:ability)
    end
end