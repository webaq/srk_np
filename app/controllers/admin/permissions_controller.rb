class Admin::PermissionsController < AdminController
  def edit
    @user = User.find(params[:user_id])
    authorize @user, :update?

    @national_projects = NationalProject.includes(:federal_projects).order(:name).order('federal_projects.name')
  end

  def update
    @user = User.find(params[:user_id])
    authorize @user, :update?

    if @user.update(user_params)
      redirect_to [:admin, :users], notice: 'Изменения сохранены.'
    else
      flash.now[:alert] = @user.errors.full_messages.join(', ')
      render :show
    end
  end

  private
    def user_params
      params.require(:user).permit([
        :federal_subject_id,
        :regional_moderator,
        :urban_district_id,
        viewable_federal_project_ids: [],
        creatable_federal_project_ids: [],
        moderatable_federal_project_ids: [],
        repostable_federal_project_ids: [],
      ])
    end
end