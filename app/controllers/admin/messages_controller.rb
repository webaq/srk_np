class Admin::MessagesController < AdminController
  before_action :set_message, only: [:show, :edit, :update, :destroy]
  before_action :set_message_deliveries, only: [:show]

  def index
    authorize [:admin, Message]
    @messages = Message.includes(:sender).order(created_at: 'DESC')
    @pagy, @messages = pagy(@messages, items: 10)
  end

  def new
    @message = current_user.sended_messages.new
    authorize [:admin, @message]
  end

  def create
    @message = current_user.sended_messages.new(message_params)
    authorize [:admin, @message]

    if @message.save(context: :manual_create)
      MessageDeliveriesCreationJob.perform_later(@message)
      redirect_to [:admin, @message], notice: 'Уведомление отправляется.'
    else
      render :edit
    end
  end

  def show; end

  def edit; end

  def update
    if @message.update(message_params)
      redirect_to [:admin, @message], notice: 'Уведомление обновлено.'
    else
      render :edit
    end
  end

  def destroy
    if @message.destroy
      redirect_to admin_messages_url, notice: 'Уведомление удалено.'
    else
      render :edit
    end
  end

  private
    def set_message
      @message = Message.find(params[:id])
      authorize [:admin, @message]
    end

    def set_message_deliveries
      @message_deliveries = @message
        .message_deliveries
        .includes(user: :organization)
        .order('organizations.name')
        .order('users.last_name')
      @message_deliveries_pagy, @message_deliveries = pagy(@message_deliveries, items: 10)
    end

    def message_params
      fields = [
        :addressee,
        :subject,
        :content,
        national_project_ids: [],
        organization_ids: []
      ]

      fields << :late_published_at if params.dig(:message, :use_late_published_at) == '1'

      params.require(:message).permit(fields)
    end
end
