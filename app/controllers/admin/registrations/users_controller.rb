class Admin::Registrations::UsersController < AdminController
  before_action :set_registration
  before_action :set_new_user

  def create
    if @new_user.valid?(:registration_confirmation)
      if @registration.update(registration_params)
        PublicActivity::Activity.create!(trackable: @registration.user, key: "policy_agreement.shown")
        if @registration.user.organization&.media?
          NotifierMailer.welcome_media(@registration.user.email).deliver_later
        else
          NotifierMailer.welcome_others(@registration.user.email).deliver_later
        end
      else
        render js: "alert('#{@registration.errors.full_messages.join('.')}')"
      end
    else
      render js: "alert('#{@new_user.errors.full_messages.join('.')}')"
    end
  end

  private
    def set_registration
      authorize [:admin, User]
      @registration = Registration.find(params[:registration_id])
    end

    def set_new_user
      @new_user = User.new(user_params)
    end

    def registration_params
      {
        confirmed_user: current_user,
        confirmed_at: Time.current,
        user: @new_user,
      }
    end

    def user_params
      if params[:user][:organization_id].present?
        params[:user].delete(:organization_attributes)
      end

      params.require(:user).permit([
        :organization_id,
        :federal_subject_id,
        :email,
        :first_name,
        :last_name,
        :contact_position,
        :contact_phone,
        { creatable_federal_project_ids: [] },
        { moderatable_federal_project_ids: [] },
        { viewable_federal_project_ids: [] },
        { repostable_federal_project_ids: [] },
        { user_group_ids: [] },
        { organization_attributes: [
          :federal_subject_id,
          :kind,
          :name,
          :media_subscribers,
          :federal_subject_id,
          organization_websites_attributes: [ :address ]
        ]}
      ]).merge({
        password: SecureRandom.base58,
        encrypted_password: @registration.encrypted_password,
        confirmed_at: Time.current
      })
    end
end