class Admin::MainTopicsController < AdminController
  before_action :set_main_topic, only: [:edit, :update, :destroy]

  # GET /admin/main_topics
  # GET /admin/main_topics.json
  def index
    authorize [:admin, MainTopic]
    @main_topics = MainTopic.order(starts_at: 'DESC')

    @archived_mode = params[:archived] == '1'

    @main_topics = unless @archived_mode
      @main_topics.where(ends_at: [nil, Time.current...])
    else
      @main_topics.where('ends_at < ?', Time.current)
    end
  end

  # GET /admin/main_topics/1
  # GET /admin/main_topics/1.json
  def show
  end

  # GET /admin/main_topics/new
  def new
    authorize [:admin, MainTopic]
    @main_topic = MainTopic.new
  end

  # GET /admin/main_topics/1/edit
  def edit
  end

  # POST /admin/main_topics
  # POST /admin/main_topics.json
  def create
    authorize [:admin, MainTopic]
    @main_topic = MainTopic.new(main_topic_params)

    respond_to do |format|
      if @main_topic.save
        format.html { redirect_to [:admin, :main_topics], notice: 'Главная тема создана.' }
        # format.json { render :show, status: :created, location: @main_topic }
      else
        format.html { render :new }
        # format.json { render json: @main_topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/main_topics/1
  # PATCH/PUT /admin/main_topics/1.json
  def update
    respond_to do |format|
      if @main_topic.update(main_topic_params)
        format.html { redirect_to [:admin, :main_topics], notice: 'Главная тема обновлена.' }
        # format.json { render :show, status: :ok, location: @admin_main_topic }
      else
        format.html { render :edit }
        # format.json { render json: @admin_main_topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/main_topics/1
  # DELETE /admin/main_topics/1.json
  def destroy
    @main_topic.destroy
    respond_to do |format|
      format.html { redirect_to admin_main_topics_url, notice: 'Главная тема удалена.' }
      # format.json { head :no_content }
    end
  end

  def archive
    main_topic = MainTopic.find(params[:main_topic_id])
    main_topic.update(ends_at: Time.current)
    redirect_to admin_main_topics_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_main_topic
      @main_topic = MainTopic.find(params[:id])
      authorize [:admin, @main_topic]
    end

    # Only allow a list of trusted parameters through.
    def main_topic_params
      params.require(:main_topic).permit([
        :name,
        :content,
        :starts_at,
        :ends_at,
        { national_project_ids: [] }
      ])
    end
end
