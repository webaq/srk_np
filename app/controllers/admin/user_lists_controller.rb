class Admin::UserListsController < AdminController
  def new
    @user_list = UserList.new
    @user_list.users << User.new
  end

  def create
    @user_list = UserList.new(user_list_params)

    if @user_list.valid?
      @user_list.save!
      redirect_to admin_users_url, notice: "Добавлено пользователей: #{@user_list.users.size}"
    else
      render :new
    end
  end

  private
    def user_list_params
      params.require(:user_list).permit([
        :organization_id,
        :federal_subject_id,
        creatable_federal_project_ids: [],
        moderatable_federal_project_ids: [],
        viewable_federal_project_ids: [],
        repostable_federal_project_ids: [],
        users_attributes: [
          :email,
          :first_name,
          :last_name,
          :asked_to_send_confirmation_notification
        ]
      ])
    end

    def federal_projects_for_select
      @federal_projects_for_select ||= FederalProject
        .includes(:national_project)
        .order('national_projects.short_name')
        .order('federal_projects.name')
        .group_by(&:national_project)
        .map { |national_project, federal_projects|
          [
            national_project.short_name,
            federal_projects.map { |federal_project|
              [
                (federal_project.name || '').truncate(40),
                federal_project.id,
                { data: { tokens: "#{national_project.short_name} #{federal_project.name}" }}
              ]
            }
          ]
        }
    end
    helper_method :federal_projects_for_select
end