class Admin::BestPracticesController < AdminController
  def index
    authorize [:admin, BestPractice]

    @q = BestPractice.ransack(params[:q])
    @q.sorts = 'published_at desc' if @q.sorts.empty?

    @pagy, @best_practices = pagy(@q.result, items: params.fetch(:items, 10))
  end
end