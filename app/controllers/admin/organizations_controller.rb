class Admin::OrganizationsController < AdminController
  def index
    authorize Organization

    params[:q] ||= {}
    params[:q][:name_cont] = params[:search] if params[:search].present?
    @q = Organization.ransack(params[:q])

    respond_to do |format|
      format.html do
        @q.sorts = 'created_at desc' if @q.sorts.empty?
        @pagy, @organizations = pagy(@q.result.includes(:organization_websites).includes(:federal_subject), items: 10)
      end
      format.xlsx do
        @q.sorts = 'name asc' if @q.sorts.empty?
        @organizations = @q.result.includes(:organization_websites).includes(:federal_subject)
      end
      format.json do
        @q.sorts = 'name asc' if @q.sorts.empty?
        @organizations = @q.result.limit(10)
      end
    end
  end

  def new
    @organization = Organization.new
    authorize @organization

    render :edit
  end

  def create
    @organization = Organization.new(organization_params)
    authorize @organization

    respond_to do |format|
      if @organization.save
        format.html do
          redirect_to [:admin, :organizations], notice: 'Организация добавлена.'
        end
        format.js
      else
        format.html do
          flash.now[:alert] = @organization.errors.full_messages.join(', ')
          render :edit
        end
        format.js
      end
    end
  end

  def edit
    @organization = Organization.find(params[:id])
    authorize @organization
  end

  def update
    @organization = Organization.find(params[:id])
    authorize @organization

    respond_to do |format|
      if @organization.update(organization_params)
        format.html { redirect_to [:admin, :organizations], notice: 'Организация сохранена.' }
        format.js { head :ok }
      else
        format.html do
          @same_name_organization = Organization.find_by(name: @organization.name)
          flash.now[:alert] = @organization.errors.full_messages.join(', ')
          render :edit
        end
        format.js { render js: "alert('#{@organization.errors.full_messages}')" }
      end
    end
  end

  def destroy
    @organization = Organization.find(params[:id])
    authorize @organization

    if @organization.destroy
      redirect_to [:admin, :organizations], notice: 'Организация удалена.'
    else
      flash.now[:alert] = @organization.errors.full_messages.join(', ')
      render :edit
    end
  end

  private
    def organization_params
      params.require(:organization).permit([
        :name,
        :kind,
        :federal_subject_id,
        :contact_name,
        :contact_position,
        :contact_phone,
        :can_create_messages,
        :list_all_unapproved_press_releases,
        :skip_repost_domain_validation,
        :rated,
        :rating_addon_organization_id,
        organization_websites_attributes: [
          :id, :address, :_destroy
        ]
      ])
    end
end