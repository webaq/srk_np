class Admin::TagsController < AdminController
  before_action :set_tag, only: [:edit, :update, :destroy]

  def index
    authorize [:admin, Tag]

    @q = Tag.ransack(params[:q])
    @q.sorts = 'name asc' if @q.sorts.empty?

    @tags = @q.result.includes(:national_projects)
  end

  def new
    @tag = Tag.new
    authorize [:admin, @tag]
    render :edit
  end

  def create
    @tag = Tag.new(tag_params)
    authorize [:admin, @tag]
    if @tag.save
      redirect_to [:admin, :tags], notice: 'Тег добавлен.'
    else
      render :edit
    end
  end

  def update
    if @tag.update(tag_params)
      redirect_to [:admin, :tags], notice: 'Тег сохранён.'
    else
      render :edit
    end
  end

  def destroy
    if @tag.destroy
      redirect_to [:admin, :tags], notice: 'Тег удалён.'
    else
      render :edit
    end
  end

  private
    def tag_params
      params.require(:tag).permit([
        :name,
        { national_project_ids: [] },
      ])
    end

    def set_tag
      @tag = Tag.find(params[:id])
      authorize [:admin, @tag]
    end
end