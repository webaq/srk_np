class Admin::RepostsController < AdminController
  def index
    authorize [:admin, Repost]

    @q = Repost.ransack(params[:q])
    @q.sorts = 'created_at desc' if @q.sorts.empty?

    respond_to do |format|
      format.html do
        @pagy, @reposts = pagy(@q.result.includes(:press_release, { user: :organization }), items: params.fetch(:items, 10))
      end
      format.xlsx do
        @reposts = @q.result.includes(:press_release, { user: :organization })
      end
    end
  end
end