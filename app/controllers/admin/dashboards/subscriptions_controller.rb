class Admin::Dashboards::SubscriptionsController < AdminController
  before_action :set_subscriptions_dashboard

  def table1
  end

  def table2
  end

  def table3
  end

  def table4
  end

  def table5
  end

  def table6
  end

  def table7
  end

  private
    def set_subscriptions_dashboard
      @subscriptions_dashboard = Dashboard::SubscriptionsDashboard.new(dashboard_params)
    end

    def dashboard_params
      params.permit([
        :starts_at,
        :ends_at,
        :starts2_at,
        :ends2_at,
      ])
    end
end