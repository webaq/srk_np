class Admin::Dashboards::NationalProjectsListsController < AdminController
  def show
    @national_projects_list = Dashboard::NationalProjectsList.new(national_projects_list_params)
  end

  private
    def national_projects_list_params
      params.permit([
        :federal_subject_id,
        :starts_at,
        :ends_at,
        :starts2_at,
        :ends2_at,
        sort: {},
      ])
    end
end