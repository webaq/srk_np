class Admin::RegionalAuthoritiesActivitiesReportsController < AdminController
  def show
    authorize [:admin, :regional_authorities_activities_report]

    @starts_on, @ends_on = if params[:period].present?
      params[:period].split(' — ').map { |str| Date.parse(str) }
    else
      [Date.today - 7, Date.today]
    end
    @ends_on = @starts_on unless @ends_on

    @press_releases_counts_hash = federal_subject_scope(filtered_press_releases)
      .joins(:federal_subjects)
      .group('federal_subjects.id')
      .count('DISTINCT press_releases.id')
      .to_hash

    @repostable_users_counts_hash = filtered_repostable_users
      .joins(:federal_subject)
      .group('federal_subjects.id')
      .count('DISTINCT users.id')
      .to_hash

    @viewed_press_releases_counts_hash = federal_subject_scope(filtered_press_releases)
      .joins(:show_activities)
      .where(activities: { owner_id: User.joins(:organization).merge(Organization.media) })
      .joins(:federal_subjects)
      .group('federal_subjects.id')
      .count('DISTINCT press_releases.id')
      .to_hash

    @reposted_press_releases_counts_hash = federal_subject_scope(filtered_press_releases)
      .joins(:reposts)
      .joins(:federal_subjects)
      .group('federal_subjects.id')
      .count('DISTINCT press_releases.id')
      .to_hash

    @federal_subjects = federal_subject_scope(FederalSubject).select(:id, :name).order(:name)
  end

  private
    def filtered_press_releases
      @filtered_press_releases ||= begin
        q = PressRelease.all
        q = q.where(published_at: @starts_on.to_time..(@ends_on + 1).to_time)
        q = q.joins(user: :organization).where(organizations: { kind: params[:organization_kind_eq] }) if params[:organization_kind_eq].present?
        q
      end
    end

    def filtered_repostable_users
      @filtered_repostable_users ||= begin
        q = User.joins(:repostable_federal_project_permissions)
        q = q.where(created_at: @starts_on.to_time..(@ends_on + 1).to_time)
        q
      end
    end

    def federal_subject_scope(scope)
      if params[:federal_subject_id_eq].present?
        scope.where(federal_subjects: { id: params[:federal_subject_id_eq] })
      else
        scope
      end
    end
end