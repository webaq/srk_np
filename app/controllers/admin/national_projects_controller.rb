class Admin::NationalProjectsController < AdminController
  before_action :set_national_project, only: [:show, :edit, :update, :destroy]

  def index
    authorize [:admin, NationalProject]
    @national_projects = NationalProject.with_attached_avatar.order(:name)
  end

  def new
    @national_project = NationalProject.new
    authorize [:admin, @national_project]

    render :edit
  end

  def create
    @national_project = NationalProject.new(national_project_params)
    authorize [:admin, @national_project]

    if @national_project.save
      redirect_to [:admin, @national_project], notice: 'Направление добавлено.'
    else
      render :edit
    end
  end

  def show
    @federal_projects = @national_project.federal_projects.order(:name)
  end

  def edit
  end

  def update
    if @national_project.update(national_project_params)
      redirect_to [:admin, @national_project], notice: 'Направление сохранено.'
    else
      render :edit
    end
  end

  def destroy
    if @national_project.destroy
      redirect_to [:admin, :national_projects], notice: 'Направление удалено.'
    else
      render :edit
    end
  end

  private
    def set_national_project
      @national_project = NationalProject.find(params[:id])
      authorize [:admin, @national_project]
    end

    def national_project_params
      params.require(:national_project).permit([
        :name,
        :short_name,
        :avatar,
        :content,
        :branding,
        :used_in_rating,
      ])
    end
end