class Admin::SimilaritiesController < AdminController
  def show
  end

  def create
    @text1 = params[:text1]
    @text2 = params[:text2]
    @lexemes1 = PressRelease.generate_lexemes(@text1)
    @lexemes2 = PressRelease.generate_lexemes(@text2)
    @similarity = PressRelease.connection.select_value(PressRelease.sanitize_sql_array(['SELECT smlar(ARRAY[:a], ARRAY[:b]) AS _similarity', a: @lexemes1, b: @lexemes2].flatten)) if @lexemes1.any? && @lexemes2.any?
    render :show
  end
end