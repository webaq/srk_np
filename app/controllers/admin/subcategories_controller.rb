class Admin::SubcategoriesController < AdminController
  before_action :set_category, only: [:new, :create]
  before_action :set_subcategory, only: [:show, :edit, :update, :destroy]

  def new
    @subcategory = @category.subcategories.new
    render :edit
  end

  def create
    @subcategory = @category.subcategories.new(subcategory_params)
    if @subcategory.save
      redirect_to [:admin, @subcategory]
    else
      render :edit
    end
  end

  def edit
  end

  def show
    @press_release_values = @subcategory.press_release_values
  end

  def update
    if @subcategory.update(subcategory_params)
      redirect_to [:admin, @subcategory]
    else
      render :edit
    end
  end

  def destroy
    if @subcategory.destroy
      redirect_to [:admin, @subcategory.category]
    else
      render :edit
    end
  end

  private
    def subcategory_params
      params.require(:subcategory).permit(:name)
    end

    def set_category
      @category = Category.find(params[:category_id])
    end

    def set_subcategory
      @subcategory = Subcategory.find(params[:id])
    end
end