class Admin::UsersController < AdminController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :set_multiple_users, only: [:edit_multiple, :update_multiple, :edit_multiple_permissions, :update_multiple_permissions]
  before_action :set_national_projects, only: [:edit_multiple, :update_multiple]

  def index
    authorize [:admin, User]

    @q = User.ransack(params[:q])
    @q.sorts = 'created_at desc' if @q.sorts.empty?

    respond_to do |format|
      format.html do
        @users = @q.result#(distinct: true)
          .with_attached_avatar
          .includes(:organization)
          .references(:organization)
          .preload(:creatable_national_projects, :moderatable_national_projects, :repostable_national_projects)

        @pagy, @users = pagy(@users, items: params.fetch(:items, 10))
      end
      format.xlsx do
        @users = @q.result
          .includes(organization: :federal_subject)
        Rails.logger.debug "!!! Downloading users xlsx: #{current_user.email}"
      end
    end
  end

  def new
    @user = User.new
    authorize @user
  end

  def create
    @user = User.new(user_params.merge({
      password: SecureRandom.base58
    }))
    authorize @user

    if @user.save
      redirect_to admin_users_url, notice: 'Пользователь добавлен.'
    else
      flash.now[:alert] = @user.errors.full_messages.join(', ')
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @user.update(user_params)
      @user.update_column(:confirmed_at, Time.current) if params[:set_confirmed_at] == '1'
      redirect_to admin_user_url(@user), notice: 'Пользователь сохранён.'
    else
      flash.now[:alert] = @user.errors.full_messages.join(', ')
      render :edit
    end
  end

  def destroy
    @user.discard!
    redirect_to admin_users_url, notice: 'Пользователь удалён.'
  end

  def edit_multiple
  end

  def update_multiple
    success = true
    @users.each do |user|
      unless user.update(multiple_user_params)
        success = false
      end
    end

    if success
      redirect_to admin_users_url, notice: 'Изменения сохранены'
    else
      flash.now.alert = 'Произошла ошибка'
      render :edit_multiple
    end
  end

  def edit_multiple_permissions
  end

  def update_multiple_permissions
    # byebug

    params.fetch(:user, {}).each do |key, federal_project_ids|
      federal_projects = FederalProject.find(federal_project_ids)

      @users.each do |user|
        if params[:multiple_action] == 'add'
          # byebug
          user.public_send(key).append(federal_projects - user.public_send(key))
        else
          user.public_send(key).delete(federal_projects)
        end
      end
    end
  end

  private
    def set_user
      @user = User.find(params[:id])
      authorize [:admin, @user]
    end

    def set_national_projects
      @national_projects = NationalProject.includes(:federal_projects).order(:name).order('federal_projects.name')
    end

    def set_multiple_users
      authorize [:admin, User]
      @users = User.find(params[:user_ids] || [])
    end

    def user_params
      if params[:user][:password].blank?
        params[:user].delete(:password)
        params[:user].delete(:password_confirmation)
      end

      params.require(:user).permit([
        :first_name,
        :last_name,
        :contact_position,
        :contact_phone,
        :avatar,
        :email,
        :password,
        :password_confirmation,
        :organization_id,
        :admin,
        :editor,
        :asked_to_send_confirmation_notification,
        :subscribed_to_media_digest,
        :reposts_list_permitted,
        :show_unpublished_press_releases,
        :sex,
        :thank_you_note,
        { user_group_ids: [] },
      ])
    end

    def multiple_user_params
      params.require(:user).permit([
        viewable_federal_project_ids: [],
        creatable_federal_project_ids: [],
        moderatable_federal_project_ids: [],
        repostable_federal_project_ids: [],
      ])
    end

    def federal_projects_for_select
      @federal_projects_for_select ||= FederalProject
        .includes(:national_project)
        .order('national_projects.short_name')
        .order('federal_projects.name')
        .group_by(&:national_project)
        .map { |national_project, federal_projects|
          [
            national_project.short_name,
            federal_projects.map { |federal_project|
              [
                (federal_project.name || '').truncate(40),
                federal_project.id,
                { data: { tokens: "#{national_project.short_name} #{federal_project.name}" }}
              ]
            }
          ]
        }
    end
    helper_method :federal_projects_for_select
end