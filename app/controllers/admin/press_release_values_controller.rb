class Admin::PressReleaseValuesController < AdminController
  before_action :set_subcategory, only: [:new, :create]
  before_action :set_press_release_value, only: [:edit, :update, :destroy]

  def new
    @press_release_value = @subcategory.press_release_values.new
    render :edit
  end

  def create
    @press_release_value = @subcategory.press_release_values.new(press_release_value_params)
    if @press_release_value.save
      redirect_to [:admin, @press_release_value.subcategory]
    else
      render :edit
    end
  end

  def edit
  end

  def update
    if @press_release_value.update(press_release_value_params)
      redirect_to [:admin, @press_release_value.subcategory]
    else
      render :edit
    end
  end

  def destroy
    if @press_release_value.destroy
      redirect_to [:admin, @press_release_value.subcategory]
    else
      render :edit
    end
  end

  private
    def set_subcategory
      @subcategory = Subcategory.find(params[:subcategory_id])
    end

    def set_press_release_value
      @press_release_value = PressReleaseValue.find(params[:id])
    end

    def press_release_value_params
      params.require(:press_release_value).permit([
        :name,
        :subcategory_scope_id,
        :value,
      ])
    end
end