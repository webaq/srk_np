class Admin::PressReleaseViewsReportsController < AdminController
  before_action :set_period, only: [:show, :users, :press_releases]

  def show
    authorize [:admin, :press_release_views_report]

    @activities = ::PublicActivity::Activity
      .where(key: 'press_release.show')
      .where(created_at: @starts_on.to_time..(@ends_on + 1).to_time)

    @activities = @activities.where(owner_id: organization_kind_user_scope) if params[:organization_kind_eq].present?
    @activities = @activities.where(trackable_id: region_press_release_scope) if params[:federal_subject_id_eq].present?

    @bookmarks = Bookmark.where(created_at: @starts_on.to_time..(@ends_on + 1).to_time)

    @press_release_views_report = [
      {
        name: 'Кол-во просмотров',
        data: @activities.group_by_period(group_period, :created_at).count
      },
      {
        name: 'Взято СМИ',
        data: @bookmarks.group_by_period(group_period, :created_at).count
      }
    ]
  end

  def users
    @users = User
      .joins(:press_release_show_activities)
      .where(activities: {
        created_at: @starts_on.to_time..(@ends_on + 1).to_time
      })
      .group(:id)
      .select(:id, :first_name, :last_name, :organization_id)
      .select('COUNT(activities.id) AS activities_count')
      .preload(organization: :federal_subject)
    @users = @users.where(id: organization_kind_user_scope) if params[:organization_kind_eq].present?
    @users = @users.where(activities: { trackable_id: region_press_release_scope }) if params[:federal_subject_id_eq].present?
    @sort = params[:sort] || {"activities_count" => "desc"}
  end

  def press_releases
    @press_releases = PressRelease
      .joins(:show_activities)
      .where(activities: { created_at: @starts_on.to_time..(@ends_on + 1).to_time })
      .group(:id)
      .select(:id, :title, :published_at)
      .select('COUNT(activities.id) AS activities_count')
      .order(activities_count: 'DESC')
    @press_releases = @press_releases.where(activities: { owner_id: organization_kind_user_scope }) if params[:organization_kind_eq].present?
    @press_releases = @press_releases.joins(:federal_subjects).where(federal_subjects: { id: params[:federal_subject_id_eq] }) if params[:federal_subject_id_eq].present?
    @press_releases = @press_releases.limit(100)
  end

  private
    def set_period
      @starts_on, @ends_on = if params[:period].present?
        params[:period].split(' — ').map { |str| Date.parse(str) }
      else
        [Date.today - 7, Date.today]
      end
      @ends_on = @starts_on unless @ends_on
    end

    def organization_kind_user_scope
      User.joins(:organization).where(organizations: { kind: params[:organization_kind_eq] })
    end

    def region_press_release_scope
      PressRelease.joins(:federal_subjects).where(federal_subjects: { id: params[:federal_subject_id_eq] })
    end

    def group_period
      duration = @ends_on - @starts_on
      if duration > 3.months.in_days
        :month
      elsif duration > 4.weeks.in_days
        :week
      else
        :day
      end
    end
end
