class Admin::PlacementsController < AdminController
  def index
    authorize [:admin, Placement]

    @starts_on, @ends_on = if params[:period1].present?
      params[:period1].split(' — ').map { |str| Date.parse(str) }
    else
      [1.week.ago.to_date, Date.current]
    end
    @ends_on = @starts_on unless @ends_on

    @placements = Placement
      .includes(user: :organization)
      .includes(:national_projects)
      .includes(:federal_subjects)
      .order(created_at: 'DESC')
      .where(created_at: @starts_on.to_time..(@ends_on + 1).to_time)

    respond_to do |format|
      format.html do
        @pagy, @placements = pagy(@placements)
      end
      format.xlsx
    end
  end
end