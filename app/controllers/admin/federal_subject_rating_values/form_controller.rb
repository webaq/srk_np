class Admin::FederalSubjectRatingValues::FormController < AdminController
  before_action :set_rating_month

  def show
    @calculator = Rating::FederalSubjectValuesCalculator.new(calculator_params.merge({
      starts_at: @rating_month.starts_at,
      ends_at: @rating_month.ends_at
    }))
  end

  private

  def calculator_params
    params.require(:calculator).permit(:federal_subject_id, :quality)
  end

  def set_rating_month
    @rating_month = RatingMonth.find(params[:rating_month_id])
  end
end