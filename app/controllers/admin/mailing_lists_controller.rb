class Admin::MailingListsController < AdminController
  before_action :set_mailing_list, only: [:show, :edit, :update, :destroy]

  def index
    authorize [:admin, MailingList]
    # @mailing_lists = MailingList.order(:name)
    @support_emails = SupportEmail.order(:email)
    @subscribers = UnapprovedPressReleasesDigestSubscriber.order(:email)
  end

  # def show
#   end
#
#   def new
#     @mailing_list = MailingList.new
#     authorize [:admin, @mailing_list]
#   end
#
#   def edit
#   end
#
#   def create
#     @mailing_list = MailingList.new(mailing_list_params)
#     authorize [:admin, @mailing_list]
#
#     respond_to do |format|
#       if @mailing_list.save
#         format.html { redirect_to admin_mailing_lists_url, notice: 'Список рассылки создан.' }
#         # format.json { render :show, status: :created, location: @support_email }
#       else
#         format.html { render :new }
#         # format.json { render json: @support_email.errors, status: :unprocessable_entity }
#       end
#     end
#   end
#
#   def update
#     respond_to do |format|
#       if @mailing_list.update(mailing_list_params)
#         format.html { redirect_to admin_mailing_lists_url, notice: 'Список рассылки обновлён.' }
#         # format.json { render :show, status: :ok, location: @support_email }
#       else
#         format.html { render :edit }
#         # format.json { render json: @support_email.errors, status: :unprocessable_entity }
#       end
#     end
#   end
#
#   def destroy
#     @mailing_list.destroy
#     respond_to do |format|
#       format.html { redirect_to admin_mailing_lists_url, notice: 'Список рассылки удалён.' }
#       # format.json { head :no_content }
#     end
#   end

  private
    def set_mailing_list
      @mailing_list = MailingList.find(params[:id])
      authorize [:admin, @mailing_list]
    end

    def mailing_list_params
      params.require(:mailing_list).permit([
        :name,
        mailing_list_items_attributes: [
          :id, :email, :_destroy
        ]
      ])
    end
end
