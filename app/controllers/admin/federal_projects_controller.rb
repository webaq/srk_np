class Admin::FederalProjectsController < AdminController
  def new
    @national_project = NationalProject.find(params[:national_project_id])
    @federal_project = @national_project.federal_projects.new
    authorize @federal_project

    render :edit
  end

  def create
    @national_project = NationalProject.find(params[:national_project_id])
    @federal_project = @national_project.federal_projects.new(federal_project_params)
    authorize @federal_project

    if @federal_project.save
      redirect_to [:admin, @federal_project], notice: 'Федеральный проект добавлен.'
    else
      render :edit
    end
  end

  def show
    @federal_project = FederalProject.find(params[:id])
    authorize @federal_project

    @measures = @federal_project.measures.order(:name)
  end

  def edit
    @federal_project = FederalProject.find(params[:id])
    authorize @federal_project
  end

  def update
    @federal_project = FederalProject.find(params[:id])
    authorize @federal_project

    if @federal_project.update(federal_project_params)
      redirect_to [:admin, @federal_project], notice: 'Федеральный проект сохранён.'
    else
      render :edit
    end
  end

  def destroy
    @federal_project = FederalProject.find(params[:id])
    authorize @federal_project

    if @federal_project.destroy
      redirect_to [:admin, @federal_project.national_project], notice: 'Федеральный проект удалён.'
    else
      render :edit
    end
  end

  private
    def federal_project_params
      params.require(:federal_project).permit([
        :name,
        :available,
      ])
    end
end