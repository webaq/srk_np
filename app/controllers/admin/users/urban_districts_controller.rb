class Admin::Users::UrbanDistrictsController < AdminController
  def index
    respond_to do |format|
      format.json do
        @urban_districts = UrbanDistrict
          .where(federal_subject_id: params[:federal_subject_id])
          .order(:name)
      end
    end
  end
end