class Admin::Users::ActivitiesController < AdminController
  before_action :set_user

  def index
    @pagy, @activities = pagy(@user.activities.includes(:trackable).order(created_at: 'DESC'), items: 10)
  end

  private

  def set_user
    @user = User.find(params[:user_id])
  end
end