class Admin::Users::AuthenticationTokensController < AdminController
  before_action :set_user

  def show
  end

  def create
    @user.update_column(:authentication_token, nil)
    @user.save
    redirect_to admin_user_url(@user)
  end

  private

  def set_user
    @user = User.find(params[:user_id])
  end
end