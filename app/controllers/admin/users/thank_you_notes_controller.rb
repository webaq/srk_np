class Admin::Users::ThankYouNotesController < AdminController
  before_action :set_user

  def show
    render pdf: 'show', layout: 'thank_you_note', margin: { top: 0, bottom: 0, left: 0, right: 0 }
  end

  private
    def set_user
      @user = User.find(params[:user_id])
    end
end