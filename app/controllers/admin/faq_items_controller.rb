class Admin::FaqItemsController < AdminController
  before_action :set_faq_item, only: [:edit, :update, :destroy]
  before_action :set_faq_item_before_movement, only: [:up, :down]

  def index
    authorize [:admin, FaqItem]
    @faq_items = FaqItem.order(:position)
  end

  def new
    @faq_item = FaqItem.new
    authorize [:admin, @faq_item]
    render :edit
  end

  def create
    @faq_item = FaqItem.new(faq_item_params)
    authorize [:admin, @faq_item]
    if @faq_item.save
      redirect_to admin_faq_items_url, notice: 'Вопрос добавлен.'
    else
      render :edit
    end
  end

  def edit; end

  def update
    if @faq_item.update(faq_item_params)
      redirect_to admin_faq_items_url, notice: 'Вопрос сохранён.'
    else
      render :edit
    end
  end

  def destroy
    if @faq_item.destroy
      redirect_to admin_faq_items_url, notice: 'Вопрос удалён.'
    else
      render :edit
    end
  end

  def up
    @faq_item.move_higher
    redirect_to admin_faq_items_url
  end

  def down
    @faq_item.move_lower
    redirect_to admin_faq_items_url
  end

  private
    def set_faq_item
      @faq_item = FaqItem.find(params[:id])
      authorize [:admin, @faq_item]
    end

    def set_faq_item_before_movement
      @faq_item = FaqItem.find(params[:faq_item_id])
      authorize [:admin, @faq_item], :update?
    end

    def faq_item_params
      params.require(:faq_item).permit([
        :question,
        :answer,
        :media_allowed,
        :non_media_allowed,
      ])
    end
end