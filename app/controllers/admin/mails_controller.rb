class Admin::MailsController < AdminController
  before_action :set_user, only: %i[
    media_digest_mail
    rejected
    press_release_approved
    registration_submitted
    welcome_media
    welcome_others
    dashboard_stats_weekly
    dashboard_stats_daily
  ]

  def about_media_digest_mail
    User.find(params[:user_ids]).each do |user|
      NotifierMailer.about_media_digest_mail(user.email).deliver
    end

    render js: "alert('Разовое письмо об уведомлении на подписки отправлено')"
  end

  def media_digest_mail
    media_digest = MediaDigest.new(@user)

    if media_digest.press_releases_count.positive?
      NotifierMailer.media_digest_mail(
        @user.email,
        media_digest.press_releases_count,
        media_digest.limited_press_releases
      ).deliver

      render js: "alert('Дайджест СМИ отправлен')"
    else
      render js: "alert('За последние 24 часа нет подходящих инфоповодов для дайджеста СМИ')"
    end
  end

  def rejected
    user = User.find(params[:user_id])
    NotifierMailer.rejected(@user.email).deliver
    render js: "alert('Письмо при отказе в СРК отправлено')"
  end

  def press_release_approved
    NotifierMailer.press_release_approved(@user.email).deliver
    render js: "alert('Письмо при размещении новости отправлено')"
  end

  def registration_submitted
    NotifierMailer.registration_submitted(@user.email).deliver
    render js: "alert('Письмо при регистрации отправлено')"
  end

  def welcome_media
    NotifierMailer.welcome_media(@user.email).deliver
    render js: "alert('Письмо при получении доступа в СРК (СМИ) отправлено')"
  end

  def welcome_others
    NotifierMailer.welcome_others(@user.email).deliver
    render js: "alert('Письмо при получении доступа в СРК (РОИВ/ФОИВ и т.д.) отправлено')"
  end

  def dashboard_stats_weekly
    starts_on = Date.current.beginning_of_week - 14.days
    ends_on = starts_on.end_of_week
    starts2_on = Date.current.beginning_of_week - 7.days
    ends2_on = starts2_on.end_of_week
    subject = 'Еженедельная статистика'

    dashboard = Dashboard.new({
      starts_on: starts_on,
      ends_on: ends_on,
      starts2_on: starts2_on,
      ends2_on: ends2_on,
      federal_subject: nil,
    })
    UserMailer.with({
      email: @user.email,
      subject: subject,
      dashboard: dashboard,
    }).dashboard_stats.deliver

    render js: "alert('Письмо отправлено')"
  end

  def dashboard_stats_daily
    starts_on = Date.current - 2.days
    ends_on = starts_on
    starts2_on = Date.current.prev_day
    ends2_on = starts2_on
    subject = 'Ежедневная статистика'

    dashboard = Dashboard.new({
      starts_on: starts_on,
      ends_on: ends_on,
      starts2_on: starts2_on,
      ends2_on: ends2_on,
      federal_subject: nil,
    })
    UserMailer.with({
      email: @user.email,
      subject: subject,
      dashboard: dashboard,
    }).dashboard_stats.deliver

    render js: "alert('Письмо отправлено')"
  end

  private
    def set_user
      @user = User.find(params[:user_id])
    end
end