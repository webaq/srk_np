class Admin::OrganizationsReportsController < AdminController
  def show
    authorize [:admin, :organizations_report]

    @q = Organization.ransack(params[:q])
    @q.sorts = 'name asc' if @q.sorts.empty?

    @organizations = @q.result.includes(:federal_subject)

    respond_to do |format|
      format.html do
        @pagy, @organizations = pagy(@organizations)
      end
      format.xlsx
    end
  end
end
