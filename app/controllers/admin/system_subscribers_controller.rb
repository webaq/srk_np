class Admin::SystemSubscribersController < AdminController
  before_action :set_subscriber, only: [:edit, :update, :destroy]

  def new
    @subscriber = SystemSubscriber.new(kind: params[:kind])
    authorize [:admin, @subscriber]
  end

  def edit; end

  def create
    @subscriber = SystemSubscriber.new(subscriber_params)
    authorize [:admin, @subscriber]

    respond_to do |format|
      if @subscriber.save
        format.html { redirect_to admin_mailing_lists_url(anchor: "system_subscribers_#{@subscriber.kind}"), notice: 'Подписчик добавлен.' }
        # format.json { render :show, status: :created, location: @support_email }
      else
        format.html { render :new }
        # format.json { render json: @support_email.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @subscriber.update(subscriber_params)
        format.html { redirect_to admin_mailing_lists_url(anchor: "system_subscribers_#{@subscriber.kind}"), notice: 'Подписчик  сохранён.' }
        # format.json { render :show, status: :ok, location: @support_email }
      else
        format.html { render :edit }
        # format.json { render json: @support_email.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @subscriber.destroy
    respond_to do |format|
      format.html { redirect_to admin_mailing_lists_url(anchor: "system_subscribers_#{@subscriber.kind}"), notice: 'Подписчик  удалён.' }
      # format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subscriber
      @subscriber = SystemSubscriber.find(params[:id])
      authorize [:admin, @subscriber]
    end

    # Only allow a list of trusted parameters through.
    def subscriber_params
      params.require(:system_subscriber).permit(:kind, :email)
    end
end
