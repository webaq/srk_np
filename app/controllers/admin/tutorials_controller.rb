class Admin::TutorialsController < AdminController
  before_action :set_tutorial, only: [:show, :edit, :update, :destroy]

  # GET /tutorials
  # GET /tutorials.json
  def index
    authorize [:admin, Tutorial]
    @tutorials = Tutorial.order(:name)
  end

  # GET /tutorials/1
  # GET /tutorials/1.json
  # def show
  # end

  # GET /tutorials/new
  def new
    @tutorial = Tutorial.new
    authorize [:admin, @tutorial]
  end

  # GET /tutorials/1/edit
  def edit
  end

  # POST /tutorials
  # POST /tutorials.json
  def create
    @tutorial = Tutorial.new(tutorial_params)

    respond_to do |format|
      if @tutorial.save
        format.html { redirect_to admin_tutorials_url, notice: 'Страница добавлена.' }
        format.json { render :show, status: :created, location: @tutorial }
      else
        format.html { render :new }
        format.json { render json: @tutorial.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tutorials/1
  # PATCH/PUT /tutorials/1.json
  def update
    respond_to do |format|
      if @tutorial.update(tutorial_params)
        format.html { redirect_to admin_tutorials_url, notice: 'Страница изменена.' }
        format.json { render :show, status: :ok, location: @tutorial }
      else
        format.html { render :edit }
        format.json { render json: @tutorial.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tutorials/1
  # DELETE /tutorials/1.json
  def destroy
    @tutorial.destroy
    respond_to do |format|
      format.html { redirect_to admin_tutorials_url, notice: 'Страница удалена.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tutorial
      @tutorial = Tutorial.find(params[:id])
      authorize [:admin, @tutorial]
    end

    # Only allow a list of trusted parameters through.
    def tutorial_params
      params.require(:tutorial).permit([
        :name,
        :content,
        :creatable_permitted,
        :repostable_permitted,
        :onboarding
      ])
    end
end
