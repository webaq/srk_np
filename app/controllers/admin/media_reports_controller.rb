class Admin::MediaReportsController < AdminController
  #
  Row = Struct.new(
    :user,

    :visits_count_week1,
    :visits_count_week2,
    :visits_count_week3,
    :visits_count_week4,

    :visits_count_month1,
    :visits_count_month2,
    :visits_count_month3,
    :visits_count_month4,

    :shows_count_week1,
    :shows_count_week2,
    :shows_count_week3,
    :shows_count_week4,

    :shows_count_month1,
    :shows_count_month2,
    :shows_count_month3,
    :shows_count_month4,

    :reposts_count_week1,
    :reposts_count_week2,
    :reposts_count_week3,
    :reposts_count_week4,

    :reposts_count_month1,
    :reposts_count_month2,
    :reposts_count_month3,
    :reposts_count_month4,

    keyword_init: true
  )

  def show
    visits_weekly = Visit.group_by_week(:created_at).group(:user_id).count
    visits_monthly = Visit.group_by_month(:created_at).group(:user_id).count

    shows_weekly = ::PublicActivity::Activity.where(key: 'press_release.show').group_by_week(:created_at).group(:owner_id).count
    shows_monthly = ::PublicActivity::Activity.where(key: 'press_release.show').group_by_month(:created_at).group(:owner_id).count

    reposts_weekly = Repost.group_by_week(:created_at).group(:user_id).count
    reposts_monthly = Repost.group_by_month(:created_at).group(:user_id).count

    @rows = User
      .joins(:organization)
      .merge(Organization.media)
      .includes(:organization)
      .includes(:federal_subject)
      .order(created_at: 'DESC')
      .map { |user|
        Row.new(
          user: user,

          visits_count_week1: visits_weekly.fetch([Date.new(2020, 9, 28), user.id], 0),
          visits_count_week2: visits_weekly.fetch([Date.new(2020, 10, 5), user.id], 0),
          visits_count_week3: visits_weekly.fetch([Date.new(2020, 10, 12), user.id], 0),
          visits_count_week4: visits_weekly.fetch([Date.new(2020, 10, 19), user.id], 0),

          visits_count_month1: visits_monthly.fetch([Date.new(2020, 7, 1), user.id], 0),
          visits_count_month2: visits_monthly.fetch([Date.new(2020, 8, 1), user.id], 0),
          visits_count_month3: visits_monthly.fetch([Date.new(2020, 9, 1), user.id], 0),
          visits_count_month4: visits_monthly.fetch([Date.new(2020, 10, 1), user.id], 0),

          shows_count_week1: shows_weekly.fetch([Date.new(2020, 9, 28), user.id], 0),
          shows_count_week2: shows_weekly.fetch([Date.new(2020, 10, 5), user.id], 0),
          shows_count_week3: shows_weekly.fetch([Date.new(2020, 10, 12), user.id], 0),
          shows_count_week4: shows_weekly.fetch([Date.new(2020, 10, 19), user.id], 0),

          shows_count_month1: shows_monthly.fetch([Date.new(2020, 7, 1), user.id], 0),
          shows_count_month2: shows_monthly.fetch([Date.new(2020, 8, 1), user.id], 0),
          shows_count_month3: shows_monthly.fetch([Date.new(2020, 9, 1), user.id], 0),
          shows_count_month4: shows_monthly.fetch([Date.new(2020, 10, 1), user.id], 0),

          reposts_count_week1: reposts_weekly.fetch([Date.new(2020, 9, 28), user.id], 0),
          reposts_count_week2: reposts_weekly.fetch([Date.new(2020, 10, 5), user.id], 0),
          reposts_count_week3: reposts_weekly.fetch([Date.new(2020, 10, 12), user.id], 0),
          reposts_count_week4: reposts_weekly.fetch([Date.new(2020, 10, 19), user.id], 0),

          reposts_count_month1: reposts_monthly.fetch([Date.new(2020, 7, 1), user.id], 0),
          reposts_count_month2: reposts_monthly.fetch([Date.new(2020, 8, 1), user.id], 0),
          reposts_count_month3: reposts_monthly.fetch([Date.new(2020, 9, 1), user.id], 0),
          reposts_count_month4: reposts_monthly.fetch([Date.new(2020, 10, 1), user.id], 0),
        )
      }
  end
end