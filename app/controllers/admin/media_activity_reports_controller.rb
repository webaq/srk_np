class Admin::MediaActivityReportsController < AdminController
  def show
    authorize [:admin, :media_activity_report]

    @q = Organization.ransack(params[:q])
    @q.sorts = 'name asc' if @q.sorts.empty?

    @organizations = @q.result.media

    case params[:filter]
    when "shown_bookmarked"
      @organizations = @organizations.where.not(shown_press_releases_count: 0, bookmarks_count: 0)
    when "shown_not_bookmarked"
      @organizations = @organizations.where.not(shown_press_releases_count: 0).where(bookmarks_count: 0)
    end

    respond_to do |format|
      format.html do
        @pagy, @organizations = pagy(@organizations)
      end
      format.xlsx
    end
  end
end
