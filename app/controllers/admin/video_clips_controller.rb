class Admin::VideoClipsController < AdminController
  before_action :set_video_album, only: %i[ new create ]
  before_action :set_video_clip, only: %i[ show edit update destroy ]

  def new
    @video_clip = @video_album.video_clips.new
    authorize [:admin, @video_album]
    render :edit
  end

  def create
    @video_clip = @video_album.video_clips.new(video_clip_params)
    authorize [:admin, @video_album]

    if @video_clip.save
      redirect_to [:admin, @video_album], notice: 'Видео сохранено.'
    else
      render :edit
    end
  end

  def show; end

  def edit; end

  def update
    if @video_clip.update(video_clip_params)
      redirect_to [:admin, @video_clip.video_album], notice: 'Видео сохранено.'
    else
      render :edit
    end
  end

  def destroy
    if @video_clip.destroy
      redirect_to [:admin, @video_clip.video_album], notice: 'Видео удалено.'
    else
      render :edit
    end
  end

  private
    def video_clip_params
      p = params.require(:video_clip).permit([
        :name,
        :image,
        :remove_image,
        :video,
        :remove_video,
        :youtube_url,
      ])
      p.merge!(image: nil) if params[:remove_image] == '1'
      p.merge!(video: nil) if params[:remove_video] == '1'
      p
    end

    def set_video_album
      @video_album = VideoAlbum.find(params[:video_album_id])
    end

    def set_video_clip
      @video_clip = VideoClip.find(params[:id])
      authorize [:admin, @video_clip.video_album]
    end
end