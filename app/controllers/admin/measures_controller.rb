class Admin::MeasuresController < AdminController
  def new
    @federal_project = FederalProject.find(params[:federal_project_id])
    @measure = @federal_project.measures.new
    authorize @measure

    render :edit
  end

  def create
    @federal_project = FederalProject.find(params[:federal_project_id])
    @measure = @federal_project.measures.new(measure_params)
    authorize @measure

    if @measure.save
      redirect_to [:admin, @federal_project], notice: 'Мера добавлена.'
    else
      render :edit
    end
  end

  def edit
    @measure = Measure.find(params[:id])
    authorize @measure
  end

  def update
    @measure = Measure.find(params[:id])
    authorize @measure

    if @measure.update(measure_params)
      redirect_to [:admin, @measure.federal_project], notice: 'Мера сохранена.'
    else
      render :edit
    end
  end

  def destroy
    @measure = Measure.find(params[:id])
    authorize @measure

    if @measure.destroy
      redirect_to [:admin, @measure.federal_project], notice: 'Мера удалена.'
    else
      render :edit
    end
  end

  private
    def measure_params
      params.require(:measure).permit([
        :name,
      ])
    end
end