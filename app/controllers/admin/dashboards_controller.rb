class Admin::DashboardsController < AdminController
  before_action :set_dashboard

  def show
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: 'show',
          layout: 'layouts/pdf.html.erb',
          viewport_size: '1280x1024',
          window_status: "FLAG_FOR_PDF"
          # javascript_delay: 3000
      end
    end
  end

  def chart1
    @dashboard.chart1_period = params[:period]
  end

  def chart2
    @dashboard.chart2_period = params[:period]
  end

  def chart3_organizations
    @dashboard.chart3_organizations_period = params[:period]
  end

  def chart3_users
    @dashboard.chart3_users_period = params[:period]
  end

  def chart4
    @dashboard.chart4_period = params[:period]
  end

  private
    def set_dashboard
      authorize :dashboard, :show?

      @starts_on, @ends_on = if params[:period1].present?
        params[:period1].split(' — ').map { |str| Date.parse(str) }
      else
        [2.week.ago.to_date.beginning_of_week, 2.week.ago.to_date.end_of_week]
      end
      @ends_on = @starts_on unless @ends_on

      @starts2_on, @ends2_on = if params[:period2].present?
        params[:period2].split(' — ').map { |str| Date.parse(str) }
      else
        [1.week.ago.to_date.beginning_of_week, 1.week.ago.to_date.end_of_week]
      end
      @ends2_on = @starts2_on unless @ends2_on

      @federal_subject = FederalSubject.find(params[:federal_subject_id_eq]) if params[:federal_subject_id_eq].present?

      @federal_subjects = FederalSubject.order(:name)

      @dashboard = Dashboard.new({
        starts_on: @starts_on,
        ends_on: @ends_on,
        starts2_on: @starts2_on,
        ends2_on: @ends2_on,
        federal_subject: @federal_subject
      })
    end
end