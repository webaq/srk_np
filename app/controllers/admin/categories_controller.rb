class Admin::CategoriesController < AdminController
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  def index
    @categories = Category.order(:position)
  end

  def new
    @category = Category.new
    render :edit
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      redirect_to [:admin, @category]
    else
      render :edit
    end
  end

  def show
    @subcategories = @category.subcategories
  end

  def update
    if @category.update(category_params)
      redirect_to [:admin, @category]
    else
      render :edit
    end
  end

  def destroy
    if @category.destroy
      redirect_to [:admin, :categories]
    else
      render :edit
    end
  end

  private
    def category_params
      params.require(:category).permit(:name, :notice)
    end

    def set_category
      @category = Category.find(params[:id])
    end
end