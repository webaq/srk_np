class Admin::RatingMonthsController < AdminController
  before_action :set_rating_month, only: [:show, :edit, :update, :destroy]

  def index
    @rating_months = RatingMonth.order(month: :desc)
  end

  def new
    @rating_month = RatingMonth.new
  end

  def create
    @rating_month = RatingMonth.new(rating_month_params)
    if @rating_month.save
      redirect_to [:admin, @rating_month]
    else
      render :new
    end
  end

  def show
    @federal_subject_rating_values = @rating_month.federal_subject_rating_values
      .includes(:federal_subject)
      .order("federal_subjects.name ASC")

    @total_values = @federal_subject_rating_values.map(&:total_value).uniq.sort.reverse
  end

  def edit
  end

  def update
    if @rating_month.update(rating_month_params)
      redirect_to [:admin, @rating_month]
    else
      render :edit
    end
  end

  def destroy
    if @rating_month.destroy
      redirect_to [:admin, :rating_months]
    else
      render :edit
    end
  end

  private

  def set_rating_month
    @rating_month = RatingMonth.find(params[:id])
  end

  def rating_month_params
    params.require(:rating_month).permit(:month)
  end
end