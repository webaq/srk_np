class Admin::UserGroupsController < AdminController
  before_action :set_user_group, only: [:show, :edit, :update, :destroy]

  def index
    authorize [:admin, UserGroup]
    @user_groups = UserGroup.order(:name)
  end

  def new
    @user_group = UserGroup.new
    authorize [:admin, @user_group]
    render :edit
  end

  def create
    @user_group = UserGroup.new(user_group_params)
    authorize [:admin, @user_group]
    if @user_group.save
      redirect_to [:admin, @user_group]
    else
      render :edit
    end
  end

  def show
  end

  def edit
  end

  def update
    if @user_group.update(user_group_params)
      redirect_to [:admin, @user_group]
    else
      render :edit
    end
  end

  def destroy
    if @user_group.destroy
      redirect_to [:admin, :user_groups]
    else
      render :edit
    end
  end

  private
    def user_group_params
      params.require(:user_group).permit(:name)
    end

    def set_user_group
      @user_group = UserGroup.find(params[:id])
      authorize [:admin, @user_group]
    end
end