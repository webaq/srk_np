class Admin::NotificationPopupsController < AdminController
  before_action :set_notification_popup, only: [:edit, :update, :destroy]

  def index
    authorize [:admin, NotificationPopup]
    @notification_popups = NotificationPopup.order(:starts_on)
  end

  def new
    authorize [:admin, NotificationPopup]
    @notification_popup = NotificationPopup.new
    render :edit
  end

  def create
    authorize [:admin, NotificationPopup]
    @notification_popup = NotificationPopup.new(notification_popup_params)
    if @notification_popup.save
      redirect_to admin_notification_popups_url, notice: "Попап создан"
    else
      render :edit
    end
  end

  def edit
  end

  def update
    if @notification_popup.update(notification_popup_params)
      redirect_to admin_notification_popups_url, notice: "Попап обновлён"
    else
      render :edit
    end
  end

  def destroy
    if @notification_popup.destroy
      redirect_to admin_notification_popups_url, notice: "Попап удалён"
    else
      render :edit
    end
  end

  private
    def notification_popup_params
      params.require(:notification_popup).permit([
        :name,
        :content,
        :starts_on,
        :ends_on,
        { organization_kinds: [] },
        :only_regional_organizations,
        :timeout,
        :close_button_when_user_clicks_link,
      ])
    end

    def set_notification_popup
      @notification_popup = NotificationPopup.find(params[:id])
      authorize [:admin, @notification_popup]
    end
end