class Admin::ComplaintsController < AdminController
  def index
    authorize [:admin, Complaint]
    @pagy, @complaints = pagy(Complaint.order(created_at: 'DESC'))
  end
end