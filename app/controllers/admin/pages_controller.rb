class Admin::PagesController < AdminController
  before_action :set_page, only: [:edit, :update, :destroy]

  def index
    authorize [:admin, Page]
    @pages = Page.order(:name)
  end

  def new
    @page = Page.new
    authorize [:admin, @page]

    render :edit
  end

  def create
    @page = Page.new(page_params)
    authorize [:admin, @page]

    if @page.save
      redirect_to admin_pages_url, notice: 'Страница добавлена.'
    else
      render :edit
    end
  end

  def edit
  end

  def update
    if @page.update(page_params)
      redirect_to admin_pages_url, notice: 'Страница сохранена.'
    else
      render :edit
    end
  end

  def destroy
    if @page.destroy
      redirect_to admin_pages_url, notice: 'Страница удалена.'
    else
      render :edit
    end
  end

  private
    def set_page
      @page = Page.find_by!(slug: params[:id])
      authorize [:admin, @page]
    end

    def page_params
      params.require(:page).permit([
        :name,
        :content,
        :public,
        :slug
      ])
    end
end