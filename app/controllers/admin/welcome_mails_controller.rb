class Admin::WelcomeMailsController < AdminController
  def create
    user = User.find(params[:user_id])
    NotifierMailer.welcome_mail(user).deliver
    redirect_to admin_user_url(user), notice: 'Welcome письмо отправлено'
  end
end