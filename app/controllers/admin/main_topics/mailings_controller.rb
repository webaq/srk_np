class Admin::MainTopics::MailingsController < AdminController
  def new
    @main_topics = MainTopic.find(params[:main_topic_ids] || [])

    if @main_topics.empty?
      render js: "alert('Не выбрана ни одна главная тема.')"
      return
    end
  end

  def create
    starts_on, ends_on = params[:period].split(' — ').map { |str| Date.parse(str) }

    if starts_on.nil? || ends_on.nil?
      render js: "alert('Нe задан период.')"
      return
    end

    press_releases = PressRelease
      .approved
      .where(main_topic_id: params[:main_topic_ids])
      .where(published_at: starts_on.to_time..ends_on.next_day.to_time)

    unless press_releases.any?
      render js: "alert('Ни одного инфоповода не найдено.')"
      return
    end

    emails = if false
      User
        .joins(:organization)
        .merge(Organization.media)
        .where(organizations: { federal_subject_id:  nil })
        .pluck(:email)
    else
      [
        'sergey@burtsev.ru',
        'a.parkhimovich@nationalpriority.ru',
        'n.lobacheva@nationalpriority.ru',
        'o.starikova@nationalpriority.ru'
      ]
    end

    emails.each do |email|
      NotifierMailer.with({
        email: email,
        starts_on: starts_on,
        ends_on: ends_on,
        press_releases: press_releases
      }).main_topics.deliver
    end
  end
end