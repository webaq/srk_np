class Admin::PressReleasesReportsController < AdminController
  def show
    authorize [:admin, :press_releases_report]
    
    @starts_on, @ends_on = if params[:period].present?
      params[:period].split(' — ').map { |str| Date.parse(str) }
    else
      [Date.today - 7, Date.today]
    end
    
    @ends_on = @starts_on unless @ends_on
    
    @press_releases_report = PressReleasesReport.new(@starts_on, @ends_on)
  end
end
