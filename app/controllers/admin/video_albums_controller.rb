class Admin::VideoAlbumsController < AdminController
  before_action :set_video_album, only: %i[ show edit update destroy ]

  def index
    @video_albums = VideoAlbum.order(:name)
    authorize [:admin, @video_albums]
  end

  def new
    @video_album = VideoAlbum.new
    authorize [:admin, @video_album]
    render :edit
  end

  def create
    @video_album = VideoAlbum.new(video_album_params)
    authorize [:admin, @video_album]

    if @video_album.save
      redirect_to [:admin, @video_album], notice: 'Альбом сохранён.'
    else
      render :edit
    end
  end

  def show; end

  def edit; end

  def update
    if @video_album.update(video_album_params)
      redirect_to [:admin, @video_album], notice: 'Альбом сохранён.'
    else
      render :edit
    end
  end

  def destroy
    if @video_album.destroy
      redirect_to [:admin, :video_albums], notice: 'Альбом удалён.'
    else
      render :edit
    end
  end

  private
    def video_album_params
      params.require(:video_album).permit([
        :name,
        :image,
        :published,
      ])
    end

    def set_video_album
      @video_album = VideoAlbum.find(params[:id])
      authorize [:admin, @video_album]
    end
end