class Admin::SendConfirmationLinksController < AdminController
  def create
    user = User.find(params[:user_id])
    user.send_confirmation_instructions
    render js: "alert('Пользователю #{user.name} отправлена ссылка на активацию.')"
  end
end