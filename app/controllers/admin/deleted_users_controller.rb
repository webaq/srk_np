class Admin::DeletedUsersController < AdminController
  before_action :set_user, only: [:update, :destroy]

  def index
    authorize [:admin, User]
    @users = User.with_discarded.discarded.order(discarded_at: 'desc')
  end

  def update
    if @user.update(user_params)
      redirect_to admin_deleted_users_url, notice: "Пользователь #{@user.email} восстановлен"
    else
      redirect_to admin_deleted_users_url, alert: @user.errors.full_messages
    end
  end

  def destroy
    if @user.destroy
      redirect_to admin_deleted_users_url, notice: "Пользователь #{@user.email} удалён навсегда"
    else
      redirect_to admin_deleted_users_url, alert: @user.errors.full_messages
    end
  end

  private
    def set_user
      @user = User.with_discarded.discarded.find(params[:id])
      authorize [:admin, @user]
    end

    def user_params
      params.require(:user).permit(:discarded_at)
    end
end