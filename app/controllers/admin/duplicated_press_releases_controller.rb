class Admin::DuplicatedPressReleasesController < AdminController
  def index
    authorize [:admin, PressRelease]

    @q = PressRelease
      .distinct
      .joins(:kept75_duplicate_press_releases)
      .preload(:kept75_duplicate_press_releases)
      .ransack(params[:q])

    @q.sorts = 'created_at desc' if @q.sorts.empty?
    @pagy, @original_press_releases = pagy(@q.result.includes([:national_projects, :federal_subjects,:user]))
  end

  def stats
    @percentages = [75] # PressReleaseOrigin::PERCENTAGES
  end
end