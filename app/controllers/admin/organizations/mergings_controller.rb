class Admin::Organizations::MergingsController < AdminController
  def create
    source_organization = Organization.find(params[:organization_id])
    destination_organization = Organization.find(params[:destination_organization_id])

    destination_organization.users << source_organization.users
    source_organization.destroy

    redirect_to admin_organizations_url(q: { name_cont: destination_organization.name })
  end
end