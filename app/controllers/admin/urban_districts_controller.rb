class Admin::UrbanDistrictsController < AdminController
  def new
    @federal_subject = FederalSubject.find(params[:federal_subject_id])
    @urban_district = @federal_subject.urban_districts.new
    authorize @urban_district

    render :edit
  end

  def create
    @federal_subject = FederalSubject.find(params[:federal_subject_id])
    @urban_district = @federal_subject.urban_districts.new(urban_district_params)
    authorize @urban_district

    if @urban_district.save
      redirect_to [:admin, @federal_subject], notice: 'Городской округ добавлен.'
    else
      render :edit
    end
  end

  def edit
    @urban_district = UrbanDistrict.find(params[:id])
    authorize @urban_district
  end

  def update
    @urban_district = UrbanDistrict.find(params[:id])
    authorize @urban_district

    if @urban_district.update(urban_district_params)
      redirect_to [:admin, @urban_district.federal_subject], notice: 'Городской округ сохранён.'
    else
      render :edit
    end
  end

  def destroy
    @urban_district = UrbanDistrict.find(params[:id])
    authorize @urban_district

    if @urban_district.destroy
      redirect_to [:admin, @urban_district.federal_subject], notice: 'Городской округ удалён.'
    else
      render :edit
    end
  end

  private
    def urban_district_params
      params.require(:urban_district).permit([
        :name,
      ])
    end
end