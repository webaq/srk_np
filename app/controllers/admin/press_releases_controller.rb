class Admin::PressReleasesController < AdminController
  def index
    authorize [:admin, PressRelease]

    @q = PressRelease.ransack(params[:q])
    @q.sorts = 'published_at desc' if @q.sorts.empty?

    respond_to do |format|
      format.html do
        @pagy, @press_releases = pagy(@q.result.includes([:national_projects, :federal_subjects, :user]))
      end
      format.xlsx do
        @press_releases = @q.result
          .limit(1000)
          .includes([
            :national_projects,
            :federal_subjects,
            :user,
            { press_release_value: [{ subcategory: :category }, :subcategory_scope]},
          ])
          .with_rich_text_content_and_embeds
        render xlsx: "index", filename: "инфоповоды_выгрузка.xlsx"
      end
    end
  end
end