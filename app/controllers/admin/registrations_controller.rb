class Admin::RegistrationsController < AdminController
  def index
    authorize [:admin, Registration]
    @registrations = Registration.kept.where(user_id: nil).order(:created_at)
    @pagy, @registrations = pagy(@registrations)
  end

  def confirmed
    authorize [:admin, Registration], :index?

    @registrations = Registration
      .kept
      .where.not(user_id: nil)
      .includes(user: :organization)
      .includes(:confirmed_user)
      .order(confirmed_at: 'desc')

    @pagy, @registrations = pagy(@registrations)
  end

  def deleted
    authorize [:admin, Registration], :index?
    @registrations = Registration.discarded.order(discarded_at: 'desc')
    @pagy, @registrations = pagy(@registrations)
  end

  def destroy
    @registration = Registration.find(params[:id])
    authorize [:admin, @registration]

    @registration.discard!
    NotifierMailer.rejected(@registration.email).deliver
    head :ok
  end
end