class Admin::PressReleases::SimiliarPressReleasesController < AdminController
  def index
    @press_release = PressRelease.find(params[:press_release_id])
    @similiar_press_releases = @press_release.similiar(0.7)
  end
end