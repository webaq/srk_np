class Admin::ApprovementsController < AdminController
  def index
    @q = PressRelease.ransack(params[:q])
    @press_releases = @q.result
      .includes(:approve_activities)
      .where(activities: { owner_id: User.where(regional_moderator: true) })
      .includes(:show_activities)
      .order(created_at: 'DESC')

    @pagy, @press_releases = pagy(@press_releases)
  end
end