class Admin::SubscriptionsController < AdminController
  before_action :set_subscription, only: [:edit, :update]

  def index
    @q = Subscription.ransack(params[:q])
    @q.sorts = 'created_at desc' if @q.sorts.empty?
    @pagy, @subscriptions = pagy(@q.result.includes(:user))
  end

  def edit; end

  def update
    @subscription.update(subscription_params)
  end

  def send_now
    @subscription = Subscription.find(params[:subscription_id])
    SubscriptionDelivery.new(params[:frequency], [@subscription]).call
  end

  private
    def set_subscription
      @subscription = Subscription.find(params[:id])
    end

    def subscription_params
      p = params.require(:subscription).permit([
        { federal_subject_ids: [] },
        { organization_ids: [] },
        { national_project_ids: [] },
        { tag_ids: [] },
        :frequency,
        :activated,
      ])

      p[:federal_subject_ids] = [] if p[:federal_subject_ids]&.include?('0')
      p[:organization_ids] = [] if p[:organization_ids]&.include?('0')
      p[:national_project_ids] = [] if p[:national_project_ids]&.include?('0')
      p[:tag_ids] = [] if p[:tag_ids]&.include?('0')

      p
    end
end