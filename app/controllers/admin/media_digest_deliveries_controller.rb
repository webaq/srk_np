class Admin::MediaDigestDeliveriesController < AdminController
  def index
    @pagy, @media_digest_deliveries = pagy(MediaDigestDelivery.order(created_at: 'DESC'))
  end

  def create
    csv = SendPulseCsv.new(params[:file].path)
    utc_zone = ActiveSupport::TimeZone.new('UTC')
    csv.enumerator.each do |row|
      next unless row['Тема письма'] == NotifierMailer::SUBSCRIPTION_MAIL_SUBJECT

      media_digest_delivery = MediaDigestDelivery.find_or_initialize_by(message_id: row['Email ID'])
      media_digest_delivery.recipient = row['Получатель'] if media_digest_delivery.new_record?
      media_digest_delivery.delivered_at = utc_zone.parse(row['Дата']) if row['Статус'] == 'Доставлено'
      media_digest_delivery.opened_at = utc_zone.parse(row['Дата']) if row['Прочитано'] == 'Прочитано'
      media_digest_delivery.clicked_at = utc_zone.parse(row['Дата']) if row['Переход по ссылке'].present?
      media_digest_delivery.save!
    end

    redirect_to admin_media_digest_deliveries_url
  end
end