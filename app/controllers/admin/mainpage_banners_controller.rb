class Admin::MainpageBannersController < AdminController
  before_action :set_mainpage_banner, only: [:edit, :update, :destroy]

  def index
    authorize [:admin, MainpageBanner]
    @mainpage_banners = MainpageBanner.order(:created_at)
  end

  def new
    authorize [:admin, MainpageBanner]
    @mainpage_banner = MainpageBanner.new
  end

  def edit
  end

  def create
    authorize [:admin, MainpageBanner]
    @mainpage_banner = MainpageBanner.new(mainpage_banner_params)

    respond_to do |format|
      if @mainpage_banner.save
        format.html { redirect_to [:admin, :mainpage_banners], notice: 'Баннер создан.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @mainpage_banner.update(mainpage_banner_params)
        format.html { redirect_to [:admin, :mainpage_banners], notice: 'Баннер обновлён.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @mainpage_banner.destroy
        format.html { redirect_to [:admin, :mainpage_banners], notice: 'Баннер удалён.' }
      else
        format.html { render :edit }
      end
    end
  end

  private
    def set_mainpage_banner
      @mainpage_banner = MainpageBanner.find(params[:id])
      authorize [:admin, @mainpage_banner]
    end

    def mainpage_banner_params
      params.require(:mainpage_banner).permit([
        :region,
        :source,
        :text,
        :url,
        :avatar,
      ])
    end
end
