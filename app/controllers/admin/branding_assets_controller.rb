class Admin::BrandingAssetsController < AdminController
  before_action :set_branding_asset, only: [:edit, :update, :destroy]

  def new
    @national_project = NationalProject.find(params[:national_project_id])
    @branding_asset = @national_project.branding_assets.new
    authorize @branding_asset
    render :edit
  end

  def create
    @national_project = NationalProject.find(params[:national_project_id])
    @branding_asset = @national_project.branding_assets.new(branding_asset_params)
    authorize @branding_asset

    if @branding_asset.save
      redirect_to [:admin, @national_project]
    else
      render :edit
    end
  end

  def edit
  end

  def update
    if @branding_asset.update(branding_asset_params)
      redirect_to [:admin, @branding_asset.national_project]
    else
      render :edit
    end
  end

  def destroy
    if @branding_asset.destroy
      redirect_to [:admin, @branding_asset.national_project]
    else
      render :edit
    end
  end

  private
    def branding_asset_params
      params.require(:branding_asset).permit([
        :description,
        :avatar,
        images: []
      ])
    end

    def set_branding_asset
      @branding_asset = BrandingAsset.find(params[:id])
      authorize @branding_asset
    end
end