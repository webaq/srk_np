class Admin::PressReleasesNationalProjectsReportsController < AdminController
  def show
    authorize [:admin, :press_release_creations_report]

    @starts_on, @ends_on = if params[:period].present?
      params[:period].split(' — ').map { |str| Date.parse(str) }
    else
      [Date.today - 7, Date.today]
    end

    @ends_on = @starts_on unless @ends_on

    @press_releases = PressRelease
      .approved
      .where(published_at: @starts_on.to_time..(@ends_on + 1).to_time)

    @press_releases = @press_releases.where(user_id: organization_kind_user_scope) if params[:organization_kind_eq].present?
    @press_releases = @press_releases.where(id: region_press_release_scope) if params[:federal_subject_id_eq].present?
    @press_releases = @press_releases.where(id: national_project_press_release_scope) if params[:national_project_id_eq].present?

    @press_releases_national_projects_report = @press_releases
      .joins(:national_projects)
      .group('national_projects.id')
      .order('national_projects.short_name')
      .pluck('national_projects.short_name, COUNT(press_releases.id)')

    @organizations = Organization
      .joins(users: { press_releases: :national_projects })
      .where(press_releases: { id: @press_releases })
      .group('organizations.id, national_projects.id')
      .select('organizations.*')
      .select('national_projects.short_name AS national_project_name')
      .select('array_agg(press_releases.id) AS press_release_ids')
      .select('COUNT(national_projects.id) AS national_projects_count')
      .preload(:federal_subject)

    @sort = params[:sort] || {"press_releases" => "desc"}
    # render json: @press_release_national_projects
  end

  private
    def organization_kind_user_scope
      User.joins(:organization).where(organizations: { kind: params[:organization_kind_eq] })
    end

    def region_press_release_scope
      PressRelease.joins(:federal_subjects).where(federal_subjects: { id: params[:federal_subject_id_eq] })
    end

    def national_project_press_release_scope
      PressRelease.joins(:national_projects).where(national_projects: { id: params[:national_project_id_eq] })
    end
end
