class Admin::NotificationPopups::PreviewsController < AdminController
  before_action :set_notification_popup

  def show
  end

  private
    def set_notification_popup
      @notification_popup = NotificationPopup.find(params[:notification_popup_id])
    end
end