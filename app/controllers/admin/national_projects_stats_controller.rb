class Admin::NationalProjectsStatsController < AdminController
  def index
    @national_projects = NationalProject.order(:short_name)

    @q = PressRelease.ransack(params[:q])

    @press_releases_counts_hash = @q.result
      .joins(:national_projects)
      .group('national_projects.id')
      .count

    @bookmarked_press_releases_counts_hash = @q.result
      .joins(:bookmarks)
      .joins(:national_projects)
      .group('national_projects.id')
      .count

    @reposts_counts_hash = @q.result
      .joins(:reposts)
      .joins(:national_projects)
      .group('national_projects.id')
      .count

    @national_projects_chart = [
      {
        name: 'Кол-во загруженных инфоповодов',
        data: @national_projects.map { |national_project| [national_project.short_name, @press_releases_counts_hash.fetch(national_project.id, 0)] },
      },

      {
        name: 'Кол-во инфоповодов, взятых СМИ',
        data: @national_projects.map { |national_project| [national_project.short_name, @bookmarked_press_releases_counts_hash.fetch(national_project.id, 0)] },
      },

      {
        name: 'Кол-во опубликованных материалов',
        data: @national_projects.map { |national_project| [national_project.short_name, @reposts_counts_hash.fetch(national_project.id, 0)] },
      },
    ]
  end
end