class Admin::FederalSubjectsController < AdminController
  before_action :set_federal_subject, only: [:show, :edit, :update, :destroy]

  def index
    authorize [:admin, FederalSubject]
    @federal_subjects = FederalSubject.order(:name)
    @regional_moderators = User.joins(:moderatable_federal_project_permissions).where(regional_moderator: true).distinct.group_by(&:federal_subject_id)
  end

  def new
    @federal_subject = FederalSubject.new
    authorize [:admin, @federal_subject]

    render :edit
  end

  def create
    @federal_subject = FederalSubject.new(federal_subject_params)
    authorize [:admin, @federal_subject]

    if @federal_subject.save
      redirect_to [:admin, @federal_subject], notice: 'Субъект Федерации добавлен.'
    else
      render :edit
    end
  end

  def show
    @urban_districts = @federal_subject.urban_districts.order(:name)
  end

  def edit
  end

  def update
    if @federal_subject.update(federal_subject_params)
      redirect_to [:admin, :federal_subjects], notice: 'Субъект Федерации сохранён.'
    else
      render :edit
    end
  end

  def destroy
    if @federal_subject.destroy
      redirect_to [:admin, :federal_subjects], notice: 'Субъект Федерации удалён.'
    else
      flash.now[:alert] = @federal_subject.errors.full_messages.join(', ')
      render :edit
    end
  end

  private
    def set_federal_subject
      @federal_subject = FederalSubject.find(params[:id])
      authorize [:admin, @federal_subject]
    end

    def federal_subject_params
      params.require(:federal_subject).permit([
        :name,
        :time_zone,
        :regional_moderated,
      ])
    end
end