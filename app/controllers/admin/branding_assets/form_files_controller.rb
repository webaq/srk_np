class Admin::BrandingAssets::FormFilesController < ApplicationController
  def new
    @blob = ActiveStorage::Blob.find(params[:blob_id])
  end
end
