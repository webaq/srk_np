class Admin::NationalProjectsReportsController < AdminController
  def show
    authorize [:admin, :national_projects_report]

    @starts_on, @ends_on = if params[:period].present?
      params[:period].split(' — ').map { |str| Date.parse(str) }
    else
      [Date.today - 7, Date.today]
    end

    @ends_on = @starts_on unless @ends_on

    @national_projects_report = NationalProjectsReport.new(
      Organization.order(:name),
      PressRelease.where(created_at: (@starts_on.to_time..(@ends_on + 1).to_time))
    )
  end
end
