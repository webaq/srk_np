class Admin::SupportEmailsController < AdminController
  before_action :set_support_email, only: [:show, :edit, :update, :destroy]

  # GET /admin/support_emails/new
  def new
    @support_email = SupportEmail.new
    authorize [:admin, @support_email]
  end

  # GET /admin/support_emails/1/edit
  def edit
  end

  # POST /admin/support_emails
  # POST /admin/support_emails.json
  def create
    @support_email = SupportEmail.new(support_email_params)
    authorize [:admin, @support_email]

    respond_to do |format|
      if @support_email.save
        format.html { redirect_to admin_mailing_lists_url, notice: 'Support email was successfully created.' }
        # format.json { render :show, status: :created, location: @support_email }
      else
        format.html { render :new }
        # format.json { render json: @support_email.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/support_emails/1
  # PATCH/PUT /admin/support_emails/1.json
  def update
    respond_to do |format|
      if @support_email.update(support_email_params)
        format.html { redirect_to admin_mailing_lists_url, notice: 'Support email was successfully updated.' }
        # format.json { render :show, status: :ok, location: @support_email }
      else
        format.html { render :edit }
        # format.json { render json: @support_email.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/support_emails/1
  # DELETE /admin/support_emails/1.json
  def destroy
    @support_email.destroy
    respond_to do |format|
      format.html { redirect_to admin_mailing_lists_url, notice: 'Support email was successfully destroyed.' }
      # format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_support_email
      @support_email = SupportEmail.find(params[:id])
      authorize [:admin, @support_email]
    end

    # Only allow a list of trusted parameters through.
    def support_email_params
      params.require(:support_email).permit(:email)
    end
end
