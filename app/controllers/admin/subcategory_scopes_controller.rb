class Admin::SubcategoryScopesController < AdminController
  before_action :set_subcategory, only: [:new, :create]
  before_action :set_subcategory_scope, only: [:edit, :update, :destroy]

  def new
    @subcategory_scope = @subcategory.subcategory_scopes.new
    render :edit
  end

  def create
    @subcategory_scope = @subcategory.subcategory_scopes.new(subcategory_scope_params)
    if @subcategory_scope.save
      redirect_to [:admin, @subcategory_scope.subcategory]
    else
      render :edit
    end
  end

  def edit
  end

  def update
    if @subcategory_scope.update(subcategory_scope_params)
      redirect_to [:admin, @subcategory_scope.subcategory]
    else
      render :edit
    end
  end

  def destroy
    if @subcategory_scope.destroy
      redirect_to [:admin, @subcategory_scope.subcategory]
    else
      render :edit
    end
  end

  private
    def set_subcategory
      @subcategory = Subcategory.find(params[:subcategory_id])
    end

    def set_subcategory_scope
      @subcategory_scope = SubcategoryScope.find(params[:id])
    end

    def subcategory_scope_params
      params.require(:subcategory_scope).permit([
        :name,
      ])
    end
end