class Admin::FederalSubjectRatingValuesController < AdminController
  before_action :set_rating_month, only: [:new, :create]
  before_action :set_federal_subject_rating_value, only: [:edit, :update, :destroy]

  def new
    @federal_subject_rating_value = @rating_month.federal_subject_rating_values.new
  end

  def create
    @federal_subject_rating_value = @rating_month.federal_subject_rating_values.new(federal_subject_rating_value_params)
    if @federal_subject_rating_value.save
      redirect_to [:admin, @federal_subject_rating_value.rating_month]
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @federal_subject_rating_value.update(federal_subject_rating_value_params)
      redirect_to [:admin, @federal_subject_rating_value.rating_month]
    else
      render :edit
    end
  end

  def destroy
    if @federal_subject_rating_value.destroy
      redirect_to [:admin, @federal_subject_rating_value.rating_month]
    else
      render :edit
    end
  end

  private

  def federal_subject_rating_value_params
    params.require(:federal_subject_rating_value).permit([
      :federal_subject_id,
      :press_releases_count,
      :press_releases_value,
      :press_releases_value_with_nullified,
      :value_with_returns,
      :quality,

      :regional_media_links_count,
      :federal_media_links_count,
      :awarded_count,
      :likeness,
      :activity_rate,

      :total_value,
    ])
  end

  def set_rating_month
    @rating_month = RatingMonth.find(params[:rating_month_id])
  end

  def set_federal_subject_rating_value
    @federal_subject_rating_value = FederalSubjectRatingValue.find(params[:id])
  end
end