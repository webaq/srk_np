class PressReleases::ApprovementsController < ApplicationController
  def create
    @press_release = PressRelease.find(params[:press_release_id])
    authorize @press_release, :approve?

    # if @press_release.max_approved_at.future?
    @press_release.update!(approved_at: Time.current, returned_at: nil)
    @press_release.create_activity key: 'press_release.approve', owner: current_user
    NotifierMailer.press_release_approved(@press_release.user.email).deliver_later unless @press_release.user == current_user
    head :ok
    # else
    #   render 'max_approved_at_passed', status: :forbidden
    # end
  end

  def destroy
    @press_release = PressRelease.find(params[:press_release_id])
    authorize @press_release, :approve?

    @press_release.update!(approved_at: nil)
    @press_release.create_activity key: 'press_release.unapprove', owner: current_user
    head :ok
  end
end