class PressReleases::TopListItemsController < ApplicationController
  before_action :set_press_release

  def create
    unless @press_release.top_list_item
      @top_list_item = @press_release.build_top_list_item
      authorize @top_list_item
      if @top_list_item.save
        TopListItem.joins(:press_release).order('press_releases.published_at DESC').offset(6).destroy_all
      else
        Rails.logger.debug "! #{@top_list_item.errors.full_messages}"
      end
      @press_release.reload
    end
    render :update
  end

  def destroy
    if @press_release.top_list_item
      authorize @press_release.top_list_item
      @press_release.top_list_item.destroy
      @press_release.reload
    end
    render :update
  end

  private
    def set_press_release
      @press_release = PressRelease.find(params[:press_release_id])
    end
end