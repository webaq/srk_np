class PressReleases::ComplaintsController < ApplicationController
  before_action :set_press_release

  def new
    @complaint = current_user.complaints.new(press_release: @press_release)
  end

  def create
    @complaint = current_user.complaints.new(complaint_params.merge(press_release: @press_release))

    if @complaint.save
      NotifierMailer.with(complaint_id: @complaint.id).complaint.deliver_later
    else
      render :reload_form
    end
  end

  private
    def set_press_release
      @press_release = PressRelease.find(params[:press_release_id])
    end

    def complaint_params
      params.require(:complaint).permit(:body)
    end
end