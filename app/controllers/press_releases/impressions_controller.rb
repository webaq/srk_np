class PressReleases::ImpressionsController < ApplicationController
  before_action :set_press_release
  before_action :set_impression

  def update
    @impression.update!(impression_params)
  end

  private
    def set_press_release
      @press_release = PressRelease.find(params[:press_release_id])
    end

    def set_impression
      @impression = current_user.press_release_impression(params[:press_release_id])
      authorize @impression, :create?
    end

    def impression_params
      params.require(:impression).permit([
        :likeness,
        :score
      ])
    end
end