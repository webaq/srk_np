class PressReleases::ModeratorsController < ApplicationController
  def index
    @press_release = PressRelease.find(params[:press_release_id])
    authorize @press_release, :show_moderators?
    
    federal_subject_scope = User.where(federal_subject_id: nil)
    federal_subject_scope = federal_subject_scope.or(User.where(federal_subject_id: @press_release.federal_subject_ids.first)) if @press_release.federal_subject_ids.one?
    
    @users = User
      .includes(:organization)
      .with_attached_avatar.order('organizations.name')
      .order(:last_name)
      .where(id:
        User.includes(:moderatable_national_projects).where(national_projects: { id: @press_release.national_project_ids })
      )
      .where(id: federal_subject_scope)
  end
end