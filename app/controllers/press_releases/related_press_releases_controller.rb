class PressReleases::RelatedPressReleasesController < ApplicationController
  def autocomplete
    term = params[:q]

    @press_releases = policy_scope(PressRelease).limit(10)

    if (id = PressRelease.extract_id_from_url(term))
      @press_releases = @press_releases.where(id: id)
    else
      @press_releases = @press_releases.ransack(title_cont: term).result
    end

    render layout: nil
  end
end