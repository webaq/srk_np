class PressReleases::BookmarkedUsersController < ApplicationController
  def index
    @press_release = PressRelease.find(params[:press_release_id])
    authorize @press_release, :show_bookmarked_users?

    @users = User.includes(:organization, :bookmarks).where(bookmarks: { press_release_id: @press_release.id }).order('organizations.name').order(:last_name)
  end
end