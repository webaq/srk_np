class PressReleases::MeasuresController < ApplicationController
  def index
    respond_to do |format|
      format.json do
        @measures = policy_scope([:press_releases, Measure])
          .where(federal_project_id: params[:federal_project_id])
          .order(:name)
      end
    end
  end
end