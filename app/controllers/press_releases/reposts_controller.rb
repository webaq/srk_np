class PressReleases::RepostsController < ApplicationController
  def create
    @press_release = PressRelease.find(params[:press_release_id])
    @repost = current_user.reposts.new(repost_params.merge(press_release: @press_release))
    authorize @repost
    @repost.save
  end

  def destroy
    @repost = Repost.find(params[:id])
    authorize @repost

    unless @repost.destroy
      render js: "alert('Что-то пошло не так…')"
    end
  end

  private
    def repost_params
      params.require(:repost).permit(:link)
    end
end