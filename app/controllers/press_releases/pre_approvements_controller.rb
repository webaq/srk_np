class PressReleases::PreApprovementsController < ApplicationController
  before_action :set_press_release

  def create
    respond_to do |format|
      if @press_release.touch(:pre_approved_at)
        @press_release.create_activity key: 'press_release.approve', owner: current_user
        # NotifierMailer.press_release_approved(@press_release.user.email).deliver_later unless @press_release.user == current_user
        format.js
      end
    end
  end

  def destroy
    respond_to do |format|
      if @press_release.update(pre_approved_at: nil)
        @press_release.create_activity key: 'press_release.unapprove', owner: current_user
        format.js
      end
    end
  end

  private
    def set_press_release
      @press_release = PressRelease.find(params[:press_release_id])
      authorize @press_release, :pre_approve?
    end
end