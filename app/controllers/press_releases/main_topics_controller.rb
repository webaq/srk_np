class PressReleases::MainTopicsController < ApplicationController
  before_action :set_press_release

  def edit
    @main_topics = MainTopic.published.order(:starts_at)
  end

  def update
    @press_release.update(press_release_params)
  end

  private
    def set_press_release
      @press_release = PressRelease.find(params[:press_release_id])
      authorize @press_release, :update_main_topic?
    end

    def press_release_params
      params.require(:press_release).permit(:main_topic_id)
    end
end