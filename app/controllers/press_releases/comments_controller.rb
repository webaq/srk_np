class PressReleases::CommentsController < ApplicationController
  def index
    @press_release = PressRelease.with_rich_text_content_and_embeds.find(params[:press_release_id])
    authorize @press_release, :comments?

    @comments = @press_release.comments.order(:created_at)
  end

  def create
    @press_release = PressRelease.find(params[:press_release_id])
    @comment = current_user.comments.new(comments_params.merge(press_release: @press_release))
    authorize @comment

    if @comment.save
      if params[:return_to_author] == '1' && policy(@press_release).return_to_author?
        @press_release.update({
          approved_at: nil,
          pre_approved_at: nil,
          returned_at: Time.current,
        })
        @press_release.create_activity key: 'press_release.return_to_author', owner: current_user
      end

      CommentCreatedNotification.new(@comment).call
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    authorize @comment

    unless @comment.destroy
      render js: "alert('Что-то пошло не так…')"
    end
  end

  private
    def comments_params
      params.require(:comment).permit(:text)
    end
end