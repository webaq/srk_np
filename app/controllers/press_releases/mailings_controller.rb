class PressReleases::MailingsController < ApplicationController
  before_action :set_press_release

  def new
    @mailing_lists = current_user.mailing_lists.order(:name)
  end

  def create
    mailing_lists = current_user.mailing_lists.find(params[:mailing_list_ids] || [])

    if mailing_lists.empty?
      render js: "alert('Список рассылки не выбран.')"
      return
    end

    emails = Set.new

    mailing_lists.each do |mailing_list|
      mailing_list.mailing_list_items.each do |mailing_list_item|
        emails.add mailing_list_item.email
      end
    end

    emails.each do |email|
      NotifierMailer.press_release_mail(email, @press_release).deliver
    end
  end

  private
    def set_press_release
      @press_release = PressRelease.find(params[:press_release_id])
    end
end