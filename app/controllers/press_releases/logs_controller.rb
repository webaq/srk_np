class PressReleases::LogsController < ApplicationController
  def index
    @press_release = PressRelease.find(params[:press_release_id])
    authorize @press_release, :show_logs?
    
    @activities = PublicActivity::Activity.where(trackable_type: 'PressRelease', trackable_id: @press_release.id).order(created_at: 'DESC')
  end
end