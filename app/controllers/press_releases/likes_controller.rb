class PressReleases::LikesController < ApplicationController
  before_action :set_press_release
  before_action :set_press_release_like, only: [:destroy]

  def create
    current_user.press_release_likes.create!(press_release: @press_release)
    @press_release.reload
    render :update
  end

  def destroy
    current_user.press_release_likes.destroy(@press_release_like)
    @press_release.reload
    render :update
  end

  private
    def set_press_release
      @press_release = PressRelease.find(params[:press_release_id])
    end

    def set_press_release_like
      @press_release_like = current_user.press_release_likes.find_by!(press_release_id: @press_release.id)
    end
end