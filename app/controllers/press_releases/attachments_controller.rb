class PressReleases::AttachmentsController < ApplicationController
  def index
    press_release = PressRelease.find(params[:press_release_id])

    respond_to do |format|
      format.zip do
        compressed_filestream = Zip::OutputStream.write_buffer do |zos|
          press_release.images.each do |image|
            zos.put_next_entry image.filename
            # zos.print image.read
            image.blob.download { |chunk| zos.write(chunk) }
          end
        end
        compressed_filestream.rewind
        send_data compressed_filestream.read, filename: "Archive.zip"
      end
    end
  end
end