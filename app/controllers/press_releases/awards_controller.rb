class PressReleases::AwardsController < ApplicationController
  before_action :set_press_release

  def create
    @press_release.update(awarded: true)
    render :update
  end

  def destroy
    @press_release.update(awarded: false)
    render :update
  end

  private
    def set_press_release
      @press_release = PressRelease.find(params[:press_release_id])
      authorize @press_release, :award?
    end
end