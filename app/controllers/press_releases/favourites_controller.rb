class PressReleases::FavouritesController < ApplicationController
  before_action :set_press_release

  def create
    unless current_user.favourite_press_releases.exists?(@press_release.id)
      current_user.favourite_press_releases << @press_release
    end
    render :update
  end

  def destroy
    current_user.favourite_press_releases.destroy(@press_release)
    render :update
  end

  private
    def set_press_release
      @press_release = PressRelease.find(params[:press_release_id])
    end
end