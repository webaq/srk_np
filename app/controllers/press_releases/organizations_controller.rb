class PressReleases::OrganizationsController < ApplicationController
  def index
    # @organiztions = Organization.where(id: policy_scope(PressRelease).joins(user: :organization).select('organizations.id')).order(:name).limit(10)
    @q = Organization.ransack(name_cont: params[:q])
    @organizations = @q.result.order(:name).limit(10)
  end
end