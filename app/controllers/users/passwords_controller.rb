# frozen_string_literal: true

class Users::PasswordsController < Devise::PasswordsController
  # GET /resource/password/new
  # def new
  #   super
  # end

  # POST /resource/password
  # def create
  #   super
  # end

  # GET /resource/password/edit?reset_password_token=abcdef
  def edit
    @user_by_token = User.with_reset_password_token(params[:reset_password_token])
    unless @user_by_token
      redirect_to root_url
      return
    end
    super
  end

  # PUT /resource/password
  def update
    self.resource = resource_class.reset_password_by_token(resource_params)
    resource.assign_attributes(extended_resource_params)
    if resource.save(context: :account_setup)
      if Devise.sign_in_after_reset_password
        flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
        set_flash_message!(:notice, flash_message)
        resource.after_database_authentication
        sign_in(resource_name, resource)
      else
        set_flash_message!(:notice, :updated_not_active)
      end
      respond_with resource, location: after_resetting_password_path_for(resource)
    else
      render :edit
    end
  end

  protected
    def extended_resource_params
      params.require(:user).permit([
        :first_name,
        :last_name,
        :contact_position,
        :contact_phone,
        :reset_password_token,
        :password,
        :password_confirmation
      ])
    end

  # def after_resetting_password_path_for(resource)
  #   super(resource)
  # end

  # The path used after sending reset password instructions
  # def after_sending_reset_password_instructions_path_for(resource_name)
  #   super(resource_name)
  # end
end
