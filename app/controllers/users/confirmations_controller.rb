# frozen_string_literal: true

class Users::ConfirmationsController < Devise::ConfirmationsController
  # GET /resource/confirmation/new
  # def new
  #   super
  # end

  # POST /resource/confirmation
  def create
    @user = User.find_first_by_auth_conditions(confirmation_token: params[:user][:confirmation_token])

    @user.assign_attributes(user_params)
    if @user.save#(context: :account_setup)
      @user.update_columns({
        confirmed_at: Time.current
      })
      bypass_sign_in @user, scope: :user

      if @user.organization&.media?
        NotifierMailer.welcome_media(@user.email).deliver_later
      else
        NotifierMailer.welcome_others(@user.email).deliver_later
      end

      redirect_to root_url
    else
      render :show
    end
  end

  # GET /resource/confirmation?confirmation_token=abcdef
  def show
    if params[:confirmation_token].blank?
      redirect_to root_url
      return
    end

    @user = User.find_first_by_auth_conditions(confirmation_token: params[:confirmation_token])

    if !@user# || @user.confirmed?
      redirect_to root_url
      return
    end
  end

  protected
    def user_params
      params.require(:user).permit([
        :first_name,
        :last_name,
        :contact_position,
        :contact_phone,
        :password,
        :password_confirmation
      ])
    end

  # The path used after resending confirmation instructions.
  # def after_resending_confirmation_instructions_path_for(resource_name)
  #   super(resource_name)
  # end

  # The path used after confirmation.
  # def after_confirmation_path_for(resource_name, resource)
  #   super(resource_name, resource)
  # end
end
