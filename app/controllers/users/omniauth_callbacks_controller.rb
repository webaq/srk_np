class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  skip_before_action :verify_authenticity_token, only: %i[vkontakte facebook odnoklassniki google_oauth2]
  around_action :process_callback, only: %i[vkontakte facebook odnoklassniki google_oauth2]

  def vkontakte; end
  def facebook; end
  def odnoklassniki; end
  def google_oauth2; end

  def failure
    unless current_user
      redirect_to new_user_session_url
    else
      redirect_to settings_profile_url
    end
  end

  private
    def process_callback
      unless current_user
        @user = User.from_omniauth(request.env["omniauth.auth"])
        if @user
          sign_in_and_redirect @user, event: :authentication #this will throw if @user is not activated
          # set_flash_message(:notice, :success, kind: "Facebook") if is_navigational_format?
        else
          # session["devise.facebook_data"] = request.env["omniauth.auth"].except(:extra) # Removing extra as it can overflow some session stores
          flash[:alert] = 'Такой пользователь не найден или аккаунт не привязан в настройках профиля.'
          redirect_to new_user_session_url
        end
      else
        omniauth_account = current_user.omniauth_accounts.find_or_initialize_by(provider: request.env["omniauth.auth"].provider)
        if omniauth_account.update(omniauth_account_params)
          redirect_to settings_profile_url, notice: 'Аккаунт успешно привязан к Вашей учётной записи'
        else
          redirect_to settings_profile_url, alert: 'Аккаунт уже привязан к другой учётной записи'
        end
      end
    end

    def omniauth_account_params
      {
        provider: request.env["omniauth.auth"].provider,
        uid: request.env["omniauth.auth"].uid,
      }
    end
end