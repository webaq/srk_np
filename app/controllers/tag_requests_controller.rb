class TagRequestsController < ApplicationController
  def new
    @tag_request = TagRequest.new
  end

  def create
    @tag_request = TagRequest.new(tag_request_params)

    if @tag_request.valid?
      UserMailer.with({
        user: current_user,
        tag_request: @tag_request,
      }).tag_request.deliver_now
    else
      render :reload_form
    end
  end

  private
    def tag_request_params
      params.require(:tag_request).permit(:subject)
    end
end