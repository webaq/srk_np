class Api::SendPulse::CallbacksController < ActionController::API
  def create
    # events = JSON.parse(params['_json'])

    params['_json'].each do |event|
      subscription_message = SubscriptionMessage.find_by(message_id: event['message_id'])
      next unless subscription_message

      time = Time.at(event['timestamp'])

      case event['event']
      when 'delivered'
        subscription_message.update!(delivered_at: time)
      when 'undelivered'
        subscription_message.update!(undelivered_at: time)
      when 'opened'
        subscription_message.update!(opened_at: time)
      when 'clicked'
        subscription_message.update!(clicked_at: time)
      when 'unsubscribed'
        subscription_message.update!(unsubscribed_at: time)
      when 'spam_by_user'
        subscription_message.update!(spamed_at: time)
      end
    end
  end
end