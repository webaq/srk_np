class Api::V1::PressReleasesController < ApiController
  def create
    @press_release = current_user.press_releases.new(press_release_params)
    authorize @press_release if @press_release.valid?

    if @press_release.save(context: :press_release_submit)
      @press_release.create_activity key: 'press_release.create', owner: current_user

      if policy(@press_release).approve?
        @press_release.approve
        @press_release.create_activity key: 'press_release.approve', owner: current_user
      end
      render :show, status: :created, location: @press_release
    else
      render json: @press_release.errors, status: :unprocessable_entity
    end
  end

  private
    def press_release_params
      fields = [
        :press_release_value_id,
        { national_project_ids: [] },
        { tag_ids: [] },
        { federal_project_ids: [] },
        { selected_federal_subject_ids: [] },
        :title,
        :content,
        { image_urls: [] },
        {
          experts_attributes: [
            :id,
            :name,
            :_destroy
          ]
        },
        :main_topic_id,
        :late_published_at,
        :starts_at,
      ]

      params.require(:press_release).permit(fields).merge({
        api_loaded: true,
        authors_contacts_published: true,
      })
    end
end