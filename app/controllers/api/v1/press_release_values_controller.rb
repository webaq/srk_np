class Api::V1::PressReleaseValuesController < ApiController
  def index
    @press_release_values = PressReleaseValue.all
      .includes(subcategory: :category)
      .includes(:subcategory_scope)
      .order("categories.position")
      .order("subcategories.position")
      .order(:position)
  end
end