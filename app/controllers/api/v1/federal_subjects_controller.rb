class Api::V1::FederalSubjectsController < ApiController
  def index
    @federal_subjects = FederalSubject.order(:name)
  end
end