class Api::V1::NationalProjectsController < ApiController
  def index
    @national_projects = NationalProject.order(:name)
  end
end