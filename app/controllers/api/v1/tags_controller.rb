class Api::V1::TagsController < ApiController
  def index
    @tags = Tag.includes(:national_projects).order(:name)
  end
end