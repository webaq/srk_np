class Api::V1::MainTopicsController < ApiController
  def index
    @main_topics = MainTopic.published.order(:name)
  end
end