class VideoClipsController < ApplicationController
  def show
    @video_clip = VideoClip.find(params[:id])
  end
end