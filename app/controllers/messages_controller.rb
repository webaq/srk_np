class MessagesController < ApplicationController
  def read
    message_delivery = current_user.message_deliveries.published.find_by(message_id: params[:message_id])
    message_delivery.touch(:read_at) unless message_delivery.read_at
    head :ok
  end
end