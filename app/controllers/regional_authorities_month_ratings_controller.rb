class RegionalAuthoritiesMonthRatingsController < ApplicationController
  before_action :set_rating_month, only: [:show, :update, :destroy]
  before_action :set_compact_header

  def index
    @rating_month = policy_scope(RatingMonth).order(:month).last
    raise unless @rating_month
    redirect_to regional_authorities_month_rating_url(@rating_month.month)
  end

  def create
    authorize RatingMonth
    @rating_month = RatingMonth.create!({
      month: RatingMonth.order(:month).last.month.next_month,
      published: false
    })
    redirect_to regional_authorities_month_rating_url(@rating_month.month)
  end

  def show
    @rating = RegionalAuthoritiesMonthRating.new(@rating_month, params[:scoring])

    @month = @rating_month.month
    @all_rating_months = policy_scope(RatingMonth).order(:month)

    if policy(RatingMonth).create?
      @new_month = RatingMonth.order(:month).last.month.next_month
    end

    respond_to do |format|
      format.html # { render 'show.pdf', layout: nil }
      format.pdf do
        render pdf: 'show', layout: nil, viewport_size: '1280x1024'
      end
    end
  end

  def update
    @rating_month.update!(rating_month_params)
    redirect_to regional_authorities_month_rating_url(@rating_month.month)
  end

  def destroy
    @rating_month.destroy!
    redirect_to regional_authorities_month_ratings_url
  end

  # def media
  #   set_month(params[:regional_authorities_month_rating_id])
  #   unless @month
  #     redirect_to regional_authorities_month_rating_url(@months.last)
  #     return
  #   end
  #
  #   @month = @months.find { |m| m == Date.parse(params[:regional_authorities_month_rating_id]) } || @months.last
  #   @rating = RegionalAuthoritiesMonthRating.new(@month)
  #   respond_to do |format|
  #     format.pdf do
  #       render pdf: 'show', layout: nil, viewport_size: '1280x1024'
  #     end
  #   end
  # end

  private
    def set_rating_month
      @rating_month = policy_scope(RatingMonth).find_by(month: Date.parse(params[:id]))
      unless @rating_month
        redirect_to regional_authorities_month_ratings_url
        return
      end
      authorize @rating_month
    end

    def set_compact_header
      @compact_header = true
    end

    def rating_month_params
      params.require(:rating_month).permit(:published)
    end
end