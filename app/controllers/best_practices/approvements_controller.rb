class BestPractices::ApprovementsController < ApplicationController
  before_action :set_best_practice

  def create
    if @best_practice.update(approved_at: Time.current)
      @best_practice.create_activity key: 'approve', owner: current_user
    end

    redirect_to @best_practice
  end

  def destroy
    if @best_practice.update(approved_at: nil)
      @best_practice.create_activity key: 'unapprove', owner: current_user
    end

    redirect_to @best_practice
  end

  private
    def set_best_practice
      @best_practice = BestPractice.find(params[:best_practice_id])
      authorize @best_practice, :approve?
    end
end