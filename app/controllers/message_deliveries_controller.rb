class MessageDeliveriesController < ApplicationController
  before_action :set_message_delivery, only: [:destroy]

  def destroy
    @message_delivery.destroy
    head :ok
  end

  private
    def set_message_delivery
      @message_delivery = current_user.message_deliveries.published.find(params[:id])
    end
end