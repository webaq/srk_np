class BestPracticesController < ApplicationController
  include Pagy::Backend
  include MainTopicsListable

  before_action :set_best_practice, only: [:show, :edit, :update, :destroy]

  def index
    @q = BestPractice.ransack(params[:q])
    @pagy, @best_practices = pagy(policy_scope(@q.result).order(published_at: 'DESC'))
  end

  def new
    authorize BestPractice
    @best_practice = BestPractice.new
    render :edit
  end

  def create
    @best_practice = current_user.best_practices.new(best_practice_params)
    if @best_practice.save
      UserMailer.with(best_practice: @best_practice).best_practice_created.deliver_later
      redirect_to @best_practice
    else
      render :edit
    end
  end

  def show; end

  def edit; end

  def update
    if @best_practice.update(best_practice_params)
      redirect_to @best_practice
    else
      render :edit
    end
  end

  def destroy
    if @best_practice.destroy
      redirect_to best_practices_url
    else
      render :edit
    end
  end

  private
    def best_practice_params
      params.require(:best_practice).permit([
        :name,
        :content,
        :filename,
        :image,
        { images: [] },
        :published_at,
      ])
    end

    def set_best_practice
      @best_practice = BestPractice.find(params[:id])
      authorize @best_practice
    end

    def use_published_at
      @best_practice.persisted? || params.dig(:best_practice, :published_at).present?
    end
    helper_method :use_published_at
end