class ExportRequestsController < ApplicationController
  def new
    @export_request = ExportRequest.new(email: current_user.email)
  end

  def create
    @export_request = ExportRequest.new(export_request_params)

    if @export_request.valid?
      UserMailer.with({
        user: current_user,
        export_request: @export_request,
      }).export_request.deliver_now
    else
      render :reload_form
    end
  end

  private
    def export_request_params
      params.require(:export_request).permit([
        :subject,
        :body,
        { federal_subject_ids: [] },
        :email,
        { filters: [:key, :value] },
      ])
    end
end