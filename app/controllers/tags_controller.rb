class TagsController < ApplicationController
  include MainTopicsListable

  def index
    @q = Tag.ransack(params[:q])
    @q.sorts = 'name asc' if @q.sorts.empty?
    @tags = @q.result
  end
end