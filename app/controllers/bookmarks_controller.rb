class BookmarksController < ApplicationController
  def create
    @press_release = PressRelease.find(params[:press_release_id])

    bookmark = current_user.bookmarks.new(press_release: @press_release)
    authorize bookmark

    if bookmark.save
    else
      render js: "alert('Что-то пошло не так…')"
    end
  end

  def destroy
    @press_release = PressRelease.find(params[:press_release_id])

    bookmark = current_user.bookmarks.find_by(press_release: @press_release)
    authorize bookmark

    if bookmark.destroy
    else
      render js: "alert('Что-то пошло не так…')"
    end
  end
end