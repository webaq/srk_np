class VideoAlbumsController < ApplicationController
  include MainTopicsListable

  def index
    @video_albums = VideoAlbum.where(published: true).order(:name)
  end

  def show
    @video_album = VideoAlbum.find(params[:id])
    @video_clips = @video_album.video_clips.order(:name)
  end
end