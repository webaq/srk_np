# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# press_office_user = User.press_office.first
# media_user = User.media.first
#
# federal_projects = FederalProject.all
# federal_subjects = [FederalSubject.first]
#
# if press_office_user && media_user
#   1000.times do |press_release_index|
#     federal_subject = federal_subjects.sample
#     urban_district = federal_subject.urban_districts.sample
#
#     press_release = PressRelease.create!({
#       status: PressRelease.statuses.keys.sample,
#       user: press_office_user,
#       federal_project: federal_projects.sample,
#       federal_subject: federal_subject,
#       urban_district: urban_district,
#       title: "Сгенерированное тестовое событие №#{press_release_index}",
#       content: "Содержательные и интересный текст сгенерированного тестового события №#{press_release_index}"
#     })
#
#     rand(20).times do |repost_index|
#       press_release.reposts.create!({
#         user: media_user,
#         link: "https://www.example.com/#{repost_index}"
#       })
#     end
#   end
# end

federal_subject = FederalSubject.find_or_create_by(name: 'Калужская область')

admin = User.find_or_initialize_by(email: 'admin@example.com')
admin.update!({
  password: 'qwerty',
  admin: true,
  confirmed_at: Time.current
})

default_user = User.find_or_initialize_by(email: 'default@example.com')
default_user.update!({
  first_name: 'Default',
  last_name: 'User',
  password: 'qwerty',
  admin: false,
  confirmed_at: Time.current
})


Page.find_or_initialize_by(slug: 'user-agreement').update!({
  name: 'Пользовательское соглашение',
  public: true
})
Page.find_or_initialize_by(slug: 'personal-data-policy').update!({
  name: 'Регламент Порядок сбора и хранения персональных данных Пользователей',
  public: true
})
Page.find_or_initialize_by(slug: 'personal-data-agreement').update!({
  name: 'Согласие на обработку персональных данных',
  public: true
})


# NationalProject
default_national_project = NationalProject.create!(name: 'Default national project')

# Federal Project
dorogi_network_federal_project = FederalProject.find_by(name: 'Дорожная сеть')

# User Permissions
press_office_user = User.find_or_initialize_by(email: 'press_office@example.com')
press_office_user.update!({
  password: 'qwerty',
  password_confirmation: 'qwerty',
  confirmed_at: Time.current,
})
press_office_user.update!({
  viewable_federal_projects: [dorogi_network_federal_project],
  creatable_federal_projects: [dorogi_network_federal_project],
})


editor_user = User.find_or_initialize_by(email: 'editor@example.com')
editor_user.update!({
  password: 'qwerty',
  password_confirmation: 'qwerty',
  confirmed_at: Time.current,
  editor: true
})

media_organization = Organization.find_or_initialize_by(name: 'Media')
media_organization.update({
  kind: :media,
})

media_user = User.find_or_initialize_by(email: 'media@example.com')
media_user.update!({
  organization: media_organization,
  password: 'qwerty',
  password_confirmation: 'qwerty',
  confirmed_at: Time.current,
})
media_user.update!({
  viewable_federal_projects: [dorogi_network_federal_project],
  repostable_federal_projects: [dorogi_network_federal_project],
})

message = Message.create!({
  sender: admin,
  organization_ids: [],
  national_projects: [default_national_project],
  subject: 'Default title',
  content: 'Default body',
})
message.message_deliveries.create!(user: default_user)


SupportRequest.create!({
  user: default_user,
  subject: 'Тема запроса',
  body: 'Текст запроса'
})


city_authority_organization = Organization.find_or_create_by(name: 'city_authority', kind: 'city_authority')
city_authority_user = User.find_or_initialize_by(email: 'city_authority@example.com')
city_authority_user.update!({
  first_name: 'city_authority',
  last_name: 'User',
  password: 'qwerty',
  admin: false,
  confirmed_at: Time.current,
  organization: city_authority_organization,
  federal_subject: federal_subject
})
city_authority_user.update!({
  viewable_federal_projects: [dorogi_network_federal_project],
  creatable_federal_projects: [dorogi_network_federal_project],
})

regional_authority_organization = Organization.find_or_create_by(name: 'regional_authority', kind: 'regional_authority')
regional_authority_user = User.find_or_initialize_by(email: 'regional_authority@example.com')
regional_authority_user.update!({
  first_name: 'regional_authority',
  last_name: 'User',
  password: 'qwerty',
  admin: false,
  confirmed_at: Time.current,
  organization: regional_authority_organization,
  federal_subject: federal_subject,
})
regional_authority_user.update!({
  viewable_federal_projects: [dorogi_network_federal_project],
  creatable_federal_projects: [dorogi_network_federal_project],
  moderatable_federal_projects: [dorogi_network_federal_project],
})

federal_authority_organization = Organization.find_or_create_by(name: 'federal_authority', kind: 'federal_authority')
federal_authority_user = User.find_or_initialize_by(email: 'federal_authority@example.com')
federal_authority_user.update!({
  first_name: 'federal_authority',
  last_name: 'User',
  password: 'qwerty',
  admin: false,
  confirmed_at: Time.current,
  organization: federal_authority_organization,
  federal_subject: nil,
})
federal_authority_user.update!({
  viewable_federal_projects: [dorogi_network_federal_project],
  creatable_federal_projects: [dorogi_network_federal_project],
  moderatable_federal_projects: [dorogi_network_federal_project],
})

guest_user = User.find_or_initialize_by(email: "guest@example.com")
guest_user.update!({
  organization: nil,
  password: 'qwerty',
  password_confirmation: 'qwerty',
  confirmed_at: Time.current,
})

superadmins_user_group = UserGroup.find_or_initialize_by(name: "Superadmins")
superadmins_user_group.update!({
  user_group_abilities: [UserGroupAbility.new(ability: :edit_user_groups)]
})

superadmin_user = User.find_or_initialize_by(email: "superadmin@example.com")
superadmin_user.update!({
  organization: nil,
  password: 'qwerty',
  password_confirmation: 'qwerty',
  confirmed_at: Time.current,
  user_groups: [superadmins_user_group],
  admin: true,
})
