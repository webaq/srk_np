class AddTimeZoneToFederalSubjects < ActiveRecord::Migration[6.0]
  def change
    add_column :federal_subjects, :time_zone, :string
  end
end
