class CreateSupportRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :support_requests do |t|
      t.references :user, null: false, foreign_key: true
      t.string :subject
      t.text :body
      t.timestamps
    end
  end
end
