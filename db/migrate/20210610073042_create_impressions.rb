class CreateImpressions < ActiveRecord::Migration[6.1]
  def change
    create_table :impressions do |t|
      t.references :press_release, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.integer :score
      t.integer :likeness
      t.timestamps
    end
  end
end
