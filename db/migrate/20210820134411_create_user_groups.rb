class CreateUserGroups < ActiveRecord::Migration[6.1]
  def change
    create_table :user_groups do |t|
      t.string :name
      t.integer :users_count, null: false, default: 0
      t.integer :user_group_abilities_count, null: false, default: 0
      t.timestamps
    end
  end
end
