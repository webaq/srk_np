class CreatePlacementNationalProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :placement_national_projects do |t|
      t.references :placement, null: false, foreign_key: true
      t.references :national_project, null: false, foreign_key: true
      t.index [:placement_id, :national_project_id], unique: true, name: 'idx_placement_id_nationa_project_id'
    end

    Placement.find_each do |placement|
      placement.national_project_ids = [placement.national_project_id]
    end

    remove_column :placements, :national_project_id
  end
end
