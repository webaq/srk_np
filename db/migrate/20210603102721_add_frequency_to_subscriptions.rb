class AddFrequencyToSubscriptions < ActiveRecord::Migration[6.1]
  def change
    add_column :subscriptions, :frequency, :integer, null: false, default: 2
    change_column_default :subscriptions, :frequency, from: 2, to: nil
  end
end
