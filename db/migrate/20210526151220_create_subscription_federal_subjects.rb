class CreateSubscriptionFederalSubjects < ActiveRecord::Migration[6.1]
  def change
    create_table :subscription_federal_subjects do |t|
      t.references :subscription, null: false, foreign_key: true
      t.references :federal_subject, null: false, foreign_key: true
      t.index [:subscription_id, :federal_subject_id], unique: true, name: :idx_subscription_federal_subjects_on_s_id_and_f_id
    end

    add_column :subscriptions, :federal_subjects_count, :integer, null: false, default: 0
  end
end
