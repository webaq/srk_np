class AddAllFederalSubjectsToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :all_federal_subjects, :boolean, null: false, default: false

    federal_subjects_total = FederalSubject.count
    PressRelease.find_each do |press_release|
      if press_release.federal_subjects.count.in? [0, federal_subjects_total]
        press_release.update!({
          all_federal_subjects: true,
          federal_subjects: []
        })
      end
    end
  end
end
