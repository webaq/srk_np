class CreateVideoAlbums < ActiveRecord::Migration[6.1]
  def change
    create_table :video_albums do |t|
      t.string :name
      t.timestamps
    end
  end
end
