class AddListAllUnapprovedPressReleasesToOrganizationss < ActiveRecord::Migration[6.0]
  def change
    add_column :organizations, :list_all_unapproved_press_releases, :boolean, null: false, default: false
  end
end
