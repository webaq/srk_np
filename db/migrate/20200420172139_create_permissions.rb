class CreatePermissions < ActiveRecord::Migration[6.0]
  def change
    create_table :permissions do |t|
      t.references :user, null: false, foreign_key: true
      t.references :federal_project, null: false, foreign_key: true

      t.boolean :can_create, null: false, default: false
      t.boolean :can_moderate, null: false, default: false
      t.boolean :can_view, null: false, default: false
      t.boolean :can_repost, null: false, default: false

      t.index [:user_id, :federal_project_id], unique: true

      t.timestamps
    end
  end
end
