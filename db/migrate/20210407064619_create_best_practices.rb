class CreateBestPractices < ActiveRecord::Migration[6.0]
  def change
    create_table :best_practices do |t|
      t.references :user, null: false, foreign_key: true
      t.string :name
      t.datetime :published_at
      t.datetime :approved_at
      t.timestamps
    end
  end
end
