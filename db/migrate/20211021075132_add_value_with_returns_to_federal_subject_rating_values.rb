class AddValueWithReturnsToFederalSubjectRatingValues < ActiveRecord::Migration[6.1]
  def change
    add_column :federal_subject_rating_values, :value_with_returns, :float
  end
end
