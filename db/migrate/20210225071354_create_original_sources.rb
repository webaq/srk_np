class CreateOriginalSources < ActiveRecord::Migration[6.0]
  def change
    create_table :original_sources do |t|
      t.references :press_release, null: false, foreign_key: true
      t.references :original_source, null: false, foreign_key: { to_table: :press_releases }
      t.index [:press_release_id, :original_source_id], unique: true, name: 'idx_original_sources_on_press_release_id_and_original_source_id'
      t.timestamps
    end

    rename_column :press_releases, :duplicates_count, :original_sources_count
    add_index :press_releases, :original_sources_count
  end
end
