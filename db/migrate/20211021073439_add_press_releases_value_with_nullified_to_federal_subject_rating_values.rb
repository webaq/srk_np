class AddPressReleasesValueWithNullifiedToFederalSubjectRatingValues < ActiveRecord::Migration[6.1]
  def change
    add_column :federal_subject_rating_values, :press_releases_value_with_nullified, :float

    change_column_null :federal_subject_rating_values, :quality, true
    change_column_null :federal_subject_rating_values, :press_releases_count, true
    change_column_null :federal_subject_rating_values, :press_releases_value, true
  end
end
