class CreateRegistrations < ActiveRecord::Migration[6.0]
  def change
    create_table :registrations do |t|
      t.integer :kind, null: false
      t.references :federal_subject, foreign_key: true
      t.string :organization_name
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :contact_position
      t.string :contact_phone
      t.string :media_subscribers
      t.string :website
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
