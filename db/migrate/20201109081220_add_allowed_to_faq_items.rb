class AddAllowedToFaqItems < ActiveRecord::Migration[6.0]
  def change
    add_column :faq_items, :media_allowed, :boolean, null: false, default: false
    add_column :faq_items, :non_media_allowed, :boolean, null: false, default: false
  end
end
