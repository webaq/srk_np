class AddAwardedToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :awarded, :boolean, null: false, default: false
  end
end
