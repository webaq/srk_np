class AddPressReleasesPublishedAtIndex < ActiveRecord::Migration[6.1]
  def change
    add_index(:press_releases, :approved_at, name: "press_releases_approved_at", if_not_exists: true)
    add_index(:press_releases, :pre_approved_at, name: "press_releases_pre_approved_at", if_not_exists: true)
    add_index(:press_releases, :published_at, name: "press_releases_published_at", if_not_exists: true)
    add_index(:press_releases, :returned_at, name: "press_releases_returned_at", if_not_exists: true)
    add_index(:press_releases, :published_at, name: "press_releases_published_at_desc", order: { published_at: :desc })
  end
end
