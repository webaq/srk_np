class ChangePublishedAtNullInBestPractices < ActiveRecord::Migration[6.0]
  def change
    BestPractice.where(published_at: nil).update_all(published_at: Time.current)
    change_column_null :best_practices, :published_at, false
  end
end
