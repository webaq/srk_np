class CreateMediaDigestDeliveries < ActiveRecord::Migration[6.0]
  def change
    create_table :media_digest_deliveries do |t|
      t.string :message_id, null: false, index: { unique: true }
      t.string :recipient, null: false, index: true
      t.datetime :delivered_at
      t.datetime :opened_at
      t.datetime :clicked_at
      t.timestamps
    end
  end
end
