class AddSkipRepostDomainValidationToOrganizations < ActiveRecord::Migration[6.0]
  def change
    add_column :organizations, :skip_repost_domain_validation, :boolean, null: false, default: false
  end
end
