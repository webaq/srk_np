class SubscriptionsToNationalProjects < ActiveRecord::Migration[6.0]
  def change
    execute "DELETE FROM subscriptions"
    remove_column :subscriptions, :federal_project_id
    add_reference :subscriptions, :national_project, foreign_key: true, null: false
    add_index :subscriptions, [:user_id, :national_project_id], unique: true
  end
end
