class RemoveCommonFromNationalProjects < ActiveRecord::Migration[6.0]
  def change
    remove_column :national_projects, :common
  end
end
