class CreatePressReleaseFederalProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :press_release_federal_projects do |t|
      t.references :press_release, null: false, foreign_key: true
      t.references :federal_project, null: false, foreign_key: true
      t.index [:press_release_id, :federal_project_id], unique: true, name: :idx_press_release_fedproject

      t.timestamps
    end
  end
end
