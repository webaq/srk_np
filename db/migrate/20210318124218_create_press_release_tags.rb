class CreatePressReleaseTags < ActiveRecord::Migration[6.0]
  def change
    create_table :press_release_tags do |t|
      t.references :press_release, null: false, foreign_key: true
      t.references :tag, null: false, foreign_key: true
      t.timestamps

      t.index [:press_release_id, :tag_id], unique: true
    end
  end
end
