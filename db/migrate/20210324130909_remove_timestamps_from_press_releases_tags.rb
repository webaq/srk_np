class RemoveTimestampsFromPressReleasesTags < ActiveRecord::Migration[6.0]
  def change
    remove_timestamps :press_release_tags
  end
end
