class RenameRegistrationsDeletedAtToDiscardedAt < ActiveRecord::Migration[6.0]
  def change
    rename_column :registrations, :deleted_at, :discarded_at
  end
end
