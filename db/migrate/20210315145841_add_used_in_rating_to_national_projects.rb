class AddUsedInRatingToNationalProjects < ActiveRecord::Migration[6.0]
  def change
    add_column :national_projects, :used_in_rating, :boolean, null: false, default: true
  end
end
