class AddFederalProjectToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_reference :press_releases, :federal_project, foreign_key: true
  end
end
