class CreatePressReleaseNationalProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :press_release_national_projects do |t|
      t.references :press_release, null: false, foreign_key: true
      t.references :national_project, null: false, foreign_key: true
      t.index [:press_release_id, :national_project_id], unique: true, name: :idx_press_release_natproject

      t.timestamps
    end
  end
end
