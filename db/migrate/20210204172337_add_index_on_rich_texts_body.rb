class AddIndexOnRichTextsBody < ActiveRecord::Migration[6.0]
  def change
    change_table :action_text_rich_texts do |t|
      t.index "to_tsvector('russian'::regconfig, body)", name: "index_action_text_rich_texts_on_to_tsvector_russian_body", using: :gin
    end
  rescue ArgumentError => error
    raise unless error.message == "Index name 'index_action_text_rich_texts_on_to_tsvector_russian_body' on table 'action_text_rich_texts' already exists"
  end
end
