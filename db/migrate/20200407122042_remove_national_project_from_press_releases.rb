class RemoveNationalProjectFromPressReleases < ActiveRecord::Migration[6.0]
  def change
    remove_reference :press_releases, :national_project

    change_column_null :press_releases, :federal_project_id, false
  end
end
