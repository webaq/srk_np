class AddRatingAddonOrganizationToOrganizations < ActiveRecord::Migration[6.0]
  def change
    add_reference :organizations, :rating_addon_organization, foreign_key: { to_table: :organizations }
  end
end
