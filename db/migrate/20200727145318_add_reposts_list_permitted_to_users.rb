class AddRepostsListPermittedToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :reposts_list_permitted, :boolean, null: false, default: false
  end
end
