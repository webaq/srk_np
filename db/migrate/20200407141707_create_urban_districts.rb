class CreateUrbanDistricts < ActiveRecord::Migration[6.0]
  def change
    create_table :urban_districts do |t|
      t.references :federal_subject, foreign_key: true, null: false
      t.string :name
      t.timestamps
    end
  end
end
