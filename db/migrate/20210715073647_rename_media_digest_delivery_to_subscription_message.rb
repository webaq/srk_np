class RenameMediaDigestDeliveryToSubscriptionMessage < ActiveRecord::Migration[6.1]
  def change
    rename_table :media_digest_deliveries, :subscription_messages

    add_index :subscription_messages, :delivered_at
    add_index :subscription_messages, :opened_at
    add_column :subscription_messages, :spamed_at, :datetime, index: true
    add_column :subscription_messages, :unsubscribed_at, :datetime, index: true
    add_column :subscription_messages, :undelivered_at, :datetime, index: true
  end
end
