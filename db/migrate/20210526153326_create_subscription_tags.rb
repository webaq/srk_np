class CreateSubscriptionTags < ActiveRecord::Migration[6.1]
  def change
    create_table :subscription_tags do |t|
      t.references :subscription, null: false, foreign_key: true
      t.references :tag, null: false, foreign_key: true
      t.index [:subscription_id, :tag_id], unique: true, name: :idx_subscription_tags_on_s_id_and_t_id
    end

    add_column :subscriptions, :tags_count, :integer, null: false, default: 0
  end
end
