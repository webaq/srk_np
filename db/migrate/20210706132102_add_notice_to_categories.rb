class AddNoticeToCategories < ActiveRecord::Migration[6.1]
  def change
    add_column :categories, :notice, :string
  end
end
