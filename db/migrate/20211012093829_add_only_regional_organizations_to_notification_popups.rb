class AddOnlyRegionalOrganizationsToNotificationPopups < ActiveRecord::Migration[6.1]
  def change
    add_column :notification_popups, :only_regional_organizations, :boolean, null: false, default: false
  end
end
