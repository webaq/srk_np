class AddShowUnpublishedPressReleasesToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :show_unpublished_press_releases, :boolean, null: false, default: false
  end
end
