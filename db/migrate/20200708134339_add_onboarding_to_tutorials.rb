class AddOnboardingToTutorials < ActiveRecord::Migration[6.0]
  def change
    change_table :tutorials do |t|
      t.boolean :repostable_onboarding, null: false, default: false
      t.boolean :creatable_onboarding, null: false, default: false
    end
  end
end
