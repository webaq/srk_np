class AddPreApprovedAtToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :pre_approved_at, :datetime, index: true
  end
end
