class AddPublishedToVideoAlbums < ActiveRecord::Migration[6.1]
  def change
    add_column :video_albums, :published, :boolean, null: false, default: true
  end
end
