class AddTerritotyToUsers < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :federal_subject, foreign_key: true, null: true
    add_reference :users, :urban_district, foreign_key: true, null: true
  end
end
