class AddThankYouNoteToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :thank_you_note, :boolean, null: false, default: false
  end
end
