class CreateFederalProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :federal_projects do |t|
      t.references :national_project, foreign_key: true, null: false
      t.string :name
      t.timestamps
    end
  end
end
