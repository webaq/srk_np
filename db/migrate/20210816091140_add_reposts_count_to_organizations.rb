class AddRepostsCountToOrganizations < ActiveRecord::Migration[6.1]
  def up
    add_column :organizations, :reposts_count, :integer, null: false, default: 0
    Repost.counter_culture_fix_counts only: [[:user, :organization]]
  end

  def down
    remove_column :organizations, :reposts_count
  end
end
