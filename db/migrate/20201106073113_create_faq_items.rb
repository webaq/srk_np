class CreateFaqItems < ActiveRecord::Migration[6.0]
  def change
    create_table :faq_items do |t|
      t.integer :position
      t.timestamps
    end
  end
end
