class CreateMainpageBanners < ActiveRecord::Migration[6.0]
  def change
    create_table :mainpage_banners do |t|
      t.string :text
      t.string :url
      t.timestamps
    end
  end
end
