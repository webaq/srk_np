class RemoveTimestampsFromMessageNationalProjects < ActiveRecord::Migration[6.0]
  def change
    remove_column :message_national_projects, :updated_at
    remove_column :message_national_projects, :created_at
  end
end
