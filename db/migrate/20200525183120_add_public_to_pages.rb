class AddPublicToPages < ActiveRecord::Migration[6.0]
  def change
    add_column :pages, :public, :boolean, null: false, default: false
  end
end
