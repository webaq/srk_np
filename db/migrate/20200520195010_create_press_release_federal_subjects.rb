class CreatePressReleaseFederalSubjects < ActiveRecord::Migration[6.0]
  def change
    create_table :press_release_federal_subjects do |t|
      t.references :press_release, null: false, foreign_key: true
      t.references :federal_subject, null: false, foreign_key: true
      t.index [:press_release_id, :federal_subject_id], unique: true, name: :idx_press_release_fedsubjects
      t.timestamps
    end
  end
end
