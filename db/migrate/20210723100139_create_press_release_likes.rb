class CreatePressReleaseLikes < ActiveRecord::Migration[6.1]
  def up
    create_table :press_release_likes do |t|
      t.references :press_release, foreign_key: true, null: false
      t.references :user, foreign_key: true, null: false
      t.index [:press_release_id, :user_id], unique: true
      t.timestamps
    end

    add_column :press_releases, :press_release_likes_count, :integer, null: false, default: 0
    add_index :press_releases, :press_release_likes_count

    Impression.where(likeness: 2).find_each do |impression|
      impression.press_release.press_release_likes.create!({
        user_id: impression.user_id,
        created_at: impression.created_at,
        updated_at: impression.updated_at
      })
    end

    remove_column :impressions, :likeness
  end

  def down
    add_column :impressions, :likeness, :integer
    remove_column :press_releases, :press_release_likes_count
    drop_table :press_release_likes
  end
end
