class CreatePlacements < ActiveRecord::Migration[6.0]
  def change
    create_table :placements do |t|
      t.references :user, foreign_key: true, null: false
      t.string :title
      t.references :federal_subject, foreign_key: true, null: false
      t.references :national_project, foreign_key: true, null: false
      t.timestamps
    end
  end
end
