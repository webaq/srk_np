class AddPasswordToRegistrations < ActiveRecord::Migration[6.0]
  def change
    change_table :registrations do |t|
      t.string :encrypted_password, null: false, default: ""
      t.bigint :confirmed_user_id
      t.datetime :confirmed_at
    end

    add_foreign_key :registrations, :users, column: :confirmed_user_id
  end
end
