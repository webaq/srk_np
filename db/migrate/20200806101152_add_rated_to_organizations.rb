class AddRatedToOrganizations < ActiveRecord::Migration[6.0]
  def change
    add_column :organizations, :rated, :boolean, null: false, default: true
  end
end
