class AddAuthorsContactsPublishedToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :authors_contacts_published, :boolean, null: false, default: false
  end
end
