class CreateMessages < ActiveRecord::Migration[6.0]
  def change
    create_table :messages do |t|
      t.references :sender, null: false, foreign_key: { to_table: :users }
      t.references :addressee, null: false, foreign_key: { to_table: :users }
      t.string :subject, null: false
      t.text :body, null: false
      t.datetime :late_published_at, null: true
      t.datetime :published_at, null: false
      t.datetime :read_at, null: true, default: nil
      t.timestamps
    end
  end
end
