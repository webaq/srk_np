class AddMeasureToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_reference :press_releases, :measure, foreign_key: true
  end
end
