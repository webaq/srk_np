class CreatePressReleaseValues < ActiveRecord::Migration[6.1]
  def change
    create_table :press_release_values do |t|
      t.references :subcategory, foreign_key: true, null: false
      t.references :subcategory_scope, foreign_key: true
      t.string :name
      t.float :value, null: false
      t.integer :position
      t.timestamps
    end
  end
end
