class CreateMainTopicNationalProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :main_topic_national_projects do |t|
      t.references :main_topic, null: false, foreign_key: true
      t.references :national_project, null: false, foreign_key: true
      t.index [:main_topic_id, :national_project_id], unique: true, name: :idx_main_topic_national_project
    end
  end
end
