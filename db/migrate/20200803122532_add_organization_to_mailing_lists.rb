class AddOrganizationToMailingLists < ActiveRecord::Migration[6.0]
  def change
    add_reference :mailing_lists, :organization, foreign_key: true
    organization = Organization.first
    MailingList.update_all(organization_id: organization.id)
    change_column_null :mailing_lists, :organization_id, false
  end
end
