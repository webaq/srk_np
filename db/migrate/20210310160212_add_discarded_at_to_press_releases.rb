class AddDiscardedAtToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :discarded_at, :datetime
    add_index :press_releases, :discarded_at
  end
end
