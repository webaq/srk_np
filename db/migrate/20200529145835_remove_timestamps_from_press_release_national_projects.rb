class RemoveTimestampsFromPressReleaseNationalProjects < ActiveRecord::Migration[6.0]
  def change
    remove_column :press_release_national_projects, :updated_at
    remove_column :press_release_national_projects, :created_at
  end
end
