class AddAvailableToFederalProjects < ActiveRecord::Migration[6.0]
  def change
    add_column :federal_projects, :available, :boolean, null: false, default: true
  end
end
