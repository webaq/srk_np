class AddNationalProjectsCountToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :national_projects_count, :integer, null: false, default: 0

    PressRelease.find_each do |press_release|
      PressRelease.reset_counters(press_release.id, :national_projects)
    end
  end
end
