class MoveMediaSubscribersFromUsersToOrganizations < ActiveRecord::Migration[6.0]
  def change
    add_column :organizations, :media_subscribers, :string
    remove_column :users, :media_subscribers
  end
end
