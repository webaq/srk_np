class AddCanCreateMessagesToOrganizations < ActiveRecord::Migration[6.0]
  def change
    add_column :organizations, :can_create_messages, :boolean, null: false, default: false
  end
end
