class AddConfirmedUsersCountToOrganizations < ActiveRecord::Migration[6.1]
  def up
    add_column :organizations, :confirmed_users_count, :integer, null: false, default: 0
    User.counter_culture_fix_counts
  end

  def down
    remove_column :organizations, :confirmed_users_count
  end
end
