class AddAdminToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :admin, :boolean, null: false, default: false
    execute("UPDATE users SET admin = TRUE WHERE role = 0")
    remove_column :users, :role
  end
end
