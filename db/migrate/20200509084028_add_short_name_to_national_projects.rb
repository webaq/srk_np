class AddShortNameToNationalProjects < ActiveRecord::Migration[6.0]
  def change
    add_column :national_projects, :short_name, :string, null: false, default: ''
  end
end
