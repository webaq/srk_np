class CreateFederalSubjectRatingValues < ActiveRecord::Migration[6.1]
  def change
    create_table :federal_subject_rating_values do |t|
      t.references :federal_subject, null: false, foreign_key: true
      t.references :rating_month, null: false, foreign_key: true

      t.float :quality, null: false
      t.integer :press_releases_count, null: false
      t.float :press_releases_value, null: false
      t.float :total_value, null: false

      t.index [:rating_month_id, :federal_subject_id], unique: true, name: :idx_federal_subject_rating_month

      t.timestamps
    end
  end
end
