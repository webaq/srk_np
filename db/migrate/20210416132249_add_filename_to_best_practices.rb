class AddFilenameToBestPractices < ActiveRecord::Migration[6.0]
  def change
    add_column :best_practices, :filename, :string
    BestPractice.update_all(filename: 'Название файла')
  end
end
