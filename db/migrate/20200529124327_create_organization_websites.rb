class CreateOrganizationWebsites < ActiveRecord::Migration[6.0]
  def change
    create_table :organization_websites do |t|
      t.references :organization, null: false, foreign_key: true
      t.string :address, null: false
      t.timestamps
    end

    Organization.find_each do |organization|
      if organization.website.present?
        organization.organization_websites.create!(address: organization.website)
      end
    end

    remove_column :organizations, :website
  end
end
