class CreateMainTopics < ActiveRecord::Migration[6.0]
  def change
    create_table :main_topics do |t|
      t.string :name
      t.timestamps
    end
  end
end
