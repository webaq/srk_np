class CreateMailingListItems < ActiveRecord::Migration[6.0]
  def change
    create_table :mailing_list_items do |t|
      t.references :mailing_list, foreign_key: true, null: false
      t.string :email
      t.timestamps
    end
  end
end
