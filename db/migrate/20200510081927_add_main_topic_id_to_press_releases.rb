class AddMainTopicIdToPressReleases < ActiveRecord::Migration[6.0]
  def change
    remove_column :press_releases, :main_topic
    add_reference :press_releases, :main_topic, foreign_key: true, null: true
  end
end
