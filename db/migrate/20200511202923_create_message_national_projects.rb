class CreateMessageNationalProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :message_national_projects do |t|
      t.references :message, null: false, foreign_key: true
      t.references :national_project, null: false, foreign_key: true
      t.index [:message_id, :national_project_id], unique: true, name: :idx_message_id_natproject_id
      t.timestamps
    end
  end
end
