class CreateSubscriptionNationalProjects < ActiveRecord::Migration[6.1]
  def change
    create_table :subscription_national_projects do |t|
      t.references :subscription, null: false, foreign_key: true
      t.references :national_project, null: false, foreign_key: true
      t.index [:subscription_id, :national_project_id], unique: true, name: :idx_subscription_national_projects_on_s_id_and_n_id
    end

    add_column :subscriptions, :national_projects_count, :integer, null: false, default: 0
  end
end
