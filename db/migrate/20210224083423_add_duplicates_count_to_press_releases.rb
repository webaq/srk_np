class AddDuplicatesCountToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :duplicates_count, :integer, null: false, default: 0
  end
end
