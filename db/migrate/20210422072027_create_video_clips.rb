class CreateVideoClips < ActiveRecord::Migration[6.1]
  def change
    create_table :video_clips do |t|
      t.references :video_album, null: false, foreign_key: true
      t.string :name
      t.string :youtube_url
      t.timestamps
    end
  end
end
