class AddDeletedAtToRegistrations < ActiveRecord::Migration[6.0]
  def change
    add_column :registrations, :deleted_at, :datetime
    add_index :registrations, :deleted_at
  end
end
