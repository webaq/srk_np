class AddQuestionToFaqItems < ActiveRecord::Migration[6.0]
  def change
    add_column :faq_items, :question, :string
  end
end
