class AddApprovedAtToPressReleases < ActiveRecord::Migration[6.0]
  def up
    add_column :press_releases, :approved_at, :datetime, default: nil, index: true
    execute("UPDATE press_releases SET approved_at = NOW() WHERE status = 1")
    remove_column :press_releases, :status
  end
end
