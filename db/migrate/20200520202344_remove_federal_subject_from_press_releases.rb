class RemoveFederalSubjectFromPressReleases < ActiveRecord::Migration[6.0]
  def change
    PressRelease.find_each do |press_release|
      if press_release.federal_subject_id.present?
        federal_subject = FederalSubject.find(press_release.federal_subject_id)
        press_release.federal_subjects << federal_subject
      end
    end

    remove_column :press_releases, :federal_subject_id
  end
end
