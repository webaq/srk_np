class ChangeMessageAddressee < ActiveRecord::Migration[6.0]
  def change
    remove_column :messages, :addressee_id
    execute("DELETE FROM message_national_projects")
    execute("DELETE FROM messages")
    add_column :messages, :addressee, :integer, null: false
    remove_column :messages, :read_at
  end
end
