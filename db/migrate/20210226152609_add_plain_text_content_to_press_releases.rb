class AddPlainTextContentToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :plain_text_content, :text
  end
end
