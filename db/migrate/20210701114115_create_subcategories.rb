class CreateSubcategories < ActiveRecord::Migration[6.1]
  def change
    create_table :subcategories do |t|
      t.references :category, foreign_key: true, null: false
      t.string :name
      t.integer :position
      t.timestamps
    end
  end
end
