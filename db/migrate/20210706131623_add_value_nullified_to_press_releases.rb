class AddValueNullifiedToPressReleases < ActiveRecord::Migration[6.1]
  def change
    add_column :press_releases, :value_nullified, :boolean, null: false, default: false, index: true
  end
end
