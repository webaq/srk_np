class AddRegionAndSourceToMainpageBanners < ActiveRecord::Migration[6.0]
  def change
    change_table :mainpage_banners do |t|
      t.string :region
      t.string :source
    end
  end
end
