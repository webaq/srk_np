class RemoveTimestampsFromPressReleaseFederalSubjects < ActiveRecord::Migration[6.0]
  def change
    remove_column :press_release_federal_subjects, :updated_at
    remove_column :press_release_federal_subjects, :created_at
  end
end
