class AddFederalSubjectToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_reference :press_releases, :federal_subject, foreign_key: true, null: true
    add_reference :press_releases, :urban_district, foreign_key: true, null: true
  end
end
