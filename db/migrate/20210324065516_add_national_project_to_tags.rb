class AddNationalProjectToTags < ActiveRecord::Migration[6.0]
  def change
    add_reference :tags, :national_project, foreign_key: true
    Tag.update_all(national_project_id: NationalProject.first.id)
    change_column_null :tags, :national_project_id, false
  end
end
