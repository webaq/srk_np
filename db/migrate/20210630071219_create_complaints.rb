class CreateComplaints < ActiveRecord::Migration[6.1]
  def change
    create_table :complaints do |t|
      t.references :user, foreign_key: true, null: false
      t.references :press_release, foreign_key: true, null: false
      t.text :body
      t.timestamps
    end
  end
end
