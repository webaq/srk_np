class AddOnboardedToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :onboarded_at, :datetime
  end
end
