class AddColumnActivatedToSubscriptions < ActiveRecord::Migration[6.1]
  def change
    add_column :subscriptions, :activated, :boolean, null: false, default: true
    add_index :subscriptions, :activated
  end
end
