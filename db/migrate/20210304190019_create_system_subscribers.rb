class CreateSystemSubscribers < ActiveRecord::Migration[6.0]
  def change
    create_table :system_subscribers do |t|
      t.string :email
      t.integer :kind, null: false, index: true
      t.timestamps
    end
  end
end
