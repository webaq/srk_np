class CreateNationalProjectTags < ActiveRecord::Migration[6.0]
  def up
    create_table :national_project_tags do |t|
      t.belongs_to :national_project, null: false, foreign_key: true
      t.belongs_to :tag, null: false, foreign_key: true
      t.index [:national_project_id, :tag_id], unique: true
    end

    Tag.find_each do |tag|
      tag.national_project_ids = [tag.national_project_id]
    end

    change_column_null :tags, :national_project_id, true
  end

  def down
    change_column_null :tags, :national_project_id, false
    drop_table :national_project_tags
  end
end
