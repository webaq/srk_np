class AddActivityRateToFederalSubjectRatingValues < ActiveRecord::Migration[6.1]
  def change
    add_column :federal_subject_rating_values, :activity_rate, :float
  end
end
