class AddRepostsCountToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :reposts_count, :integer

    PressRelease.find_each do |press_release|
      PressRelease.reset_counters(press_release.id, :reposts)
    end
  end
end
