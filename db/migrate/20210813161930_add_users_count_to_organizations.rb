class AddUsersCountToOrganizations < ActiveRecord::Migration[6.1]
  def up
    add_column :organizations, :users_count, :integer, null: false, default: 0

    Organization.find_in_batches do |group|
      group.each do |organization|
        Organization.reset_counters(organization.id, :users)
      end
    end
  end

  def down
    remove_column :organizations, :users_count
  end
end
