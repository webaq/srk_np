class CreateVisits < ActiveRecord::Migration[6.0]
  def change
    create_table :visits do |t|
      t.references :user, null: false, foreign_key: true
      t.date :created_on
      t.index [:user_id, :created_on], unique: true
      t.timestamps
    end
  end
end
