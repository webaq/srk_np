class AddKindToOrganization < ActiveRecord::Migration[6.0]
  def change
    add_column :organizations, :kind, :integer, index: true
  end
end
