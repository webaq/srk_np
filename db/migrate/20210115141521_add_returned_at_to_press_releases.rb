class AddReturnedAtToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :returned_at, :datetime, index: true
  end
end
