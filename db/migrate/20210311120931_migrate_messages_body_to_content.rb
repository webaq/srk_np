class MigrateMessagesBodyToContent < ActiveRecord::Migration[6.0]
  include ActionView::Helpers::TextHelper

  def change
    Message.find_each do |message|
      if message.body.present?
        message.update!(content: simple_format(message.body))
      end
    end
  end
end
