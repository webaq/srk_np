class EnableSmlar < ActiveRecord::Migration[6.0]
  def change
    enable_extension :smlar
    add_column :press_releases, :lexemes, :text, array: true
    add_index :press_releases, :lexemes, using: 'gin', opclass: '_text_sml_ops'
  end
end
