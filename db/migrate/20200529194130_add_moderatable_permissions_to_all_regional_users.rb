class AddModeratablePermissionsToAllRegionalUsers < ActiveRecord::Migration[6.0]
  def change
    all_federal_project_ids = FederalProject.pluck(:id)

    User.where.not(federal_subject_id: nil).find_each do |user|
      user.moderatable_federal_project_ids = all_federal_project_ids
    end
  end
end
