class CreateRatingMonths < ActiveRecord::Migration[6.0]
  def up
    create_table :rating_months do |t|
      t.date :month, null: false, index: { unique: true }
      t.boolean :published, null: false, default: false
      t.timestamps
    end

    months = [
      Date.new(2020, 6, 1),
      Date.new(2020, 7, 1),
      Date.new(2020, 8, 1),
      Date.new(2020, 9, 1)
    ]

    months.each do |month|
      RatingMonth.create!(month: month, published: true)
    end
  end

  def down
    drop_table :rating_months
  end
end
