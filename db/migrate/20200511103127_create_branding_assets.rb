class CreateBrandingAssets < ActiveRecord::Migration[6.0]
  def change
    create_table :branding_assets do |t|
      t.references :national_project, null: false, foreign_key: true
      t.text :description
      t.timestamps
    end
  end
end
