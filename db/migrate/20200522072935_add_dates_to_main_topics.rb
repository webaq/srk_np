class AddDatesToMainTopics < ActiveRecord::Migration[6.0]
  def change
    add_column :main_topics, :starts_at, :datetime, null: true, default: nil
    add_column :main_topics, :ends_at, :datetime, null: true, default: nil
  end
end
