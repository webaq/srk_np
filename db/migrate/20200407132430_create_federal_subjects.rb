class CreateFederalSubjects < ActiveRecord::Migration[6.0]
  def change
    create_table :federal_subjects do |t|
      t.string :name
      t.timestamps
    end
  end
end
