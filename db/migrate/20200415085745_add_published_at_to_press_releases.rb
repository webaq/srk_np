class AddPublishedAtToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :published_at, :datetime, index: true, null: true
    execute "UPDATE press_releases SET published_at = updated_at"
    change_column_null :press_releases, :published_at, false
  end
end
