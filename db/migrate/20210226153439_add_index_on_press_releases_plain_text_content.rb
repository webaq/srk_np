class AddIndexOnPressReleasesPlainTextContent < ActiveRecord::Migration[6.0]
  def change
    change_table :press_releases do |t|
      t.index "(setweight(to_tsvector('russian', coalesce(\"press_releases\".\"title\"::text, '')), 'A') || setweight(to_tsvector('russian', coalesce(\"press_releases\".\"plain_text_content\"::text, '')), 'B'))", name: "index_press_releases_on_full_text_search", using: :gin
    end
    # add_index :press_releases, YOUR_EXPRESSION_HERE, using: :gin, name: "index_pg_search_documents_on_content"
  end
end
