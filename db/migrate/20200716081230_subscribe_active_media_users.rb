class SubscribeActiveMediaUsers < ActiveRecord::Migration[6.0]
  def up
    all_national_projects = NationalProject.all

    users = User.where.not(last_sign_in_at: nil)
      .includes(organization: :federal_subject)
      .where(organizations: { kind: Organization.kinds[:media] })

    users.find_each do |user|
      user.subscribed_federal_subjects = [user.organization.federal_subject].compact
      user.subscribed_national_projects = all_national_projects
    end
  end

  def down
  end
end
