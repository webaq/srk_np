class CreatePressReleaseRelations < ActiveRecord::Migration[6.1]
  def change
    create_table :press_release_relations do |t|
      t.references :press_release, null: false, foreign_key: true
      t.references :related_press_release, null: false, foreign_key: { to_table: :press_releases }
      t.integer :position
      t.timestamps
    end
  end
end
