class CreateNotificationPopups < ActiveRecord::Migration[6.1]
  def change
    create_table :notification_popups do |t|
      t.string :name, null: false
      t.datetime :starts_on, null: false
      t.datetime :ends_on, null: false
      t.string :organization_kinds, array: true
      t.timestamps
    end
  end
end
