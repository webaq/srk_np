class AddSubscribedToMediaDigestToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :subscribed_to_media_digest, :boolean, null: false, default: false
  end
end
