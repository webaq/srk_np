class CreateFederalSubjectSubscriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :federal_subject_subscriptions do |t|
      t.references :user, null: false, foreign_key: true
      t.references :federal_subject, null: false, foreign_key: true
      t.index [:user_id, :federal_subject_id], unique: true, name: :idx_user_id_subscribed_federal_subject_id
      t.timestamps
    end
  end
end
