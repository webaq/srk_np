class ChangeDefaultOrganizationsRated < ActiveRecord::Migration[6.0]
  def change
    change_column_default :organizations, :rated, false
  end
end
