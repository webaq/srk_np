class AddPermittedToTutorials < ActiveRecord::Migration[6.0]
  def change
    add_column :tutorials, :creatable_permitted, :boolean, null: false, default: false
    add_column :tutorials, :repostable_permitted, :boolean, null: false, default: false
    remove_column :tutorials, :creatable_onboarding
    remove_column :tutorials, :repostable_onboarding
    add_column :tutorials, :onboarding, :boolean, null: false, default: false
  end
end
