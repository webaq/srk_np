class DropSubscriptions < ActiveRecord::Migration[6.1]
  def up
    drop_table :subscriptions
    drop_table :federal_subject_subscriptions
  end
end
