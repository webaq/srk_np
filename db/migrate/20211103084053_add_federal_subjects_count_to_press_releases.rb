class AddFederalSubjectsCountToPressReleases < ActiveRecord::Migration[6.1]
  def change
    add_column :press_releases, :federal_subjects_count, :integer, null: false, default: 0

    # RUN IN CONSOLE
    # PressRelease.find_each { |press_release| PressRelease.reset_counters(press_release.id, :federal_subjects) }

    add_index :press_releases, :federal_subjects_count
  end
end
