class CreateFavourites < ActiveRecord::Migration[6.0]
  def change
    create_table :favourites do |t|
      t.references :user, null: false, foreign_key: true
      t.references :press_release, null: false, foreign_key: true
      t.index [:user_id, :press_release_id], unique: true
      t.timestamps
    end
  end
end
