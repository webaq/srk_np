class AddShownPressReleasesCountToOrganizations < ActiveRecord::Migration[6.1]
  def up
    add_column :organizations, :shown_press_releases_count, :integer, null: false, default: 0
    PublicActivity::Activity.counter_culture_fix_counts
  end

  def down
    remove_column :organizations, :shown_press_releases_count
  end
end
