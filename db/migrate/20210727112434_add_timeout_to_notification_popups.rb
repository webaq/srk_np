class AddTimeoutToNotificationPopups < ActiveRecord::Migration[6.1]
  def change
    add_column :notification_popups, :timeout, :integer, null: false, default: 30
  end
end
