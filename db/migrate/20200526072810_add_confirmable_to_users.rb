class AddConfirmableToUsers < ActiveRecord::Migration[6.0]
  def up
    add_column :users, :confirmation_token, :string, index: { unique: true }
    add_column :users, :confirmed_at, :datetime
    add_column :users, :confirmation_sent_at, :datetime

    execute('UPDATE users SET confirmed_at = last_sign_in_at')
  end

  def down
    remove_column :users, :confirmation_sent_at
    remove_column :users, :confirmed_at
    remove_column :users, :confirmation_token
  end
end
