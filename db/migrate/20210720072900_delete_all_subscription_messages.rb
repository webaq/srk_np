class DeleteAllSubscriptionMessages < ActiveRecord::Migration[6.1]
  def change
    SubscriptionMessage.delete_all
    add_reference :subscription_messages, :user, null: false, foreign_key: true
  end
end
