class CreateSubcategoryScopes < ActiveRecord::Migration[6.1]
  def change
    create_table :subcategory_scopes do |t|
      t.references :subcategory, foreign_key: true, null: false
      t.string :name
      t.integer :position
      t.timestamps
    end
  end
end
