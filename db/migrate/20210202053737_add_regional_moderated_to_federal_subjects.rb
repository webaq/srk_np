class AddRegionalModeratedToFederalSubjects < ActiveRecord::Migration[6.0]
  def change
    add_column :federal_subjects, :regional_moderated, :boolean, null: false, default: false
  end
end
