class CreateUserGroupAbilities < ActiveRecord::Migration[6.1]
  def change
    create_table :user_group_abilities do |t|
      t.references :user_group, null: false, foreign_key: true
      t.integer :ability, null: false
      t.timestamps
    end
  end
end
