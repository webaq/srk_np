class AddBookmarksCountToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :bookmarks_count, :integer

    PressRelease.find_each do |press_release|
      PressRelease.reset_counters(press_release.id, :bookmarks)
    end
  end
end
