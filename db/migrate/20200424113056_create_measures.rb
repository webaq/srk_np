class CreateMeasures < ActiveRecord::Migration[6.0]
  def change
    create_table :measures do |t|
      t.references :federal_project, null: false, foreign_key: true
      t.string :name
      t.timestamps
    end
  end
end
