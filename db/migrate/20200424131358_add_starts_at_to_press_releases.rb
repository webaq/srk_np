class AddStartsAtToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :starts_at, :datetime
  end
end
