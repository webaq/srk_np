class CreateTopListItems < ActiveRecord::Migration[6.0]
  def change
    create_table :top_list_items do |t|
      t.references :press_release, null: false, foreign_key: true, index: { unique: true }
      t.timestamps
    end
  end
end
