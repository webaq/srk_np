class AddCloseButtonWhenUserClicksLinkToNotificationPopups < ActiveRecord::Migration[6.1]
  def change
    add_column :notification_popups, :close_button_when_user_clicks_link, :boolean, null: false, default: false
  end
end
