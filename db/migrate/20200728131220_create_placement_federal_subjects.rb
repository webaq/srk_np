class CreatePlacementFederalSubjects < ActiveRecord::Migration[6.0]
  def change
    create_table :placement_federal_subjects do |t|
      t.references :placement, null: false, foreign_key: true
      t.references :federal_subject, null: false, foreign_key: true
      t.index [:placement_id, :federal_subject_id], unique: true, name: 'idx_placement_id_federal_subject_id'
    end

    Placement.find_each do |placement|
      placement.federal_subject_ids = [placement.federal_subject_id]
    end

    remove_column :placements, :federal_subject_id
  end
end
