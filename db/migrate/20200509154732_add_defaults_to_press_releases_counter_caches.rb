class AddDefaultsToPressReleasesCounterCaches < ActiveRecord::Migration[6.0]
  def change
    change_column_default :press_releases, :reposts_count, 0
    change_column_default :press_releases, :bookmarks_count, 0
    execute("UPDATE press_releases SET reposts_count = 0 WHERE reposts_count IS NULL")
    execute("UPDATE press_releases SET bookmarks_count = 0 WHERE bookmarks_count IS NULL")
    change_column_null :press_releases, :reposts_count, false
    change_column_null :press_releases, :bookmarks_count, false
  end
end
