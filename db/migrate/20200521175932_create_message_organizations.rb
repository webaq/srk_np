class CreateMessageOrganizations < ActiveRecord::Migration[6.0]
  def change
    create_table :message_organizations do |t|
      t.references :message, null: false, foreign_key: true
      t.references :organization, null: false, foreign_key: true
      t.index [:message_id, :organization_id], unique: true
      t.timestamps
    end
  end
end
