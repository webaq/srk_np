class AddMainTopicToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :main_topic, :string
    add_column :press_releases, :late_published_at, :datetime, null: true
  end
end
