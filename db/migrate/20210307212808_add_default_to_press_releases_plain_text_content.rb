class AddDefaultToPressReleasesPlainTextContent < ActiveRecord::Migration[6.0]
  def change
    change_column_default :press_releases, :plain_text_content, ''
    change_column_default :press_releases, :lexemes, []
  end
end
