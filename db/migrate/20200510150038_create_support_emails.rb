class CreateSupportEmails < ActiveRecord::Migration[6.0]
  def change
    create_table :support_emails do |t|
      t.string :email, null: false
      t.timestamps
    end
  end
end
