class CreateUnapprovedPressReleasesDigestSubscribers < ActiveRecord::Migration[6.0]
  def change
    create_table :unapproved_press_releases_digest_subscribers do |t|
      t.string :email
      t.timestamps
    end
  end
end
