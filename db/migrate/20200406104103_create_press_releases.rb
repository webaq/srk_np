class CreatePressReleases < ActiveRecord::Migration[6.0]
  def change
    create_table :press_releases do |t|
      t.references :user, foreign_key: true, null: false
      t.references :national_project, foreign_key: true, null: false
      t.integer :status, index: true, null: false
      t.string :title
      t.text :content
      t.timestamps
    end
  end
end
