class RemoveNationalProjectIdFromTags < ActiveRecord::Migration[6.0]
  def change
    remove_reference :tags, :national_project
  end
end
