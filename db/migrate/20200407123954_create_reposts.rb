class CreateReposts < ActiveRecord::Migration[6.0]
  def change
    create_table :reposts do |t|
      t.references :press_release, foreign_key: true, null: false
      t.references :user, foreign_key: true, null: false
      t.string :link
      t.timestamps
    end
  end
end
