class AddAwardedCountToFederalSubjectRatingValues < ActiveRecord::Migration[6.1]
  def change
    add_column :federal_subject_rating_values, :awarded_count, :integer
  end
end
