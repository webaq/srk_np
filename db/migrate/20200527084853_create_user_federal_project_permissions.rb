class CreateUserFederalProjectPermissions < ActiveRecord::Migration[6.0]
  def change
    create_table :user_federal_project_permissions do |t|
      t.references :user, null: false, foreign_key: true
      t.references :federal_project, null: false, foreign_key: true
      t.integer :permission, null: false, index: true
      t.index [:user_id, :federal_project_id, :permission], unique: true, name: :idx_user_fedproject_permission
    end

    Permission.find_each do |permission|
      if permission.can_view?
        UserFederalProjectPermission.create!({
          user_id: permission.user_id,
          federal_project_id: permission.federal_project_id,
          permission: 'viewable'
        })
      end

      if permission.can_create?
        UserFederalProjectPermission.create!({
          user_id: permission.user_id,
          federal_project_id: permission.federal_project_id,
          permission: 'creatable'
        })
      end

      if permission.can_moderate?
        UserFederalProjectPermission.create!({
          user_id: permission.user_id,
          federal_project_id: permission.federal_project_id,
          permission: 'moderatable'
        })
      end

      if permission.can_repost?
        UserFederalProjectPermission.create!({
          user_id: permission.user_id,
          federal_project_id: permission.federal_project_id,
          permission: 'repostable'
        })
      end
    end
  end
end
