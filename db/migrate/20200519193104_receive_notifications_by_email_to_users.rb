class ReceiveNotificationsByEmailToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :receive_notifications_by_email, :boolean, null: false, default: true
  end
end
