class AddFederalSubjectToOrganizations < ActiveRecord::Migration[6.0]
  def change
    add_reference :organizations, :federal_subject, foreign_key: true
  end
end
