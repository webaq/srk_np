class AddCommonToNationalPriorities < ActiveRecord::Migration[6.0]
  def change
    add_column :national_projects, :common, :boolean, null: false, default: false, index: true
  end
end
