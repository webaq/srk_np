class AddContactToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :contact_name, :string
    add_column :users, :contact_position, :string
    add_column :users, :contact_phone, :string
    add_column :users, :website, :string
    add_column :users, :media_registration_number, :string
    add_column :users, :media_subscribers, :integer
  end
end
