class AddDuplicatedPressReleasesCountToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :duplicated_press_releases_count, :integer, null: false, default: 0, index: true
  end
end
