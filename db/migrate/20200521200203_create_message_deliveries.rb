class CreateMessageDeliveries < ActiveRecord::Migration[6.0]
  def change
    create_table :message_deliveries do |t|
      t.references :message, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.datetime :read_at, null: true, default: nil, index: true
      t.timestamps
    end

    Message.find_each do |message|
      message.relevant_users.each do |user|
        message.message_deliveries.create!(user: user)
      end
    end
  end
end
