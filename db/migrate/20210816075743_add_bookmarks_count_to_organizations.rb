class AddBookmarksCountToOrganizations < ActiveRecord::Migration[6.1]
  def up
    add_column :organizations, :bookmarks_count, :integer, null: false, default: 0
    Bookmark.counter_culture_fix_counts only: [[:user, :organization]]
  end

  def down
    remove_column :organizations, :bookmarks_count
  end
end
