class ChangeAddresseeNullInMessages < ActiveRecord::Migration[6.0]
  def change
    change_column_null :messages, :addressee, true
  end
end
