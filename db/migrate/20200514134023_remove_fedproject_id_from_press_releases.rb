class RemoveFedprojectIdFromPressReleases < ActiveRecord::Migration[6.0]
  def change
    PressRelease.find_each do |press_release|
      federal_project = FederalProject.find(press_release.federal_project_id)
      press_release.national_projects << federal_project.national_project
      press_release.federal_projects << federal_project
    end

    remove_column :press_releases, :federal_project_id
  end
end
