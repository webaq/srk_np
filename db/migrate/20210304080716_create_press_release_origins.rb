class CreatePressReleaseOrigins < ActiveRecord::Migration[6.0]
  def change
    remove_reference :press_releases, :original_press_release, foreign_key: { to_table: :press_releases }
    remove_column :press_releases, :duplicated_press_releases_count

    create_table :press_release_origins do |t|
      t.references :press_release, null: false, foreign_key: true
      t.references :original_press_release, null: false, foreign_key: { to_table: :press_releases }
      t.integer :percentage, null: false, index: true
      t.index [:press_release_id, :percentage], unique: true
      t.timestamps
    end
  end
end
