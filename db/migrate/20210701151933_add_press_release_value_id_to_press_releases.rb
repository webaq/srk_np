class AddPressReleaseValueIdToPressReleases < ActiveRecord::Migration[6.1]
  def change
    add_reference :press_releases, :press_release_value, foreign_key: true
  end
end
