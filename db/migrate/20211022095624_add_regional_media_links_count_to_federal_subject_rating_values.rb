class AddRegionalMediaLinksCountToFederalSubjectRatingValues < ActiveRecord::Migration[6.1]
  def change
    add_column :federal_subject_rating_values, :regional_media_links_count, :integer
  end
end
