class AddRegionalModeratorToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :regional_moderator, :boolean, null: false, default: false
  end
end
