class CreateOmniauthAccounts < ActiveRecord::Migration[6.0]
  def change
    create_table :omniauth_accounts do |t|
      t.references :user, null: false, foreign_key: true
      t.string :provider
      t.string :uid
      t.index [:provider, :uid], unique: true
      t.timestamps
    end
  end
end
