class AddSourceToPressReleases < ActiveRecord::Migration[6.0]
  def change
    add_column :press_releases, :api_loaded, :boolean, null: false, default: false
  end
end
