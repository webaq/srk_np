class CreateSubscriptionOrganizations < ActiveRecord::Migration[6.1]
  def change
    create_table :subscription_organizations do |t|
      t.references :subscription, null: false, foreign_key: true
      t.references :organization, null: false, foreign_key: true
      t.index [:subscription_id, :organization_id], unique: true, name: :idx_subscription_organizations_on_s_id_and_o_id
    end

    add_column :subscriptions, :organizations_count, :integer, null: false, default: 0
  end
end
