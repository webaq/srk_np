class CreateExperts < ActiveRecord::Migration[6.0]
  def change
    create_table :experts do |t|
      t.references :press_release, foreign_key: true, null: false
      t.integer :position
      t.string :name
      t.string :title
      t.string :phone
      t.timestamps
    end
  end
end
