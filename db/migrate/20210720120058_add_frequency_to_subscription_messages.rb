class AddFrequencyToSubscriptionMessages < ActiveRecord::Migration[6.1]
  def change
    add_column :subscription_messages, :frequency, :integer, null: false, index: true
    change_column_null :subscription_messages, :user_id, true
  end
end
