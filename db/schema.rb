# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_11_03_084053) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "smlar"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index "to_tsvector('russian'::regconfig, body)", name: "index_action_text_rich_texts_on_to_tsvector_russian_body", using: :gin
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "activities", force: :cascade do |t|
    t.string "trackable_type"
    t.bigint "trackable_id"
    t.string "owner_type"
    t.bigint "owner_id"
    t.string "key"
    t.text "parameters"
    t.string "recipient_type"
    t.bigint "recipient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type"
    t.index ["owner_type", "owner_id"], name: "index_activities_on_owner_type_and_owner_id"
    t.index ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type"
    t.index ["recipient_type", "recipient_id"], name: "index_activities_on_recipient_type_and_recipient_id"
    t.index ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type"
    t.index ["trackable_type", "trackable_id"], name: "index_activities_on_trackable_type_and_trackable_id"
  end

  create_table "best_practices", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "name"
    t.datetime "published_at", null: false
    t.datetime "approved_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "filename"
    t.index ["user_id"], name: "index_best_practices_on_user_id"
  end

  create_table "bookmarks", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "press_release_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["press_release_id"], name: "index_bookmarks_on_press_release_id"
    t.index ["user_id", "press_release_id"], name: "index_bookmarks_on_user_id_and_press_release_id", unique: true
    t.index ["user_id"], name: "index_bookmarks_on_user_id"
  end

  create_table "branding_assets", force: :cascade do |t|
    t.bigint "national_project_id", null: false
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["national_project_id"], name: "index_branding_assets_on_national_project_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "notice"
  end

  create_table "comments", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "press_release_id", null: false
    t.text "text"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["press_release_id"], name: "index_comments_on_press_release_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "complaints", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "press_release_id", null: false
    t.text "body"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["press_release_id"], name: "index_complaints_on_press_release_id"
    t.index ["user_id"], name: "index_complaints_on_user_id"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at", precision: 6
    t.datetime "updated_at", precision: 6
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "experts", force: :cascade do |t|
    t.bigint "press_release_id", null: false
    t.integer "position"
    t.string "name"
    t.string "title"
    t.string "phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["press_release_id"], name: "index_experts_on_press_release_id"
  end

  create_table "faq_items", force: :cascade do |t|
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "question"
    t.boolean "media_allowed", default: false, null: false
    t.boolean "non_media_allowed", default: false, null: false
  end

  create_table "favourites", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "press_release_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["press_release_id"], name: "index_favourites_on_press_release_id"
    t.index ["user_id", "press_release_id"], name: "index_favourites_on_user_id_and_press_release_id", unique: true
    t.index ["user_id"], name: "index_favourites_on_user_id"
  end

  create_table "federal_projects", force: :cascade do |t|
    t.bigint "national_project_id", null: false
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "available", default: true, null: false
    t.index ["national_project_id"], name: "index_federal_projects_on_national_project_id"
  end

  create_table "federal_subject_rating_values", force: :cascade do |t|
    t.bigint "federal_subject_id", null: false
    t.bigint "rating_month_id", null: false
    t.float "quality"
    t.integer "press_releases_count"
    t.float "press_releases_value"
    t.float "total_value", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "press_releases_value_with_nullified"
    t.float "value_with_returns"
    t.integer "regional_media_links_count"
    t.integer "federal_media_links_count"
    t.integer "awarded_count"
    t.float "likeness"
    t.float "activity_rate"
    t.index ["federal_subject_id"], name: "index_federal_subject_rating_values_on_federal_subject_id"
    t.index ["rating_month_id", "federal_subject_id"], name: "idx_federal_subject_rating_month", unique: true
    t.index ["rating_month_id"], name: "index_federal_subject_rating_values_on_rating_month_id"
  end

  create_table "federal_subjects", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "time_zone"
    t.bigint "id_address_book"
    t.boolean "regional_moderated", default: false, null: false
  end

  create_table "impressions", force: :cascade do |t|
    t.bigint "press_release_id", null: false
    t.bigint "user_id", null: false
    t.integer "score"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["press_release_id"], name: "index_impressions_on_press_release_id"
    t.index ["user_id"], name: "index_impressions_on_user_id"
  end

  create_table "mailing_list_items", force: :cascade do |t|
    t.bigint "mailing_list_id", null: false
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["mailing_list_id"], name: "index_mailing_list_items_on_mailing_list_id"
  end

  create_table "mailing_lists", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "organization_id", null: false
    t.index ["organization_id"], name: "index_mailing_lists_on_organization_id"
  end

  create_table "main_topic_national_projects", force: :cascade do |t|
    t.bigint "main_topic_id", null: false
    t.bigint "national_project_id", null: false
    t.index ["main_topic_id", "national_project_id"], name: "idx_main_topic_national_project", unique: true
    t.index ["main_topic_id"], name: "index_main_topic_national_projects_on_main_topic_id"
    t.index ["national_project_id"], name: "index_main_topic_national_projects_on_national_project_id"
  end

  create_table "main_topics", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "starts_at"
    t.datetime "ends_at"
  end

  create_table "mainpage_banners", force: :cascade do |t|
    t.string "text"
    t.string "url"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "region"
    t.string "source"
  end

  create_table "measures", force: :cascade do |t|
    t.bigint "federal_project_id", null: false
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["federal_project_id"], name: "index_measures_on_federal_project_id"
  end

  create_table "message_deliveries", force: :cascade do |t|
    t.bigint "message_id", null: false
    t.bigint "user_id", null: false
    t.datetime "read_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["message_id"], name: "index_message_deliveries_on_message_id"
    t.index ["read_at"], name: "index_message_deliveries_on_read_at"
    t.index ["user_id"], name: "index_message_deliveries_on_user_id"
  end

  create_table "message_national_projects", force: :cascade do |t|
    t.bigint "message_id", null: false
    t.bigint "national_project_id", null: false
    t.index ["message_id", "national_project_id"], name: "idx_message_id_natproject_id", unique: true
    t.index ["message_id"], name: "index_message_national_projects_on_message_id"
    t.index ["national_project_id"], name: "index_message_national_projects_on_national_project_id"
  end

  create_table "message_organizations", force: :cascade do |t|
    t.bigint "message_id", null: false
    t.bigint "organization_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["message_id", "organization_id"], name: "index_message_organizations_on_message_id_and_organization_id", unique: true
    t.index ["message_id"], name: "index_message_organizations_on_message_id"
    t.index ["organization_id"], name: "index_message_organizations_on_organization_id"
  end

  create_table "messages", force: :cascade do |t|
    t.bigint "sender_id", null: false
    t.string "subject", null: false
    t.text "body"
    t.datetime "late_published_at"
    t.datetime "published_at", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "addressee"
    t.index ["sender_id"], name: "index_messages_on_sender_id"
  end

  create_table "national_project_tags", force: :cascade do |t|
    t.bigint "national_project_id", null: false
    t.bigint "tag_id", null: false
    t.index ["national_project_id", "tag_id"], name: "index_national_project_tags_on_national_project_id_and_tag_id", unique: true
    t.index ["national_project_id"], name: "index_national_project_tags_on_national_project_id"
    t.index ["tag_id"], name: "index_national_project_tags_on_tag_id"
  end

  create_table "national_projects", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "short_name", default: "", null: false
    t.boolean "used_in_rating", default: true, null: false
  end

  create_table "notification_popups", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "starts_on", null: false
    t.datetime "ends_on", null: false
    t.string "organization_kinds", array: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "timeout", default: 30, null: false
    t.boolean "close_button_when_user_clicks_link", default: false, null: false
    t.boolean "only_regional_organizations", default: false, null: false
  end

  create_table "omniauth_accounts", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "provider"
    t.string "uid"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["provider", "uid"], name: "index_omniauth_accounts_on_provider_and_uid", unique: true
    t.index ["user_id"], name: "index_omniauth_accounts_on_user_id"
  end

  create_table "organization_websites", force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.string "address", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["organization_id"], name: "index_organization_websites_on_organization_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name"
    t.string "contact_name"
    t.string "contact_position"
    t.string "contact_phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "can_create_messages", default: false, null: false
    t.boolean "list_all_unapproved_press_releases", default: false, null: false
    t.boolean "skip_repost_domain_validation", default: false, null: false
    t.bigint "federal_subject_id"
    t.integer "kind"
    t.string "media_subscribers"
    t.boolean "rated", default: false, null: false
    t.bigint "rating_addon_organization_id"
    t.integer "users_count", default: 0, null: false
    t.integer "confirmed_users_count", default: 0, null: false
    t.integer "bookmarks_count", default: 0, null: false
    t.integer "reposts_count", default: 0, null: false
    t.integer "shown_press_releases_count", default: 0, null: false
    t.index ["federal_subject_id"], name: "index_organizations_on_federal_subject_id"
    t.index ["rating_addon_organization_id"], name: "index_organizations_on_rating_addon_organization_id"
  end

  create_table "pages", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "slug", null: false
    t.boolean "public", default: false, null: false
  end

  create_table "placement_federal_subjects", force: :cascade do |t|
    t.bigint "placement_id", null: false
    t.bigint "federal_subject_id", null: false
    t.index ["federal_subject_id"], name: "index_placement_federal_subjects_on_federal_subject_id"
    t.index ["placement_id", "federal_subject_id"], name: "idx_placement_id_federal_subject_id", unique: true
    t.index ["placement_id"], name: "index_placement_federal_subjects_on_placement_id"
  end

  create_table "placement_national_projects", force: :cascade do |t|
    t.bigint "placement_id", null: false
    t.bigint "national_project_id", null: false
    t.index ["national_project_id"], name: "index_placement_national_projects_on_national_project_id"
    t.index ["placement_id", "national_project_id"], name: "idx_placement_id_nationa_project_id", unique: true
    t.index ["placement_id"], name: "index_placement_national_projects_on_placement_id"
  end

  create_table "placements", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_placements_on_user_id"
  end

  create_table "press_release_federal_projects", force: :cascade do |t|
    t.bigint "press_release_id", null: false
    t.bigint "federal_project_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["federal_project_id"], name: "index_press_release_federal_projects_on_federal_project_id"
    t.index ["press_release_id", "federal_project_id"], name: "idx_press_release_fedproject", unique: true
    t.index ["press_release_id"], name: "index_press_release_federal_projects_on_press_release_id"
  end

  create_table "press_release_federal_subjects", force: :cascade do |t|
    t.bigint "press_release_id", null: false
    t.bigint "federal_subject_id", null: false
    t.index ["federal_subject_id"], name: "index_press_release_federal_subjects_on_federal_subject_id"
    t.index ["press_release_id", "federal_subject_id"], name: "idx_press_release_fedsubjects", unique: true
    t.index ["press_release_id"], name: "index_press_release_federal_subjects_on_press_release_id"
  end

  create_table "press_release_likes", force: :cascade do |t|
    t.bigint "press_release_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["press_release_id", "user_id"], name: "index_press_release_likes_on_press_release_id_and_user_id", unique: true
    t.index ["press_release_id"], name: "index_press_release_likes_on_press_release_id"
    t.index ["user_id"], name: "index_press_release_likes_on_user_id"
  end

  create_table "press_release_national_projects", force: :cascade do |t|
    t.bigint "press_release_id", null: false
    t.bigint "national_project_id", null: false
    t.index ["national_project_id"], name: "index_press_release_national_projects_on_national_project_id"
    t.index ["press_release_id", "national_project_id"], name: "idx_press_release_natproject", unique: true
    t.index ["press_release_id"], name: "index_press_release_national_projects_on_press_release_id"
  end

  create_table "press_release_origins", force: :cascade do |t|
    t.bigint "press_release_id", null: false
    t.bigint "original_press_release_id", null: false
    t.integer "percentage", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["original_press_release_id"], name: "index_press_release_origins_on_original_press_release_id"
    t.index ["percentage"], name: "index_press_release_origins_on_percentage"
    t.index ["press_release_id", "percentage"], name: "index_press_release_origins_on_press_release_id_and_percentage", unique: true
    t.index ["press_release_id"], name: "index_press_release_origins_on_press_release_id"
  end

  create_table "press_release_relations", force: :cascade do |t|
    t.bigint "press_release_id", null: false
    t.bigint "related_press_release_id", null: false
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["press_release_id"], name: "index_press_release_relations_on_press_release_id"
    t.index ["related_press_release_id"], name: "index_press_release_relations_on_related_press_release_id"
  end

  create_table "press_release_tags", force: :cascade do |t|
    t.bigint "press_release_id", null: false
    t.bigint "tag_id", null: false
    t.index ["press_release_id", "tag_id"], name: "index_press_release_tags_on_press_release_id_and_tag_id", unique: true
    t.index ["press_release_id"], name: "index_press_release_tags_on_press_release_id"
    t.index ["tag_id"], name: "index_press_release_tags_on_tag_id"
  end

  create_table "press_release_values", force: :cascade do |t|
    t.bigint "subcategory_id", null: false
    t.bigint "subcategory_scope_id"
    t.string "name"
    t.float "value", null: false
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["subcategory_id"], name: "index_press_release_values_on_subcategory_id"
    t.index ["subcategory_scope_id"], name: "index_press_release_values_on_subcategory_scope_id"
  end

  create_table "press_releases", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "title"
    t.text "content"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "urban_district_id"
    t.integer "reposts_count", default: 0, null: false
    t.datetime "published_at", null: false
    t.datetime "approved_at"
    t.bigint "measure_id"
    t.datetime "starts_at"
    t.datetime "late_published_at"
    t.integer "bookmarks_count", default: 0, null: false
    t.bigint "main_topic_id"
    t.boolean "authors_contacts_published", default: false, null: false
    t.boolean "all_federal_subjects", default: false, null: false
    t.integer "national_projects_count", default: 0, null: false
    t.datetime "pre_approved_at"
    t.boolean "api_loaded", default: false, null: false
    t.datetime "returned_at"
    t.text "lexemes", default: [], array: true
    t.boolean "awarded", default: false, null: false
    t.text "plain_text_content", default: ""
    t.datetime "discarded_at"
    t.bigint "press_release_value_id"
    t.boolean "value_nullified", default: false, null: false
    t.integer "press_release_likes_count", default: 0, null: false
    t.integer "federal_subjects_count", default: 0, null: false
    t.index "((setweight(to_tsvector('russian'::regconfig, COALESCE((title)::text, ''::text)), 'A'::\"char\") || setweight(to_tsvector('russian'::regconfig, COALESCE(plain_text_content, ''::text)), 'B'::\"char\")))", name: "index_press_releases_on_full_text_search", using: :gin
    t.index ["approved_at"], name: "press_releases_approved_at"
    t.index ["discarded_at"], name: "index_press_releases_on_discarded_at"
    t.index ["federal_subjects_count"], name: "index_press_releases_on_federal_subjects_count"
    t.index ["lexemes"], name: "index_press_releases_on_lexemes", opclass: :_text_sml_ops, using: :gin
    t.index ["main_topic_id"], name: "index_press_releases_on_main_topic_id"
    t.index ["measure_id"], name: "index_press_releases_on_measure_id"
    t.index ["pre_approved_at"], name: "press_releases_pre_approved_at"
    t.index ["press_release_likes_count"], name: "index_press_releases_on_press_release_likes_count"
    t.index ["press_release_value_id"], name: "index_press_releases_on_press_release_value_id"
    t.index ["published_at"], name: "press_releases_published_at"
    t.index ["published_at"], name: "press_releases_published_at_desc", order: :desc
    t.index ["returned_at"], name: "press_releases_returned_at"
    t.index ["urban_district_id"], name: "index_press_releases_on_urban_district_id"
    t.index ["user_id"], name: "index_press_releases_on_user_id"
  end

  create_table "rating_months", force: :cascade do |t|
    t.date "month", null: false
    t.boolean "published", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["month"], name: "index_rating_months_on_month", unique: true
  end

  create_table "registrations", force: :cascade do |t|
    t.integer "kind", null: false
    t.bigint "federal_subject_id"
    t.string "organization_name"
    t.string "email"
    t.string "first_name"
    t.string "last_name"
    t.string "contact_position"
    t.string "contact_phone"
    t.string "media_subscribers"
    t.string "website"
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "encrypted_password", default: "", null: false
    t.bigint "confirmed_user_id"
    t.datetime "confirmed_at"
    t.datetime "discarded_at"
    t.index ["discarded_at"], name: "index_registrations_on_discarded_at"
    t.index ["federal_subject_id"], name: "index_registrations_on_federal_subject_id"
    t.index ["user_id"], name: "index_registrations_on_user_id"
  end

  create_table "reposts", force: :cascade do |t|
    t.bigint "press_release_id", null: false
    t.bigint "user_id", null: false
    t.string "link"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["press_release_id"], name: "index_reposts_on_press_release_id"
    t.index ["user_id"], name: "index_reposts_on_user_id"
  end

  create_table "subcategories", force: :cascade do |t|
    t.bigint "category_id", null: false
    t.string "name"
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_subcategories_on_category_id"
  end

  create_table "subcategory_scopes", force: :cascade do |t|
    t.bigint "subcategory_id", null: false
    t.string "name"
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["subcategory_id"], name: "index_subcategory_scopes_on_subcategory_id"
  end

  create_table "subscription_federal_subjects", force: :cascade do |t|
    t.bigint "subscription_id", null: false
    t.bigint "federal_subject_id", null: false
    t.index ["federal_subject_id"], name: "index_subscription_federal_subjects_on_federal_subject_id"
    t.index ["subscription_id", "federal_subject_id"], name: "idx_subscription_federal_subjects_on_s_id_and_f_id", unique: true
    t.index ["subscription_id"], name: "index_subscription_federal_subjects_on_subscription_id"
  end

  create_table "subscription_messages", force: :cascade do |t|
    t.string "message_id", null: false
    t.string "recipient", null: false
    t.datetime "delivered_at"
    t.datetime "opened_at"
    t.datetime "clicked_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "spamed_at"
    t.datetime "unsubscribed_at"
    t.datetime "undelivered_at"
    t.bigint "user_id"
    t.integer "frequency", null: false
    t.index ["delivered_at"], name: "index_subscription_messages_on_delivered_at"
    t.index ["message_id"], name: "index_subscription_messages_on_message_id", unique: true
    t.index ["opened_at"], name: "index_subscription_messages_on_opened_at"
    t.index ["recipient"], name: "index_subscription_messages_on_recipient"
    t.index ["user_id"], name: "index_subscription_messages_on_user_id"
  end

  create_table "subscription_national_projects", force: :cascade do |t|
    t.bigint "subscription_id", null: false
    t.bigint "national_project_id", null: false
    t.index ["national_project_id"], name: "index_subscription_national_projects_on_national_project_id"
    t.index ["subscription_id", "national_project_id"], name: "idx_subscription_national_projects_on_s_id_and_n_id", unique: true
    t.index ["subscription_id"], name: "index_subscription_national_projects_on_subscription_id"
  end

  create_table "subscription_organizations", force: :cascade do |t|
    t.bigint "subscription_id", null: false
    t.bigint "organization_id", null: false
    t.index ["organization_id"], name: "index_subscription_organizations_on_organization_id"
    t.index ["subscription_id", "organization_id"], name: "idx_subscription_organizations_on_s_id_and_o_id", unique: true
    t.index ["subscription_id"], name: "index_subscription_organizations_on_subscription_id"
  end

  create_table "subscription_tags", force: :cascade do |t|
    t.bigint "subscription_id", null: false
    t.bigint "tag_id", null: false
    t.index ["subscription_id", "tag_id"], name: "idx_subscription_tags_on_s_id_and_t_id", unique: true
    t.index ["subscription_id"], name: "index_subscription_tags_on_subscription_id"
    t.index ["tag_id"], name: "index_subscription_tags_on_tag_id"
  end

  create_table "subscriptions", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "national_projects_count", default: 0, null: false
    t.integer "federal_subjects_count", default: 0, null: false
    t.integer "organizations_count", default: 0, null: false
    t.integer "tags_count", default: 0, null: false
    t.boolean "activated", default: true, null: false
    t.integer "frequency", null: false
    t.index ["activated"], name: "index_subscriptions_on_activated"
    t.index ["user_id"], name: "index_subscriptions_on_user_id", unique: true
  end

  create_table "support_emails", force: :cascade do |t|
    t.string "email", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "support_requests", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "subject"
    t.text "body"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_support_requests_on_user_id"
  end

  create_table "system_subscribers", force: :cascade do |t|
    t.string "email"
    t.integer "kind", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["kind"], name: "index_system_subscribers_on_kind"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "top_list_items", force: :cascade do |t|
    t.bigint "press_release_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["press_release_id"], name: "index_top_list_items_on_press_release_id", unique: true
  end

  create_table "tutorials", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "creatable_permitted", default: false, null: false
    t.boolean "repostable_permitted", default: false, null: false
    t.boolean "onboarding", default: false, null: false
  end

  create_table "unapproved_press_releases_digest_subscribers", force: :cascade do |t|
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "urban_districts", force: :cascade do |t|
    t.bigint "federal_subject_id", null: false
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["federal_subject_id"], name: "index_urban_districts_on_federal_subject_id"
  end

  create_table "user_federal_project_permissions", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "federal_project_id", null: false
    t.integer "permission", null: false
    t.index ["federal_project_id"], name: "index_user_federal_project_permissions_on_federal_project_id"
    t.index ["permission"], name: "index_user_federal_project_permissions_on_permission"
    t.index ["user_id", "federal_project_id", "permission"], name: "idx_user_fedproject_permission", unique: true
    t.index ["user_id"], name: "index_user_federal_project_permissions_on_user_id"
  end

  create_table "user_group_abilities", force: :cascade do |t|
    t.bigint "user_group_id", null: false
    t.integer "ability", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_group_id"], name: "index_user_group_abilities_on_user_group_id"
  end

  create_table "user_group_users", force: :cascade do |t|
    t.bigint "user_group_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_group_id", "user_id"], name: "index_user_group_users_on_user_group_id_and_user_id", unique: true
    t.index ["user_group_id"], name: "index_user_group_users_on_user_group_id"
    t.index ["user_id"], name: "index_user_group_users_on_user_id"
  end

  create_table "user_groups", force: :cascade do |t|
    t.string "name"
    t.integer "users_count", default: 0, null: false
    t.integer "user_group_abilities_count", default: 0, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "first_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "federal_subject_id"
    t.bigint "urban_district_id"
    t.string "contact_name"
    t.string "contact_position"
    t.string "contact_phone"
    t.string "website"
    t.string "media_registration_number"
    t.boolean "admin", default: false, null: false
    t.bigint "organization_id"
    t.string "last_name"
    t.boolean "editor", default: false, null: false
    t.boolean "receive_notifications_by_email", default: true, null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "discarded_at"
    t.datetime "onboarded_at"
    t.boolean "subscribed_to_media_digest", default: false, null: false
    t.boolean "reposts_list_permitted", default: false, null: false
    t.boolean "show_unpublished_press_releases", default: false, null: false
    t.string "authentication_token", limit: 30
    t.integer "sex"
    t.boolean "thank_you_note", default: false, null: false
    t.boolean "regional_moderator", default: false, null: false
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true
    t.index ["discarded_at"], name: "index_users_on_discarded_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["federal_subject_id"], name: "index_users_on_federal_subject_id"
    t.index ["organization_id"], name: "index_users_on_organization_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["urban_district_id"], name: "index_users_on_urban_district_id"
  end

  create_table "video_albums", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "published", default: true, null: false
  end

  create_table "video_clips", force: :cascade do |t|
    t.bigint "video_album_id", null: false
    t.string "name"
    t.string "youtube_url"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["video_album_id"], name: "index_video_clips_on_video_album_id"
  end

  create_table "visits", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.date "created_on"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id", "created_on"], name: "index_visits_on_user_id_and_created_on", unique: true
    t.index ["user_id"], name: "index_visits_on_user_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "best_practices", "users"
  add_foreign_key "bookmarks", "press_releases"
  add_foreign_key "bookmarks", "users"
  add_foreign_key "branding_assets", "national_projects"
  add_foreign_key "comments", "press_releases"
  add_foreign_key "comments", "users"
  add_foreign_key "complaints", "press_releases"
  add_foreign_key "complaints", "users"
  add_foreign_key "experts", "press_releases"
  add_foreign_key "favourites", "press_releases"
  add_foreign_key "favourites", "users"
  add_foreign_key "federal_projects", "national_projects"
  add_foreign_key "federal_subject_rating_values", "federal_subjects"
  add_foreign_key "federal_subject_rating_values", "rating_months"
  add_foreign_key "impressions", "press_releases"
  add_foreign_key "impressions", "users"
  add_foreign_key "mailing_list_items", "mailing_lists"
  add_foreign_key "mailing_lists", "organizations"
  add_foreign_key "main_topic_national_projects", "main_topics"
  add_foreign_key "main_topic_national_projects", "national_projects"
  add_foreign_key "measures", "federal_projects"
  add_foreign_key "message_deliveries", "messages"
  add_foreign_key "message_deliveries", "users"
  add_foreign_key "message_national_projects", "messages"
  add_foreign_key "message_national_projects", "national_projects"
  add_foreign_key "message_organizations", "messages"
  add_foreign_key "message_organizations", "organizations"
  add_foreign_key "messages", "users", column: "sender_id"
  add_foreign_key "national_project_tags", "national_projects"
  add_foreign_key "national_project_tags", "tags"
  add_foreign_key "omniauth_accounts", "users"
  add_foreign_key "organization_websites", "organizations"
  add_foreign_key "organizations", "federal_subjects"
  add_foreign_key "organizations", "organizations", column: "rating_addon_organization_id"
  add_foreign_key "placement_federal_subjects", "federal_subjects"
  add_foreign_key "placement_federal_subjects", "placements"
  add_foreign_key "placement_national_projects", "national_projects"
  add_foreign_key "placement_national_projects", "placements"
  add_foreign_key "placements", "users"
  add_foreign_key "press_release_federal_projects", "federal_projects"
  add_foreign_key "press_release_federal_projects", "press_releases"
  add_foreign_key "press_release_federal_subjects", "federal_subjects"
  add_foreign_key "press_release_federal_subjects", "press_releases"
  add_foreign_key "press_release_likes", "press_releases"
  add_foreign_key "press_release_likes", "users"
  add_foreign_key "press_release_national_projects", "national_projects"
  add_foreign_key "press_release_national_projects", "press_releases"
  add_foreign_key "press_release_origins", "press_releases"
  add_foreign_key "press_release_origins", "press_releases", column: "original_press_release_id"
  add_foreign_key "press_release_relations", "press_releases"
  add_foreign_key "press_release_relations", "press_releases", column: "related_press_release_id"
  add_foreign_key "press_release_tags", "press_releases"
  add_foreign_key "press_release_tags", "tags"
  add_foreign_key "press_release_values", "subcategories"
  add_foreign_key "press_release_values", "subcategory_scopes"
  add_foreign_key "press_releases", "main_topics"
  add_foreign_key "press_releases", "measures"
  add_foreign_key "press_releases", "press_release_values"
  add_foreign_key "press_releases", "urban_districts"
  add_foreign_key "press_releases", "users"
  add_foreign_key "registrations", "federal_subjects"
  add_foreign_key "registrations", "users"
  add_foreign_key "registrations", "users", column: "confirmed_user_id"
  add_foreign_key "reposts", "press_releases"
  add_foreign_key "reposts", "users"
  add_foreign_key "subcategories", "categories"
  add_foreign_key "subcategory_scopes", "subcategories"
  add_foreign_key "subscription_federal_subjects", "federal_subjects"
  add_foreign_key "subscription_federal_subjects", "subscriptions"
  add_foreign_key "subscription_messages", "users"
  add_foreign_key "subscription_national_projects", "national_projects"
  add_foreign_key "subscription_national_projects", "subscriptions"
  add_foreign_key "subscription_organizations", "organizations"
  add_foreign_key "subscription_organizations", "subscriptions"
  add_foreign_key "subscription_tags", "subscriptions"
  add_foreign_key "subscription_tags", "tags"
  add_foreign_key "subscriptions", "users"
  add_foreign_key "support_requests", "users"
  add_foreign_key "top_list_items", "press_releases"
  add_foreign_key "urban_districts", "federal_subjects"
  add_foreign_key "user_federal_project_permissions", "federal_projects"
  add_foreign_key "user_federal_project_permissions", "users"
  add_foreign_key "user_group_abilities", "user_groups"
  add_foreign_key "user_group_users", "user_groups"
  add_foreign_key "user_group_users", "users"
  add_foreign_key "users", "federal_subjects"
  add_foreign_key "users", "organizations"
  add_foreign_key "users", "urban_districts"
  add_foreign_key "video_clips", "video_albums"
  add_foreign_key "visits", "users"
end
