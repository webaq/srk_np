ENV['RAILS_ENV'] ||= 'test'
require_relative "../config/environment"
require "rails/test_help"
require 'minitest/mock'
require 'mocha/minitest'
require 'webmock/minitest'
require "minitest/mock"

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  # parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
end

class ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
end

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  def sign_in(user)
    visit '/users/sign_in'
    fill_in 'E-mail', with: user.email
    fill_in 'Пароль', with: 'qwerty'
    click_on 'Вход'
    # page.driver.post user_session_path, user: { email: user.email, password: user.password }
  end
end