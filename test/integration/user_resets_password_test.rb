require 'test_helper'

class UserResetsPasswordTest < ActionDispatch::IntegrationTest
  test "should get reset password instructions email" do
    user = users(:one)

    get '/'
    assert_redirected_to new_user_session_url

    follow_redirect!
    assert_select 'a', text: 'Забыли пароль?', href: new_user_password_path

    get new_user_password_path
    assert_select 'input', type: 'submit', value: 'Выслать новый пароль'

    assert_emails 1 do
      post user_password_path, params: {
        user: {
          email: user.email
        }
      }
      user.reload
    end
    assert_redirected_to new_user_session_url

    follow_redirect!
    assert_select 'div', /письмо с инструкциями по восстановлению пароля/
  end

  test "can change password" do
    user = users(:one)
    token = user.send_reset_password_instructions

    get edit_user_password_url(reset_password_token: token)
    assert_select 'input', type: 'submit', value: 'Изменить мой пароль'

    patch user_password_path, params: {
      user: {
        reset_password_token: token,
        password: 'new-password',
        password_confirmation: 'new-password'
      }
    }
    assert_redirected_to root_url
    follow_redirect!
    assert_redirected_to press_releases_url
    follow_redirect!
    assert_response :success
  end
end
