require 'test_helper'

class NationalProjectTest < ActiveSupport::TestCase
  test "it has valid fixtures" do
    %i[ one two ].each do |key|
      national_project = national_projects(key)
      national_project.valid?
      assert_empty national_project.errors
    end
  end
end
