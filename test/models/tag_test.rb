require 'test_helper'

class TagTest < ActiveSupport::TestCase
  test "has valid fixtures" do
    %i[ one ].each do |key|
      tag = tags(key)
      tag.valid?
      assert_empty tag.errors
    end
  end

  test "it strips name" do
    tag = Tag.new(name: '   abc     ')
    assert_equal 'abc', tag.name
  end

  test "it has unique name" do
    tags(:one).update!(name: 'abc')
    tag = Tag.new(name: 'Abc')
    tag.valid?
    assert_includes tag.errors, :name
  end
end
