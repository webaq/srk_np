require "test_helper"

class PressReleaseValueTest < ActiveSupport::TestCase
  test "has valid fixtures" do
    %i[ one two ].each do |key|
      press_release_value = press_release_values(key)
      press_release_value.valid?
      assert_empty press_release_value.errors
    end
  end
end
