require "test_helper"

class VideoClipTest < ActiveSupport::TestCase
  test "has valid fixture" do
    video_clip = video_clips(:one)
    video_clip.valid?
    assert_empty video_clip.errors
  end

  test "remove video" do
    video_clip = video_clips(:one)
    assert video_clip.video.attached?
    video_clip.update!(video: nil)
    assert_not video_clip.video.attached?
  end
end
