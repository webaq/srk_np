require 'test_helper'

class OmniauthAccountTest < ActiveSupport::TestCase
  test "has valid fixtured" do
    %i[ one ].each do |key|
      omniauth_account = omniauth_accounts(key)
      omniauth_account.valid?
      assert_empty omniauth_account.errors
    end
  end

  test "provider is required" do
    omniauth_account = OmniauthAccount.new(provider: nil)
    omniauth_account.valid?
    assert_includes omniauth_account.errors, :provider
  end

  test "uid is required" do
    omniauth_account = OmniauthAccount.new(uid: nil)
    omniauth_account.valid?
    assert_includes omniauth_account.errors, :uid
  end

  test "provider & uid has to be unique" do
    existing_omniauth_account = omniauth_accounts(:one)
    new_omniauth_account = OmniauthAccount.new(provider: existing_omniauth_account.provider, uid: existing_omniauth_account.uid)
    new_omniauth_account.valid?
    assert_includes new_omniauth_account.errors, :uid
    assert_equal ['уже привязан к другой учётной записи'], new_omniauth_account.errors[:uid]
  end
end
