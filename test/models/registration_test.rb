require 'test_helper'

class RegistrationTest < ActiveSupport::TestCase
  test "it has valid fixture" do
    registration = registrations(:one)

    registration.valid?
    assert_empty registration.errors

    registration.valid?(:create)
    assert_empty registration.errors
  end

  test "is invalid if user with same email exists" do
    users(:one).dup.update!(email: 'user@Example.com', password: 'qwerty')

    registration = Registration.new(email: 'user@example.com')
    refute registration.valid?
    assert_includes registration.errors, :email
  end

  test "is invalid if registration with same email exists" do
    registrations(:one).update!(email: 'new_registration@example.com')

    registration = Registration.new(email: 'New_Registration@example.com')
    refute registration.valid?
    assert_includes registration.errors, :email
  end

  test "is invalid if new media and website blank" do
    registration = Registration.new(kind: 'media', organization_name: 'Новая организация')
    refute registration.valid?
    assert_includes registration.errors, :website
  end

  test "is invalid if new media and subscribers blank" do
    registration = Registration.new(kind: 'media', organization_name: 'Новая организация')
    refute registration.valid?
    assert_includes registration.errors, :media_subscribers
  end

  test "it strips email on setting" do
    registration = Registration.new(email: ' new_registration@example.com  ')
    assert_equal 'new_registration@example.com', registration.email
  end

  test "has confirmed_user" do
    user = users(:admin)
    registration = Registration.new(confirmed_user_id: user.id)
    assert_equal user, registration.confirmed_user
  end

  test "it is invalid with invalid phone format" do
    registration = Registration.new(contact_phone: '123')
    registration.valid?
    assert_includes registration.errors, :contact_phone

    registration = Registration.new(contact_phone: ' (123) 123-4567')
    registration.valid?
    assert_includes registration.errors, :contact_phone

    registration = Registration.new(contact_phone: '(123) 123-4567 ')
    registration.valid?
    assert_includes registration.errors, :contact_phone

    registration = Registration.new(contact_phone: '+7 (123) 456-78-90')
    registration.valid?
    assert_not_includes registration.errors, :contact_phone
  end

  test "error messages" do
    registration = Registration.new
    assert_equal 'Поле не может быть пустым', registration.errors.full_message(:organization_name, 'Поле не может быть пустым')
  end

  test "soft delete" do
    registration = registrations(:one)
    assert_not registration.discarded?
    registration.discard!
    assert registration.discarded?
  end

  test "registration with discarded confirmed_user" do
    registration = registrations(:one)
    user = users(:one)
    registration.update(confirmed_user: user)
    user.discard!
    registration.reload
    assert_not_nil registration.confirmed_user
  end
end
