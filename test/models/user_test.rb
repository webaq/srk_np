require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "has valid fixtures" do
    %i[ one two regional_moderator ].each do |key|
      user = users(key)
      user.valid?
      assert_empty user.errors
    end
  end

  test "media_dorogi fixture" do
    user = users(:media_dorogi)
    assert_equal [federal_projects(:dorogi)], user.viewable_federal_projects
    assert_equal [federal_projects(:dorogi)], user.repostable_federal_projects
  end

  test "it has name" do
    user = User.new(first_name: 'Ivan')
    assert_equal(
      'Ivan',
      user.name
    )
  end

  test "is has viewable_federal_project_ids" do
    user = users(:one)
    assert_empty user.user_federal_project_permissions
    assert_empty user.viewable_federal_projects
    user.viewable_federal_projects << federal_projects(:dorogi)
    assert_equal 1, user.user_federal_project_permissions.count
    assert_equal [federal_projects(:dorogi)], user.viewable_federal_projects
    assert_equal [federal_projects(:dorogi).id], user.viewable_federal_project_ids
    assert_equal [national_projects(:dorogi).id], user.viewable_national_project_ids
  end

  test "is has creatable_federal_project_ids" do
    user = users(:one)
    assert_empty user.user_federal_project_permissions
    assert_empty user.creatable_federal_projects
    user.creatable_federal_projects << federal_projects(:dorogi)
    assert_equal 1, user.user_federal_project_permissions.count
    assert_equal [federal_projects(:dorogi)], user.creatable_federal_projects
    assert_equal [federal_projects(:dorogi).id], user.creatable_federal_project_ids
    assert_equal [national_projects(:dorogi).id], user.creatable_national_project_ids
  end

  test "is has moderatable_federal_project_ids" do
    user = users(:one)
    assert_empty user.user_federal_project_permissions
    assert_empty user.moderatable_federal_projects
    user.moderatable_federal_projects << federal_projects(:dorogi)
    assert_equal 1, user.user_federal_project_permissions.count
    assert_equal [federal_projects(:dorogi)], user.moderatable_federal_projects
    assert_equal [federal_projects(:dorogi).id], user.moderatable_federal_project_ids
    assert_equal [national_projects(:dorogi).id], user.moderatable_national_project_ids
  end

  test "is has repostable_federal_project_ids" do
    user = users(:one)
    assert_empty user.user_federal_project_permissions
    assert_empty user.repostable_federal_projects
    user.repostable_federal_projects << federal_projects(:dorogi)
    assert_equal 1, user.user_federal_project_permissions.count
    assert_equal [federal_projects(:dorogi)], user.repostable_federal_projects
    assert_equal [federal_projects(:dorogi).id], user.repostable_federal_project_ids
    assert_equal [national_projects(:dorogi).id], user.repostable_national_project_ids
  end

  test 'all_federal_subjects_dorogi_federal_project_media user has repostable national_project_ids' do
    user = users(:all_federal_subjects_dorogi_federal_project_media)
    assert_includes(
      user.repostable_national_project_ids,
      national_projects(:dorogi).id
    )
  end

  test 'all_federal_subjects_dorogi_federal_project_media user has repostable federal_project_ids' do
    user = users(:all_federal_subjects_dorogi_federal_project_media)
    assert_equal 2, user.user_federal_project_permissions.count
    assert_equal 1, user.user_federal_project_permissions.repostable.count
    assert_equal 1, user.repostable_federal_project_permissions.count
    assert_includes(
      user.repostable_federal_project_ids,
      federal_projects(:dorogi).id
    )
  end

  test "can be instantialized from registration" do
    registration = registrations(:one)
    user = User.new_from_registration(registration)
    assert_equal registration.email, user.email
    assert_equal registration.first_name, user.first_name
    assert_equal registration.last_name, user.last_name
    assert_equal registration.contact_position, user.contact_position
    assert_equal registration.contact_phone, user.contact_phone
  end

  test "setting encrypted password overrides password in same hash" do
    user = User.new({
      password: '123',
      encrypted_password: 'qwe'
    })
    assert_equal 'qwe', user.encrypted_password
  end

  test "create new user with permission" do
    federal_project = federal_projects(:one)
    user = users(:one).dup
    user.update!({
      email: 'new_user@example.com',
      password: 'qwerty',
      viewable_federal_project_ids: [federal_project.id],
      creatable_federal_project_ids: [federal_project.id],
      moderatable_federal_project_ids: [federal_project.id],
      repostable_federal_project_ids: [federal_project.id],
    })
    assert_includes user.viewable_federal_projects, federal_project
    assert_includes user.creatable_federal_projects, federal_project
    assert_includes user.moderatable_federal_projects, federal_project
    assert_includes user.repostable_federal_projects, federal_project
  end

  test "repostable user has repostable onboarding" do
    user = users(:one)
    user.repostable_federal_projects << federal_projects(:one)

    turotial = tutorials(:one)
    turotial.update!(repostable_permitted: true, onboarding: true)

    assert_equal turotial, user.relevant_onboarding
  end

  test "creatable user has creatable onboarding" do
    user = users(:one)
    user.creatable_federal_projects << federal_projects(:one)

    turotial = tutorials(:one)
    turotial.update!(creatable_permitted: true, onboarding: true)

    assert_equal turotial, user.relevant_onboarding
  end

  test "by_local_hour scope" do
    federal_subject = federal_subjects(:one)
    organization = organizations(:one)
    user = users(:one)

    user.update!(organization: nil)
    travel_to Time.zone.local(2020, 1, 1, 9) do
      assert_includes User.by_current_local_hour(9), user
    end

    user.update!(organization: organization)
    organization.update!(federal_subject: nil)
    travel_to Time.zone.local(2020, 1, 1, 9) do
      assert_includes User.by_current_local_hour(9), user
    end

    organization.update!(federal_subject: federal_subject)
    federal_subject.update!(time_zone: nil)
    travel_to Time.zone.local(2020, 1, 1, 9) do
      assert_includes User.by_current_local_hour(9), user
    end

    federal_subject.update!(time_zone: '')
    travel_to Time.zone.local(2020, 1, 1, 9) do
      assert_includes User.by_current_local_hour(9), user
    end

    federal_subject.update!(time_zone: 'Europe/Moscow')
    travel_to Time.zone.local(2020, 1, 1, 9) do
      assert_includes User.by_current_local_hour(9), user
    end

    federal_subject.update!(time_zone: 'Europe/Kaliningrad')
    travel_to Time.zone.local(2020, 1, 1, 9) do
      refute_includes User.by_current_local_hour(9), user
    end

    federal_subject.update!(time_zone: 'Europe/Kaliningrad')
    travel_to Time.zone.local(2020, 1, 1, 10) do
      assert_includes User.by_current_local_hour(9), user
    end
  end

  test "ransack" do
    user = users(:admin)
    assert_includes User.ransack(admin_true: '1').result, user
  end

  test "mailing_lists" do
    user = users(:one).tap { |u| u.update!(organization: nil) }
    assert_empty user.mailing_lists
  end

  test "soft delete" do
    user = users(:one)
    assert_not user.discarded?
    user.discard!
    assert user.discarded?
  end

  test "really destroy" do
    user = users(:one)
    registration = registrations(:one).tap { |r| r.update!(user: user) }
    user.reload

    assert_equal registration, user.registration
    assert_equal user, registration.user

    user.destroy!

    assert_nil User.with_discarded.find_by(id: user.id)
    assert_nil Registration.find_by(id: registration.id)
  end

  test "user_group_users" do
    user = users(:one)
    # user_group = user_groups(:edit_user_groups)
    assert user.user_group_users
    assert user.user_groups
    assert user.user_group_abilities
  end

  test "registration_confirmation validness scope" do
    user = User.new

    user.user_group_ids = []
    user.valid?(:registration_confirmation)
    assert_includes user.errors, :user_groups

    user.user_group_ids = [user_groups(:one).id]
    user.valid?(:registration_confirmation)
    assert_not_includes user.errors, :user_groups
  end
end
