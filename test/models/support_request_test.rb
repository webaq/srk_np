require 'test_helper'

class SupportRequestTest < ActiveSupport::TestCase
  test "is has valid fixture one" do
    support_request = support_requests(:one)
    support_request.valid?
    assert_empty support_request.errors
  end

  test "it has localized subject attribute name" do
    assert_equal('Тема', SupportRequest.human_attribute_name(:subject))
  end

  test "is is invalid without user" do
    support_request = SupportRequest.new(user: nil)
    support_request.valid?
    assert_includes support_request.errors, :user
  end

  test "it is invalid without subject" do
    support_request = SupportRequest.new(subject: '')
    support_request.valid?
    assert_includes support_request.errors, :subject
  end

  test "it is invalid without body" do
    support_request = SupportRequest.new(body: '')
    support_request.valid?
    assert_includes support_request.errors, :body
  end
end
