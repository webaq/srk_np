require 'test_helper'

class MainTopicTest < ActiveSupport::TestCase
  test "has valid fixture one" do
    main_topic = main_topics(:one)
    main_topic.valid?
    assert_empty main_topic.errors
  end

  test "is invalid without national_projects" do
    main_topic = MainTopic.new(national_projects: [])
    main_topic.valid?
    assert_includes main_topic.errors, :national_projects

    main_topic.national_projects = [national_projects(:one)]
    main_topic.valid?
    refute_includes main_topic.errors, :national_projects
  end

  test "is published or not" do
    main_topic = MainTopic.new

    main_topic.starts_at = nil
    main_topic.ends_at = nil
    assert main_topic.published?

    main_topic.starts_at = 1.day.ago
    main_topic.ends_at = nil
    assert main_topic.published?

    main_topic.starts_at = 1.day.from_now
    main_topic.ends_at = nil
    refute main_topic.published?

    main_topic.starts_at = nil
    main_topic.ends_at = 1.day.ago
    refute main_topic.published?

    main_topic.starts_at = nil
    main_topic.ends_at = 1.day.from_now
    assert main_topic.published?
  end
end
