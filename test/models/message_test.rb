require 'test_helper'

class MessageTest < ActiveSupport::TestCase
  test "it has valid fixture" do
    assert messages(:one).valid?
  end

  test "is invalid without content" do
    message = Message.new(content: '')
    message.valid?
    assert_includes message.errors, :content
  end

  test "it has organizations_count 1 for 1 organization" do
    # message = messages(:yesterday_to_press_offices)
    # message.update!(organizations: [organizations(:national_priority)])
    # assert_equal 1, message.organizations_count
  end

  test "relevant press_offices" do
    message = messages(:to_press_offices)
    assert_includes message.relevant_users, users(:dorogi_creator)
  end

  test "relevant moderators" do
    message = messages(:to_press_offices)
    message.update_column(:addressee, Message.addressees[:moderator])
    assert_includes message.relevant_users, users(:dorogi_creator_and_moderator)
  end

  test "relevant media" do
    message = messages(:to_press_offices)
    message.update_column(:addressee, Message.addressees[:media])
    assert_includes message.relevant_users, users(:media_dorogi)
  end

  test "relevant_users includes press_office user if addressee press_office" do
    message = messages(:one)
    message.addressee = 'press_office'
    user = users(:one)
    user.creatable_federal_projects << federal_projects(:one)
    assert_includes message.relevant_users, user
  end

  test "relevant_users do not include non press_office user if addressee press_office" do
    message = messages(:one)
    message.addressee = 'press_office'
    user = users(:one)
    refute_includes message.relevant_users, user
  end

  test "relevant_users includes press_office user if addressee blank" do
    message = messages(:one)
    message.addressee = nil
    user = users(:one)
    user.creatable_federal_projects << federal_projects(:one)
    assert_includes message.relevant_users, user
  end

  test "relevant_users includes non press_office user if addressee blank" do
    message = messages(:one)
    message.addressee = nil
    user = users(:one)
    assert_includes message.relevant_users, user
  end

  test "requires national_project" do
    message = Message.new(national_projects: [])
    message.valid?(:manual_create)
    assert_includes message.errors, :national_projects

    message = Message.new(national_projects: [])
    message.valid?
    assert_not_includes message.errors, :national_projects
  end
end
