require 'test_helper'

class OrganizationWebsiteTest < ActiveSupport::TestCase
  test "strips spaces around address" do
    organization_website = OrganizationWebsite.new(address: ' http://www.example.com   ')
    assert_equal 'http://www.example.com', organization_website.address
  end
end
