require 'test_helper'

class MailingListTest < ActiveSupport::TestCase
  test "has valid fixture" do
    mailing_list = mailing_lists(:one)
    mailing_list.valid?
    assert_empty mailing_list.errors
  end

  test "is is invalid without organization" do
    mailing_list = MailingList.new(organization: nil)
    mailing_list.valid?
    assert_includes mailing_list.errors, :organization
  end
end
