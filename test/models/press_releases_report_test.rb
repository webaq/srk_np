require 'test_helper'

class PressReleasesReportTest < ActiveSupport::TestCase
  test "it responds to chart_json" do
    press_releases_report = PressReleasesReport.new(Date.today - 7, Date.today)
    assert press_releases_report.respond_to?(:chart_json)
  end
end
