require 'test_helper'

class DashboardTest < ActiveSupport::TestCase
  setup do
    @dashboard = Dashboard.new({
      starts_on: Date.today.prev_month.beginning_of_month,
      ends_on: Date.today.prev_month.end_of_month,
      starts2_on: Date.today.beginning_of_month,
      ends2_on: Date.today.end_of_month,
      federal_subject: nil
    })
  end
end