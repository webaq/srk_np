require 'test_helper'

class SystemSubscriberTest < ActiveSupport::TestCase
  test "has valid fixtures" do
    %i[ one two ].each do |key|
      system_subscriber = system_subscribers(key)
      system_subscriber.valid?
      assert_empty system_subscriber.errors
    end
  end
end
