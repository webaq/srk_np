require 'test_helper'

class FederalProjectTest < ActiveSupport::TestCase
  test "has valid fixtures" do
    %i[ one ].each do |key|
      federal_project = federal_projects(key)
      federal_project.valid?
      assert_empty federal_project.errors
    end
  end

  test "destroy restricted if press_releases attached " do
    federal_project = federal_projects(:one)
    press_releases(:one).update!(federal_projects: [federal_project])
    assert_not federal_project.destroy
  end

  test "destroy success" do
    federal_project = federal_projects(:one)
    assert federal_project.destroy
  end

  test "viewable_users" do
    federal_project = federal_projects(:one)
    user = users(:one)

    user.user_federal_project_permissions.delete_all
    assert_nil UserFederalProjectPermission.viewable.find_by(user: user, federal_project: federal_project)
    assert_not_includes federal_project.viewable_users, user

    federal_project.viewable_users << [user]

    assert UserFederalProjectPermission.viewable.find_by(user: user, federal_project: federal_project)
    assert_includes federal_project.viewable_users, user

    # raises when adding already existing user
    assert_raise do
      federal_project.viewable_users << user
    end
  end

  test "creatable_users" do
    federal_project = federal_projects(:one)
    user = users(:one)

    user.user_federal_project_permissions.delete_all
    assert_nil UserFederalProjectPermission.creatable.find_by(user: user, federal_project: federal_project)
    assert_not_includes federal_project.creatable_users, user

    federal_project.creatable_users << user

    assert UserFederalProjectPermission.creatable.find_by(user: user, federal_project: federal_project)
    assert_includes federal_project.creatable_users, user
  end

  test "moderatable_users" do
    federal_project = federal_projects(:one)
    user = users(:one)

    user.user_federal_project_permissions.delete_all
    assert_nil UserFederalProjectPermission.moderatable.find_by(user: user, federal_project: federal_project)
    assert_not_includes federal_project.moderatable_users, user

    federal_project.moderatable_users << user

    assert UserFederalProjectPermission.moderatable.find_by(user: user, federal_project: federal_project)
    assert_includes federal_project.moderatable_users, user
  end

  test "repostable_users" do
    federal_project = federal_projects(:one)
    user = users(:one)

    user.user_federal_project_permissions.delete_all
    assert_nil UserFederalProjectPermission.repostable.find_by(user: user, federal_project: federal_project)
    assert_not_includes federal_project.repostable_users, user

    federal_project.repostable_users << user

    assert UserFederalProjectPermission.repostable.find_by(user: user, federal_project: federal_project)
    assert_includes federal_project.repostable_users, user
  end

  test "import_permitted_users" do
    federal_project = federal_projects(:one)
    user = users(:one)

    user.user_federal_project_permissions.delete_all
    assert_nil UserFederalProjectPermission.viewable.find_by(user: user, federal_project: federal_project)
    assert_not_includes federal_project.viewable_users, user

    skip
    federal_project.import_permitted_users(:viewable, [user])

    assert UserFederalProjectPermission.viewable.find_by(user: user, federal_project: federal_project)
    assert_includes federal_project.viewable_users, user

    # not raises when adding already existing user
    federal_project.import_permitted_users(:viewable, [user])
    assert_includes federal_project.viewable_users, user
  end
end
