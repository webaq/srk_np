require "test_helper"

class SubscriptionDeliveryTest < ActiveSupport::TestCase
  include ActionMailer::TestHelper

  test "call daily" do
    assert_nil SubscriptionDelivery.new(:daily).call
  end

  test "call weekly" do
    assert_nil SubscriptionDelivery.new(:weekly).call

    subscription = subscriptions(:one)
    press_releases(:one).update!(published_at: 1.week.ago)

    stub_request(:post, "https://api.sendpulse.com/oauth/access_token")
      .to_return(body: '{"access_token":"abc","expires_in":3600}', headers: { content_type: 'application/json' })

    stub_request(:post, "https://api.sendpulse.com/smtp/emails")
      .to_return(body: '{"id":"def"}', headers: { content_type: 'application/json' })


    assert_difference "SubscriptionMessage.count", 1 do
      SubscriptionDelivery.new(:weekly, [subscription]).call
    end
    subscription_message = SubscriptionMessage.last
    assert_equal subscription.user, subscription_message.user
    assert_equal "def", subscription_message.message_id
    assert_equal subscription.user.email, subscription_message.recipient
    assert_equal subscription.frequency, subscription_message.frequency
  end
end