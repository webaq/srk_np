require 'test_helper'

class UserListTest < ActiveSupport::TestCase
  test "users_attributes assignment" do
    user_list = UserList.new(users_attributes: {
      1 => { email: 'email1@example.com' },
      2 => { email: 'email2@example.com' }
    })
    assert_equal 2, user_list.users.size
  end

  test "it is valid if all users valid" do
    user_list = UserList.new(users_attributes: {
      1 => { email: 'email1@example.com' },
      2 => { email: 'email2@example.com' }
    })
    assert user_list.valid?
  end

  test "it is invvalid if any user invalid" do
    user_list = UserList.new(users_attributes: {
      1 => { email: '' },
      2 => { email: 'email2@example.com' }
    })
    assert !user_list.valid?
  end
end