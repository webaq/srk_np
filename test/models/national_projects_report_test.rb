require 'test_helper'

class NationalProjectsReportTest < ActiveSupport::TestCase
  test "generates rows" do
    organizations_scope = Organization.all
    press_releases_scope = PressRelease.all
    national_projects_report = NationalProjectsReport.new(organizations_scope, press_releases_scope)
    assert national_projects_report.rows
  end
end
