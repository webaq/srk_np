require 'test_helper'

class PressReleaseTest < ActiveSupport::TestCase
  test "has valid fixtures" do
    %i[ one two three ].each do |key|
      press_release = press_releases(key)

      press_release.valid?
      assert_empty press_release.errors

      press_release.valid?(:press_release_submit)
      assert_empty press_release.errors
    end
  end

  test "approved_and_published_dorogi fixture" do
    press_release = press_releases(:approved_and_published_dorogi)
    assert press_release.valid?
  end

  test "it is valid with required fields" do
    press_release = PressRelease.new({
      user: users(:one),
      federal_subject_ids: [federal_subjects(:moscow).id],
      national_project_ids: [national_projects(:dorogi).id],
      title: 'Название инфоповода',
    })
    assert press_release.valid?
  end

  test "it is invalid without national projects" do
    press_release = PressRelease.new
    press_release.valid?
    assert press_release.errors[:national_projects].any?
  end

  test "all_federal_subjects is true if selected_federal_subjects_ids set to 100%" do
    press_release = PressRelease.new({
      selected_federal_subject_ids: FederalSubject.pluck(:id)
    })
    assert press_release.all_federal_subjects?
  end

  test "federal_subjects empty if selected_federal_subjects_id set to 100%" do
    press_release = PressRelease.new({
      selected_federal_subject_ids: FederalSubject.pluck(:id)
    })
    assert_empty press_release.federal_subjects
  end

  test "selected_federal_subject_ids is 100% if all_federal_subjects" do
    press_release = PressRelease.new({
      all_federal_subjects: true
    })
    assert_equal FederalSubject.pluck(:id), press_release.selected_federal_subject_ids
  end

  test "it can be approved" do
    press_release = press_releases(:one)
    assert !press_release.approved?
    press_release.approve
    assert press_release.approved?
  end

  test "it updates national_projects_count" do
    press_release = PressRelease.create!({
      user: users(:admin),
      federal_subject_ids: [federal_subjects(:moscow).id],
      national_project_ids: [national_projects(:dorogi).id],
      title: 'Название инфоповода',
    })
    assert_equal 1, press_release.national_projects_count

    press_release.update!({ national_project_ids: [national_projects(:demography).id] })
    assert_equal 1, press_release.national_projects_count
  end

  test "need_pre_approvement" do
    federal_subject = FederalSubject.new
    organization = Organization.new
    user = User.new(organization: organization)
    press_release = PressRelease.new(user: user)

    organization.kind = 'regional_authority'
    organization.federal_subject = federal_subject
    assert press_release.need_pre_approvement?

    organization.kind = 'city_authority'
    organization.federal_subject = federal_subject
    assert press_release.need_pre_approvement?

    organization.kind = 'other'
    organization.federal_subject = federal_subject
    assert press_release.need_pre_approvement?

    organization.kind = 'federal_authority'
    organization.federal_subject = federal_subject
    assert_not press_release.need_pre_approvement?

    organization.kind = 'regional_authority'
    organization.federal_subject = nil
    assert_not press_release.need_pre_approvement?

    user.organization = nil
    assert_not press_release.need_pre_approvement?
  end

  test "content should be uniq" do
    press_releases(:one).update!(content: 'инфоповод, а внутри какой-то текст 123')
    new_press_release = PressRelease.new

    new_press_release.content = 'какой-то текст' # 0.7 similarity
    new_press_release.valid?(:press_release_submit)
    assert_not_includes new_press_release.errors, :content

    new_press_release.content = 'внутри какой-то текст инфоповод 123' # 1.0 similarity
    new_press_release.valid?(:press_release_submit)
    assert_includes new_press_release.errors, :content

    assert_equal ['Ранее такая новость уже была загружена в СРК. Пожалуйста, загружайте в СРК только уникальные инфоповоды.'], new_press_release.errors.full_messages_for(:content)
  end

  test "content_uniqueness should not check same press_release" do
    press_release = press_releases(:one)
    press_release.update!(content: 'инфоповод, а внутри какой-то текст')
    press_release.valid?(:press_release_submit)
    assert_not_includes press_release.errors, :content
  end

  test "generate lexemes" do
    lexemes = PressRelease.generate_lexemes('Ранее такая новость уже была загружена в СРК. Пожалуйста, загружайте в СРК только уникальные инфоповоды.')
    assert_equal ["загруж", "загружа", "инфоповод", "новост", "пожалуйст", "ран", "срк", "так", "уникальн"], lexemes
  end

  test "generate lexemes on content set" do
    press_release = press_releases(:one)
    press_release.update!(content: 'Ранее такая новость уже была загружена в СРК. Пожалуйста, загружайте в СРК только уникальные инфоповоды.')
    assert_equal ["загруж", "загружа", "инфоповод", "новост", "пожалуйст", "ран", "срк", "так", "уникальн"], press_release.lexemes
  end

  test "similiar" do
    press_release1 = press_releases(:one)
    press_release2 = press_releases(:two)

    press_release1.update!(content: 'инфоповод, а внутри какой-то текст 123')
    press_release2.content = 'какой-то текст'

    assert_includes press_release2.similiar(0.5), press_release1
  end

  test "class similiar" do
    press_release = press_releases(:one)

    press_release.update!(content: 'инфоповод, а внутри какой-то текст 123')
    similiar_press_releases = PressRelease.similiar_press_releases('какой-то текст', 0.5)

    assert_includes similiar_press_releases, press_release
  end

  test "set plain_text_content" do
    press_release = PressRelease.new(content: '<div>123</div>')
    assert_equal '123', press_release.plain_text_content
  end

  test "origin cant be the press_release itself" do
    press_release = press_releases(:one).tap do |press_release|
      press_release.update!({
        content: '1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20',
      })
    end
    assert_not_equal press_release, press_release.find_origin(75)
  end

  test "set origins on create" do
    press_release1 = press_releases(:one).tap do |press_release|
      press_release.update!({
        content: '1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20',
        created_at: 3.hours.ago,
      })
    end

    press_release2 = press_releases(:one).tap do |press_release|
      press_release.update!({
        content: '1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20',
        created_at: 2.hours.ago,
      })
    end

    press_release3 = PressRelease.new({
      content: '1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20',
      created_at: 1.hours.ago,
    })

    assert_equal press_release1, press_release3.origin75_press_release
  end

  test "set origins on update" do
    press_release1 = press_releases(:one).tap do |press_release|
      press_release.update!({
        content: '1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20',
        created_at: 3.hours.ago,
      })
    end
    press_release2 = press_releases(:two)
    assert_nil press_release2.origin75_press_release

    press_release2.update!({
      content: '1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20',
      created_at: 2.hours.ago,
    })
    assert_equal press_release1, press_release2.origin75_press_release
  end

  test "update duplicates' origins on update" do
    press_release1 = press_releases(:one)
    press_release2 = press_releases(:two)
    press_release2.origin75_press_release = press_release1
    press_release1.update!(content: 'новый контент')
    press_release2.reload
    assert_not_equal press_release1, press_release2.origin75_press_release
  end

  test "update duplicates' origins on delete" do
    press_release1 = press_releases(:one).tap { |pr|
      pr.update!({
        content: '1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20',
        created_at: 3.hours.ago,
      })
    }
    press_release2 = press_releases(:two).tap { |pr|
      pr.update!({
        content: '1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20',
        created_at: 2.hours.ago,
      })
    }
    press_release3 = press_releases(:three).tap { |pr|
      pr.update!({
        content: '1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20',
        created_at: 1.hours.ago,
      })
    }

    assert_equal press_release1, press_release3.origin75_press_release

    press_release1.reload
    assert_includes press_release1.duplicate_press_releases, press_release3

    press_release1.destroy!
    press_release3.reload
    assert_equal press_release2, press_release3.origin75_press_release
  end

  test "soft delete" do
    press_release = press_releases(:one)
    assert_not press_release.discarded?
    press_release.discard!
    assert press_release.discarded?
  end

  test "tag is required" do
    press_release = PressRelease.new(tag_ids: [])
    assert_not press_release.valid?(:press_release_submit)
    assert_includes press_release.errors, :tags
  end

  test "press_releases_with_same_tags" do
    press_release1 = press_releases(:one)
    press_release2 = press_releases(:two)
    tag = tags(:one)

    press_release1.update!({
      published_at: 1.hours.ago,
      approved_at: Time.current,
      tag_ids: [tag.id],
    })
    press_release2.update!({
      published_at: 2.hour.ago,
      tag_ids: [tag.id],
    })

    assert_includes press_release2.press_releases_with_same_tags, press_release1
  end

  test "cannot be approved if returned" do
    press_release = PressRelease.new({
      returned_at: Time.current,
      approved_at: Time.current,
    })
    press_release.valid?
    assert_includes press_release.errors, :approved_at
  end

  test "returned? if returned_at" do
    press_release = PressRelease.new

    press_release.returned_at = nil
    assert_not press_release.returned?

    press_release.returned_at = Time.current
    assert press_release.returned?
  end

  test "max_appoved_at" do
    press_release = PressRelease.new(published_at: Time.new(2021, 5, 18))
    assert_equal Time.new(2021, 6, 10), press_release.max_approved_at
  end

  test "related_press_releases" do
    press_release1 = press_releases(:one)
    press_release2 = press_releases(:two)
    press_release1.update!({
      related_press_release_ids: [press_release2.id]
    })
    assert_includes press_release1.related_press_releases, press_release2
    assert_equal press_release1.id, press_release2.press_release_related_relations.last.press_release_id
  end

  test "extract_id_from_url" do
    url = "http://localhost:3000/press_releases/123"
    assert_equal 123, PressRelease.extract_id_from_url(url)

    url = ""
    assert_nil PressRelease.extract_id_from_url(url)

    url = nil
    assert_nil PressRelease.extract_id_from_url(url)
  end

  test "category required" do
    press_release = PressRelease.new(press_release_value_id: nil)
    press_release.valid?(:press_release_submit)
    assert_includes press_release.errors, :category

    press_release = PressRelease.new(press_release_value: press_release_values(:one))
    press_release.valid?(:press_release_submit)
    assert_not_includes press_release.errors, :category
  end
end
