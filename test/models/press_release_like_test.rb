require "test_helper"

class PressReleaseLikeTest < ActiveSupport::TestCase
  test "has valid fixtures" do
    %i[ one ].each do |key|
      press_release_like = press_release_likes(key)
      press_release_like.valid?
      assert_empty press_release_like.errors
    end
  end
end
