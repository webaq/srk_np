require 'test_helper'

class PressReleasesFilterTest < ActiveSupport::TestCase
  test "defaults" do
    press_releases_filter = PressReleasesFilter.new(nil, nil, {})
    assert_equal [], press_releases_filter.national_project_ids
  end

  test "set national_project_ids" do
    press_releases_filter = PressReleasesFilter.new(nil, nil, { national_project_ids: ['1'] })
    assert_equal [1], press_releases_filter.national_project_ids
  end
end