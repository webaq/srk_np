require 'test_helper'

class PageTest < ActiveSupport::TestCase
  test "is invalid without slug" do
    page = Page.new
    page.valid?
    assert_includes page.errors, :slug
  end

  test "is invalid if slug is not unique" do
    page = Page.new(slug: pages(:user_agreement).slug)
    page.valid?
    assert_includes page.errors, :slug
  end

  test "is invalid without name" do
    page = Page.new
    page.valid?
    assert_includes page.errors, :name
  end
end
