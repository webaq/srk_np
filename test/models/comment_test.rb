require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  test "has valid fixtures" do
    %i[ one ].each do |key|
      comment = comments(key)
      comment.valid?
      assert_empty comment.errors
    end
  end
end
