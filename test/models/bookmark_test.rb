require 'test_helper'

class BookmarkTest < ActiveSupport::TestCase
  test "has valid fixtures" do
    %i[ one ].each do |key|
      bookmark = bookmarks(key)
      bookmark.valid?
      assert_empty bookmark.errors
    end
  end
end
