require 'test_helper'

class BestPracticeTest < ActiveSupport::TestCase
  test "is sets published_at if it is nil" do
    best_practice = BestPractice.new(published_at: nil)
    best_practice.valid?
    assert_not_nil best_practice.published_at
  end
end
