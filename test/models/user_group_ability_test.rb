require "test_helper"

class UserGroupAbilityTest < ActiveSupport::TestCase
  test "ability validness" do
    user_group_ability = UserGroupAbility.new

    user_group_ability.ability = nil
    user_group_ability.valid?
    assert_includes user_group_ability.errors, :ability

    user_group_ability.ability = :edit_user_groups
    user_group_ability.valid?
    assert_not_includes user_group_ability.errors, :ability
  end
end
