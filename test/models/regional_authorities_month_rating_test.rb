require "test_helper"

class RegionalAuthoritiesMonthRatingTest < ActiveSupport::TestCase
  test "scoring" do
    rating = RegionalAuthoritiesMonthRating.new(nil)
    assert_equal :quantity, rating.scoring

    rating = RegionalAuthoritiesMonthRating.new(nil, nil)
    assert_equal :quantity, rating.scoring

    rating = RegionalAuthoritiesMonthRating.new(nil, "value")
    assert_equal :value, rating.scoring
  end

  test "federal_authorities_stats" do
    rating_month_mock = Minitest::Mock.new
    rating_month_mock.expect :nil?, false
    rating_month_mock.expect :starts_at, Time.current
    rating_month_mock.expect :ends_at, Time.current
    rating_month_mock.expect :prev_starts_at, Time.current
    rating_month_mock.expect :prev_ends_at, Time.current

    rating = RegionalAuthoritiesMonthRating.new(rating_month_mock, "value")
    assert rating.federal_authorities_stats
  end

  test "federal_subjects_press_releases_stats" do
    rating_month_mock = Minitest::Mock.new
    rating_month_mock.expect :nil?, false
    rating_month_mock.expect :starts_at, Time.current
    rating_month_mock.expect :ends_at, Time.current
    rating_month_mock.expect :prev_starts_at, Time.current
    rating_month_mock.expect :prev_ends_at, Time.current

    rating = RegionalAuthoritiesMonthRating.new(rating_month_mock, "value")
    assert rating.federal_subjects_press_releases_stats
  end
end