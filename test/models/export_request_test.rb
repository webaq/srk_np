require 'test_helper'

class ExportRequestTest < ActiveSupport::TestCase
  test "it has localized subject attribute name" do
    assert_equal('Тема', ExportRequest.human_attribute_name(:subject))
  end

  test "it is invalid without body" do
    export_request = ExportRequest.new(body: '')
    export_request.valid?
    assert_includes export_request.errors, :body
  end
end
