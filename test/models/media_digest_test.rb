require 'test_helper'

class MediaDigestTest < ActiveSupport::TestCase
  test "press_releases" do
    user = users(:one)
    media_digest = MediaDigest.new(user)
    assert media_digest.respond_to?(:press_releases_count)
  end
end