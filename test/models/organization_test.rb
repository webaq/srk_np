require 'test_helper'

class OrganizationTest < ActiveSupport::TestCase
  test "has valid fixtures" do
    %i[ one federal_authority1 federal_authority2 federal_authority3 ].each do |key|
      organization = organizations(key)
      organization.valid?
      assert_empty organization.errors
    end
  end

  test "it has valid fixture media" do
    organization = organizations(:media)
    organization.valid?
    assert_empty organization.errors
    assert organization.media?
  end

  test "media fixture" do
    assert organizations(:media).valid?
    assert_equal ['http://www.example.com'], organizations(:media).organization_websites.pluck(:address)
  end

  test "can be instantialized from registration" do
    registration = registrations(:one)
    registration.website = 'http://www.example.com'
    registration.media_subscribers = '1000 человек'

    organization = Organization.new_from_registration(registration)
    assert_equal registration.kind, organization.kind
    assert_equal registration.organization_name, organization.name
    assert_equal 'http://www.example.com', organization.organization_websites.first&.address
    assert_equal '1000 человек', organization.media_subscribers
  end

  test "can be deleted with message_organizations" do
    organization = organizations(:one)
    messages(:one).organizations << organization
    assert organization.destroy
  end

  test "is admin if Национальные приоритеты" do
    organization = Organization.new(name: '123')
    refute organization.admin?

    organization = Organization.new(name: 'АНО Национальные приоритеты')
    assert organization.admin?
  end

  test "it has mailing lists" do
    organization = Organization.new
    assert organization.respond_to? :mailing_lists
  end

  test "is strips name" do
    organization = Organization.new(name: ' название  ')
    assert_equal 'название', organization.name
  end

  test "confirmed_users_counter_cache" do
    organization = organizations(:one)
    user = users(:one)
    organization.users = [user]

    user.update!(confirmed_at: nil)
    organization.reload
    assert_not organization.users.first.confirmed?
    assert_equal 0, organization.confirmed_users_count

    user.update!(confirmed_at: Time.current)
    organization.reload
    assert organization.users.first.confirmed?
    assert_equal 1, organization.confirmed_users_count
  end

  test "bookmarks_count" do
    organization = organizations(:one)
    user = users(:one)
    user.bookmarks.destroy_all
    organization.users = [user]

    assert_equal organization, user.organization
    assert_equal 0, organization.bookmarks_count

    user.bookmarks.create!(press_release: press_releases(:one))
    organization.reload
    assert_equal 1, organization.bookmarks_count
  end

  test "reposts_count" do
    organization = organizations(:one)
    user = users(:one)
    user.reposts.destroy_all
    organization.users = [user]

    assert_equal organization, user.organization
    assert_equal 0, organization.reposts_count

    user.reposts.create!(press_release: press_releases(:one), link: "http://www.yandex.ru")
    organization.reload
    assert_equal 1, organization.reposts_count
  end

  test "shown_press_releases_count" do
    organization = organizations(:one)
    user = users(:one)
    # user.reposts.destroy_all
    organization.users = [user]

    assert_equal organization, user.organization
    assert_equal 0, organization.shown_press_releases_count

    press_releases(:one).create_activity key: 'press_release.show', owner: user
    organization.reload
    assert_equal 1, organization.shown_press_releases_count
  end
end
