require 'test_helper'

class RatingMonthTest < ActiveSupport::TestCase
  test "has valid fixtures" do
    %i[ one two ].each do |key|
      rating_month = rating_months(key)
      rating_month.valid?
      assert_empty rating_month.errors
    end
  end

  test "last_month?" do
    refute rating_months(:one).last_month?
    assert rating_months(:two).last_month?
  end
end
