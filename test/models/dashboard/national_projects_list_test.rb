require "test_helper"

class Dashboard::NationalProjectsListTest < ActiveSupport::TestCase
  test "initialize" do
    t = Time.current
    national_projects_list = Dashboard::NationalProjectsList.new(starts_at: t)
    assert_equal t, national_projects_list.starts_at

    national_projects_list = Dashboard::NationalProjectsList.new(starts_at: "2021-06-28 00:00:00 +0300")
    assert_equal Time.new(2021, 6, 28), national_projects_list.starts_at
  end
end