require 'test_helper'

class CommentCreatedNotificationTest < ActiveSupport::TestCase
  test "call" do
    comment = comments(:one)
    comment_created_notification = CommentCreatedNotification.new(comment)

    assert_difference 'Message.count', 1 do
      assert_difference 'MessageDelivery.count', 1 do
        assert comment_created_notification.call
      end
    end
  end
end