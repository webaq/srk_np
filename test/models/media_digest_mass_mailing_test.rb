require 'test_helper'

class MediaDigestMassMailingTest < ActiveSupport::TestCase
  test "should send mail if there are any press_releases" do
    skip
    media_digest_mass_mailing = MediaDigestMassMailing.new
    media_digest_mass_mailing.stubs(:users).returns([users(:one)])
    MediaDigest.any_instance.stubs(:press_releases_count).returns(1)

    mock = mock()
    mock.expects(:deliver)
    NotifierMailer.expects(:media_digest_mail).returns(mock)

    assert media_digest_mass_mailing.call
  end

  test "should not send mail if there are no press_releases" do
    media_digest_mass_mailing = MediaDigestMassMailing.new
    media_digest_mass_mailing.stubs(:users).returns([users(:one)])
    MediaDigest.any_instance.stubs(:press_releases_count).returns(0)

    NotifierMailer.expects(:media_digest_mail).never

    assert media_digest_mass_mailing.call
  end

  test "users include media user" do
    federal_subject = federal_subjects(:one).tap { |fs| fs.update!(time_zone: 'Europe/Moscow' )}
    organization = organizations(:media).tap { |fs| fs.update!(federal_subject: federal_subject) }
    user = users(:one).tap { |u| u.update!(organization: organization) }
    media_digest_mass_mailing = MediaDigestMassMailing.new

    travel_to Time.zone.local(2020, 1, 1, 9) do
      assert_includes media_digest_mass_mailing.users, user
    end
  end

  test "users include user subcribed to media digest" do
    user = users(:one).tap { |u| u.update!(subscribed_to_media_digest: true) }
    media_digest_mass_mailing = MediaDigestMassMailing.new

    travel_to Time.zone.local(2020, 1, 1, 9) do
      assert_includes media_digest_mass_mailing.users, user
    end
  end
end