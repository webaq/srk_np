require 'test_helper'

class TagRequestTest < ActiveSupport::TestCase
  test "it has localized subject attribute name" do
    assert_equal('Тег', TagRequest.human_attribute_name(:subject))
  end

  test "it is invalid without subject" do
    tag_request = TagRequest.new(subject: '')
    tag_request.valid?
    assert_includes tag_request.errors, :subject
  end
end
