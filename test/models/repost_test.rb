require 'test_helper'

class RepostTest < ActiveSupport::TestCase
  test "is valid with valid link" do
    repost = Repost.new(link: 'http://www.example.com')
    repost.valid?
    refute_includes repost.errors, :link
  end

  test "is invalid with invalid link" do
    repost = Repost.new(link: '')
    repost.valid?
    assert_includes repost.errors, :link

    repost = Repost.new(link: '123')
    repost.valid?
    assert_includes repost.errors, :link

    repost = Repost.new(link: 'http://gazeta- kurkino.ru/news/v-oblastnoy- onkodispanser-post/?')
    repost.valid?
    assert_includes repost.errors, :link
  end

  # test "is invalid if user's organization website blank" do
  #   organization = Organization.new(organization_websites: [])
  #   user = User.new(organization: organization)
  #   repost = Repost.new(user: user, link: 'https://www.example1.com/123')
  #   repost.valid?
  #   assert(repost.errors[:link].any?)
  # end

  # test "is invalid if link doesn't match organization domain" do
  #   organization = Organization.new(organization_websites: [
  #     OrganizationWebsite.new(address: 'https://www.example.com/')
  #   ])
  #   user = User.new(organization: organization)
  #   repost = Repost.new(user: user, link: 'https://www.example2.com/123')
  #   repost.valid?
  #   assert(repost.errors[:link].any?)
  # end

  # test "is valid if link matches one of the organization's domains" do
  #   organization = Organization.new(organization_websites: [
  #     OrganizationWebsite.new(address: 'https://www.example2.com/'),
  #     OrganizationWebsite.new(address: 'https://www.example.com/')
  #   ])
  #   user = User.new(organization: organization)
  #   repost = Repost.new(user: user, link: 'https://www.example.com/123')
  #   repost.valid?
  #   assert_empty repost.errors[:link]
  # end

  # test "is valid if link matches one of the organization's domains (cyrillic)" do
  #   organization = Organization.new(organization_websites: [
  #     OrganizationWebsite.new(address: 'https://www.example2.com/'),
  #     OrganizationWebsite.new(address: 'https://стопкоронавирус.рф')
  #   ])
  #   user = User.new(organization: organization)
  #   repost = Repost.new(user: user, link: 'https://стопкоронавирус.рф/what-to-do/all/')
  #   repost.valid?
  #   assert_empty repost.errors[:link]
  # end

  # test "is valid if link does not match one of the organization's domains but organization is allowed" do
  #   organization = Organization.new(organization_websites: [], skip_repost_domain_validation: true)
  #   user = User.new(organization: organization)
  #   repost = Repost.new(user: user, link: 'https://www.example.com/123')
  #   repost.valid?
  #   assert_empty repost.errors[:link]
  # end

  test "strips spaces around link" do
    repost = Repost.new(link: ' http://www.example.com   ')
    assert_equal 'http://www.example.com', repost.link
  end
end
