require 'test_helper'

class PlacementTest < ActiveSupport::TestCase
  test "is has valid fixture one" do
    placement = placements(:one)
    placement.valid?
    assert_empty placement.errors
  end

  test "is invalid without national_project" do
    placement = Placement.new(national_projects: [])
    placement.valid?
    assert_includes placement.errors, :national_projects
  end

  test "is invalid without federal_subject" do
    placement = Placement.new(federal_subjects: [])
    placement.valid?
    assert_includes placement.errors, :federal_subjects
  end

  test "is invalid without title" do
    placement = Placement.new(title: '')
    placement.valid?
    assert_includes placement.errors, :title
  end

  test "is invalid without images" do
    placement = Placement.new(images: [])
    placement.valid?
    assert_includes placement.errors, :images
  end
end
