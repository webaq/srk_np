require "test_helper"

class Rating::FederalAuthoritiesPeriodPeformancesTest < ActiveSupport::TestCase
  test "call" do
    federal_authority1 = organizations(:federal_authority1)
    federal_authority2 = organizations(:federal_authority2)
    addon_organization = organizations(:one)
    user1 = users(:one)
    user2 = users(:two)
    user3 = users(:three)
    press_release1 = press_releases(:one)
    press_release2 = press_releases(:two)
    press_release3 = press_releases(:three)
    press_release_value1 = press_release_values(:one)
    press_release_value2 = press_release_values(:two)

    federal_authority1.update({ rated: true })
    federal_authority2.update({ rated: true })
    addon_organization.update({ rating_addon_organization: federal_authority2 })

    user1.update!({ organization: federal_authority1 })
    user2.update!({ organization: federal_authority2 })
    user3.update!({ organization: addon_organization })
    press_release_value1.update!(value: 0.1)
    press_release_value2.update!(value: 0.2)

    press_release1.update!({
      user: user1,
      published_at: Time.new(2021, 8, 15),
      press_release_value: press_release_value1,
      approved_at: Time.current,
    })
    press_release2.update!({
      user: user2,
      published_at: Time.new(2021, 8, 16),
      press_release_value: press_release_value2,
      approved_at: Time.current,
    })
    press_release3.update!({
      user: user3,
      published_at: Time.new(2021, 8, 16),
      press_release_value: press_release_value2,
      approved_at: Time.current,
    })

    items = Rating::FederalAuthoritiesPeriodPerformances.new(Time.new(2021, 8)..Time.new(2021, 9)).call

    assert_equal({
      federal_authority2.id => Rating::FederalAuthorityPerformance.new(owned_value: 0.2, addons: [[addon_organization.id, 0.2]]),
      federal_authority1.id => Rating::FederalAuthorityPerformance.new(owned_value: 0.1, addons: []),
    }, items)
  end
end