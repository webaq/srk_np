require "test_helper"

class Rating::FederalAuthoritiesRatingTest < ActiveSupport::TestCase
  test "call" do
    federal_authority1 = organizations(:federal_authority1)
    federal_authority1.update!(rated: true)
    # federal_authority2 = organizations(:federal_authority1)
    # addon_organization = organizations(:one)

    mock = Minitest::Mock.new

    # current period mock
    mock.expect :call, ->{
      {
        federal_authority1.id => Rating::FederalAuthorityPerformance.new(owned_value: 0.11, addons: []),
      }
    }, [Object]

    # prev period mock
    mock.expect :call, ->{
      {
        federal_authority1.id => Rating::FederalAuthorityPerformance.new(owned_value: 0.1, addons: []),
      }
    }, [Object]

    rows = nil
    Rating::FederalAuthoritiesPeriodPerformances.stub :new, mock do
      rows = Rating::FederalAuthoritiesRating.new(Time.new(2021, 8)..Time.new(2021, 9), Time.new(2021, 7)..Time.new(2021, 8)).call
    end

    assert_equal [
      RegionalAuthoritiesMonthRating::FederalSubjectRow.new(name: federal_authority1.name, current_count: 0.11, prev_count: 0.1, rank: 0, partition: 0)
    ], rows
  end
end