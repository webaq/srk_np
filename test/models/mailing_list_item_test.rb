require 'test_helper'

class MailingListItemTest < ActiveSupport::TestCase
  test "has valid fixture" do
    mailing_list_item = mailing_list_items(:one)
    mailing_list_item.valid?
    assert_empty mailing_list_item.errors
  end
end
