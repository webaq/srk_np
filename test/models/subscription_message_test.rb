require 'test_helper'

class SubscriptionMessagesTest < ActiveSupport::TestCase
  test "has valid fixtures" do
    %i[ one ].each do |key|
      subscription_message = subscription_messages(key)
      subscription_message.valid?
      assert_empty subscription_message.errors
    end
  end
end
