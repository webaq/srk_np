require 'test_helper'

class MessageDeliveryTest < ActiveSupport::TestCase
  test "has valid fixtures" do
    %i[ one ].each do |key|
      message_delivery = message_deliveries(key)
      message_delivery.valid?
      assert_empty message_delivery.errors
    end
  end
end
