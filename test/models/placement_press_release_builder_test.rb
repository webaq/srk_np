require 'test_helper'

class PlacementPressReleaseBuilderTest < ActiveSupport::TestCase
  test "press_release" do
    placement = placements(:one)
    placement.images.attach(io: File.open(file_fixture('placement.jpg')), filename: 'placement.jpg')

    placement_press_release_builder = PlacementPressReleaseBuilder.new(placement)
    press_release = placement_press_release_builder.press_release

    assert press_release.images.any?
  end
end