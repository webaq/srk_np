require 'test_helper'

class SubscriptionPressReleasesFilterTest < ActiveSupport::TestCase
  test "call" do
    subscription = subscriptions(:one)

    subscription_press_releases_filter = SubscriptionPressReleasesFilter.new(subscription, 'daily')
    assert subscription_press_releases_filter.call
  end
end