require 'test_helper'

class DashboardStatsMailingTest < ActiveSupport::TestCase
  test "emails" do
    dashboard_stats_mailing = DashboardStatsMailing.new(['test@example.com'])
    assert_equal ['test@example.com'], dashboard_stats_mailing.emails

    SystemSubscriber.stubs(:dashboard_stats).returns([users(:one)])
    dashboard_stats_mailing = DashboardStatsMailing.new
    assert_equal [users(:one).email], dashboard_stats_mailing.emails
  end

  test "send_weekly" do
    UserMailer.any_instance.expects(:dashboard_stats).once
    DashboardStatsMailing.new(['test@example.com']).send_weekly
  end
end
