require "test_helper"

class SubscriptionTest < ActiveSupport::TestCase
  test "has valid fixtures" do
    %i[ one ].each do |key|
      subscription = subscriptions(key)
      subscription.valid?
      assert_empty subscription.errors
    end
  end

  test "national_projects_count" do
    subscription = subscriptions(:one)
    assert_equal 0, subscription.federal_subjects_count
    assert_equal 0, subscription.national_projects_count

    subscription.update!({
      federal_subject_ids: [federal_subjects(:one).id],
      national_project_ids: [national_projects(:one).id],
    })
    assert_equal 1, subscription.federal_subjects_count
    assert_equal 1, subscription.national_projects_count

    subscription.update!({
      federal_subject_ids: [],
      national_project_ids: [],
    })
    assert_equal 0, subscription.federal_subjects_count
    assert_equal 0, subscription.national_projects_count
  end
end
