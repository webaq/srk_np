require "test_helper"

class NotificationPopupTest < ActiveSupport::TestCase
  test "has valid fixture" do
    notification_popup = notification_popups(:one)
    notification_popup.valid?
    assert_empty notification_popup.errors
  end

  test "for_user" do
    notification_popup = notification_popups(:one)
    notification_popup.update!(organization_kinds: [:federal_authority])
    user = users(:federal_authority)
    # organization_mock = Minitest::Mock.new
    # organization_mock.expect :kind, :federal_authority
    # user_mock.expect :organization, organization_mock
    assert_nil user.organization.federal_subject
    assert_equal notification_popup, NotificationPopup.for_user(user)

    notification_popup.update!(only_regional_organizations: true)
    assert_nil NotificationPopup.for_user(user)
  end
end
