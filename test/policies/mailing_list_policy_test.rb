require 'test_helper'

class MailingListPolicyTest < ActiveSupport::TestCase
  setup do
    @organization = organizations(:one)
    @organization.federal_authority!

    @user = users(:one)
    @user.update!(organization: @organization)
  end

  test "index" do
    @user.update!(organization: nil)
    refute Pundit.policy!(@user, :mailing_list).index?

    @user.update!(organization: @organization)
    @user.organization.media!
    refute Pundit.policy!(@user, :mailing_list).index?

    skip
    @user.update!(organization: @organization)
    @user.organization.federal_authority!
    assert Pundit.policy!(@user, :mailing_list).index?
  end

  test "update" do
    mailing_list = mailing_lists(:one).tap { |ml| ml.update!(organization: @organization) }
    assert Pundit.policy!(@user, mailing_list).update?
  end
end
