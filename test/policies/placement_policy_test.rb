require 'test_helper'

class PlacementPolicyTest < ActiveSupport::TestCase
  test "convert_to_press_release" do
    user = users(:one)
    refute Pundit.policy!(user, :placement).convert_to_press_release?

    user = users(:admin)
    assert Pundit.policy!(user, :placement).convert_to_press_release?
  end
end
