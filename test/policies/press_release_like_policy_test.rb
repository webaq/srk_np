require 'test_helper'

class PressReleaseLikePolicyTest < ActiveSupport::TestCase
  setup do
    @user = users(:one)
    @organization = organizations(:one)
    @press_release = press_releases(:one)
    @federal_subject = federal_subjects(:one)
  end

  test "create?" do
    @user.organization = @organization
    @organization.federal_subject = @federal_subject
    @press_release.federal_subjects = [@federal_subject]
    assert_not Pundit.policy!(@user, PressReleaseLike.new(press_release: @press_release)).create?

    @organization.federal_subject = federal_subjects(:two)
    assert Pundit.policy!(@user, PressReleaseLike.new(press_release: @press_release)).create?
  end
end
