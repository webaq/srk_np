require 'test_helper'

class MainTopicPolicyTest < ActiveSupport::TestCase
  test "index?" do
    assert Pundit.policy!(users(:one), MainTopic).index?
  end

  test "show?" do
    assert Pundit.policy!(users(:one), MainTopic).show?
  end
end
