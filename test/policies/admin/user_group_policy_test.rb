require 'test_helper'

class Admin::UserGroupPolicyTest < ActiveSupport::TestCase
  test "index?" do
    user_mock = MiniTest::Mock.new
    user_mock.expect :has_ability?, false, [:edit_user_groups]
    assert_not Pundit.policy!(user_mock, [:admin, UserGroup]).index?

    user_mock = MiniTest::Mock.new
    user_mock.expect :has_ability?, true, [:edit_user_groups]
    assert Pundit.policy!(user_mock, [:admin, UserGroup]).index?
  end

  test "user_editing_scope" do
    superadmins_user_group = user_groups(:superadmins)
    federal_authority_user_group = user_groups(:federal_authorities)

    user_mock = MiniTest::Mock.new

    user_mock.expect :has_ability?, false, [:edit_user_groups]
    assert_includes Admin::UserGroupPolicy::UserEditingScope.new(user_mock, UserGroup).resolve, federal_authority_user_group
    user_mock.expect :has_ability?, false, [:edit_user_groups]
    assert_not_includes Admin::UserGroupPolicy::UserEditingScope.new(user_mock, UserGroup).resolve, superadmins_user_group

    user_mock.expect :has_ability?, true, [:edit_user_groups]
    assert_includes Admin::UserGroupPolicy::UserEditingScope.new(user_mock, UserGroup).resolve, federal_authority_user_group
    user_mock.expect :has_ability?, true, [:edit_user_groups]
    assert_includes Admin::UserGroupPolicy::UserEditingScope.new(user_mock, UserGroup).resolve, superadmins_user_group
  end
end
