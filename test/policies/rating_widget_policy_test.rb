require 'test_helper'

class RatingWidgetPolicyTest < ActiveSupport::TestCase
  test "show" do
    user_mock = Minitest::Mock.new
    user_mock.expect :has_ability?, false, [:show_rating]
    assert_not Pundit.policy!(user_mock, :rating_widget).show?

    user_mock = Minitest::Mock.new
    user_mock.expect :has_ability?, true, [:show_rating]
    assert Pundit.policy!(user_mock, :rating_widget).show?
  end
end
