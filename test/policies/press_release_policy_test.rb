require 'test_helper'

class PressReleasePolicyTest < ActiveSupport::TestCase
  test "regional_moderator scope" do
    federal_subject = federal_subjects(:one)
    user = users(:regional_moderator).tap { |user|
      user.update(federal_subject: federal_subject)
    }
    press_release = press_releases(:one).tap { |press_release|
      press_release.update!({
        user: users(:one).tap { |author| author.update!(federal_subject: federal_subject) }
      })
    }

    assert_includes Pundit.policy_scope!(user, PressRelease), press_release
  end

  test "update_main_topic?" do
    editor = users(:one).tap { |u| u.update!(editor: true) }
    press_release = press_releases(:one)

    assert Pundit.policy!(editor, press_release).update_main_topic?
  end

  test "approve?" do
    federal_subject = federal_subjects(:one).tap { |federal_subject|
      federal_subject.update!(regional_moderated: true)
    }

    author = users(:one).tap { |user|
      user.update!(federal_subject: federal_subject)
    }

    user = users(:two).tap { |user|
      user.update!({
        regional_moderator: true,
        federal_subject: federal_subject,
        moderatable_federal_projects: [federal_projects(:one)],
      })
    }

    press_release = press_releases(:one).tap { |press_release|
      press_release.update!(user: author)
    }

    assert Pundit.policy!(user, press_release).approve?
  end

  test "pre_approve?" do
    federal_project = federal_projects(:one)
    federal_subject = federal_subjects(:one)
    city_authority_organization = organizations(:one).tap { |o| o.update!(kind: 'city_authority', federal_subject: federal_subject) }
    user = users(:one).tap { |u| u.update!(organization: city_authority_organization) }
    press_release = press_releases(:one).tap { |p| p.update!(user: user, federal_projects: [federal_project]) }

    assert press_release.need_pre_approvement?

    regional_moderator = users(:two).tap { |u| u.update!(federal_subject: federal_subject, moderatable_federal_projects: [federal_project], regional_moderator: true) }

    assert Pundit.policy!(regional_moderator, press_release).pre_approve?
  end
end
