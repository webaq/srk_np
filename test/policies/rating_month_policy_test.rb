require 'test_helper'

class RatingMonthPolicyTest < ActiveSupport::TestCase
  setup do
    @admin = users(:admin)
    @user = users(:one)
    @rating_month = rating_months(:one)
  end

  test "index?" do
    assert Pundit.policy!(@admin, RatingMonth).index?
    assert Pundit.policy!(@user, RatingMonth).index?
  end

  test "show?" do
    @rating_month.published = false

    user_mock = Minitest::Mock.new
    user_mock.expect :admin?, false
    assert_not Pundit.policy!(user_mock, @rating_month).show?

    user_mock = Minitest::Mock.new
    user_mock.expect :admin?, true
    assert Pundit.policy!(user_mock, @rating_month).show?

    @rating_month.published = true

    user_mock = Minitest::Mock.new
    user_mock.expect :admin?, true
    assert Pundit.policy!(user_mock, @rating_month).show?

    user_mock = Minitest::Mock.new
    user_mock.expect :admin?, false
    user_mock.expect :has_ability?, false, [:show_rating]
    assert_not Pundit.policy!(user_mock, @rating_month).show?

    user_mock = Minitest::Mock.new
    user_mock.expect :admin?, false
    user_mock.expect :has_ability?, true, [:show_rating]
    assert Pundit.policy!(user_mock, @rating_month).show?
  end

  test "create?" do
    assert Pundit.policy!(@admin, RatingMonth).create?
    refute Pundit.policy!(@user, RatingMonth).create?
  end

  test "update?" do
    assert Pundit.policy!(@admin, RatingMonth).update?
    refute Pundit.policy!(@user, RatingMonth).update?
  end

  test "destroy?" do
    assert Pundit.policy!(@admin, RatingMonth).destroy?
    refute Pundit.policy!(@user, RatingMonth).destroy?
  end

  test "scope" do
    @rating_month.update!(published: false)
    assert_includes Pundit.policy_scope!(@admin, RatingMonth), @rating_month
    refute_includes Pundit.policy_scope!(@user, RatingMonth), @rating_month

    @rating_month.update!(published: true)
    assert_includes Pundit.policy_scope!(@admin, RatingMonth), @rating_month
    assert_includes Pundit.policy_scope!(@user, RatingMonth), @rating_month
  end
end