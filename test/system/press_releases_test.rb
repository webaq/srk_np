require "application_system_test_case"

class PressReleasesTest < ApplicationSystemTestCase
  test "bookmark press_release" do
    user = users(:media_dorogi)
    press_release = press_releases(:approved_and_published_dorogi)

    sign_in(user)
    visit press_release_url(press_release)

    assert_selector 'a', text: 'Взять в работу'
    refute_selector 'form', id: 'new_repost'

    click_on "Взять в работу"

    assert_selector 'form', id: 'new_repost'
    assert_selector 'a', text: 'Не брать в работу'
  end

  test "unbookmark press_release" do
    user = users(:media_dorogi)
    press_release = press_releases(:approved_and_published_dorogi)
    user.bookmarks.create!(press_release: press_release)

    sign_in(user)
    visit press_release_url(press_release)

    assert_selector 'a', text: 'Не брать в работу'
    assert_selector 'form', id: 'new_repost'

    click_on "Не брать в работу"

    refute_selector 'form', id: 'new_repost'
    assert_selector 'a', text: 'Взять в работу'
  end
  
  test "add repost" do
    user = users(:media_dorogi)
    press_release = press_releases(:approved_and_published_dorogi)
    user.bookmarks.create!(press_release: press_release)
    
    sign_in(user)
    visit press_release_url(press_release)
    
    fill_in 'repost_link', with: 'http://www.example.com/pages/123'
    click_on 'Отправить'
    
    assert_selector 'a', text: 'http://www.example.com/pages/123'
  end
  
  test "delete owned repost" do
    user = users(:media_dorogi)
    press_release = press_releases(:approved_and_published_dorogi)
    press_release.bookmarks.create!(user: user)
    press_release.reposts.create!(user: user, link: 'http://www.example.com/pages/123')
    
    sign_in(user)
    visit press_release_url(press_release)
    
    assert_selector 'a', text: 'http://www.example.com/pages/123'
    
    within '#reposts' do
      accept_alert do
        click_on 'удалить'
      end
    end
    
    refute_selector 'a', text: 'http://www.example.com/pages/123'
  end
end
