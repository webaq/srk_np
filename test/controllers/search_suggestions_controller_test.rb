require 'test_helper'

class SearchSuggestionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:one)
  end

  test "search contains" do
    tags(:one).update!(name: 'профилактикасердечнососудистыхзаболеваний')
    get search_suggestions_url, params: {
      q: 'сосудист'
    }
    assert_select 'a', '#профилактикасердечнососудистыхзаболеваний'
  end

  test "search without space" do
    tags(:one).update!(name: 'детскиеспортивныецентры')
    get search_suggestions_url, params: {
      q: 'детские спортивные'
    }
    assert_select 'a', '#детскиеспортивныецентры'
  end
end