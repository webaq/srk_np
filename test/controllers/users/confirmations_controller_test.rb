require 'test_helper'

class Users::ConfirmationsControllerTest < ActionDispatch::IntegrationTest
  test "should show registration form if token is still valid" do
    user = users(:unconfirmed)

    token = travel -5.hours do
      user.send_confirmation_instructions
      user.confirmation_token
    end
    get "/users/confirmation?confirmation_token=#{token}"
    assert_select 'h1', /Для начала работы с системой/
  end

  test "should redirect to root if token blank" do
    get "/users/confirmation"
    assert_redirected_to root_url
  end

  test "unconfirmed should be confirmed and get welcome mail" do
    user = users(:unconfirmed)
    assert_nil user.confirmed_at

    assert_emails 1 do
      user.send_confirmation_instructions
    end

    assert_emails 1 do
      post "/users/confirmation", params: {
        user: {
          confirmation_token: user.confirmation_token,
          #first_name: 'Иван',
          #last_name: 'Иванов',
          #contact_position: 'Директор',
          #contact_phone: '(495) 123 4567',
          password: 'new-password',
          password_confirmation: 'new-password'
        }
      }
    end
    user.reload
    refute_nil user.confirmed_at
    #assert_equal 'Иван', user.first_name
    #assert_equal 'Иванов', user.last_name
    #assert_equal 'Директор', user.contact_position
    #assert_equal '(495) 123 4567', user.contact_phone

    assert_redirected_to root_url
    follow_redirect!
    assert_redirected_to press_releases_url
    follow_redirect!
    assert_response :success
  end
end
