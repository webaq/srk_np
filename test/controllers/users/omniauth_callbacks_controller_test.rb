require 'test_helper'

class Users::OmniauthCallbacksControllerTest < ActionDispatch::IntegrationTest
  setup do
    OmniAuth.config.test_mode = false
    OmniAuth.config.mock_auth[:vkontakte] = nil
  end

  test "vkontakte" do
    omniauth_account = omniauth_accounts(:one)

    # request.env["devise.mapping"] = Devise.mappings[:user] # If using Devise
    # request.env["omniauth.auth"] =
    OmniAuth.config.test_mode = true
    OmniAuth.config.mock_auth[:vkontakte] = OmniAuth::AuthHash.new({
      provider: 'vkontakte',
      uid: omniauth_account.uid
    })

    get '/users/auth/vkontakte/callback'
    assert_redirected_to root_url
  end

  test "vkontakte no user found" do
    omniauth_account = omniauth_accounts(:one)

    # request.env["devise.mapping"] = Devise.mappings[:user] # If using Devise
    # request.env["omniauth.auth"] =
    OmniAuth.config.test_mode = true
    OmniAuth.config.mock_auth[:vkontakte] = OmniAuth::AuthHash.new({
      provider: 'vkontakte',
      uid: omniauth_account.uid + 'not found'
    })

    get '/users/auth/vkontakte/callback'
    assert_redirected_to new_user_session_url
  end

  test "failure" do
    get '/users/auth/vkontakte/callback'
    assert_redirected_to new_user_session_url
  end

  test "alert on omniauth_account not unique" do
    omniauth_account = omniauth_accounts(:one)
    user = users(:two)

    assert_not_equal user, omniauth_account.user

    sign_in user

    OmniAuth.config.test_mode = true
    OmniAuth.config.mock_auth[:vkontakte] = OmniAuth::AuthHash.new({
      provider: 'vkontakte',
      uid: omniauth_account.uid
    })

    get "/users/auth/vkontakte/callback"
    assert_redirected_to settings_profile_url
    assert_equal "Аккаунт уже привязан к другой учётной записи", flash[:alert]
  end

  test "omniauth_account created" do
    user = users(:one)
    sign_in user

    OmniAuth.config.test_mode = true
    OmniAuth.config.mock_auth[:vkontakte] = OmniAuth::AuthHash.new({
      provider: 'vkontakte',
      uid: 'new_uid'
    })

    get "/users/auth/vkontakte/callback"
    assert_redirected_to settings_profile_url
    assert_equal "Аккаунт успешно привязан к Вашей учётной записи", flash[:notice]
  end
end