require 'test_helper'

class PlacementsControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    sign_in(users(:admin))
    placement = placements(:one)
    get placement_url(placement)
    assert_response :success
  end
end