require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test "should show public user agreement page to unauthorized user" do
    page = pages(:user_agreement)
    get page_url(page)
    assert_response :success
    assert_select 'h1', page.name
  end

  test "should not show non public page to unauthorized user" do
    page = pages(:non_public)
    get page_url(page)
    assert_redirected_to new_user_session_url
  end

  test "should show non public page to authorized user" do
    page = pages(:non_public)
    sign_in(users(:one))
    get page_url(page)
    assert_response :success
    assert_select 'h1', page.name
  end
end