require 'test_helper'

class WelcomeControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:one)
  end

  test "get help" do
    get help_url, xhr: true
    assert_response :success
  end

  test "faq" do
    get faq_url
    assert_response :success
  end
end