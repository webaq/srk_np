require 'test_helper'

class RegionalAuthoritiesMonthRatingsControllerTest < ActionDispatch::IntegrationTest
  test "index" do
    sign_in users(:admin)
    rating_month = rating_months(:two)
    assert rating_month.last_month?
    get regional_authorities_month_ratings_url
    assert_redirected_to regional_authorities_month_rating_url(rating_month.month)
  end

  test "create" do
    sign_in users(:admin)

    assert_difference 'RatingMonth.count', 1 do
      post regional_authorities_month_ratings_url
    end

    rating_month = RatingMonth.last
    assert_redirected_to regional_authorities_month_rating_url(rating_month.month)
  end

  test "show non-existing redirects" do
    sign_in users(:admin)
    refute RatingMonth.find_by(month: Date.new(2020, 6, 1))
    get regional_authorities_month_rating_url('2020-06-01')
    assert_redirected_to regional_authorities_month_ratings_url
  end

  test "show existing" do
    sign_in users(:admin)
    rating_month = rating_months(:one)
    skip
    get regional_authorities_month_rating_url(rating_month.month)
    assert_response :success
  end

  test "update" do
    sign_in users(:admin)

    rating_month = rating_months(:one)
    refute rating_month.published?

    patch regional_authorities_month_rating_url(rating_month.month), params: {
      rating_month: {
        published: true
      }
    }

    rating_month.reload
    assert rating_month.published?

    assert_redirected_to regional_authorities_month_rating_url(rating_month.month)
  end

  test "destroy" do
    sign_in users(:admin)
    rating_month = rating_months(:one)
    assert_difference 'RatingMonth.count', -1 do
      delete regional_authorities_month_rating_url(rating_month.month)
    end
    assert_redirected_to regional_authorities_month_ratings_url
  end
end