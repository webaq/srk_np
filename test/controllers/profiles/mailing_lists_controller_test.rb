require 'test_helper'

class Profiles::MailingListControllerTest < ActionDispatch::IntegrationTest
  test "index" do
    skip
    organization = organizations(:one).tap { |o| o.federal_authority! }
    user = users(:one).tap { |u| u.update!(organization: organization) }
    sign_in(user)

    get profile_mailing_lists_url
    assert_response :success
  end
end