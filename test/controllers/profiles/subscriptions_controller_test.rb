require 'test_helper'

class Profiles::SubscriptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
    sign_in @user
  end

  test "index" do
    get profile_subscription_url
    assert_response :success
  end

  test "create" do
    @user.subscription.destroy!
    @user.reload
    assert_nil @user.subscription

    assert_difference '::PublicActivity::Activity.count', 1 do
      assert_emails 1 do
        assert_difference 'Subscription.count', 1 do
          post profile_subscription_url, params: {
            subscription: {
              frequency: 'weekly',
            }
          }
          assert_redirected_to profile_subscription_url
        end
      end
    end

    @user.reload
    assert @user.subscription

    @activity = ::PublicActivity::Activity.last
    assert_equal 'Subscription', @activity.trackable_type
    assert_equal @user.id, @activity.owner_id
    assert_equal 'create', @activity.key
  end

  test "update" do
    federal_subject = federal_subjects(:one)
    organization = organizations(:one)
    national_project = national_projects(:one)
    tag = tags(:one)

    patch profile_subscription_url, params: {
      subscription: {
        federal_subject_ids: [federal_subject.id],
        organization_ids: [organization.id],
        national_project_ids: [national_project.id],
        tag_ids: [tag.id],
      }
    }
    assert_redirected_to profile_subscription_url
    assert_includes @user.subscription.federal_subjects, federal_subject
    assert_includes @user.subscription.organizations, organization
    assert_includes @user.subscription.national_projects, national_project
    assert_includes @user.subscription.tags, tag
  end

  test "destroy" do
    assert_difference 'Subscription.count', -1 do
      delete profile_subscription_url
      assert_redirected_to profile_subscription_url
    end
    @user.reload
    assert_nil @user.subscription
  end

end