require 'test_helper'

class Profiles::FavouritesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @press_release = press_releases(:one)
    @user = users(:one)
    sign_in @user
  end

  test "index" do
    get profile_favourites_url
    assert_response :success
  end

  test "destroy" do
    @user.favourite_press_releases << @press_release
    assert @user.favourite_press_releases.exists?(@press_release.id)
    delete profile_favourite_url(@press_release), xhr: true
    assert_response :success
    assert_not @user.favourite_press_releases.exists?(@press_release.id)
  end
end