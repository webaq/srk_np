require 'test_helper'

class ExportRequestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:media_dorogi)
  end

  test "new" do
    get new_export_request_url, xhr: true
    assert_response :success
  end

  test "create" do
    assert_emails 1 do
      post export_requests_url, params: {
        export_request: {
          subject: 'тема',
          body: 'описание',
          federal_subject_ids: [federal_subjects(:one).id],
          email: 'test@example.com',
        }
      }, xhr: true
    end
    assert_response :success
  end
end