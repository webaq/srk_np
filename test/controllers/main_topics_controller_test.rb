require 'test_helper'

class MainTopicsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in(users(:one))
  end

  test "index" do
    get main_topics_url
    assert_response :success
  end

  test "show" do
    get main_topic_url(main_topics(:one))
    assert_response :success
  end
end