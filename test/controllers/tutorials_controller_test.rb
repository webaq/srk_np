require 'test_helper'

class TutorialsControllerTest < ActionDispatch::IntegrationTest
  test "should redirect if not signed in" do
    tutorial = tutorials(:one)
    get tutorial_url(tutorial)
    assert_redirected_to new_user_session_url
  end

  test "should show" do
    tutorial = tutorials(:one)
    tutorial.update!(creatable_permitted: true)

    sign_in(users(:creatable))

    get tutorial_url(tutorial)
    assert_response :success
  end

  test "should raise if not authorized" do
    tutorial = tutorials(:one)
    sign_in(users(:one))
    assert_raise ActiveRecord::RecordNotFound do
      get tutorial_url(tutorial)
    end
  end

  test "should redirect from index to first tutorial" do
    tutorial = tutorials(:one)
    tutorial.update!(creatable_permitted: true)

    sign_in(users(:creatable))

    get tutorials_url
    assert_redirected_to tutorial_url(tutorial)
  end
end