require 'test_helper'

class TagRequestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:one)
  end

  test "new" do
    get new_tag_request_url, xhr: true
    assert_response :success
  end

  test "create" do
    assert_emails 1 do
      post tag_requests_url, params: {
        tag_request: {
          subject: 'новый тег',
        }
      }, xhr: true
    end
    assert_response :success
  end
end