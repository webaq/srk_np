require 'test_helper'

class Placements::PressReleaseConversionsControllerTest < ActionDispatch::IntegrationTest
  test "should raise on create unless admin" do
    placement = placements(:one)
    sign_in(users(:one))

    assert_raise Pundit::NotAuthorizedError do
      post placement_press_release_conversion_url(placement)
    end
  end

  test "should create" do
    placement = placements(:one)
    sign_in(users(:admin))

    assert_difference 'Placement.count', -1 do
      assert_difference 'PressRelease.count', 1 do
        post placement_press_release_conversion_url(placement)
      end
    end

    assert_redirected_to admin_placements_url
  end
end