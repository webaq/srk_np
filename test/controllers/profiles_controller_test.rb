require 'test_helper'

class ProfilesControllerTest < ActionDispatch::IntegrationTest
  test "should show settings" do
    sign_in(users(:one))
    get settings_profile_url
    assert_response :success
  end

  test "should update" do
    sign_in(users(:one))
    patch profile_url, params: {
      user: {
        password: '',
        contact_position: ''
      }
    }
    assert_redirected_to settings_profile_url
  end

  test "should show mailing_lists for authority user" do
    skip
    organization = organizations(:one).tap { |o| o.federal_authority! }
    user = users(:one).tap { |u| u.update!(organization: organization) }

    sign_in(user)

    get profile_path
    assert_response :success
    assert_select 'a', 'Списки рассылки организации'
  end
end