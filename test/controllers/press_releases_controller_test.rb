require "test_helper"

class PressReleasesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @press_release = press_releases(:one)
  end

  test "user without organization should get index#all_unapproved" do
    user = users(:one).tap { |u| u.update!({
      organization: nil
    }) }
    sign_in(user)
    get press_releases_url(kind: 'all_unapproved')
    assert_response :success
  end

  test "media user should get index" do
    sign_in(users(:all_federal_subjects_dorogi_federal_project_media))

    get press_releases_url

    assert_response :success
    assert_select 'a', 'Все'
    assert_select 'a', 'Главные темы'
    assert_select 'a', 'В работе'
    assert_select 'a', 'Опубликовано'
    assert_select 'a', 'Анонсы'
  end

  test "should show onboarding to repostable user first time" do
    tutorials(:one).update!(repostable_permitted: true, onboarding: true, name: 'Туториал')
    user = users(:repostable).tap { |u| u.update!(onboarded_at: nil) }

    sign_in(user)
    get press_releases_url

    assert_response :success
    assert_select 'h1', 'Туториал'
  end

  test "should not show onboarding to repostable user second time" do
    tutorials(:one).update!(repostable_permitted: true, onboarding: true, name: 'Туториал')
    user = users(:repostable).tap { |u| u.update!(onboarded_at: Time.current) }

    sign_in(user)
    get press_releases_url

    assert_response :success
    assert_select 'h1', text: 'Туториал', count: 0
  end

  test "should not select federal_subjects by default" do
    sign_in(users(:one))
    get press_releases_url
    assert_select 'select#federal_subject_id option[selected]', count: 0
  end

  test "should select media user's federal_subject by default" do
    federal_subject = federal_subjects(:one)
    organization = organizations(:media).tap { |o| o.update!(federal_subject: federal_subject) }
    user = users(:one).tap { |u| u.update!(organization: organization) }

    sign_in(user)
    get press_releases_url

    assert_select 'select#federal_subject_id option[selected]', value: federal_subject.id
  end

  test "should create visit on index" do
    user = users(:one)
    sign_in(user)
    get press_releases_url
    assert user.visits.find_by(created_on: Date.today)
  end

  test "creator should create" do
    related_press_release = press_releases(:two)
    user = users(:dorogi_creator)
    sign_in user

    assert_difference 'PressRelease.count', 1 do
      post press_releases_url, params: {
        press_release: {
          title: 'Новый инфоповод',
          national_project_ids: [national_projects(:dorogi).id],
          tag_ids: [tags(:one).id],
          selected_federal_subject_ids: [federal_subjects(:moscow).id],
          related_press_release_ids: [related_press_release.id],
          press_release_value_id: press_release_values(:one).id,
        }
      }
    end

    press_release = PressRelease.last

    assert_redirected_to press_release
    assert_not press_release.approved?
    assert_not_includes press_release.related_press_releases, related_press_release
  end

  test "create by admin" do
    user = users(:admin)
    related_press_release = press_releases(:two)
    sign_in user

    assert_difference 'PressRelease.count', 1 do
      post press_releases_url, params: {
        press_release: {
          press_release_value_id: press_release_values(:one).id,
          title: 'Новый инфоповод',
          national_project_ids: [national_projects(:dorogi).id],
          tag_ids: [tags(:one).id],
          selected_federal_subject_ids: [federal_subjects(:moscow).id],
          press_release_relations_attributes: [
            related_press_release_id: related_press_release.id
          ]
        }
      }
    end

    press_release = PressRelease.last
    assert_includes press_release.related_press_releases, related_press_release
  end

  test "creator_and_moderator should create and approve" do
    user = users(:dorogi_creator_and_moderator)
    sign_in(user)

    assert_difference 'PressRelease.count', 1 do
      post press_releases_url, params: {
        press_release: {
          press_release_value_id: press_release_values(:one).id,
          title: 'Новый инфоповод',
          national_project_ids: [national_projects(:dorogi).id],
          tag_ids: [tags(:one).id],
          selected_federal_subject_ids: [federal_subjects(:moscow).id]
        }
      }
    end

    press_release = PressRelease.last

    assert_redirected_to press_release
    skip
    assert press_release.approved?
  end

  test "should destroy" do
    press_release = press_releases(:one)

    sign_in(users(:admin))
    assert_difference('PressRelease.with_discarded.count', 0) do
      assert_difference('PressRelease.count', -1) do
        delete press_release_url(press_release)
      end
    end
    assert_redirected_to press_releases_url
  end

  test "should show" do
    press_release = press_releases(:one)
    sign_in(users(:admin))
    get press_release_url(press_release)
    assert_response :success
  end

  test "should download docx" do
    press_release = press_releases(:one)
    sign_in(users(:admin))
    get press_release_url(press_release, format: 'docx')
    assert_response :success
  end

  test "update keeping main_topic" do
    main_topic = main_topics(:one)
    @press_release.update!(main_topic: main_topic)
    assert_equal main_topic, @press_release.main_topic
    sign_in users(:admin)

    patch press_release_url(@press_release), params: {
      press_release: {
        main_topic_id: main_topic.id,
        use_main_topic: '1',
      }
    }
    assert_redirected_to press_release_url(@press_release)

    @press_release.reload
    assert_equal main_topic, @press_release.main_topic
  end

  test "update removing main_topic" do
    main_topic = main_topics(:one)
    @press_release.update!(main_topic: main_topic)
    assert_equal main_topic, @press_release.main_topic
    sign_in users(:admin)

    patch press_release_url(@press_release), params: {
      press_release: {
        main_topic_id: main_topic.id,
        use_main_topic: '0',
      }
    }
    assert_redirected_to press_release_url(@press_release)

    @press_release.reload
    assert_nil @press_release.main_topic
  end

  test "update reset returned_at" do
    @press_release.update!(returned_at: Time.current)
    assert @press_release.returned_at

    sign_in users(:admin)
    patch press_release_url(@press_release), params: {
      press_release: {
        title: @press_release.title,
      }
    }
    @press_release.reload
    assert_nil @press_release.returned_at
  end

  test "index by national_projects" do
    sign_in users(:admin)

    national_project1 = national_projects(:one)
    national_project2 = national_projects(:two)
    press_release1 = press_releases(:one)
    press_release2 = press_releases(:two)

    press_release1.national_projects = [national_project1]
    press_release2.national_projects = [national_project2]

    get press_releases_url
    assert_select 'a' do
      assert_select '[href=?]', press_release_path(press_release1), count: 1
      assert_select '[href=?]', press_release_path(press_release2), count: 1
    end

    get press_releases_url(national_project_ids: [national_project1.id])
    assert_select 'a' do
      assert_select '[href=?]', press_release_path(press_release1), count: 1
      assert_select '[href=?]', press_release_path(press_release2), count: 0
    end
  end

  test "create tag_log if tag used" do
    tag = tags(:one)
    user = users(:one)
    sign_in user

    assert_difference 'tag.show_activities.count', 1 do
      get press_releases_url, params: {
        tag_id: tag.id
      }
    end

    tag_activity = tag.show_activities.last
    assert_equal user, tag_activity.owner
  end

  test "index json" do
    sign_in users(:admin)
    press_release = press_releases(:one)
    press_release.update!(title: 'test')
    get press_releases_url(q: 'test', format: :json)
    assert_response :success
  end

  test "show categories_tutorial only once" do
    user = users(:admin)
    sign_in user

    assert_difference "PublicActivity::Activity.count", 1 do
      get new_press_release_url
    end
    assert_select ".modal h1", text: "Уважаемые коллеги!", count: 1
    activity = PublicActivity::Activity.last
    assert_equal user, activity.trackable
    assert_equal "categories_tutorial.shown", activity.key

    assert_difference "PublicActivity::Activity.count", 0 do
      get new_press_release_url
    end
    assert_select ".modal h1", text: "Уважаемые коллеги!", count: 0
  end
end