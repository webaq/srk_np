require 'test_helper'

class Admin::OrganizationsReportsControllerTest < ActionDispatch::IntegrationTest
  test "should show" do
    sign_in(users(:admin))
    get admin_organizations_report_url
    assert_response :success
  end

  test "should download xlsx" do
    sign_in(users(:admin))
    get admin_organizations_report_url(format: :xlsx)
    assert_response :success
  end
end