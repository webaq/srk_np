require 'test_helper'

class Admin::MessagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @message = messages(:one)
    sign_in users(:admin)
  end

  test "default user should not get index" do
    sign_in(users(:one))
    assert_raise Pundit::NotAuthorizedError do
      get admin_messages_url
    end
  end

  test "should get index" do
    get admin_messages_url
    assert_response :success
  end

  test "create" do
    federal_project = federal_projects(:one)
    national_project = national_projects(:one)
    user = users(:one)

    national_project.federal_projects = [federal_project]
    federal_project.moderatable_users = [user]

    assert_difference 'MessageDelivery.count', 1 do
      assert_difference 'Message.count', 1 do
        post admin_messages_url, params: {
          message: {
            addressee: 'moderator',
            national_project_ids: [national_project.id],
            subject: 'тема',
            content: 'текст',
          }
        }
      end

      perform_enqueued_jobs
    end

    message = Message.last
    assert_equal 'тема', message.subject
    assert_equal 'текст', message.content.to_plain_text

    message_delivery = MessageDelivery.last
    assert_equal message, message_delivery.message
    assert_equal user, message_delivery.user

    assert_redirected_to admin_message_url(message)
  end

  test "should show message" do
    get admin_message_url(@message)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_message_url(@message)
    assert_response :success
  end

  test "should update message" do
    patch admin_message_url(@message), params: { message: { content: 'новый текст' } }
    @message.reload
    assert_equal 'новый текст', @message.content.to_plain_text
    assert_redirected_to admin_message_url(@message)
  end

  test "should destroy message" do
    assert_difference('Message.count', -1) do
      delete admin_message_url(@message)
    end

    assert_redirected_to admin_messages_url
  end
end
