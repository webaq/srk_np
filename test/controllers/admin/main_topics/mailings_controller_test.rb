require 'test_helper'

class Admin::MainTopics::MailingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in(users(:admin))
  end

  test "new" do
    get new_admin_main_topics_mailing_url, params: {
      main_topic_ids: [main_topics(:one).id]
    }, xhr: true
    assert_response :success
  end

  test "create" do
    main_topic = main_topics(:one)
    press_release = press_releases(:one)
    press_release.update!({
      main_topic: main_topic,
      approved_at: Time.current,
      published_at: Time.current
    })

    skip
    assert_emails 1 do
      post admin_main_topics_mailings_url, params: {
        main_topic_ids: [main_topic.id],
        period: "#{Date.today} — #{Date.today}"
      }, xhr: true
    end
    assert_response :success
  end
end