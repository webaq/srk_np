require 'test_helper'

class Admin::FederalSubjectsControllerTest < ActionDispatch::IntegrationTest
  test "default user should not get index" do
    sign_in(users(:one))
    assert_raise Pundit::NotAuthorizedError do
      get admin_federal_subjects_url
    end
  end

  test "admin should get index" do
    sign_in(users(:admin))
    get admin_federal_subjects_url
    assert_response :success
  end

  test "should new" do
    sign_in(users(:admin))
    get new_admin_federal_subject_url
    assert_response :success
  end

  test "should create" do
    sign_in(users(:admin))
    assert_difference 'FederalSubject.count', 1 do
      post admin_federal_subjects_url, params: {
        federal_subject: {
          name: 'Новый субъект',
          time_zone: 'Europe/Moscow'
        }
      }
    end
    federal_subject = FederalSubject.last
    assert_equal 'Новый субъект', federal_subject.name
    assert_equal 'Europe/Moscow', federal_subject.time_zone

    assert_redirected_to admin_federal_subject_url(federal_subject)
  end

  test "should show" do
    sign_in(users(:admin))
    get admin_federal_subject_url(federal_subjects(:one))
    assert_response :success
  end

  test "should edit" do
    sign_in(users(:admin))
    get edit_admin_federal_subject_url(federal_subjects(:one))
    assert_response :success
  end

  test "should update" do
    federal_subject = federal_subjects(:one)

    sign_in(users(:admin))
    patch admin_federal_subject_url(federal_subject), params: {
      federal_subject: {
        name: 'Новое название'
      }
    }
    federal_subject.reload
    assert_equal 'Новое название', federal_subject.name
    assert_redirected_to admin_federal_subjects_url
  end

  test "should destroy" do
    sign_in(users(:admin))
    skip
    assert_difference 'FederalSubject.count', -1 do
      delete admin_federal_subject_url(federal_subjects(:one))
    end
    assert_redirected_to admin_federal_subjects_url
  end

end