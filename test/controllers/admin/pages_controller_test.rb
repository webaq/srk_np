require 'test_helper'

class Admin::PagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @page = pages(:non_public)
    sign_in(users(:admin))
  end

  test "should raise if not authorized" do
    sign_in(users(:one))
    assert_raise Pundit::NotAuthorizedError do
      get admin_pages_url
    end
  end

  test "should get index" do
    get admin_pages_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_page_url
    assert_response :success
  end

  test "should create page" do
    assert_difference('Page.count') do
      post admin_pages_url, params: {
        page: {
          name: 'Новая страница',
          slug: 'uniq-slug'
        }
      }
    end

    assert_redirected_to admin_pages_url
  end

  # test "should show admin_page" do
  #   get admin_page_url(@page)
  #   assert_response :success
  # end

  test "should get edit" do
    get edit_admin_page_url(@page)
    assert_response :success
  end

  test "should update page" do
    patch admin_page_url(@page), params: {
      page: {
        name: 'Новое название'
      }
    }
    assert_redirected_to admin_pages_url
  end

  test "should destroy page" do
    assert_difference('Page.count', -1) do
      delete admin_page_url(@page)
    end

    assert_redirected_to admin_pages_url
  end
end
