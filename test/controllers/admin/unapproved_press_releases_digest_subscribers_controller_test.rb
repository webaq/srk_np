require 'test_helper'

class Admin::UnapprovedPressReleasesDigestSubscribersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @subscriber = unapproved_press_releases_digest_subscribers(:one)
    sign_in(users(:admin))
  end

  # replaced with support_emails index
  # test "should get index" do
  #   get admin_unapproved_press_releases_digest_subscribers_url
  #   assert_response :success
  # end

  test "should get new" do
    get new_admin_unapproved_press_releases_digest_subscriber_url
    assert_response :success
  end

  test "should create main_topic" do
    assert_difference('UnapprovedPressReleasesDigestSubscriber.count') do
      post admin_unapproved_press_releases_digest_subscribers_url, params: { unapproved_press_releases_digest_subscriber: { email: 'new@example.com' } }
    end

    assert_redirected_to admin_mailing_lists_url
  end

  test "should get edit" do
    get edit_admin_unapproved_press_releases_digest_subscriber_url(@subscriber)
    assert_response :success
  end

  test "should update main_topic" do
    patch admin_unapproved_press_releases_digest_subscriber_url(@subscriber), params: { unapproved_press_releases_digest_subscriber: { email: 'new@example.com' } }
    assert_redirected_to admin_mailing_lists_url
  end

  test "should destroy main_topic" do
    assert_difference('UnapprovedPressReleasesDigestSubscriber.count', -1) do
      delete admin_unapproved_press_releases_digest_subscriber_url(@subscriber)
    end

    assert_redirected_to admin_mailing_lists_url
  end
end
