require 'test_helper'

class Admin::Users::AuthenticationTokensControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:admin)
  end

  test "post create new" do
    @user = users(:one).tap { |u| u.update_column(:authentication_token, nil) }

    refute @user.authentication_token.present?
    post admin_user_authentication_token_url(@user)
    assert_redirected_to admin_user_url(@user)

    @user.reload
    assert @user.authentication_token.present?
  end

  test "post recreate" do
    @user = users(:one).tap { |u| u.update_column(:authentication_token, 'abcdef') }
    assert_equal 'abcdef', @user.authentication_token

    post admin_user_authentication_token_url(@user)
    assert_redirected_to admin_user_url(@user)

    @user.reload
    assert @user.authentication_token.present?
    refute_equal 'abcdef', @user.authentication_token
  end
end