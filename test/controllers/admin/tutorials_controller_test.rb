require 'test_helper'

class Admin::TutorialsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in(users(:admin))
    @tutorial = tutorials(:one)
  end

  test "should get index" do
    get admin_tutorials_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_tutorial_url
    assert_response :success
  end

  test "should create tutorial" do
    assert_difference('Tutorial.count') do
      post admin_tutorials_url, params: { tutorial: { name: @tutorial.name } }
    end

    assert_redirected_to admin_tutorials_url
  end

  # test "should show tutorial" do
  #   get admin_tutorial_url(@tutorial)
  #   assert_response :success
  # end

  test "should get edit" do
    get edit_admin_tutorial_url(@tutorial)
    assert_response :success
  end

  test "should update tutorial" do
    patch admin_tutorial_url(@tutorial), params: { tutorial: { name: @tutorial.name } }
    assert_redirected_to admin_tutorials_url
  end

  test "should destroy tutorial" do
    assert_difference('Tutorial.count', -1) do
      delete admin_tutorial_url(@tutorial)
    end

    assert_redirected_to admin_tutorials_url
  end
end
