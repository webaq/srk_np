require 'test_helper'

class Admin::DashboardControllerTest < ActionDispatch::IntegrationTest
  test "should show" do
    sign_in(users(:admin))
    get admin_dashboard_url
    assert_response :success
  end

  test "should generate pdf" do
    skip
    sign_in(users(:admin))
    get admin_dashboard_url(format: :pdf)
    assert_response :success
  end

  test "chart3_organizations" do
    sign_in users(:admin)
    get chart3_organizations_admin_dashboard_url, params: { period: 'day' }, xhr: true
    assert_response :success
  end
end