require 'test_helper'

class Admin::MailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
    sign_in(users(:admin))
  end

  test "about_media_digest_mail" do
    assert_emails 1 do
      post about_media_digest_mail_admin_mails_url, params: { user_ids: [@user.id] }, xhr: true
    end
    assert_response :success

    email = ActionMailer::Base.deliveries.last
    assert_equal [@user.email], email.to
  end

  # test "media_digest_mail" do
  #   MediaDigest.any_instance.stubs(:press_releases).returns(PressRelease.all)
  #   assert_emails 1 do
  #     post media_digest_mail_admin_mails_url, params: { user_id: @user.id }, xhr: true
  #   end
  #   assert_response :success
  # 
  #   email = ActionMailer::Base.deliveries.last
  #   assert_equal [@user.email], email.to
  # end

  test "press_release_approved" do
    assert_emails 1 do
      post press_release_approved_admin_mails_url, params: { user_id: @user.id }, xhr: true
    end
    assert_response :success

    email = ActionMailer::Base.deliveries.last
    assert_equal [@user.email], email.to
  end

  test "rejected" do
    assert_emails 1 do
      post rejected_admin_mails_url, params: { user_id: @user.id }, xhr: true
    end
    assert_response :success

    email = ActionMailer::Base.deliveries.last
    assert_equal [@user.email], email.to
  end

  test "registration_submitted" do
    assert_emails 1 do
      post registration_submitted_admin_mails_url, params: { user_id: @user.id }, xhr: true
    end
    assert_response :success

    email = ActionMailer::Base.deliveries.last
    assert_equal [@user.email], email.to
  end

  test "welcome_media" do
    assert_emails 1 do
      post welcome_media_admin_mails_url, params: { user_id: @user.id }, xhr: true
    end
    assert_response :success

    email = ActionMailer::Base.deliveries.last
    assert_equal [@user.email], email.to
  end

  test "welcome_others" do
    assert_emails 1 do
      post welcome_others_admin_mails_url, params: { user_id: @user.id }, xhr: true
    end
    assert_response :success

    email = ActionMailer::Base.deliveries.last
    assert_equal [@user.email], email.to
  end
end