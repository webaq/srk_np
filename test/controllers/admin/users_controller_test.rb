require 'test_helper'

class Admin::UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
    sign_in users(:admin)
  end

  test "should get index" do
    get admin_users_url
    assert_response :success
  end

  test "index sort by organization_name" do
    get admin_users_url, params: {
      q: {
        s: 'organization_name asc'
      }
    }
    assert_response :success
  end

  test "should get index xlsx" do
    get admin_users_url(format: :xlsx)
    assert_response :success
  end

  test "should create user without sending confirmation link" do
    assert_difference('User.count') do
      assert_emails 0 do
        post admin_users_url, params: {
          user: {
            first_name: 'Имя',
            last_name: 'Фамилия',
            email: 'new_user@example.com'
          }
        }
      end
    end

    assert_redirected_to admin_users_url
  end

  test "should create user with sending confirmation link" do
    assert_difference('User.count') do
      assert_emails 1 do
        post admin_users_url, params: {
          user: {
            first_name: 'Имя',
            last_name: 'Фамилия',
            email: 'new_user@example.com',
            asked_to_send_confirmation_notification: '1'
          }
        }
      end
    end

    assert_redirected_to admin_users_url
  end

  test "should show" do
    user = users(:one)
    sign_in(users(:admin))
    get admin_user_url(user)
    assert_response :success
  end

  test "should update user" do
    assert_emails 0 do
      patch admin_user_url(@user), params: {
        user: {
          first_name: 'Новое имя',
          last_name: 'Новая фамилия'
        }
      }
    end
    assert_redirected_to admin_user_url(@user)
  end

  test "should activate user" do
    user = users(:unconfirmed)
    patch admin_user_url(user), params: {
      set_confirmed_at: '1',
      user: { first_name: 'test' }
    }
    user.reload
    assert user.confirmed?
  end

  test "should not activate user" do
    user = users(:unconfirmed)
    patch admin_user_url(user), params: {
      set_confirmed_at: '0',
      user: { first_name: 'test' }
    }
    user.reload
    assert !user.confirmed?
  end

  test "should edit multiple" do
    get edit_multiple_admin_users_path, params: {
      user_ids: [users(:one).id]
    }
    assert_response :success
  end

  test "should update multiple" do
    patch update_multiple_admin_users_path, params: {
      user_ids: [users(:one).id],
      user: {
        moderatable_federal_project_ids: [federal_projects(:one).id]
      }
    }
    assert_redirected_to admin_users_url
  end

  test "destroy" do
    assert_difference 'User.with_discarded.count', 0 do
      assert_difference 'User.count', -1 do
        delete admin_user_url(@user)
      end
    end
    assert_redirected_to admin_users_url
  end
end