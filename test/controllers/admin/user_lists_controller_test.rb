require 'test_helper'

class Admin::UserListsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in(users(:admin))
  end

  test "should get new" do
    get new_admin_user_list_url
    assert_response :success
  end

  test "should create with sending confirmation links" do
    assert_emails 2 do
      post admin_user_lists_url, params: {
        user_list: {
          users_attributes: {
            1 => { email: '1@example.com', asked_to_send_confirmation_notification: '1' },
            2 => { email: '2@example.com' },
            3 => { email: '3@example.com', asked_to_send_confirmation_notification: '1' },
          }
        }
      }
    end
    assert_redirected_to admin_users_url
  end
end
