require 'test_helper'

class Admin::SendConfirmationLinksControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in(users(:admin))
  end

  test "should send link" do
    user = users(:admin)
    assert_emails 1 do
      post admin_user_send_confirmation_link_url(user), xhr: true
    end
    assert_response :success
  end
end