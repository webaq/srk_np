require 'test_helper'

class Admin::OrganizationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in(users(:admin))
  end

  test "should get index" do
    get admin_organizations_url
    assert_response :success
  end

  test "should get index xlsx" do
    get admin_organizations_url(format: :xlsx)
    assert_response :success
  end

  test "should create organization" do
    assert_difference('Organization.count') do
      post admin_organizations_url, params: {
        organization: {
          name: 'Название',
          organization_websites_attributes: [
            address: 'https://www.example.com'
          ],
          skip_repost_domain_validation: true
        }
      }
    end

    organization = Organization.last
    assert_equal 'Название', organization.name
    assert_equal ['https://www.example.com'], organization.organization_websites.pluck(:address)
    assert organization.skip_repost_domain_validation

    assert_redirected_to admin_organizations_url
  end

  test "should update organization" do
    patch admin_organization_url(organizations(:blank)), params: {
      organization: {
        name: 'Новое название',
      }
    }
    assert_redirected_to admin_organizations_url
  end

  test "should destroy organization" do
    assert_difference('Organization.count', -1) do
      delete admin_organization_url(organizations(:blank))
    end

    assert_redirected_to admin_organizations_url
  end
end