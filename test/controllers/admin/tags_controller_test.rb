require 'test_helper'

class Admin::TagsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @national_project = national_projects(:one)
    @tag = tags(:one)
    sign_in users(:admin)
  end

  test "index" do
    get admin_tags_url
    assert_response :success
  end

  test "new" do
    get new_admin_tag_url
    assert_response :success
  end

  test "create" do
    assert_difference 'Tag.count', 1 do
      post admin_tags_url, params: {
        tag: {
          name: 'новый тег',
          national_project_ids: [@national_project.id]
        }
      }
    end

    assert_redirected_to admin_tags_url

    tag = Tag.last
    assert_equal 'новый тег', tag.name
    assert_equal [@national_project], tag.national_projects
  end

  test "edit" do
    get edit_admin_tag_url(@tag)
    assert_response :success
  end

  test "update" do
    patch admin_tag_url(@tag), params: {
      tag: {
        name: 'новое название'
      }
    }

    assert_redirected_to admin_tags_url

    @tag.reload
    assert_equal 'новое название', @tag.name
  end

  test "destroy" do
    assert_difference 'Tag.count', -1 do
      delete admin_tag_url(@tag)
    end
    assert_redirected_to admin_tags_url
  end
end