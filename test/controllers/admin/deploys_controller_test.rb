require 'test_helper'

class Admin::DeploysControllerTest < ActionDispatch::IntegrationTest
  test "get show" do
    sign_in(users(:admin))
    get admin_deploy_url
    assert_response :success
  end
end