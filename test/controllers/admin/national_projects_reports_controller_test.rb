require 'test_helper'

class Admin::NationalProjectsReportsControllerTest < ActionDispatch::IntegrationTest
  test "admin should get show" do
    sign_in(users(:admin))
    get admin_national_projects_report_url
    assert_response :success
  end

  test "should get show for period" do
    sign_in(users(:admin))
    get admin_national_projects_report_url(period: "#{Date.today - 7} — #{Date.today}")
    assert_response :success
  end

  test "should get show for one day" do
    sign_in(users(:admin))
    get admin_national_projects_report_url(period: "#{Date.today}")
    assert_response :success
  end

  test "non-admin should raise" do
    sign_in(users(:one))
    assert_raise Pundit::NotAuthorizedError do
      get admin_national_projects_report_url
    end
  end

end
