require 'test_helper'

class Admin::RepostsControllerTest < ActionDispatch::IntegrationTest
  test "admin should get index" do
    sign_in(users(:admin))
    get admin_reposts_url
    assert_response :success
  end
end