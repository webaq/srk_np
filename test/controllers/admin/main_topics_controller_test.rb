require 'test_helper'

class Admin::MainTopicsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @main_topic = main_topics(:one)
    sign_in(users(:editor))
  end

  test "should get index" do
    get admin_main_topics_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_main_topic_url
    assert_response :success
  end

  test "should create main_topic" do
    national_project = national_projects(:one)

    assert_difference('MainTopic.count') do
      post admin_main_topics_url, params: {
        main_topic: {
          name: 'Новая главная тема',
          national_project_ids: [national_project.id]
        }
      }
    end

    main_topic = MainTopic.last
    assert_equal 'Новая главная тема', main_topic.name
    assert_equal [national_project], main_topic.national_projects

    assert_redirected_to admin_main_topics_url
  end

  # test "should show admin_main_topic" do
  #   get admin_main_topic_url(@main_topic)
  #   assert_response :success
  # end

  test "should get edit" do
    get edit_admin_main_topic_url(@main_topic)
    assert_response :success
  end

  test "should update main_topic" do
    patch admin_main_topic_url(@main_topic), params: { main_topic: { name: @main_topic.name } }
    assert_redirected_to admin_main_topics_url
  end

  test "should destroy main_topic" do
    assert_difference('MainTopic.count', -1) do
      delete admin_main_topic_url(@main_topic)
    end

    assert_redirected_to admin_main_topics_url
  end

  test "should archive" do
    main_topic = main_topics(:one).tap { |mt| mt.update!(ends_at: nil) }
    patch admin_main_topic_archive_url(main_topic)
    main_topic.reload
    refute_nil main_topic.ends_at
    assert_redirected_to admin_main_topics_url
  end
end
