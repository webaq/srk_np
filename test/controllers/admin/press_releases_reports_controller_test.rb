require 'test_helper'

class Admin::PressReleasesReportsControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in(users(:admin))
  end

  test "should get show" do
    get admin_press_releases_report_url
    assert_response :success
  end

  test "non-admin should raise" do
    sign_in(users(:one))
    assert_raise Pundit::NotAuthorizedError do
      get admin_press_releases_report_url
    end
  end
end
