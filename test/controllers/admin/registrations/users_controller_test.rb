require 'test_helper'

class Admin::Registrations::UsersControllerTest < ActionDispatch::IntegrationTest
  test "should raise for not admin" do
    sign_in(users(:one))
    assert_raise Pundit::NotAuthorizedError do
      post admin_registration_users_url(registrations(:one))
    end
  end

  test "should create user and organization" do
    registration = registrations(:one)

    sign_in(users(:admin))

    assert_emails 1 do
      assert_difference 'Organization.count', 1 do
        assert_difference 'User.count', 1 do
          post admin_registration_users_url(registration), params: {
            user: {
              email: 'new_user@example.com',
              organization_id: '',
              organization_attributes: {
                kind: registration.kind,
                name: 'Новая организация'
              },
              user_group_ids: [user_groups(:one).id],
            }
          }, xhr: true
        end
      end
    end
    assert_response :success

    user = User.last
    registration.reload
    assert_equal user.id, registration.user_id
  end

  test "should create user for existing organization" do
    registration = registrations(:one)
    organization = organizations(:one).tap { |o| o.update_column(:name, 'Старое название организации') }

    sign_in(users(:admin))

    assert_emails 1 do
      assert_difference 'Organization.count', 0 do
        assert_difference 'User.count', 1 do
          post admin_registration_users_url(registration), params: {
            user: {
              email: 'new_user@example.com',
              organization_id: organization.id,
              organization_attributes: {
                kind: registration.kind,
                name: 'Новое название организации'
              },
              user_group_ids: [user_groups(:one).id],
            }
          }, xhr: true
        end
      end
    end
    assert_response :success

    user = User.last
    assert_equal 'new_user@example.com', user.email
    assert_equal organization.id, user.organization.id
    assert_equal 'Старое название организации', user.organization.name
  end

  test "should not create invalid user" do
    registration = registrations(:one)

    assert_emails 0 do
      assert_difference 'Organization.count', 0 do
        assert_difference 'User.count', 0 do
          post admin_registration_users_url(registration), params: {
            user: {
              email: '',
              organization_id: '',
              organization_attributes: {
                kind: registration.kind,
                name: 'Новая организация'
              }
            }
          }, xhr: true
        end
      end
    end
  end

end