require "test_helper"

class Admin::PressReleasesControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:admin)
  end

  test "index" do
    get admin_press_releases_url
    assert_response :success
  end

  test "index.xlsx" do
    get admin_press_releases_url(format: :xlsx)
    assert_response :success
  end
end