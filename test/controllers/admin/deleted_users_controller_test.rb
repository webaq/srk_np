require 'test_helper'

class Admin::DeletedUsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @deleted_user = users(:one).tap { |u| u.discard! }
    sign_in users(:admin)
  end

  test "index" do
    get admin_deleted_users_url
    assert_response :success
    assert_select 'td', @deleted_user.email
  end

  test "update" do
    patch admin_deleted_user_url(@deleted_user), params: {
      user: {
        discarded_at: nil
      }
    }
    assert_redirected_to admin_deleted_users_url
    follow_redirect!

    assert_select 'td', text: @deleted_user.email, count: 0
    @deleted_user.reload
    assert @deleted_user.kept?
  end

  test "delete" do
    @deleted_user.press_releases.destroy_all

    assert_difference 'User.with_discarded.count', -1 do
      delete admin_deleted_user_url(@deleted_user)
    end

    assert_redirected_to admin_deleted_users_url
    follow_redirect!

    assert_select 'div', /удалён навсегда/

    assert_nil User.with_discarded.find_by(id: @deleted_user.id)
  end
end