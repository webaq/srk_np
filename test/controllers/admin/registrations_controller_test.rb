require 'test_helper'

class Admin::RegistrationsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    sign_in(users(:admin))
    get admin_registrations_url
    assert_response :success
  end

  test "should get confirmed" do
    sign_in(users(:admin))
    get confirmed_admin_registrations_url
    assert_response :success
  end

  test "should get deleted" do
    registrations(:one).discard!

    sign_in(users(:admin))

    get deleted_admin_registrations_url
    assert_response :success
  end

  test "should raise for non admin" do
    sign_in(users(:one))
    assert_raise Pundit::NotAuthorizedError do
      get admin_registrations_url
    end
  end

  test "should destroy" do
    sign_in(users(:admin))
    assert_emails 1 do
      assert_difference 'Registration.count', 0 do
        assert_difference 'Registration.kept.count', -1 do
          delete admin_registration_url(registrations(:one))
        end
      end
    end
    assert_response :ok
  end

end