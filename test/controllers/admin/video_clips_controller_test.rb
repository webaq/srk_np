require 'test_helper'

class Admin::VideoClipsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @video_clip = video_clips(:one)
    sign_in users(:admin)
  end

  test "edit" do
    @video_clip.update!(video: nil)
    get edit_admin_video_clip_url(@video_clip)
    assert_response :success
  end

  test "delete video" do
    assert @video_clip.video.attached?

    patch admin_video_clip_url(@video_clip), params: {
      remove_video: '1',
      video_clip: {
        name: @video_clip.name,
      },
    }
    assert_redirected_to admin_video_album_url(@video_clip.video_album)
    @video_clip.reload
    assert_not @video_clip.video.attached?
  end
end