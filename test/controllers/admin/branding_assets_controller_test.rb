require "test_helper"

class Admin::BrandingAssetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @national_project = national_projects(:one)
    sign_in users(:admin)
  end

  test "new" do
    get new_admin_national_project_branding_asset_url(@national_project)
    assert_response :success
  end
end