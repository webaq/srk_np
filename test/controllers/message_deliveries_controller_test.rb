require 'test_helper'

class MessageDeliveriesControllerTest < ActionDispatch::IntegrationTest
  test "destroy" do
    user = users(:one)

    message_delivery = message_deliveries(:one)
    message_delivery.update!(user: user)

    sign_in user

    assert_difference 'MessageDelivery.count', -1 do
      delete message_delivery_url(message_delivery), xhr: true
    end
    assert_response :success
  end
end