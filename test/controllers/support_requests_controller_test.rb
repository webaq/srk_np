require 'test_helper'

class SupportRequestsControllerTest < ActionDispatch::IntegrationTest
  test "create" do
    user = users(:one)

    assert_difference 'SupportRequest.count', 1 do
      post support_requests_url, params: {
        email: user.email,
        support_request: {
          subject: 'subject',
          body: 'body'
        }
      }, xhr: true
    end

    assert_response :success

    support_request = SupportRequest.last
    assert_equal user, support_request.user
    assert_equal 'subject', support_request.subject
    assert_equal 'body', support_request.body
  end
end