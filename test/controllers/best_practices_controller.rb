require 'test_helper'

class BestPracticesControllerTest < ActionDispatch::IntegrationTest
  setup do
    sign_in users(:admin)
  end

  test "index" do
    sign_in users(:one)
    get best_practices_url
    assert_response :success
  end

  test "create" do
    assert_emails 1 do
      assert_difference 'BestPractice.count', 1 do
        post best_practices_url, params: {
          best_practice: {
            name: 'новое название',
            content: 'новый текст',
            image: fixture_file_upload("files/example.pdf"),
          }
        }
      end
    end

    best_practice = BestPractice.last

    assert_redirected_to best_practice_url(best_practice)
  end

  test "create with custom published_at" do
    assert_difference 'BestPractice.count', 1 do
      post best_practices_url, params: {
        best_practice: {
          name: 'новое название',
          content: 'новый текст',
          image: fixture_file_upload("files/example.pdf"),
          published_at: Time.new(2021, 4, 8, 15)
        }
      }
    end

    best_practice = BestPractice.last
    assert_equal Time.new(2021, 4, 8, 15), best_practice.published_at
  end
end