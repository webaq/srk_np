require 'test_helper'

class RegistrationsControllerTest < ActionDispatch::IntegrationTest
  test "should show new" do
    get new_registration_url
    assert_response :success
  end

  test "should create" do
    registration = registrations(:one)

    assert_emails 1 do
      assert_difference 'Registration.count', 1 do
        post registrations_url, params: {
          registration: {
            kind: registration.kind,
            federal_subject_id: registration.federal_subject_id,
            organization_name: registration.organization_name,
            email: 'new_registration@example.com',
            first_name: registration.first_name,
            last_name: registration.last_name,
            contact_position: registration.contact_position,
            contact_phone: registration.contact_phone,
            website: registration.website,
            media_subscribers: registration.media_subscribers
          }
        }
      end
    end

    assert_response :success
  end

end