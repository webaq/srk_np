require 'test_helper'

class PressReleases::MailingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @press_release = press_releases(:one)

    mailing_list_item = mailing_list_items(:one).tap { |mli| mli.update!(email: 'test@example.com') }
    @mailing_list = mailing_list_item.mailing_list
    @mailing_list.update(mailing_list_items: [mailing_list_item])
    user = users(:admin)
    user.update!(organization: @mailing_list.organization)

    sign_in user
  end

  test "get new" do
    get new_press_release_mailing_url(@press_release), xhr: true
    assert_response :success
  end

  test "post create" do
    assert_emails 1 do
      post press_release_mailings_url(@press_release), params: {
        mailing_list_ids: [@mailing_list.id]
      }, xhr: true
    end
    assert_response :success
  end
end