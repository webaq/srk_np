require "test_helper"

class PressReleases::ComplaintsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @press_release = press_releases(:one)
    sign_in users(:admin)
  end

  test "new" do
    get new_press_release_complaint_url(@press_release), xhr: true
    assert_response :success
  end

  test "create" do
    assert_emails 1 do
      post press_release_complaints_url(@press_release), params: {
        complaint: {
          body: 'test'
        }
      }, xhr: true
    end

    assert_response :success
  end
end