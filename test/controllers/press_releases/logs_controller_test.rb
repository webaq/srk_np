require "test_helper"

class PressReleases::LogsControllerTest < ActionDispatch::IntegrationTest
  test "admin should get index" do
    sign_in(users(:admin))
    press_release = press_releases(:approved_and_published_dorogi)
    get press_release_logs_url(press_release)
    assert_response :success
  end
  
  test "blank user should raise" do
    sign_in(users(:all_federal_subjects_dorogi_federal_project_media))
    press_release = press_releases(:approved_and_published_dorogi)
    assert_raise Pundit::NotAuthorizedError do
      get press_release_logs_url(press_release)
    end
  end
end