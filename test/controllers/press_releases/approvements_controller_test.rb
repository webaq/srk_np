require 'test_helper'

class PressReleases::ApprovementsControllerTest < ActionDispatch::IntegrationTest
  test "approve press_release" do
    press_release = press_releases(:one)
    federal_subject = federal_subjects(:one)
    federal_subject.update!(regional_moderated: true)
    press_release.federal_subjects = [federal_subject]
    user = users(:regional_moderator)
    user.update!(federal_subject: federal_subject)
    assert_nil press_release.approved_at

    sign_in user
    skip
    assert_emails 1 do
      post press_release_approvement_url(press_release), xhr: true
      assert_response :success
    end

    press_release.reload
    assert press_release.approved_at
  end

  test "approve later max_approved_at" do
    press_release = press_releases(:one)
    press_release.update!(published_at: 2.months.ago)

    sign_in users(:regional_moderator)
    assert_raise Pundit::NotAuthorizedError do
      post press_release_approvement_url(press_release), xhr: true
    end
    press_release.reload
    assert_nil press_release.approved_at

    sign_in users(:admin)
    post press_release_approvement_url(press_release), xhr: true
    assert_response :success
    press_release.reload
    assert press_release.approved_at
  end

  test "unapprove press_release" do
    press_release = press_releases(:one)
    press_release.update!(approved_at: Time.current)

    sign_in(users(:admin))
    delete press_release_approvement_url(press_release), xhr: true

    press_release.reload
    assert_nil press_release.approved_at
  end

  test "approve returned press_releases" do
    press_release = press_releases(:one)
    press_release.update!(approved_at: nil, returned_at: Time.current)

    sign_in users(:admin)
    post press_release_approvement_url(press_release), xhr: true
    press_release.reload

    assert press_release.approved_at
    assert_nil press_release.returned_at
  end
end