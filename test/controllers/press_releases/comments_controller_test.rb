require 'test_helper'

class PressReleases::CommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @press_release = press_releases(:one)
  end

  test "create by moderator" do
    user = users(:admin)
    sign_in user

    assert_difference '@press_release.comments.count', 1 do
      post press_release_comments_url(@press_release), params: {
        comment: {
          text: 'текст комментария'
        }
      }, xhr: true
      assert_response :ok
    end

    comment = @press_release.comments.last
    assert_equal 'текст комментария', comment.text

    @press_release.reload
    assert_nil @press_release.returned_at
  end

  test "create and return to author" do
    user = users(:admin)
    sign_in user

    assert_difference '@press_release.activities.count', 1 do
      assert_difference '@press_release.comments.count', 1 do
        post press_release_comments_url(@press_release), params: {
          comment: {
            text: 'текст комментария'
          },
          return_to_author: '1'
        }, xhr: true
        assert_response :ok
      end
    end

    comment = @press_release.comments.last
    assert_equal 'текст комментария', comment.text

    @press_release.reload
    assert_not_nil @press_release.returned_at

    activity = ::PublicActivity::Activity.last
    assert_equal 'press_release.return_to_author', activity.key
  end
end