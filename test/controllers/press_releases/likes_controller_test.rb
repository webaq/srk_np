require "test_helper"

class PressReleases::LikesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @press_release = press_releases(:one)
    @user = users(:admin)
    sign_in @user
  end

  test "create" do
    assert_not @user.liked?(@press_release)
    assert_difference "PressReleaseLike.count", 1 do
      post press_release_like_url(@press_release), xhr: true
    end
    press_release_like = PressReleaseLike.last
    assert_equal @press_release, press_release_like.press_release
    assert_equal @user, press_release_like.user
    assert @user.liked?(@press_release)
    assert_response :success
  end

  test "destroy" do
    press_release_like = press_release_likes(:one)
    press_release_like.update!({
      press_release: @press_release,
      user: @user,
    })

    assert @user.liked?(@press_release)
    assert_difference "PressReleaseLike.count", -1 do
      delete press_release_like_url(@press_release), xhr: true
    end
    assert_not @user.liked?(@press_release)
    assert_response :success
  end
end