require 'test_helper'

class PressReleases::FavouritesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @press_release = press_releases(:one)
    @user = users(:one)
    sign_in @user
  end

  test "create" do
    assert_not @user.favourite_press_releases.exists?(@press_release.id)
    post press_release_favourite_url(@press_release), xhr: true
    assert_response :success
    assert @user.favourite_press_releases.exists?(@press_release.id)
  end

  test "destroy" do
    @user.favourite_press_releases << @press_release
    assert @user.favourite_press_releases.exists?(@press_release.id)
    delete press_release_favourite_url(@press_release), xhr: true
    assert_response :success
    assert_not @user.favourite_press_releases.exists?(@press_release.id)
  end
end