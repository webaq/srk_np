require 'test_helper'

class PressReleases::MainTopicsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @press_release = press_releases(:one)
    sign_in(users(:admin))
  end

  test "edit" do
    get edit_press_release_main_topic_url(@press_release), xhr: true
    assert_response :success
  end

  test "update" do
    main_topic = main_topics(:one)
    @press_release.update!(main_topic: nil)

    patch press_release_main_topic_url(@press_release), params: {
      press_release: {
        main_topic_id: main_topics(:one).id
      }
    }, xhr: true
    assert_response :success

    @press_release.reload
    assert_equal main_topic, @press_release.main_topic
  end
end