require 'test_helper'

class Api::SendPulse::CallbacksControllerTest < ActionDispatch::IntegrationTest
  test "post" do
    subscription_message = subscription_messages(:one)
    assert_nil subscription_message.delivered_at

    post api_send_pulse_callbacks_url, params: [
      {
        "smtp_server_response_code"     => "250",
        "smtp_server_response_subcode"  => "",
        "sender"                        => "john.doe@sendpulse.com",
        "smtp_server_response"          => "custom_text_response_from_recipients_server",
        "timestamp"                     => 1490953933,
        "message_id"                    => subscription_message.message_id,
        "recipient"                     => "doe.john@sendpulse.com",
        "event"                         => "delivered",
        "subject"                       => "Дайджест по национальным проектам"
      }
    ], as: :json

    subscription_message.reload
    assert_equal Time.new(2017, 3, 31, 12, 52, 13), subscription_message.delivered_at

    assert_response :success
  end

  test "post opened" do
    subscription_message = subscription_messages(:one)
    assert_nil subscription_message.opened_at
    post api_send_pulse_callbacks_url, params: [
      {
        "event": "opened",
        "timestamp": 1490962764,
        "message_id": subscription_message.message_id,
        "recipient": "doe.john@sendpulse.com",
        "sender": "john.doe@sendpulse.com",
        "subject": "utf8_hello_world"
      }
    ], as: :json
    subscription_message.reload
    assert_equal Time.new(2017, 3, 31, 15, 19, 24), subscription_message.opened_at
    assert_response :success
  end
end
