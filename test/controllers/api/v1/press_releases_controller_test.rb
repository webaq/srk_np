require 'test_helper'

class Api::V1::PressReleasesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:creatable)
  end

  test "post create" do
    national_project = national_projects(:one)
    federal_subject = federal_subjects(:one)

    assert_difference 'PressRelease.count', 1 do
      post api_v1_press_releases_url, params: {
        user_email: @user.email,
        user_token: @user.authentication_token,
        press_release: {
          press_release_value_id: press_release_values(:one).id,
          title: "Новый инфоповод",
          content: 'Текст',
          national_project_ids: [national_project.id],
          tag_ids: [tags(:one).id],
          selected_federal_subject_ids: [federal_subject.id]
        }
      }, as: :json
    end
    assert_response :success

    press_release = PressRelease.last
    refute_nil press_release.published_at
  end

  test "post with image urls" do
    national_project = national_projects(:one)
    federal_subject = federal_subjects(:one)
    main_topic = main_topics(:one)
    published_at = 1.hour.from_now
    starts_at = 2.hour.from_now

    assert_difference 'PressRelease.count', 1 do
      post api_v1_press_releases_url, params: {
        user_email: @user.email,
        user_token: @user.authentication_token,
        press_release: {
          press_release_value_id: press_release_values(:one).id,
          title: "Новый инфоповод",
          content: 'Текст',
          national_project_ids: [national_project.id],
          tag_ids: [tags(:one).id],
          selected_federal_subject_ids: [federal_subject.id],
          image_urls: ['https://upload.wikimedia.org/wikipedia/commons/e/e9/Felis_silvestris_silvestris_small_gradual_decrease_of_quality.png'],
          main_topic_id: main_topic.id,
          late_published_at: published_at.strftime('%d.%m.%Y %H:%M'),
          starts_at: starts_at.strftime('%d.%m.%Y %H:%M'),
          experts_attributes: [
            name: 'Иван Иванов'
          ]
        }
      }, as: :json
    end
    assert_response :success

    press_release = PressRelease.last
    assert_equal @user, press_release.user
    assert_equal 'Новый инфоповод', press_release.title
    assert_equal 'Текст', press_release.content.body.to_html
    assert_equal [national_project], press_release.national_projects
    assert_equal [federal_subject], press_release.federal_subjects
    assert_equal 1, press_release.images.size
    assert_equal main_topic, press_release.main_topic
    assert_equal published_at.strftime('%d.%m.%Y %H:%M'), press_release.published_at.strftime('%d.%m.%Y %H:%M')
    assert_equal starts_at.strftime('%d.%m.%Y %H:%M'), press_release.starts_at.strftime('%d.%m.%Y %H:%M')
    assert_equal ['Иван Иванов'], press_release.experts.map(&:name)
    assert_equal true, press_release.api_loaded?
  end
end