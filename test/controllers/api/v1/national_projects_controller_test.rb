require 'test_helper'

class Api::V1::NationalProjectsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:creatable).tap { |u| u.update!(authentication_token: "1G8_s7P-V-4MGojaKD7a")}
  end

  test "get index" do
    get api_v1_national_projects_url, params: {
      user_email: @user.email,
      user_token: @user.authentication_token,
    }, as: :json
    assert_response :success
  end
end