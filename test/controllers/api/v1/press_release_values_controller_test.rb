require "test_helper"

class Api::V1::PressReleaseValuesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:creatable)
  end

  test "get index w/o user unauthorized" do
    get api_v1_press_release_values_url, as: :json
    assert_response :unauthorized
  end

  test "get index" do
    get api_v1_press_release_values_url, params: {
      user_email: @user.email,
      user_token: @user.authentication_token,
    }, as: :json
    assert_response :success
  end
end