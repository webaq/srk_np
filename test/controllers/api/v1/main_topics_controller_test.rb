require 'test_helper'

class Api::V1::MainTopicsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:creatable)
  end

  test "get index w/o user unauthorized" do
    get api_v1_main_topics_url, as: :json
    assert_response :unauthorized
  end

  test "get index" do
    get api_v1_main_topics_url, params: {
      user_email: @user.email,
      user_token: @user.authentication_token,
    }, as: :json
    assert_response :success
  end
end