require 'test_helper'

class NotifierMailerTest < ActionMailer::TestCase
  test "support_request" do
    support_request = support_requests(:one).tap { |sr| sr.update!({
      id: 1111,
      subject: 'Тема запроса'
    }) }

    email = NotifierMailer.support_request(support_request, ['test@example.com'])
    assert_emails 1 do
      email.deliver_now
    end

    assert_equal ['test@example.com'], email.to
    assert_equal "СРК помощь 1111: Тема запроса", email.subject
  end

  test "main_topics" do
    press_release = press_releases(:one)
    press_release.update!(main_topic: main_topics(:one))

    email = NotifierMailer.with({
      email: 'test@example.com',
      starts_on: Date.yesterday,
      ends_on: Date.today,
      press_releases: [press_release]
    }).main_topics

    assert_emails 1 do
      email.deliver_now
    end
  end

  test "complaint" do
    press_release = press_releases(:one)
    user = users(:one)

    email = NotifierMailer.with({
      press_release_id: press_release.id,
      user_id: user.id,
      body: 'test',
    }).complaint

    assert_emails 1 do
      email.deliver_now
    end
  end
end
