require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  test "dashboard_stats" do
    dashboard = Dashboard.new({
      starts_on: Date.new(2020, 8, 3),
      ends_on: Date.new(2020, 8, 9),
      starts2_on: Date.new(2020, 8, 10),
      ends2_on: Date.new(2020, 8, 16),
      federal_subject: nil,
    })

    email = UserMailer.with({
      email: 'test@example.com',
      subject: 'Еженедельная статистика',
      dashboard: dashboard,
    }).dashboard_stats

    assert_equal ['test@example.com'], email.to
    assert_equal "Еженедельная статистика", email.subject

    assert_emails 1 do
      email.deliver_now
    end
  end

  test "tag_request" do
    user = users(:one)

    email = UserMailer.with({
      user: user,
      tag_request: TagRequest.new({
        subject: 'новый тег',
      }),
    }).tag_request

    assert_equal ['support.srk@nationalpriority.ru'], email.to
    assert_equal 'СРК запрос тега: новый тег', email.subject

    assert_emails 1 do
      email.deliver_now
    end
  end

  test "export_request" do
    user = users(:one)

    email = UserMailer.with({
      user: user,
      export_request: ExportRequest.new({
        subject: 'тема',
        federal_subject_ids: [],
        email: 'test@example.com',
      }),
    }).export_request

    assert_equal ['support.srk@nationalpriority.ru'], email.to
    assert_equal 'СРК запрос на выгрузку', email.subject

    assert_emails 1 do
      email.deliver_now
    end
  end
end
