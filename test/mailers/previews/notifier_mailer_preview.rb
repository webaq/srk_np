# Preview all emails at http://localhost:3000/rails/mailers/notifier_mailer
class NotifierMailerPreview < ActionMailer::Preview
  def complaint
    press_release = PressRelease.last
    user = User.first

    NotifierMailer.with({
      press_release_id: press_release.id,
      user_id: user.id,
      body: 'текст жалобы'
    }).complaint
  end

  def support_request
    support_request = SupportRequest.last
    NotifierMailer.support_request(support_request)
  end

  def welcome_mail
    NotifierMailer.welcome_mail(User.first)
  end

  def unapproved_press_releases_mail
    NotifierMailer.unapproved_press_releases_mail(['test@example.com'], PressRelease.unapproved.order(:created_at))
  end

  def media_digest_mail
    press_releases = PressRelease.order(created_at: 'DESC').limit(5)
    NotifierMailer.media_digest_mail('test@example.com', 44, press_releases)
  end

  def about_media_digest_mail
    NotifierMailer.about_media_digest_mail('test@example.com')
  end

  def rejected
    NotifierMailer.rejected('test@example.com')
  end

  def registration_submitted
    NotifierMailer.registration_submitted('test@example.com')
  end

  def press_release_approved
    NotifierMailer.press_release_approved('test@example.com')
  end

  def welcome_media
    NotifierMailer.welcome_media('test@example.com')
  end

  def welcome_others
    NotifierMailer.welcome_others('test@example.com')
  end

  def press_release_mail
    press_release = PressRelease.find(3753)
    NotifierMailer.press_release_mail('test@example.com', press_release)
  end

  def one_time_mail
    NotifierMailer.one_time_mail('test@example.com')
  end

  def main_topics
    NotifierMailer.with({
      email: 'test@example.com',
      starts_on: Date.yesterday,
      ends_on: Date.today,
      press_releases: PressRelease.where.not(main_topic_id: nil).last(5)
    }).main_topics
  end

  def press_releases_export
    NotifierMailer.press_releases_export(User.first, PressRelease.order(published_at: 'DESC').limit(10))
  end

  def subscription_created
    subscription = Subscription.last
    NotifierMailer.with(subscription_id: subscription.id).subscription_created
  end

  def subscription_mail
    NotifierMailer.subscription_mail('test@example.com', PressRelease.limit(20), 'прошедшую неделю')
  end
end
