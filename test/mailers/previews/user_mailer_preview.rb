# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  def dashboard_stats
    dashboard = Dashboard.new({
      starts_on: Date.new(2020, 8, 3),
      ends_on: Date.new(2020, 8, 9),
      starts2_on: Date.new(2020, 8, 10),
      ends2_on: Date.new(2020, 8, 16),
      federal_subject: nil,
    })

    UserMailer.with({
      email: 'test@example.com',
      subject: 'Еженедельная статистика',
      dashboard: dashboard,
    }).dashboard_stats
  end

  def god_nauki
    UserMailer.with({
      email: 'test@example.com',
      subject: 'Ежемесячная рассылка по Году науки',
      starts_on: 1.month.ago.to_date.beginning_of_month,
      ends_on: 1.month.ago.to_date.end_of_month,
      created_press_releases_count: 98,
      taken_press_releases_count: 73,
    }).god_nauki
  end

  def tag_request
    UserMailer.with({
      user: User.first,
      tag_request: TagRequest.new({
        subject: 'новый тег',
      }),
    }).tag_request
  end

  def export_request
    UserMailer.with({
      user: User.first,
      export_request: ExportRequest.new({
        subject: 'тема',
        federal_subject_ids: [],
        email: 'test@example.com',
        filters: [
          {"key"=>"Период", "value"=>"Неделя"},
          {"key"=>"Направления", "value"=>"Демография"}
        ],
      }),
    }).export_request
  end

  def best_practice_created
    UserMailer.with(best_practice: BestPractice.last).best_practice_created
  end
end
