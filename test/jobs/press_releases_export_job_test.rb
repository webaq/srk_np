require 'test_helper'

class PressReleasesExportJobTest < ActiveJob::TestCase
  test "sends mail" do
    user = users(:one)
    assert PressReleasesExportJob.perform_now(user.id, {})
  end
end
