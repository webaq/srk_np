class UserMailerHelperTest < ActionView::TestCase
  test "percentage_value_with_sign" do
    assert_equal "-1%", number_to_percentage_with_sign(-1.111)
    assert_equal "0%", number_to_percentage_with_sign(-0.111)
    assert_equal "0%", number_to_percentage_with_sign(0)
    assert_equal "+0%", number_to_percentage_with_sign(0.111)
  end

  test "percentage_diff" do
    assert_nil percentage_diff(1, 0.04)
  end
end