class ApplicationHelperTest < ActionView::TestCase
  test "should return truncated strings" do
    str1 = "Why do we use it?"
    str2 = "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."

    assert_equal(['Why do...', ''], truncate_two_strings(str1, str2, 10))
    assert_equal(['Why do we use it?', ''], truncate_two_strings(str1, str2, 17))
    assert_equal(['Why do we use it?', 'There...'], truncate_two_strings(str1, str2, 27))
  end

  test "truncate two strings" do
    text1 = "Три заявки от Тамбовской области будут направлены на всероссийский конкурс проектов благоустройства"
    text2 = "Благодаря Всероссийскому конкурсу лучших проектов формирования комфортной городской среды в малых городах и исторических поселениях нацпроекта «Жилье и городская среда» города Кирсанов, Рассказово и Уварово Тамбовской области станут претендентами на получение федеральных грантов для благоустройства общественных пространств. Заявки муниципалитетов рассмотрели члены областной межведомственной комиссии по реализации федерального проекта «Формирование комфортной городской среды» в онлайн-режиме.\nТак, муниципальной командой Кирсанова разработан проект благоустройства территории в микрорайоне сахарного завода. Городом Рассказово представлен проект реновации городского сада, как части единой рекреационной зоны, а Уварово – проект обустройство городского парка «Вишневый берег».\nКак отметили в управлении топливно-энергетического комплекса и жилищно-коммунального хозяйства области, администрации городов учли советы экспертов и рекомендации, разработанные Минстроем России, привлекли к работе над проектами профессиональных архитекторов, в том числе и из столицы.\nЧлены областной межведомственной комиссии по реализации федерального проекта «Формирование комфортной городской среды» одобрили все три заявки, которые будут направлены в Минстрой России до 1 июня 2020 года.\n\nАдминистрация Тамбовской области"
    assert_equal(['Три заявки от Тамбовской области будут направлены на всероссийский конкурс проектов благоустройства', '...'], truncate_two_strings(text1, text2, 100))
  end

  test "should convert to filename" do
    str = 'В рамках нацпроекта «Культура» Уфимское училище искусств получило очередную партию музыкальных инструментов'
    result = 'v_ramkah_natsproekta_kultura_ufimskoe'
    assert_equal result, convert_to_filename(str)
  end
end