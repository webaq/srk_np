source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

# ruby '2.6.6'

gem 'rails', '~> 6.1.3', '>= 6.1.3.1'
gem 'pg', '~> 1.1'
gem 'puma', '~> 5.0'
gem 'sass-rails', '>= 6'
gem 'webpacker', '~> 5.0'
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.4', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'web-console', '>= 4.1.0'
  gem 'listen', '~> 3.3'
  gem 'spring'
  gem 'rails_real_favicon'
  gem 'capistrano', '~> 3.9'
  gem "capistrano-rails", '~> 1.4'
  gem 'capistrano-rbenv', '~> 2.1'
  gem 'capistrano3-delayed-job', '~> 1.0'
  gem 'capistrano3-puma', github: "seuros/capistrano-puma"
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 3.26'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'
  gem 'mocha'
  gem "webmock"
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'devise'
gem 'devise-i18n'
gem 'rails-i18n', '~> 6.0.0'
gem 'simple_form'
gem "pundit"
# gem "scoped_search"
gem 'pagy'
gem 'meta-tags'
gem 'acts_as_list'
gem "cocoon"
gem 'icalendar'
gem 'pjax_rails', '~> 0.5.1'
gem 'htmltoword'
gem 'airbrake'
gem 'ransack'
gem 'groupdate'
gem 'caxlsx'
gem 'caxlsx_rails'
gem "chartkick"
gem 'public_activity'
gem 'addressable'
gem 'whenever', require: false
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'
gem 'simple_token_authentication', '~> 1.0'

gem "omniauth", "~> 1.9.1" # Can not move to 2.0 because of devise - https://github.com/heartcombo/devise/pull/5327
gem 'omniauth-vkontakte'
gem 'omniauth-facebook'
gem 'omniauth-odnoklassniki'
gem 'omniauth-google-oauth2'

gem 'inline_svg'
gem 'pg_search'
gem 'premailer-rails'
gem 'mjml-rails'
gem 'discard', '~> 1.2'

gem 'delayed_job_active_record'
gem "delayed_job_web"
gem "daemons" # for delayed_job
gem "aws-sdk-s3", require: false
gem "httparty"
gem 'counter_culture', '~> 2.0'
gem 'dotenv-rails'
gem 'rack-mini-profiler', '~> 2.0'
