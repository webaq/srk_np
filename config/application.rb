require_relative "boot"
ENV['RANSACK_FORM_BUILDER'] = '::SimpleForm::FormBuilder'
require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Srk2
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    config.i18n.available_locales = :ru
    config.i18n.default_locale = :ru
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]

    config.time_zone = 'Europe/Moscow'

    config.generators do |g|
      g.scaffold_stylesheet false
      g.test_framework :test_unit, fixtures: false
    end

    config.after_initialize do
      ActionText::ContentHelper.allowed_attributes.add 'style'
      ActionText::ContentHelper.allowed_attributes.add 'controls'

      ActionText::ContentHelper.allowed_tags.add 'video'
      ActionText::ContentHelper.allowed_tags.add 'audio'
      ActionText::ContentHelper.allowed_tags.add 'source'
    end

    config.action_dispatch.rescue_responses["Pundit::NotAuthorizedError"] = :forbidden
    config.active_model.i18n_customize_full_message = true
    config.active_job.queue_adapter = :delayed_job
  end
end
