Ransack.configure do |config|
  # config.add_predicate 'date_equals',
  #   arel_predicate: 'eq',
  #   formatter: proc { |v| v.to_date },
  #   validator: proc { |v| v.present? },
  #   type: :string
  #
  # config.add_predicate 'date_cont',
  #   arel_predicate: 'eq',
  #   formatter: proc { |v| v.to_date },
  #   validator: proc { |v| v.present? },
  #   type: :string


  config.add_predicate 'eq_or_nil', # Name your predicate
    # What non-compound ARel predicate will it use? (eq, matches, etc)
    arel_predicate: 'eq',
    # Format incoming values as you see fit. (Default: Don't do formatting)
    formatter: proc { |v| v == 0 ? nil : v },
    # Validate a value. An "invalid" value won't be used in a search.
    # Below is default.
    validator: proc { |v| v.present? },
    # Should compounds be created? Will use the compound (any/all) version
    # of the arel_predicate to create a corresponding any/all version of
    # your predicate. (Default: true)
    compounds: false,
    # Force a specific column type for type-casting of supplied values.
    # (Default: use type from DB column)
    type: :integer
    # Use LOWER(column on database).
    # (Default: false)
    # case_insensitive: true

end
