Rails.application.config.active_storage.content_types_to_serve_as_binary.delete("image/svg+xml")
Rails.application.config.active_storage.variable_content_types += %w[ image/svg+xml ] # image/heic image/heif
# Rails.application.config.active_storage.variant_processor = :vips
