PublicActivity::Activity.class_eval do
  belongs_to :user, required: false, foreign_key: :owner_id

  scope :press_release_show, ->{ where(key: 'press_release.show') }

  counter_culture [:user, :organization],
    column_name: proc {|activity| activity.key == 'press_release.show' && activity.user.organization ? 'shown_press_releases_count' : nil },
    column_names: {
      PublicActivity::Activity.press_release_show.joins(user: :organization) => :shown_press_releases_count
    }

  # counter_culture :trackable,
  #   column_name: proc {|activity| activity.key == 'press_release.show' ? 'shown_activities_count' : nil },
  #   column_names: {
  #     PublicActivity::Activity.press_release_show => :shown_activities_count
  #   }
end
