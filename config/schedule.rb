# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
#
set :chronic_options, hours24: true

# job_type :envcommand, 'cd :path && RAILS_ENV=:environment :task'
job_type :script, "cd :path && :environment_variable=:environment :bundle_command bin/:task :output"
every :reboot do
  script 'delayed_job restart'
end

if @environment == 'production'
  # every 1.day, at: '8:00' do
  #   runner "UnapprovedPressReleasesDigest.new.call"
  # end

  # every 1.hour do
  #   runner "MediaDigestMassMailing.new.call"
  # end

  every :monday, at: '9:00' do
    runner "DashboardStatsMailing.new.send_weekly"
  end

  every :friday, at: '10:00' do
    runner "GodNaukiMailing.new.send_weekly"
  end

  every '0 10 3 * *' do
    runner "GodNaukiMailing.new.send_monthly"
  end

  every 1.day, at: "9:00" do
    runner "SubscriptionDelivery.new(:daily).call"
  end

  every :monday, at: "8:00" do
    runner "SubscriptionDelivery.new(:weekly).call"
  end
end
