Rails.application.routes.draw do
  devise_for :users, controllers: {
    confirmations: 'users/confirmations',
    omniauth_callbacks: 'users/omniauth_callbacks',
  }

  authenticated :user, -> user { user.admin? }  do
    mount DelayedJobWeb, at: "/delayed_job"
  end

  root 'welcome#index'

  resource :policy_agreement, only: [:create]

  resources :best_practices do
    resource :approvement, only: [:create, :destroy], module: :best_practices
  end

  get :help, to: 'welcome#help'
  get :faq, to: 'welcome#faq'

  resource :profile, only: [:show, :update] do
    get :settings
    resource :thank_you, only: [:show], module: :profiles do
      get :download
    end
    resource :subscription, module: :profiles
    resources :mailing_lists, except: [:show], controller: 'profiles/mailing_lists'
    resources :omniauth_accounts, only: [:destroy], module: 'profiles'
    resources :favourites, only: [:index, :destroy], module: :profiles
  end

  draw :admin
  draw :api

  resources :pages, only: [:show]

  namespace :press_releases do
    resources :measures, only: [:index]
    resources :urban_districts, only: [:index]
    resources :related_press_releases, only: [] do
      get :autocomplete, on: :collection
    end
    resources :form_files, only: [:new]
    resources :organizations
  end
  resources :press_releases, shallow: true do
    resources :complaints, only: [:new, :create], module: :press_releases
    resource :impression, only: [:update], module: :press_releases
    resource :approvement, only: [:create, :destroy], controller: 'press_releases/approvements'
    resource :pre_approvement, only: [:create, :destroy], controller: 'press_releases/pre_approvements'
    resource :bookmark, only: [:create, :destroy]
    resources :reposts, only: [:create, :destroy], controller: 'press_releases/reposts'
    resources :comments, only: [:index, :create, :destroy], controller: 'press_releases/comments'
    resources :bookmarked_users, only: [:index], controller: 'press_releases/bookmarked_users'
    resources :reposted_users, only: [:index], controller: 'press_releases/reposted_users'
    resources :attachments, only: [:index], controller: 'press_releases/attachments'
    resources :logs, only: [:index], controller: 'press_releases/logs'
    resources :moderators, only: [:index], controller: 'press_releases/moderators'
    resources :mailings, only: [:new, :create], controller: 'press_releases/mailings'
    resource :main_topic, only: [:edit, :update], controller: 'press_releases/main_topics'
    resource :top_list_item, only: [:create, :destroy], controller: 'press_releases/top_list_items'
    resource :award, only: [:create, :destroy], module: 'press_releases'
    resource :favourite, only: [:create, :destroy], controller: 'press_releases/favourites'
    resource :like, only: [:create, :destroy], module: :press_releases
  end

  resources :registrations, only: [:new, :create] do
    collection do
      get :organizations
    end
  end

  resources :messages, only: [] do
    patch :read
  end
  resources :message_deliveries, only: [:destroy]

  resources :support_requests, only: [:new, :create]

  resources :national_projects, only: [:index, :show] do
    get :branding
  end
  get 'brandings', to: 'national_projects#index', national_project_branding_link: true

  resources :main_topics, only: [:index, :show]
  resources :tutorials, only: [:index, :show]

  resources :regional_authorities_month_ratings, only: [:index, :create, :show, :update, :destroy] do
    # get :media
  end

  resources :placements, only: [:new, :create, :show, :edit, :update, :destroy] do
    resource :press_release_conversion, only: [:create], controller: 'placements/press_release_conversions'
  end

  resources :tag_requests, only: [:new, :create]
  resources :tags, only: [:index]
  resources :search_suggestions, only: [:index]
  resources :export_requests, only: [:new, :create]

  resources :video_albums, only: [:index, :show], shallow: true do
    resources :video_clips, only: [:show]
  end
end
