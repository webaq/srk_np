# config valid for current version and patch releases of Capistrano
lock "~> 3.16.0"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true
set :application, "srk"
#set :repo_url, "ssh://git@git.jetbrains.space/digitaltechs/os/srk.git"
set :repo_url, "git@bitbucket.org:webaq/srk_np.git"

# set :stages, ["dev", "production"]

set :rbenv_ruby, File.read('.ruby-version').strip
set :puma_conf, -> { "#{shared_path}/config/puma.rb" }
set :puma_init_active_record, true
set :puma_preload_app, true
set :puma_service_unit_name, "puma-srk"
set :puma_phased_restart, false # https://github.com/seuros/capistrano-puma/pull/329
set :puma_threads, [0, 5]
set :puma_workers, 2

# Default value for :linked_files is []
# append :linked_files, "config/database.yml"
append :linked_files, 'config/database.yml', 'config/master.key', ".env.production.local" # 'config/puma.rb'

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system', 'vendor/bundle', '.bundle', 'storage', 'node_modules'

set :nginx_server_name, 'srk.nationalpriority.ru'
set :nginx_sites_available_path, '/etc/nginx/sites-available'
set :nginx_sites_enabled_path, '/etc/nginx/sites-enabled'
set :nginx_ssl_certificate, '/etc/certs/fullchain.pem'
set :nginx_ssl_certificate_key, '/etc/certsprivkey.pem'

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure
set :keep_assets, 30
# set :whenever_roles, -> { stage == :dev ? nil : :db }
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }
