namespace :admin do
  resources :categories, shallow: true do
    resources :subcategories, shallow: true do
      resources :subcategory_scopes
      resources :press_release_values
    end
  end

  resources :complaints, only: [:index]

  resources :notification_popups, only: [:index, :new, :create, :edit, :update, :destroy] do
    resource :preview, only: [:show], module: :notification_popups
  end

  resources :press_releases, only: [:index] do
    resources :similiar_press_releases, only: [:index], module: :press_releases
  end

  resources :rating_months, shallow: true do
    resources :federal_subject_rating_values do
      get :form, on: :collection, to: "federal_subject_rating_values/form#show"
    end
  end

  resources :faq_items, except: [:show] do
    patch :up
    patch :down
  end

  resources :federal_subjects, shallow: true do
    resources :urban_districts, except: [:index, :show]
  end

  resources :mainpage_banners

  resources :messages

  resources :national_projects, shallow: true do
    resources :federal_projects, except: [:index] do
      resources :measures, except: [:index, :show]
    end
    resources :branding_assets
  end

  namespace :branding_assets do
    resources :form_files, only: [:new]
  end

  resource :national_projects_report, only: [:show]
  resource :press_releases_report, only: [:show]
  resource :organizations_report, only: [:show]
  resource :media_activity_report, only: [:show]
  resource :press_release_views_report, only: [:show] do
    get :users
    get :press_releases
  end
  resource :press_releases_national_projects_report, only: [:show]
  resource :regional_authorities_activities_report, only: [:show]

  resources :reposts, only: [:index]

  resources :organizations, except: [:show] do
    resource :merging, only: [:create], controller: 'organizations/mergings'
  end

  namespace :main_topics do
    resources :mailings, only: [:new, :create]
  end
  resources :main_topics, except: [:show] do
    patch :archive
  end

  resources :pages, except: [:show]

  namespace :users do
    resources :urban_districts, only: [:index]
  end
  resources :users do
    resources :activities, only: [:index], module: :users
    resources :mails, only: [:index], module: :users
    resource :permissions, only: [:edit, :update]
    resource :send_confirmation_link, only: [:create]
    resource :authentication_token, only: [:show, :create], module: :users
    resource :thank_you_note, only: [:show], module: :users

    collection do
      get :edit_multiple
      patch :update_multiple

      get :edit_multiple_permissions
      patch :update_multiple_permissions
    end
  end

  resources :deleted_users, only: [:index, :update, :destroy]

  resource :mails, only: [] do
    post :about_media_digest_mail
    post :media_digest_mail
    post :press_release_approved
    post :rejected
    post :registration_submitted
    post :welcome_media
    post :welcome_others
    post :dashboard_stats_weekly
    post :dashboard_stats_daily
  end

  resources :user_lists, only: [:new, :create]

  resources :registrations, only: [:index, :destroy] do
    collection do
      get :confirmed
      get :deleted
    end
    resources :users, only: [:create], controller: 'registrations/users'
  end

  resources :tutorials

  resources :mailing_lists, except: [:show]
  resources :support_emails, except: [:index, :show]
  resources :unapproved_press_releases_digest_subscribers, except: [:index, :show]

  resources :media_digest_deliveries, only: [:index, :create]

  resource :deploy, only: :show
  resource :media_report, only: [:show]
  resources :placements, only: [:index]

  resource :similarity, only: [:show, :create]
  resources :duplicated_press_releases, only: [:index] do
    get :stats, on: :collection
  end

  resources :system_subscribers, only: [:index, :new, :create, :edit, :update, :destroy]

  resource :dashboard do
    get :chart1
    get :chart2
    get :chart3_organizations
    get :chart3_users
    get :chart4
    resource :national_projects_list, only: [:show], module: :dashboards
    resource :subscriptions, only: [], module: :dashboards do
      get :table1
      get :table2
      get :table3
      get :table4
      get :table5
      get :table6
      get :table7
    end
  end

  resources :tags

  resources :video_albums, shallow: true do
    resources :video_clips
  end

  resources :best_practices, only: [:index]
  resources :national_projects_stats, only: [:index]
  resources :approvements, only: [:index]

  resources :subscriptions, only: [:index, :edit, :update] do
    post :send_now
  end

  resources :user_groups, shallow: true do
    resources :user_group_abilities
  end
end
