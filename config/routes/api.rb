namespace :api do
  namespace :send_pulse do
    resources :callbacks, only: [:create]
  end

  namespace :v1 do
    resources :federal_subjects, only: [:index]
    resources :main_topics, only: [:index]
    resources :national_projects, only: [:index]
    resources :press_release_values, only: [:index]
    resources :press_releases, only: [:create]
    resources :tags, only: [:index]
  end
end