# СРК

## Модели


### Bookmark
Факт взятие пользователем (СМИ) инфоповода в работу.

```
belongs_to :user
belongs_to :press_release
```


### BrandingAsset
Вложение к нацпроекту, имеет свою аватарку и может содержать несколько приложенных файлов. Выводятся списком на странице нацпроекта, чтобы пользователь мог скачать и использовать (брендбук, логотип и т.д.)

```
belongs_to :national_project
```


### Comment
Комментарий к инфоповоду. Комментируют модераторы и автор инфоповода на странице инфоповода в процессе его утверждения перед его публикацией.

```
belongs_to :user
belongs_to :press_release
```


### Expert
Дополнительный "контакт", несколько которых можно указать при создании/редактировании инфоповода. Выводятся списком на странице инфоповода, чтобы СМИ знали, с кем можно связаться за подробностями.

```
belongs_to :press_release
```


### FederalProject
Федаральный проект. Принадлежат нацпроектам и могут иметь множество "Мер" ("Меры" были заложены, но потом не использовались)

```
belongs_to :national_project
has_many :measures
```


### FederalSubjectSubscription
Факт подписки пользователем на инфоповоды соответствующего субъекта федерации.

```
belongs_to :federal_subject
belongs_to :user
```


### FederalSubject
Субъект федерации - основной объект, обозначающий регион и использующийся во многих местах системы: регион инфоповода, регион, к которому приписан пользователь и тд. Может иметь множество "Городских округов" (Но, как и "Меры", были заложены, но сейчас не используются).

```
has_many :urban_districts
has_many :press_release_federal_subjects
has_many :users
has_many :organizations
```


### MainTopic
Так называемая "Главная тема", отдельный объект, их создаёт редакция, а инфоповод может быть привязан или не привязан к какой-нибудь "главной теме". Используется как дополнительный фильтр инфоповодов.

```
has_many :press_releases
```

### MainpageBanner
Баннер на главной странице :) Полностью самостоятельный объект, просто выводится на главной странице каруселью.


### Measure
Мера, множество которых принадлежит федеральному проекту, и, в свою очередь, могла привязываться к инфоповоду при создании/редактировании инфоповода, но так и не стали использовать. Если так и не пригодится, **кандидат на удаление**.

```
belongs_to :federal_project
has_many :press_releases
```


### MessageDelivery
Объект, связывающий *уведомление* и *пользователя*. Создаются при создании самого уведомления по указанным в уведомлении фильтрам пользователей (Message#relevant_users). Количество непрочитаннх MessageDeliveries выводится рядом с аватаркой пользователя, а при прочтении уведомления пользователем, отмечается как прочитанное.

```
belongs_to :message
belongs_to :user
```


### MessageNationalProject
Объект привязки уведомления к нацпроекту.

```
belongs_to :message
belongs_to :national_project
```


### MessageOrganization
Объект привязки уведомления к организации. Используется для определения релевантных получателей, которые должны получить уведомления (для которых будет создан объект MessageDelivery)

```
belongs_to :message
belongs_to :organization
```


### Message
Уведомление. Относится к отправителю и через связывающие объекты имеет отношение к множеству нацпроектов и множеству организаций для определения релевантных получателей.

```
belongs_to :sender, class_name: "User"
has_many :message_national_projects
has_many :message_organizations, dependent: :destroy
has_many :message_deliveries, dependent: :destroy
```


### NationalProject
Национальный проект. Имеет множество федеральных проектов. К нему привязывают инфоповоды при создании/редактировании инфоповода. К нему привязываются уведомления при создании уведомления. И через Subscription пользователи подписываются на инфоповоды определённых нацпроектов.

```
has_many :federal_projects
has_many :branding_assets
has_many :message_national_projects
has_many :press_release_national_projects
has_many :subscriptions
```


### OrganizationWebsite
Адрес сайта организации.

```
belongs_to :organization
```


### Organization
Организация одного из нескольких типов: ФОИВ, РОИВ, Муниципалитет, СМИ или Иное. Имеет привязанных пользователей. Имеет привязанные сайты организации. Может быть привязана к какому-то региону (FederalSubject). Может быть указана в уведомлении, как один из адресатов для определения релевантных получателей уведомления.

```
has_many :users
has_many :organization_websites
belongs_to :federal_subject, required: false
has_many :message_organizations, dependent: :destroy
```


### Page
Просто самостоятельная страничка, не имеет связи с другими объектами. Используется для публикации "Согласия на обработку персональных данных" и т.д.


### Permission
**Неактуальный объект. Кандидат на удаление.** Раньше использовался для фиксации права пользователя на определённое действие (создание инфоповода, модерацию инфоповода, просмотр инфоповода, добавление ссылки к инфоповоду) над инфоповодами привязанными к определённому федеральному проекту. На смену этой модели пришла UserFederalProjectPermission.

```
belongs_to :user
belongs_to :federal_project
```


### PressReleaseFederalProject
Объект, связующий инфоповод с федеральным проектом.

```
belongs_to :press_release
belongs_to :federal_project
```


### PressReleaseFederalSubject
Объект, связующий инфоповод с субъектом федерации.

```
belongs_to :press_release
belongs_to :federal_subject
```


### PressReleaseNationalProject
Объект, связующий инфоповод с нацпроектом.

```
belongs_to :press_release
belongs_to :national_project
```


### PressRelease
Инфоповод –&nbsp;центральный объект системы. Имеет автора (:user). Через связующие объекты привязывается к нацпроектам, федеральным проектам и субъектам федерации (Регион). Может быть привязан к Главной теме. Может иметь множество дополнительных контактных лиц (Expert).

Может быть взят пользователем в работу (Bookmark). Пользователи могут добавлять комментарии (Comment) и ссылки публикации в своих СМИ (Repost).

Привязки к Мере (Measure) и Городскому округу (UrbanDistrict) были заложены, но не используются.

```
  belongs_to :user
  has_many :bookmarks, dependent: :destroy
  has_many :reposts, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :experts
  has_many :press_release_national_projects
  has_many :press_release_federal_projects
  has_many :press_release_federal_subjects
  belongs_to :main_topic, required: false
  belongs_to :measure, required: false
  belongs_to :urban_district, required: false
```


### Registration
Заявка на регистрацию пользователя в системе. Кандидат указывает регион (FederalSubject).

Если администратор утверждают заявку, создаётся пользователь и записывается в user. Пользователь, который утвердил заявку записывается в confirmed_user.

Если администратор отклоняет заявку, она удаляется soft delete (acts_as_paranoid). 

```
belongs_to :federal_subject
belongs_to :user
belongs_to :confirmed_user
```


### Repost
Ссылка на публикацию инфоповода в СМИ. Имеет своего автора (кто добавил) и принадлежит инфоповоду.

```
belongs_to :user
belongs_to :press_release
```


### Subscription
Факт подписки пользователем на инфоповоды определённого нацпроекта.

```
belongs_to :user
belongs_to :national_project
```


### SupportEmail
Объект, который хранит E-mail адрес, на который приходят обращения по кнопке "Помощь".


### Tutorial
Тоже страница, как Page, но Tutorial выводятся с навигацией на другие Tutorial и может быть отмечен, чтобы показываться пользователю при первом заходе в систему в зависимости от типа пользователя (так называемый Onboarding), но эта функция ещё не активна –&nbsp;на утверждении.


### UnapprovedPressReleasesDigestSubscriber
Похоже на SupportEmail, только хранит E-mail адрес получателя дайджеста по неутверждённым инфоповодам.


### UrbanDistrict
Городской округ. Так же как и Measure в данный момент не используется.

```
belongs_to :federal_subject
has_many :press_releases
has_many :users
```


### UserFederalProjectPermission
Объект, который определяет возможности пользователя (не админа). Фиксирует пользователя, федеральный проект и доступное действие (создание инфоповода или модерацию инфоповода или просмотр инфоповода или добавление ссылки к инфоповоду).

```
belongs_to :user
belongs_to :federal_project
enum permission: { viewable: 1, creatable: 2, moderatable: 3, repostable: 4 }
```


### User
Пользователь. Может быть админом (admin) или редактором (editor) и имеет права на инфоповоды определённых федеральных проектов, заданные в UserFederalProjectPermission.

Может быть автором инфоповодов (PressRelease), брать инфоповод в работу (Bookmark), добавлять к инфоповоду комментарий (Comment).

Может быть привязан к какому-то региону (FederalSubject) и городскому округу (UrbanDistrict, но сейчас не используется).

Имеет подписки на нацпроекты (Subscription) и регионы (федеральные субъекты).

Может относиться к организации.

Может быть автором уведомлений (sended_messages) и быть получателем (message_deliveries).

Может иметь отсылку к заявке на регистрацию (Registration), если пользователь был создан через заявку.

Имеет soft delete, как и Registration (acts_as_paranoid).

```
has_many :user_federal_project_permissions
has_many :press_releases
has_many :bookmarks
has_many :reposts
has_many :comments
belongs_to :federal_subject
belongs_to :urban_district
has_many :subscriptions
has_many :federal_subject_subscriptions
belongs_to :organization
has_many :sended_messages, class_name: 'Message'
has_many :message_deliveries
has_one :registration
```